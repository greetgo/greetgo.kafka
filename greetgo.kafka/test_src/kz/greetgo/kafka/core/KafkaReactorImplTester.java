package kz.greetgo.kafka.core;

import kz.greetgo.kafka.consumer.AutoOffsetReset;
import kz.greetgo.kafka.consumer.ConsumerDefinition;
import kz.greetgo.kafka.consumer.TestLoggerDestinationInteractive;
import kz.greetgo.kafka.consumer.annotations.ConsumersFolder;
import kz.greetgo.kafka.consumer.annotations.GroupId;
import kz.greetgo.kafka.consumer.annotations.KafkaDescription;
import kz.greetgo.kafka.consumer.annotations.Topic;
import kz.greetgo.kafka.consumer.config.ConsumerConfigDefaults;
import kz.greetgo.kafka.consumer.config.ConsumerReactorConfigFactory;
import kz.greetgo.kafka.core.config.EventConfigFileFactory;
import kz.greetgo.kafka.core.config.EventConfigFileFactoryOverStorage;
import kz.greetgo.kafka.core.config.EventConfigStorageZooKeeper;
import kz.greetgo.kafka.core.config.ZooConnectParams;
import kz.greetgo.kafka.core.logger.LoggerDestination;
import kz.greetgo.kafka.model.Box;
import kz.greetgo.kafka.model.ModelKryo;
import kz.greetgo.kafka.model.ModelKryo2;
import kz.greetgo.kafka.producer.ProducerFacade;
import kz.greetgo.kafka.producer.config.ProducerConfigDefaults;
import kz.greetgo.kafka.producer.config.ProducerReactorConfigFactory;
import kz.greetgo.strconverter.simple.StrConverterSimple;
import kz.greetgo.util.fui.FUI;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class KafkaReactorImplTester {

  public static void main(String[] args) throws Exception {
    new KafkaReactorImplTester().execute();
  }

  String bootstrapServers = "localhost:9092";//single
//  String bootstrapServers = "localhost:9093,localhost:9094,localhost:9095";//cluster

  @ConsumersFolder("top")
  public static class TestController {

    private final Path consumerOutsDir;

    public TestController(Path consumerOutsDir) {
      this.consumerOutsDir = consumerOutsDir;
    }

    public void print(String fileName, String message) {
      var file = consumerOutsDir.resolve(fileName + ".txt").toFile();
      file.getParentFile().mkdirs();
      try (var printWriter = new PrintWriter(new FileOutputStream(file, true), false, StandardCharsets.UTF_8)) {
        printWriter.println(message);
      } catch (FileNotFoundException e) {
        throw new RuntimeException(e);
      }
    }

    @KafkaDescription("Это описание 1 этого consumer-а")
    @Topic("test_topic1")
    @GroupId("gr1-2")
    public void consumer1(ModelKryo model) {
      print("consumer1", "NrZV1klFnJ :: Thread " + Thread.currentThread().getName()
        + " :: Come from consumer1: " + model);
    }

    @KafkaDescription("Это описание 2 этого consumer-а")
    @Topic("test_topic2")
    @GroupId("gr3")
    public void consumer2(ModelKryo2 model) {
      print("consumer2", "W547X24HlS :: Thread " + Thread.currentThread().getName()
        + " :: Come from consumer2: " + model);
    }

  }

  public void execute() throws Exception {
    EventConfigStorageZooKeeper storageZooKeeper = new EventConfigStorageZooKeeper(
      "greetgo_kafka/KafkaReactorImplTest", () -> "localhost:2181", ZooConnectParams.builder().build()
    );

    EventConfigFileFactory cff = new EventConfigFileFactoryOverStorage(storageZooKeeper);

    var producerRCF = ProducerReactorConfigFactory.builder()
                                                  .configFactory(cff.cd("producers"))
                                                  .defaults(profile -> ProducerConfigDefaults.defaults())
                                                  .build();

    var consumerRCF = ConsumerReactorConfigFactory.builder()
                                                  .configFactory(cff.cd("consumers"))
                                                  .defaults(profile -> ConsumerConfigDefaults.withDefaults())
                                                  .build();

    StrConverterSimple strConverter = new StrConverterSimple();
    strConverter.convertRegistry().register(ModelKryo.class);
    strConverter.convertRegistry().register(ModelKryo2.class);
    strConverter.convertRegistry().register(Box.class);

    Path baseDir = Paths.get("build/" + getClass().getSimpleName());

    var controller = new TestController(baseDir.resolve("consumer_outs"));

    var cdv = new ConsumerDefinitionValidator() {
      @Override
      public void validate(ConsumerDefinition consumerDefinition) {
        System.out.println("T1kFl49hBG :: validate " + consumerDefinition.logDisplay());
      }
    };

    LoggerDestination testConsumerLogger = new TestLoggerDestinationInteractive();

    var kafkaReactor = KafkaReactor.builder()
                                   .consumerConfigFactory(consumerRCF)
                                   .producerConfigFactory(producerRCF)
                                   .addControllers(List.of(controller))
                                   .strConverter(() -> strConverter)
                                   .consumerHostId(() -> "test-host")
                                   .authorSupplier(() -> "author123")
                                   .onLogger(logger -> logger.setDestination(testConsumerLogger))
                                   .bootstrapServers(() -> bootstrapServers)
                                   .consumerDefinitionValidator(cdv)
                                   .makeConsumerDescriptionMandatory()
                                   .topicPrefix(() -> "asoka-")
                                   .consumerThreadPrefix(() -> "asd")
                                   .build();

    //noinspection CodeBlock2Expr
    kafkaReactor.addConsumerDefinitionCustomBuilder(
      ConsumerDefinition.customBuilder()
                        .autoOffsetReset(AutoOffsetReset.EARLIEST)
                        .name("test-custom")
                        .topic("test_topic1")
                        .topic("test_topic2")
                        .customConsumer((box, boxMeta) -> {
                          controller.print("CUSTOM", "t3Bp8JRGlP :: Thread "
                            + Thread.currentThread().getName() + " :: Come from CUSTOM: " + box.body);
                        })
    );

    kafkaReactor.startConsumers();

    AtomicBoolean working = new AtomicBoolean(true);


    var fui = new FUI(baseDir);

    File test_topic1_dir = baseDir.resolve("test_topic1").toFile();
    File test_topic2_dir = baseDir.resolve("test_topic2").toFile();

    test_topic1_dir.mkdirs();
    test_topic2_dir.mkdirs();

    var test_topic1_go1 = baseDir.resolve("data").resolve("test_topic1_go1");
    var test_topic1_go2 = baseDir.resolve("data").resolve("test_topic1_go2");
    var test_topic2_go1 = baseDir.resolve("data").resolve("test_topic2_go1");
    var test_topic2_go2 = baseDir.resolve("data").resolve("test_topic2_go2");

    {
      createFileIfAbsent(test_topic1_go1, "name=John:age=32:wow=5426543:hello=greetings");
      createFileIfAbsent(test_topic1_go2, "name=Simon:age=12:wow=76548684:hello=hi");
      createFileIfAbsent(test_topic2_go1, "id=45316:surname=Leon:loaderCount=432");
      createFileIfAbsent(test_topic2_go2, "id=4:surname=Stone:loaderCount=44");
    }

    goButton(fui, "data_go/test_topic1_go1", test_topic1_go1, test_topic1_dir);
    goButton(fui, "data_go/test_topic1_go2", test_topic1_go2, test_topic1_dir);
    goButton(fui, "data_go/test_topic2_go1", test_topic2_go1, test_topic2_dir);
    goButton(fui, "data_go/test_topic2_go2", test_topic2_go2, test_topic2_dir);

    ProducerFacade<Object> producer = kafkaReactor.createProducer("main");

    var producerThread1 = new ProducerThread(producer, working, ModelKryo.class,
                                             test_topic1_dir, "test_topic1");
    var producerThread2 = new ProducerThread(producer, working, ModelKryo2.class,
                                             test_topic2_dir, "test_topic2");

    producerThread1.start();
    producerThread2.start();

    System.out.println("s3d1Cm4O4C :: Application started");

    fui.go();

    kafkaReactor.close();

    System.out.println("3cNKU1kE59 :: Close application");

    working.set(false);

    producerThread1.join();
    producerThread2.join();

    System.out.println("i3iFs1c4ZH :: Producers threads joined");

    kafkaReactor.join();

    System.out.println("lzA5dYfdZA :: By by");
  }

  private void goButton(FUI fui, String title, Path file, File toDir) {
    fui.button(title, () -> {
      toDir.mkdirs();
      var dest = toDir.toPath().resolve(file.toFile().getName());
      dest.toFile().getParentFile().mkdirs();
      try {
        Files.copy(file, dest);
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
    });
  }

  private void createFileIfAbsent(Path path, String text) throws IOException {
    if (!Files.exists(path)) {
      path.toFile().getParentFile().mkdirs();
      Files.write(path, text.getBytes(StandardCharsets.UTF_8));
    }
  }

  static class ProducerThread extends Thread {

    private final ProducerFacade<Object> producer;
    private final AtomicBoolean          working;
    private final Class<?>               aClass;
    private final File                   dir;
    private final String                 topic;

    public ProducerThread(ProducerFacade<Object> producer, AtomicBoolean working, Class<?> aClass, File dir, String topic) {
      this.producer = producer;
      this.working  = working;
      this.aClass   = aClass;
      this.dir      = dir;
      this.topic    = topic;
    }

    @Override
    public void run() {
      try {
        runInner();
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
    }

    public void runInner() throws Exception {
      SimpleDateFormat sdf = new SimpleDateFormat("HH-mm-ss-SSS");
      while (working.get() && dir.exists()) {

        File[] files = dir.listFiles();
        if (files != null) {
          for (File file : files) {
            Object object = readFromFile(aClass, file);

            producer
              .sending(object)
              .toTopic(topic)
              .go()
              .awaitAndGet();

            System.out.println("1U61wq1lh4 :: Producer sent object `" + object + "` to " + topic);
          }

          for (File file : files) {
            File file2 = Paths
              .get(dir + "_sent")
              .resolve(file.getName() + "_" + sdf.format(new Date()))
              .toFile();

            file2.getParentFile().mkdirs();
            file.renameTo(file2);
          }
        }

        Thread.sleep(700);
      }

      System.out.println("ez0I57htQ6 :: close thread " + Thread.currentThread().getName());
    }
  }

  private static Object readFromFile(Class<?> aClass, File file) throws Exception {
    Method readFromFile = aClass.getMethod("readFromFile", File.class);
    return readFromFile.invoke(null, file);
  }

}
