package kz.greetgo.kafka.core.config;

import kz.greetgo.util.RND;
import kz.greetgo.util.fui.FUI;

import java.nio.file.Paths;
import java.util.concurrent.atomic.AtomicBoolean;

import static java.nio.charset.StandardCharsets.UTF_8;

public class EventConfigStorageFsTester {

  public static void main(String[] args) throws Exception {
    new EventConfigStorageFsTester().execute();
  }

  private void execute() throws Exception {
    var baseDir = Paths.get("build/" + getClass().getSimpleName());

    var fui = new FUI(baseDir);

    var conf = EventConfigStorage.builderInFs()
                                 .rootDir(baseDir.resolve("conf"))
                                 .build();

    conf.writeContent("status/asd.txt", ("Тест уау " + RND.str(10)).getBytes(UTF_8));
    conf.writeContent("status/dsa.txt", ("Синус балалайка " + RND.str(10)).getBytes(UTF_8));

    conf.ensureLookingFor("status/dsa.txt");
    conf.ensureLookingFor("status/asd.txt");

    AtomicBoolean working = new AtomicBoolean(true);

    conf.addEventHandler((path, type) -> {
      System.out.print("vNw9TUbSF2 :: Event " + type);
      System.out.println(" on " + path);
    });

    var thread = new Thread(() -> {
      while (working.get()) {
        conf.ping();
        try {
          Thread.sleep(600);
        } catch (InterruptedException e) {
          break;
        }
      }
    });
    thread.start();

    System.out.println("eV6lxb6Wft :: Application has been started");

    fui.go();

    System.out.println("w67DZi85g6 :: Going out...");

    working.set(false);
    thread.join();
    System.out.println("8mUp62vUoZ :: By by");
  }

}
