package kz.greetgo.kafka.core.config;

import kz.greetgo.util.RND;
import org.testng.annotations.Test;

import java.nio.file.Paths;
import java.util.Date;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;


public class EventConfigStorageFsTest {

  @Test
  public void readContent__noFile() {

    var configStorage = EventConfigStorage.builderInFs()
                                          .rootDir("build/" + getClass().getSimpleName())
                                          .build();

    var content = configStorage.readContent("asd-" + RND.str(10));

    assertThat(content).isNull();

  }

  @Test
  public void createdAt__noFile() {

    var configStorage = EventConfigStorage.builderInFs()
                                          .rootDir("build/" + getClass().getSimpleName())
                                          .build();

    Optional<Date> createdAt = configStorage.createdAt("asd-" + RND.str(10));

    assertThat(createdAt).isNotNull();
    assertThat(createdAt.orElse(null)).isNull();

  }

  @Test
  public void lastModifiedAt__noFile() {

    var configStorage = EventConfigStorage.builderInFs()
                                          .rootDir(Paths.get("build/" + getClass().getSimpleName()))
                                          .build();

    Optional<Date> createdAt = configStorage.lastModifiedAt("asd-" + RND.str(10));

    assertThat(createdAt).isNotNull();
    assertThat(createdAt.orElse(null)).isNull();

  }
}
