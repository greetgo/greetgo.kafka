package kz.greetgo.kafka.core.probe_app;

import kz.greetgo.kafka.consumer.annotations.Direct;
import kz.greetgo.kafka.consumer.annotations.GroupId;
import kz.greetgo.kafka.consumer.annotations.KafkaDescription;
import kz.greetgo.kafka.consumer.annotations.Topic;
import kz.greetgo.kafka.model.Client;
import kz.greetgo.kafka.producer.ProducerFacade;

public class ProbeController {

  public ProducerFacade producer;

  @KafkaDescription("Main consumer description")
  @Topic("main")
  @GroupId("main-consumer")
  public void mainConsumer(Client client) {
    System.out.println("IVaSjEZaLn :: MAIN   Come " + client
                         + " in thread " + Thread.currentThread().getName());
  }

  @KafkaDescription("Direct consumer description")
  @Topic("main")
  @GroupId("direct-consumer")
  @Direct
  public void directConsumer(Client client) {
    System.out.println("rY1EpDFb3r :: DIRECT Come " + client
                         + " in thread " + Thread.currentThread().getName());
  }

  @KafkaDescription("sequence-start")
  @Topic("sequence-start")
  @GroupId("sequence-start")
  public void sequenceStart(Client client) {
    System.out.println("IVaSjEZaLn :: SEQ_START         Come " + client
                         + " in thread " + Thread.currentThread().getName());
    producer.sending(client).toTopic("sequence-finish").goWithPortion();
  }

  @KafkaDescription("sequence-finish-over")
  @Topic("sequence-finish")
  @GroupId("sequence-finish-over")
  public void sequenceFinishOver(Client client) {

    System.out.println("25HtYH7Cfl :: SEQ_FINISH_OVER   Come " + client
                         + " in thread " + Thread.currentThread().getName());
  }

  @KafkaDescription("sequence-finish-direct")
  @Topic("sequence-finish")
  @GroupId("sequence-finish-direct")
  @Direct
  public void sequenceFinishDirect(Client client) {
    System.out.println("WzeDZ0E8z8 :: SEQ_FINISH_DIRECT Come " + client
                         + " in thread " + Thread.currentThread().getName());
  }

}
