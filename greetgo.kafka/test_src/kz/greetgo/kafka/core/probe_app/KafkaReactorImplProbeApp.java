package kz.greetgo.kafka.core.probe_app;

import kz.greetgo.kafka.consumer.AutoOffsetReset;
import kz.greetgo.kafka.consumer.ConsumerDefinition;
import kz.greetgo.kafka.consumer.TestLoggerDestinationInteractive;
import kz.greetgo.kafka.consumer.config.ConsumerConfigDefaults;
import kz.greetgo.kafka.consumer.config.ConsumerReactorConfigFactory;
import kz.greetgo.kafka.core.ConsumerDefinitionValidator;
import kz.greetgo.kafka.core.KafkaReactor;
import kz.greetgo.kafka.core.config.EventConfigFileFactory;
import kz.greetgo.kafka.core.config.EventConfigFileFactoryOverStorage;
import kz.greetgo.kafka.core.config.EventConfigStorageZooKeeper;
import kz.greetgo.kafka.core.config.ZooConnectParams;
import kz.greetgo.kafka.core.logger.LoggerDestination;
import kz.greetgo.kafka.model.Box;
import kz.greetgo.kafka.model.Client;
import kz.greetgo.kafka.producer.ProducerFacade;
import kz.greetgo.kafka.producer.config.ProducerConfigDefaults;
import kz.greetgo.kafka.producer.config.ProducerReactorConfigFactory;
import kz.greetgo.strconverter.simple.StrConverterSimple;
import kz.greetgo.util.RND;
import kz.greetgo.util.fui.FUI;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class KafkaReactorImplProbeApp {
  static final String bootstrapServers = "localhost:9092";//single

  public static void main(String[] args) {
    new KafkaReactorImplProbeApp().exec();
  }

  private void exec() {
    Path baseDir = Paths.get("build/" + getClass().getSimpleName());
    var  fui     = new FUI(baseDir);

    startKafkaReactor();

    createButtons(fui);

    fui.go();

    System.out.println("J7llL24dfF :: Start reactor to close...");

    kafkaReactor.close();
    kafkaReactor.join();

    System.out.println("0pU3k301L2 :: Reactor closed. App exited");
  }


  KafkaReactor           kafkaReactor;
  ProducerFacade<Object> producer;

  private void startKafkaReactor() {
    EventConfigStorageZooKeeper storageZooKeeper = new EventConfigStorageZooKeeper(
      "greetgo_kafka/" + getClass().getSimpleName(), () -> "localhost:2181", ZooConnectParams.builder().build()
    );

    EventConfigFileFactory cff = new EventConfigFileFactoryOverStorage(storageZooKeeper);

    var producerRCF = ProducerReactorConfigFactory.builder()
                                                  .configFactory(cff.cd("probe-producers"))
                                                  .defaults(profile -> ProducerConfigDefaults.defaults())
                                                  .build();

    var consumerRCF = ConsumerReactorConfigFactory.builder()
                                                  .configFactory(cff.cd("probe-consumers"))
                                                  .defaults(profile -> ConsumerConfigDefaults.withDefaults())
                                                  .build();

    StrConverterSimple strConverter = new StrConverterSimple();
    strConverter.convertRegistry().register(Box.class);
    strConverter.convertRegistry().register(Client.class);

    ProbeController controller = new ProbeController();

    var cdv = new ConsumerDefinitionValidator() {
      @Override
      public void validate(ConsumerDefinition consumerDefinition) {
        System.out.println("12jpPX2pwr :: validate " + consumerDefinition.logDisplay());
      }
    };

    LoggerDestination testConsumerLogger = new TestLoggerDestinationInteractive();

    kafkaReactor = KafkaReactor.builder()
                               .consumerConfigFactory(consumerRCF)
                               .producerConfigFactory(producerRCF)
                               .addControllers(List.of(controller))
                               .strConverter(() -> strConverter)
                               .consumerHostId(() -> "probe-host")
                               .authorSupplier(() -> "author321")
                               .onLogger(logger -> logger.setDestination(testConsumerLogger))
                               .bootstrapServers(() -> bootstrapServers)
                               .consumerDefinitionValidator(cdv)
                               .makeConsumerDescriptionMandatory()
                               .topicPrefix(() -> "probe-")
                               .consumerThreadPrefix(() -> "probe-")
                               .build();

    producer            = kafkaReactor.createProducer("main");
    controller.producer = producer;

    //noinspection CodeBlock2Expr
    kafkaReactor.addConsumerDefinitionCustomBuilder(
      ConsumerDefinition.customBuilder()
                        .autoOffsetReset(AutoOffsetReset.EARLIEST)
                        .name("test-custom")
                        .topic("main")
                        .topic("more")
                        .customConsumer((box, boxMeta) -> {
                          System.out.println("I1Fv187d9K :: CUSTOM Come " + box.body
                                               + " in thread " + Thread.currentThread().getName());
                        })
    );

    kafkaReactor.startConsumers();


  }

  private void createButtons(FUI fui) {
    fui.button("send-client-to-main", () -> {

      var client = new Client();
      client.id         = RND.str(10);
      client.surname    = RND.str(10);
      client.name       = RND.str(10);
      client.patronymic = RND.str(10);

      System.out.println("4H3Z49ZOA2 :: send client " + client + " to `main`"
                           + " in thread " + Thread.currentThread().getName());

      producer.sending(client).toTopic("main").go().awaitAndGet();

    });

    fui.button("send-client-to-main-threads-with-blocked", () -> {

      var thread1 = new Thread("producer-opened") {
        @Override
        public void run() {
          var client = new Client();
          client.id         = RND.str(10);
          client.surname    = RND.str(10);
          client.name       = RND.str(10);
          client.patronymic = RND.str(10);

          System.out.println("110sOZ9dXk :: send client " + client + " to `main`"
                               + " in thread " + Thread.currentThread().getName());

          producer.sending(client).toTopic("main").go().awaitAndGet();
        }
      };
      var thread2 = new Thread("producer-localBlocked") {
        @Override
        public void run() {
          var client = new Client();
          client.id         = RND.str(10);
          client.surname    = RND.str(10);
          client.name       = RND.str(10);
          client.patronymic = RND.str(10);

          try (var ignore = kafkaReactor.consumerFilterRegistrar().registerForLocalThread((x, y, z) -> false)) {

            System.out.println("5kxS0K4LQO :: send client " + client + " to `main`"
                                 + " in thread " + Thread.currentThread().getName());

            producer.sending(client).toTopic("main").go().awaitAndGet();

          }

        }
      };

      thread1.start();
      thread2.start();
      try {
        thread1.join();
        thread2.join();
      } catch (InterruptedException e) {
        throw new RuntimeException(e);
      }

    });

    fui.button("send-client-to-main-blocked", () -> {

      {
        var client = new Client();
        client.id         = RND.str(10);
        client.surname    = RND.str(10);
        client.name       = RND.str(10);
        client.patronymic = RND.str(10);

        try (var ignore = kafkaReactor.consumerFilterRegistrar().registerForLocalThread((cd, box, key) -> false)) {

          System.out.println("5kxS0K4LQO :: LOCKED send client " + client + " to `main`"
                               + " in thread " + Thread.currentThread().getName());

          producer.sending(client).toTopic("main").go().awaitAndGet();

        }
      }
    });

    fui.button("portion-send-client-to-main-blocked", () -> {

      {
        var client = new Client();
        client.id         = RND.str(10);
        client.surname    = RND.str(10);
        client.name       = RND.str(10);
        client.patronymic = RND.str(10);

        try (var ignore = kafkaReactor.consumerFilterRegistrar().registerForLocalThread((cd, box, key) -> false)) {

          System.out.println("bPlQ4wZvm8 :: LOCKED send client " + client + " to `main`"
                               + " in thread " + Thread.currentThread().getName());

          producer.sending(client).toTopic("main").goWithPortion();

        }
      }
    });

    fui.button("sequence-send-locked", () -> {

      {
        var client = new Client();
        client.id         = RND.str(10);
        client.surname    = RND.str(10);
        client.name       = RND.str(10);
        client.patronymic = RND.str(10);

        try (var ignore = kafkaReactor.consumerFilterRegistrar().registerForLocalThread((cd, box, key) -> false)) {

          System.out.println("98s1ib6d5n :: LOCKED send client " + client + " to `sequence-start`"
                               + " in thread " + Thread.currentThread().getName());

          producer.sending(client).toTopic("sequence-start").goWithPortion();

        }
      }
    });

    fui.button("sequence-send", () -> {
      {
        var client = new Client();
        client.id         = RND.str(10);
        client.surname    = RND.str(10);
        client.name       = RND.str(10);
        client.patronymic = RND.str(10);

        System.out.println("gTDDraRT9W :: send client " + client + " to `sequence-start`"
                             + " in thread " + Thread.currentThread().getName());

        producer.sending(client).toTopic("sequence-start").goWithPortion();
      }
    });
  }

}
