package kz.greetgo.kafka.model;

import kz.greetgo.kafka.core.HasStrKafkaKey;

public class Client implements HasStrKafkaKey {
  public String id;
  public String surname;
  public String name;
  public String patronymic;

  @Override
  public String extractStrKafkaKey() {
    return id;
  }

  @Override
  public String toString() {
    return "Client{" +
      "id='" + id + '\'' +
      ", name='" + name + '\'' +
      '}';
  }
}
