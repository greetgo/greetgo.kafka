package kz.greetgo.kafka.model;

import kz.greetgo.kafka.core.HasStrKafkaKey;

import java.io.File;
import java.nio.file.Files;

public class ModelKryo implements HasStrKafkaKey {
  public String name;
  public int age;
  public Long wow;
  public Object hello;

  @Override
  public String extractStrKafkaKey() {
    return name;
  }

  @Override
  public String toString() {
    return "ModelKryo{name='" + name + "', age=" + age + ", wow=" + wow + ", hello=" + hello + '}';
  }


  @SuppressWarnings("unused")
  public static ModelKryo readFromFile(File file) throws Exception {
    String content = Files.readString(file.toPath());

    ModelKryo ret = new ModelKryo();

    for (String line : content.split(":")) {
      int idx = line.indexOf('=');
      if (idx < 0) {
        continue;
      }

      String key = line.substring(0, idx).trim();
      String value = line.substring(idx + 1).trim();

      switch (key) {
        case "name":
          ret.name = value;
          break;
        case "age":
          ret.age = Integer.parseInt(value);
          break;
        case "wow":
          ret.wow = Long.valueOf(value);
          break;
        case "hello":
          ret.hello = value;
          break;
      }
    }

    return ret;
  }
}
