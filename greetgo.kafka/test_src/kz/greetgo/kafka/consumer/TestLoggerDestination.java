package kz.greetgo.kafka.consumer;

import kz.greetgo.kafka.core.logger.LoggerDestination;
import org.apache.kafka.clients.consumer.ConsumerRecords;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

public class TestLoggerDestination implements LoggerDestination {

  public final List<Throwable> errorList = new ArrayList<>();

  @Override
  public void logConsumerErrorInvoking(Throwable throwable, String invokingInfo, Profile profile) {
    errorList.add(throwable);
  }

  @Override
  public void logProducerConfigOnCreating(String producerName, Map<String, Object> configMap) {}

  @Override
  public void logProducerClosed(String producerName, Profile profile) {}

  @Override
  public void logConsumerIllegalAccessExceptionInvoking(IllegalAccessException e, String invokingInfo) {}

  @Override
  public void debug(Supplier<String> message) {}

  @Override
  public void logConsumerReactorRefresh(ConsumerDefinition consumerDefinition, int currentCount, int needCount, Profile profile) {}

  @Override
  public void logConsumerStartWorker(ConsumerDefinition consumerDefinition, long workerId, Profile workerProfile) {}

  @Override
  public void logConsumerFinishWorker(ConsumerDefinition consumerDefinition, long workerId, Profile workerProfile) {}

  @Override
  public void logConsumerWorkerConfig
    (ConsumerDefinition consumerDefinition, long workerId, Map<String, Object> configMap, Profile workerProfile) {}

  @Override
  public void logConsumerPollExceptionHappened(RuntimeException exception, ConsumerDefinition consumerDefinition, Profile workerProfile) {}

  @Override
  public void logConsumerCommitSyncExceptionHappened
    (RuntimeException exception, ConsumerDefinition consumerDefinition) {}

  @Override
  public void logProducerCreated(String producerName, Profile profile) {}

  @Override
  public void logProducerValidationError(Throwable error) {}

  @Override
  public void logProducerAwaitAndGetError(String errorCode, Exception exception) {}

  @Override
  public void logProducerDirectConsumerError(String lineCode, Exception error, ConsumerDefinition consumerDefinition) {}

  @Override
  public void logConsumerLogDirectInvokeError(Throwable error) {}

  @Override
  public void logConsumerPollZero(ConsumerDefinition consumerDefinition) {}

  @Override
  public void logConsumerPollPerformDelay(ConsumerDefinition consumerDefinition,
                                          long pollPerformDelayMs,
                                          ConsumerRecords<byte[], Object> records) {}

  @Override
  public void logErrorRefreshTopicList(Exception error) {}
}
