package kz.greetgo.kafka.consumer;

import kz.greetgo.kafka.consumer.annotations.KafkaProfile;
import kz.greetgo.kafka.consumer.annotations.Offset;
import kz.greetgo.kafka.consumer.annotations.Partition;
import kz.greetgo.kafka.consumer.annotations.ProfileRef;
import kz.greetgo.kafka.consumer.annotations.Topic;
import kz.greetgo.kafka.consumer.annotations.TopicContent;
import kz.greetgo.kafka.consumer.annotations.TopicRef;
import kz.greetgo.kafka.model.Box;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static java.nio.charset.StandardCharsets.UTF_8;
import static kz.greetgo.kafka.consumer.annotations.TopicContentType.BYTES;
import static kz.greetgo.kafka.consumer.annotations.TopicContentType.STR;
import static org.assertj.core.api.Assertions.assertThat;

public class ConsumerDefinitionOnControllerTest2 extends ConsumerDefinitionOnControllerTestParent {

  static class CallWithBox {

    public final List<Box>     comeData       = new ArrayList<>();
    public final List<String>  comeTopics     = new ArrayList<>();
    public final List<Integer> comePartitions = new ArrayList<>();
    public final List<Long>    comeOffsets    = new ArrayList<>();

    @SuppressWarnings("unused")
    public void method1(Box box,
                        @TopicRef String topic,
                        @Partition int partition,
                        @Offset long offset) {
      comeData.add(box);
      comeTopics.add(topic);
      comePartitions.add(partition);
      comeOffsets.add(offset);
    }

    public final List<String> comeStrings = new ArrayList<>();

    @Topic("test1")
    @SuppressWarnings("unused")
    public void method2(String stringData, @TopicRef String topic, @Partition int partition, @Offset long offset) {
      comeStrings.add(stringData);
      comeTopics.add(topic);
      comePartitions.add(partition);
      comeOffsets.add(offset);
    }
  }

  @Test
  public void invoke_callWithBox_1() {
    CallWithBox                    controller = new CallWithBox();
    ConsumerDefinitionOnController def        = createDefinition(controller, "method1", () -> "tst_");

    InvokeSessionFactory invoker = def.getInvokeSessionFactory();

    var b = newRecordBuilder().appendInBox("tst_asd1", 11, 432, "Hello in Box")
                              .appendInBox("tst_asd2", 13, 124, "Status in Box");

    try (InvokeSession session = invoker.createSession()) {

      InvokeResult result = session.invoke(b.build(), new Profile(null));

      assertThat(result.invocationError()).isNull();

    }

    assertThat(controller.comeData).hasSize(2);

    assertThat(controller.comeData.get(0).body).isEqualTo("Hello in Box");
    assertThat(controller.comeData.get(1).body).isEqualTo("Status in Box");

    assertThat(controller.comeTopics.get(0)).isEqualTo("asd1");
    assertThat(controller.comeTopics.get(1)).isEqualTo("asd2");

    assertThat(controller.comePartitions.get(0)).isEqualTo(11);
    assertThat(controller.comePartitions.get(1)).isEqualTo(13);

    assertThat(controller.comeOffsets.get(0)).isEqualTo(432L);
    assertThat(controller.comeOffsets.get(1)).isEqualTo(124L);
  }

  @Test
  public void invoke_callWithBox_2() {
    CallWithBox                    controller = new CallWithBox();
    ConsumerDefinitionOnController def        = createDefinition(controller, "method2");

    InvokeSessionFactory invoker = def.getInvokeSessionFactory();

    var b = newRecordBuilder().appendInBox("asd1", 11, 432, "Hello in Box")
                              .appendInBox("asd2", 13, 124, 2345L)
                              .appendInBox("asd2", 13, 124, "Status in Box")
                              .appendInBox("asd2", 13, 124, new Object());

    try (InvokeSession session = invoker.createSession()) {

      InvokeResult result = session.invoke(b.build(), new Profile(null));

      assertThat(result.invocationError()).isNull();

    }

    assertThat(controller.comeStrings).hasSize(2);

    assertThat(controller.comeStrings.get(0)).isEqualTo("Hello in Box");
    assertThat(controller.comeStrings.get(1)).isEqualTo("Status in Box");

    assertThat(controller.comeTopics.get(0)).isEqualTo("asd1");
    assertThat(controller.comeTopics.get(1)).isEqualTo("asd2");

    assertThat(controller.comePartitions.get(0)).isEqualTo(11);
    assertThat(controller.comePartitions.get(1)).isEqualTo(13);

    assertThat(controller.comeOffsets.get(0)).isEqualTo(432L);
    assertThat(controller.comeOffsets.get(1)).isEqualTo(124L);
  }

  @Test
  public void invoke_callWithBox_3() {
    CallWithBox                    controller = new CallWithBox();
    ConsumerDefinitionOnController def        = createDefinition(controller, "method1");

    InvokeSessionFactory invoker = def.getInvokeSessionFactory();

    var b = newRecordBuilder().appendInBox("asd1", 11, 432, "Hello in Box")
                              .appendNaked("asd1", 11, 433, 1000L)
                              .appendInBox("asd2", 13, 124, "Status in Box");

    try (InvokeSession session = invoker.createSession()) {

      InvokeResult result = session.invoke(b.build(), new Profile(null));

      assertThat(result.invocationError()).isNull();

    }

    assertThat(controller.comeData).hasSize(2);

    assertThat(controller.comeData.get(0).body).isEqualTo("Hello in Box");
    assertThat(controller.comeData.get(1).body).isEqualTo("Status in Box");

    assertThat(controller.comeTopics.get(0)).isEqualTo("asd1");
    assertThat(controller.comeTopics.get(1)).isEqualTo("asd2");

    assertThat(controller.comePartitions.get(0)).isEqualTo(11);
    assertThat(controller.comePartitions.get(1)).isEqualTo(13);

    assertThat(controller.comeOffsets.get(0)).isEqualTo(432L);
    assertThat(controller.comeOffsets.get(1)).isEqualTo(124L);
  }

  static class CallNakedStr {

    public final List<String>  comeStrings    = new ArrayList<>();
    public final List<String>  comeTopics     = new ArrayList<>();
    public final List<Integer> comePartitions = new ArrayList<>();
    public final List<Long>    comeOffsets    = new ArrayList<>();

    @Topic("test1")
    @TopicContent(STR)
    @SuppressWarnings("unused")
    public void method1(String str, @TopicRef String topic, @Partition int partition, @Offset long offset) {
      comeStrings.add(str);
      comeTopics.add(topic);
      comePartitions.add(partition);
      comeOffsets.add(offset);
    }

  }

  @Test
  public void invoke_CallNakedStr_1() {
    CallNakedStr                   controller = new CallNakedStr();
    ConsumerDefinitionOnController def        = createDefinition(controller, "method1");

    InvokeSessionFactory invoker = def.getInvokeSessionFactory();

    var b = newRecordBuilder().appendNaked("asd1", 11, 433, "Red dragon")
                              .appendNaked("dsa1", 1, 1, 4532534L)
                              .appendNaked("asd2", 13, 411, "Status of death");

    try (InvokeSession session = invoker.createSession()) {

      InvokeResult result = session.invoke(b.build(), new Profile(null));

      assertThat(result.invocationError()).isNull();

    }

    assertThat(controller.comeStrings).hasSize(2);

    assertThat(controller.comeStrings.get(0)).isEqualTo("Red dragon");
    assertThat(controller.comeStrings.get(1)).isEqualTo("Status of death");

    assertThat(controller.comeTopics.get(0)).isEqualTo("asd1");
    assertThat(controller.comeTopics.get(1)).isEqualTo("asd2");

    assertThat(controller.comePartitions.get(0)).isEqualTo(11);
    assertThat(controller.comePartitions.get(1)).isEqualTo(13);

    assertThat(controller.comeOffsets.get(0)).isEqualTo(433L);
    assertThat(controller.comeOffsets.get(1)).isEqualTo(411L);
  }

  static class CallNakedBytes {

    public final List<byte[]>  comeBytes      = new ArrayList<>();
    public final List<String>  comeTopics     = new ArrayList<>();
    public final List<Integer> comePartitions = new ArrayList<>();
    public final List<Long>    comeOffsets    = new ArrayList<>();

    @Topic("test1")
    @TopicContent(BYTES)
    @SuppressWarnings("unused")
    public void method1(byte[] bytes, @TopicRef String topic, @Partition int partition, @Offset long offset) {
      comeBytes.add(bytes);
      comeTopics.add(topic);
      comePartitions.add(partition);
      comeOffsets.add(offset);
    }

  }

  @Test
  public void invoke_CallNakedBytes_1() {
    CallNakedBytes                 controller = new CallNakedBytes();
    ConsumerDefinitionOnController def        = createDefinition(controller, "method1");

    InvokeSessionFactory invoker = def.getInvokeSessionFactory();

    var b = newRecordBuilder().appendNaked("asd1", 11, 433, "Red dragon".getBytes(UTF_8))
                              .appendNaked("dsa1", 1, 1, 4532534L)
                              .appendNaked("asd2", 13, 411, "Status of death".getBytes(UTF_8));

    try (InvokeSession session = invoker.createSession()) {

      InvokeResult result = session.invoke(b.build(), new Profile(null));

      assertThat(result.invocationError()).isNull();

    }

    assertThat(controller.comeBytes).hasSize(2);

    assertThat(controller.comeBytes.get(0)).isEqualTo("Red dragon".getBytes(UTF_8));
    assertThat(controller.comeBytes.get(1)).isEqualTo("Status of death".getBytes(UTF_8));

    assertThat(controller.comeTopics.get(0)).isEqualTo("asd1");
    assertThat(controller.comeTopics.get(1)).isEqualTo("asd2");

    assertThat(controller.comePartitions.get(0)).isEqualTo(11);
    assertThat(controller.comePartitions.get(1)).isEqualTo(13);

    assertThat(controller.comeOffsets.get(0)).isEqualTo(433L);
    assertThat(controller.comeOffsets.get(1)).isEqualTo(411L);
  }


  static class C_useProfileRef {

    public final List<String> comeProfiles = new ArrayList<>();
    public final List<Box>    comeBoxes    = new ArrayList<>();

    @Topic("test1")
    @KafkaProfile("4qWI43e9O9")
    @SuppressWarnings("unused")
    public void method1(Box box, @ProfileRef String profile) {
      comeBoxes.add(box);
      comeProfiles.add(profile);
    }

  }

  @Test
  public void invoke_C_useProfileRef() {
    var                            controller = new C_useProfileRef();
    ConsumerDefinitionOnController def        = createDefinition(controller, "method1");

    InvokeSessionFactory invoker = def.getInvokeSessionFactory();

    var b = newRecordBuilder().appendInBox("top1", 13, 17, "Hello 9Sa7wAQ9Jd")
                              .appendInBox("top2", 19, 27, "Hello JdpIM98sBM");

    try (InvokeSession session = invoker.createSession()) {

      InvokeResult result = session.invoke(b.build(), new Profile("bHokNraStF"));

      assertThat(result.invocationError()).isNull();

    }

    assertThat(controller.comeBoxes).hasSize(2);
    assertThat(controller.comeProfiles).hasSize(2);

    assertThat(controller.comeProfiles.get(0)).isEqualTo("bHokNraStF");
    assertThat(controller.comeProfiles.get(1)).isEqualTo("bHokNraStF");
  }


}
