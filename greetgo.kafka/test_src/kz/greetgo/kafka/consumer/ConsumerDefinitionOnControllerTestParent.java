package kz.greetgo.kafka.consumer;

import kz.greetgo.kafka.core.logger.Logger;
import kz.greetgo.kafka.model.Box;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.common.TopicPartition;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import static java.util.stream.Collectors.groupingBy;
import static kz.greetgo.kafka.util_for_tests.ReflectionUtil.findMethod;

public abstract class ConsumerDefinitionOnControllerTestParent {

  protected ConsumerDefinitionOnController createDefinition(Object controller, String methodName) {
    return createDefinition(controller, methodName, null);
  }

  protected ConsumerDefinitionOnController createDefinition(Object controller, String methodName, Supplier<String> topicPrefix) {
    return new ConsumerDefinitionOnController(controller,
                                              findMethod(controller, methodName),
                                              new Logger(),
                                              topicPrefix != null ? topicPrefix : () -> null,
                                              null, null, null);
  }

  public static class RecordBuilder {

    private final List<ConsumerRecord<byte[], Object>> records = new ArrayList<>();

    ConsumerRecords<byte[], Object> build() {

      Map<TopicPartition, List<ConsumerRecord<byte[], Object>>> map
        = records.stream()
                 .collect(groupingBy(x -> new TopicPartition(x.topic(), x.partition())));

      return new ConsumerRecords<>(map);
    }

    public RecordBuilder appendInBox(String topic, int partition, long offset, Object boxBody) {
      Box box = new Box();
      box.body = boxBody;
      records.add(new ConsumerRecord<>(
        topic, partition, offset, new byte[]{}, box
      ));
      return this;
    }

    public RecordBuilder appendNaked(String topic, int partition, long offset, Object data) {
      records.add(new ConsumerRecord<>(
        topic, partition, offset, new byte[]{}, data
      ));
      return this;
    }
  }


  protected RecordBuilder newRecordBuilder() {
    return new RecordBuilder();
  }

}
