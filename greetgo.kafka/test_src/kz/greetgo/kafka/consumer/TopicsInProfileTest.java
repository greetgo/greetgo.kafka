package kz.greetgo.kafka.consumer;

import org.testng.annotations.Test;

import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class TopicsInProfileTest {

  @Test
  public void distinct() {

    TopicsInProfile a1 = TopicsInProfile.of(Set.of("t1", "t2", "t3"), "test1");
    TopicsInProfile a2 = TopicsInProfile.of(Set.of("t4", "t5", "t6"), "test1");
    TopicsInProfile b1 = TopicsInProfile.of(Set.of("a1", "a2", "a3"), "aron");
    TopicsInProfile b2 = TopicsInProfile.of(Set.of("a4", "a5", "a6"), "aron");
    TopicsInProfile n1 = TopicsInProfile.of(Set.of("n1", "n2", "n3"));
    TopicsInProfile n2 = TopicsInProfile.of(Set.of("n4", "n5", "n6"));
    TopicsInProfile L  = TopicsInProfile.of(Set.of(), "left");

    final Set<TopicsInProfile> source = Set.of(a1, a2, b1, b2, n1, n2, L);

    //
    //
    final Set<TopicsInProfile> result = TopicsInProfile.distinct(source);
    //
    //

    assertThat(result).isEqualTo(Set.of(
      TopicsInProfile.of(Set.of("t1", "t2", "t3", "t4", "t5", "t6"), "test1"),
      TopicsInProfile.of(Set.of("a1", "a2", "a3", "a4", "a5", "a6"), "aron"),
      TopicsInProfile.of(Set.of("n1", "n2", "n3", "n4", "n5", "n6"))
    ));

  }

  @Test
  public void prefixTopics() {

    TopicsInProfile a1 = TopicsInProfile.of(Set.of("t1", "t2", "t3"), "test1");
    TopicsInProfile b1 = TopicsInProfile.of(Set.of("a1", "a2", "a3"), "aron");
    TopicsInProfile n1 = TopicsInProfile.of(Set.of("n1", "n2", "n3"));

    final Set<TopicsInProfile> source = Set.of(a1, b1, n1);

    //
    //
    final Set<TopicsInProfile> result = TopicsInProfile.prefixTopics("_A_", source);
    //
    //

    TopicsInProfile a1_expect = TopicsInProfile.of(Set.of("_A_t1", "_A_t2", "_A_t3"), "test1");
    TopicsInProfile b1_expect = TopicsInProfile.of(Set.of("_A_a1", "_A_a2", "_A_a3"), "aron");
    TopicsInProfile n1_expect = TopicsInProfile.of(Set.of("_A_n1", "_A_n2", "_A_n3"));

    assertThat(result).isEqualTo(Set.of(a1_expect, b1_expect, n1_expect));

  }
}