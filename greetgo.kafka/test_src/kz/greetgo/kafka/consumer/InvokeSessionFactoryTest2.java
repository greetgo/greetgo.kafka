package kz.greetgo.kafka.consumer;

import kz.greetgo.kafka.consumer.annotations.DataRef;
import kz.greetgo.kafka.consumer.annotations.HeadersRef;
import kz.greetgo.kafka.consumer.annotations.KafkaId;
import kz.greetgo.kafka.consumer.annotations.KafkaKeyRef;
import kz.greetgo.kafka.consumer.annotations.Offset;
import kz.greetgo.kafka.consumer.annotations.Partition;
import kz.greetgo.kafka.consumer.annotations.ProfileRef;
import kz.greetgo.kafka.consumer.annotations.Timestamp;
import kz.greetgo.kafka.consumer.annotations.Topic;
import kz.greetgo.kafka.consumer.annotations.TopicRef;
import kz.greetgo.kafka.core.logger.Logger;
import kz.greetgo.kafka.model.Box;
import kz.greetgo.util.RND;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.common.record.TimestampType;
import org.testng.annotations.Test;

import java.lang.reflect.Method;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import static java.nio.charset.StandardCharsets.UTF_8;
import static kz.greetgo.kafka.consumer.RecordUtil.headersFromMap;
import static kz.greetgo.kafka.consumer.RecordUtil.recordWithOffset;
import static kz.greetgo.kafka.consumer.RecordUtil.recordsOf;
import static kz.greetgo.kafka.util_for_tests.ReflectionUtil.findMethod;
import static org.apache.kafka.clients.consumer.ConsumerRecord.NULL_SIZE;
import static org.assertj.core.api.Assertions.assertThat;

public class InvokeSessionFactoryTest2 {

  static class C1 {

    private List<Object> list;

    @Topic("test1")
    @SuppressWarnings("unused")
    public void method1(List<Object> list) {
      this.list = list;
    }

  }

  @Test
  public void build_invoke__object_list() {
    C1     c1     = new C1();
    Method method = findMethod(c1, "method1");

    Box box1 = new Box();
    Box box2 = new Box();
    Box box3 = new Box();
    box1.body = "record1";
    box2.body = "record2";
    box3.body = "record3";

    ConsumerRecord<byte[], Object> record1 = recordWithOffset("test1", 1, box1);
    ConsumerRecord<byte[], Object> record2 = recordWithOffset("test1", 2, box2);
    ConsumerRecord<byte[], Object> record3 = recordWithOffset("test1", 3, box3);

    ConsumerRecords<byte[], Object> records = recordsOf(List.of(record1, record2, record3));

    InvokeSessionFactory invokeSessionFactory = InvokeSessionFactory.builderController(c1, method, new Logger(), (x, y) -> true, null)
                                                                    .build();

    boolean toCommit;

    try (InvokeSession invokeSession = invokeSessionFactory.createSession()) {

      //
      //
      toCommit = invokeSession.invoke(records, new Profile(null)).needToCommit();
      //
      //

    }

    assertThat(toCommit).isTrue();
    assertThat(c1.list).isNotNull();
    assertThat(c1.list.get(0)).isEqualTo("record1");
    assertThat(c1.list.get(1)).isEqualTo("record2");
    assertThat(c1.list.get(2)).isEqualTo("record3");
  }


  static class C2 {

    private List<Box> boxList;

    @Topic("test1")
    @SuppressWarnings("unused")
    public void method1(List<Box> boxList) {
      this.boxList = boxList;
    }

  }

  @Test
  public void build_invoke__box_list() {
    C2     c2     = new C2();
    Method method = findMethod(c2, "method1");

    Box box1 = new Box();
    Box box2 = new Box();
    Box box3 = new Box();

    ConsumerRecord<byte[], Object> record1 = recordWithOffset("test1", 1, box1);
    ConsumerRecord<byte[], Object> record2 = recordWithOffset("test1", 2, box2);
    ConsumerRecord<byte[], Object> record3 = recordWithOffset("test1", 3, box3);

    ConsumerRecords<byte[], Object> records = recordsOf(List.of(record1, record2, record3));

    InvokeSessionFactory invokeSessionFactory = InvokeSessionFactory.builderController(c2, method, new Logger(), (x, y) -> true, null)
                                                                    .build();

    boolean toCommit;

    try (InvokeSession invokeSession = invokeSessionFactory.createSession()) {

      //
      //
      toCommit = invokeSession.invoke(records, new Profile(null)).needToCommit();
      //
      //

    }

    assertThat(toCommit).isTrue();
    assertThat(c2.boxList).isNotNull();
    assertThat(c2.boxList.get(0)).isSameAs(box1);
    assertThat(c2.boxList.get(1)).isSameAs(box2);
    assertThat(c2.boxList.get(2)).isSameAs(box3);
  }

  public static class AcceptModel1 {
    @DataRef
    public Object data;

    @HeadersRef
    public Map<String, String> headers;

    @ProfileRef
    public String profile;

    @KafkaId
    public String kafkaId;

    @Timestamp
    public long timestampLong;

    @Timestamp
    public Date timestampDate;

    @TopicRef
    public String topic;

    @Partition
    public int partition;

    @Offset
    public long offset;

    @KafkaKeyRef
    public byte[] keyBytes;

    @KafkaKeyRef
    public String keyStrUtf8;
  }

  static class C3 {

    List<AcceptModel1> list = null;

    @SuppressWarnings("unused")
    public void method1(List<AcceptModel1> list) {
      this.list = list;
    }

  }

  @Test
  public void build_invoke__model_list() {
    C3     c3     = new C3();
    Method method = findMethod(c3, "method1");

    String              topic1     = RND.str(10);
    String              topic2     = RND.str(10);
    String              data1      = RND.str(10);
    String              data2      = RND.str(10);
    String              kafkaId1   = RND.str(10);
    String              kafkaId2   = RND.str(10);
    int                 partition1 = RND.plusInt(20_000);
    int                 partition2 = RND.plusInt(20_000);
    long                offset1    = RND.plusLong(20_000_000_000_000L);
    long                offset2    = RND.plusLong(20_000_000_000_000L);
    String              key1       = RND.str(15);
    String              key2       = RND.str(13);
    Map<String, String> headers1   = Map.of("a1", "b1", "c1", "d1");
    Map<String, String> headers2   = Map.of("a2", "b2", "c2", "d2");
    Date                timestamp1 = RND.dateYears(-2, 0);
    Date                timestamp2 = RND.dateYears(-2, 0);

    Box box1 = new Box();
    Box box2 = new Box();
    box1.body = data1;
    box2.body = data2;
    box1.id   = kafkaId1;
    box2.id   = kafkaId2;

    String profile = RND.str(10);

    final ConsumerRecord<byte[], Object> record1 = new ConsumerRecord<>(topic1, partition1, offset1,
                                                                        timestamp1.getTime(), TimestampType.CREATE_TIME,
                                                                        NULL_SIZE, NULL_SIZE,
                                                                        key1.getBytes(UTF_8), box1,
                                                                        headersFromMap(headers1),
                                                                        Optional.empty());
    final ConsumerRecord<byte[], Object> record2 = new ConsumerRecord<>(topic2, partition2, offset2,
                                                                        timestamp2.getTime(), TimestampType.CREATE_TIME,
                                                                        NULL_SIZE, NULL_SIZE,
                                                                        key2.getBytes(UTF_8), box2,
                                                                        headersFromMap(headers2),
                                                                        Optional.empty());

    ConsumerRecords<byte[], Object> records = recordsOf(List.of(record1, record2));

    InvokeSessionFactoryControllerBuilder builder = InvokeSessionFactory.builderController(c3, method, new Logger(), (x, y) -> true, null);

    InvokeSessionFactory invokeSessionFactory = builder.build();

    boolean toCommit;

    try (InvokeSession invokeSession = invokeSessionFactory.createSession()) {

      //
      //
      toCommit = invokeSession.invoke(records, new Profile(profile)).needToCommit();
      //
      //

    }

    assertThat(c3.list).hasSize(2);
    assertThat(toCommit).isTrue();

    AcceptModel1 m1 = c3.list.get(0);
    AcceptModel1 m2 = c3.list.get(1);

    assertThat(Set.of(m1.kafkaId, m2.kafkaId)).isEqualTo(Set.of(kafkaId1, kafkaId2));

    if (!Objects.equals(m1.kafkaId, kafkaId1)) {
      var tmp = m1;
      m1 = m2;
      m2 = tmp;
    }

    assertThat(m1).isNotNull();
    assertThat(m2).isNotNull();

    assertThat(m1.data).isEqualTo(data1);
    assertThat(m2.data).isEqualTo(data2);

    assertThat(m1.headers).isEqualTo(headers1);
    assertThat(m2.headers).isEqualTo(headers2);

    assertThat(m1.timestampDate).isEqualTo(timestamp1);
    assertThat(m2.timestampDate).isEqualTo(timestamp2);

    assertThat(m1.timestampLong).isEqualTo(timestamp1.getTime());
    assertThat(m2.timestampLong).isEqualTo(timestamp2.getTime());

    assertThat(m1.topic).isEqualTo(topic1);
    assertThat(m2.topic).isEqualTo(topic2);

    assertThat(m1.partition).isEqualTo(partition1);
    assertThat(m2.partition).isEqualTo(partition2);

    assertThat(m1.offset).isEqualTo(offset1);
    assertThat(m2.offset).isEqualTo(offset2);

    assertThat(m1.keyBytes).isEqualTo(key1.getBytes(UTF_8));
    assertThat(m2.keyBytes).isEqualTo(key2.getBytes(UTF_8));

    assertThat(m1.keyStrUtf8).isEqualTo(key1);
    assertThat(m2.keyStrUtf8).isEqualTo(key2);
  }

  static class C4 {

    private ConsumerRecords<byte[], Object> records = null;

    @Topic("test1")
    @SuppressWarnings("unused")
    public void method1(ConsumerRecords<byte[], Object> records) {
      this.records = records;
    }

  }

  @Test
  public void build_invoke__directRecords() {
    C4     c4     = new C4();
    Method method = findMethod(c4, "method1");

    ConsumerRecord<byte[], Object> record1 = new ConsumerRecord<>("test1", 1, 1, new byte[0], "text1");
    ConsumerRecord<byte[], Object> record2 = new ConsumerRecord<>("test1", 1, 2, new byte[0], "text2");
    ConsumerRecord<byte[], Object> record3 = new ConsumerRecord<>("test1", 1, 3, new byte[0], "text3");

    ConsumerRecords<byte[], Object> records = recordsOf(List.of(record1, record2, record3));

    InvokeSessionFactory invokeSessionFactory = InvokeSessionFactory.builderController(c4, method, new Logger(), (x, y) -> true, null)
                                                                    .build();

    boolean toCommit;

    try (InvokeSession invokeSession = invokeSessionFactory.createSession()) {

      //
      //
      toCommit = invokeSession.invoke(records, new Profile(null)).needToCommit();
      //
      //

    }

    assertThat(toCommit).isTrue();
    assertThat(c4.records).isSameAs(records);
  }

}
