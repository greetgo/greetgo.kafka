package kz.greetgo.kafka.consumer;

import kz.greetgo.kafka.consumer.annotations.ConsumerName;
import kz.greetgo.kafka.consumer.annotations.ConsumersFolder;
import kz.greetgo.kafka.consumer.annotations.Direct;
import kz.greetgo.kafka.consumer.annotations.GroupId;
import kz.greetgo.kafka.consumer.annotations.KafkaNotifier;
import kz.greetgo.kafka.consumer.annotations.KafkaProfile;
import kz.greetgo.kafka.consumer.annotations.Topic;
import kz.greetgo.kafka.consumer.annotations.TopicListMethod;
import kz.greetgo.kafka.core.logger.Logger;
import kz.greetgo.kafka.model.Box;
import kz.greetgo.util.RND;
import org.testng.annotations.Test;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Set;

import static kz.greetgo.kafka.util_for_tests.ReflectionUtil.findMethod;
import static org.assertj.core.api.Assertions.assertThat;

public class ConsumerDefinitionOnControllerTest1 {

  static class C_getAutoOffsetReset {
    @Topic("test1")
    @GroupId("CoolGroupId1")
    @SuppressWarnings("unused")
    public void method1(Box box) {}

    @KafkaNotifier
    @Topic("test1")
    @GroupId("CoolGroupId2")
    @SuppressWarnings("unused")
    public void method2(Box box) {}
  }

  @Test
  public void getAutoOffsetReset_EARLIEST() {
    C_getAutoOffsetReset controller = new C_getAutoOffsetReset();
    Method               method     = findMethod(controller, "method1");

    ConsumerDefinitionOnController consumerDefinition = new ConsumerDefinitionOnController(controller, method,
                                                                                           new Logger(),
                                                                                           null,
                                                                                           null,
                                                                                           null,
                                                                                           (x, y, z) -> true);

    //
    //
    AutoOffsetReset autoOffsetReset = consumerDefinition.getAutoOffsetReset();
    //
    //

    assertThat(autoOffsetReset).isEqualTo(AutoOffsetReset.EARLIEST);
  }

  @Test
  public void getAutoOffsetReset_LATEST() {
    C_getAutoOffsetReset controller = new C_getAutoOffsetReset();
    Method               method     = findMethod(controller, "method2");

    String consumerHostId = RND.str(10);

    var consumerDefinition = new ConsumerDefinitionOnController(controller,
                                                                method,
                                                                new Logger(),
                                                                null,
                                                                consumerHostId,
                                                                null,
                                                                (x, y, z) -> true);

    //
    //
    AutoOffsetReset autoOffsetReset = consumerDefinition.getAutoOffsetReset();
    String          groupId         = consumerDefinition.getGroupId();
    //
    //

    assertThat(autoOffsetReset).isEqualTo(AutoOffsetReset.LATEST);
    assertThat(groupId).isEqualTo("CoolGroupId2" + consumerHostId);
  }

  static class C_getGroupId {
    @Topic("test1")
    @SuppressWarnings("unused")
    public void methodHelloWorld(Box box) {}

    @Topic("test1")
    @SuppressWarnings("unused")
    @GroupId("Hello_World_Group_ID")
    public void methodForAnnotation() {}
  }

  @Test
  public void getGroupId_fromMethod() {
    C_getGroupId controller = new C_getGroupId();
    Method       method     = findMethod(controller, "methodHelloWorld");

    var consumerDefinition = new ConsumerDefinitionOnController(controller,
                                                                method,
                                                                new Logger(),
                                                                null,
                                                                null,
                                                                null,
                                                                (x, y, z) -> true);

    //
    //
    String groupId = consumerDefinition.getGroupId();
    //
    //

    assertThat(groupId).isEqualTo("methodHelloWorld");
  }

  @Test
  public void getGroupId_fromAnnotation() {
    C_getGroupId controller = new C_getGroupId();
    Method       method     = findMethod(controller, "methodForAnnotation");

    var consumerDefinition = new ConsumerDefinitionOnController(controller,
                                                                method,
                                                                new Logger(),
                                                                null,
                                                                null,
                                                                null,
                                                                (x, y, z) -> true);

    //
    //
    String groupId = consumerDefinition.getGroupId();
    //
    //

    assertThat(groupId).isEqualTo("Hello_World_Group_ID");
  }

  static class C_getFolderPath_null {

    @Topic("topic")
    @SuppressWarnings("unused")
    public void method1() {}

  }

  @Test
  public void getFolderPath_null() {
    C_getFolderPath_null controller = new C_getFolderPath_null();
    Method               method     = findMethod(controller, "method1");

    var consumerDefinition = new ConsumerDefinitionOnController(controller,
                                                                method,
                                                                new Logger(),
                                                                null,
                                                                null,
                                                                null,
                                                                (x, y, z) -> true);

    //
    //
    String getFolderPath = consumerDefinition.getFolderPath();
    //
    //

    assertThat(getFolderPath).isNull();

  }

  @ConsumersFolder("hi_from_consumers_path")
  static class C_getFolderPath_ok {

    @Topic("topic")
    @SuppressWarnings("unused")
    public void method1() {}

  }

  @Test
  public void getFolderPath_ok() {
    C_getFolderPath_ok controller = new C_getFolderPath_ok();
    Method             method     = findMethod(controller, "method1");

    ConsumerDefinitionOnController consumerDefinition = new ConsumerDefinitionOnController(controller,
                                                                                           method,
                                                                                           new Logger(),
                                                                                           null,
                                                                                           null,
                                                                                           null,
                                                                                           (x, y, z) -> true);

    //
    //
    String getFolderPath = consumerDefinition.getFolderPath();
    //
    //

    assertThat(getFolderPath).isEqualTo("hi_from_consumers_path");

  }


  static class C_getConsumerName {

    @Topic("topic")
    @SuppressWarnings("unused")
    public void helloWorldConsumerWOW() {}

    @Topic("topic")
    @ConsumerName("ThisIsACoolConsumer")
    @SuppressWarnings("unused")
    public void method2() {}
  }

  @Test
  public void getConsumerName_fromMethod() {
    C_getConsumerName controller = new C_getConsumerName();
    Method            method     = findMethod(controller, "helloWorldConsumerWOW");

    ConsumerDefinitionOnController consumerDefinition = new ConsumerDefinitionOnController(controller,
                                                                                           method,
                                                                                           new Logger(),
                                                                                           null,
                                                                                           null,
                                                                                           null,
                                                                                           (x, y, z) -> true);

    //
    //
    String consumerName = consumerDefinition.getConsumerName();
    //
    //

    assertThat(consumerName).isEqualTo("helloWorldConsumerWOW");
  }

  @Test
  public void getConsumerName_fromAnnotation() {
    C_getConsumerName controller = new C_getConsumerName();
    Method            method     = findMethod(controller, "method2");

    ConsumerDefinitionOnController consumerDefinition = new ConsumerDefinitionOnController(controller,
                                                                                           method,
                                                                                           new Logger(),
                                                                                           null,
                                                                                           null,
                                                                                           null,
                                                                                           (x, y, z) -> true);

    //
    //
    String consumerName = consumerDefinition.getConsumerName();
    //
    //

    assertThat(consumerName).isEqualTo("ThisIsACoolConsumer");
  }

  static class C_isAutoCommit {

    @Topic("topic")
    @SuppressWarnings("unused")
    public void method1() {}

  }

  @Test
  public void isAutoCommit() {
    C_isAutoCommit controller = new C_isAutoCommit();
    Method         method     = findMethod(controller, "method1");

    ConsumerDefinitionOnController consumerDefinition = new ConsumerDefinitionOnController(controller,
                                                                                           method,
                                                                                           new Logger(),
                                                                                           null,
                                                                                           null,
                                                                                           null,
                                                                                           (x, y, z) -> true);

    //
    //
    boolean autoCommit = consumerDefinition.isAutoCommit();
    //
    //

    assertThat(autoCommit).isFalse();
  }

  @ConsumersFolder("topFolder")
  static class C_logDisplay {

    @Topic("topic")
    @SuppressWarnings("unused")
    @ConsumerName("ThisIsACoolConsumer")
    public void method1() {}

  }

  @Test
  public void logDisplay_fromAnnotation() {
    C_logDisplay controller = new C_logDisplay();
    Method       method     = findMethod(controller, "method1");

    ConsumerDefinitionOnController consumerDefinition = new ConsumerDefinitionOnController(controller,
                                                                                           method,
                                                                                           new Logger(),
                                                                                           null,
                                                                                           null,
                                                                                           null,
                                                                                           (x, y, z) -> true);

    //
    //
    String logDisplay = consumerDefinition.logDisplay();
    //
    //

    assertThat(logDisplay).isEqualTo("topFolder/C_logDisplay.method1[ThisIsACoolConsumer]");
  }

  static class C2_logDisplay {

    @Topic("topic")
    @SuppressWarnings("unused")
    public void coolMethodName() {}

  }

  @Test
  public void logDisplay_fromMethod() {
    C2_logDisplay controller = new C2_logDisplay();
    Method        method     = findMethod(controller, "coolMethodName");

    ConsumerDefinitionOnController consumerDefinition = new ConsumerDefinitionOnController(controller,
                                                                                           method,
                                                                                           new Logger(),
                                                                                           null,
                                                                                           null,
                                                                                           null,
                                                                                           (x, y, z) -> true);

    //
    //
    String logDisplay = consumerDefinition.logDisplay();
    //
    //

    assertThat(logDisplay).isEqualTo("C2_logDisplay.[coolMethodName]");
  }

  static class ClassFor_topicList {

    @Topic("topicHelloWorld")
    @SuppressWarnings("unused")
    public void method1() {}

    @Topic({"topicA1", "topicB2"})
    @SuppressWarnings("unused")
    public void method2() {}

  }

  @Test
  public void topicList_oneTopic() {
    ClassFor_topicList controller = new ClassFor_topicList();
    Method             method     = findMethod(controller, "method1");

    ConsumerDefinitionOnController consumerDefinition = new ConsumerDefinitionOnController(controller,
                                                                                           method,
                                                                                           new Logger(),
                                                                                           null,
                                                                                           null,
                                                                                           null,
                                                                                           (x, y, z) -> true);

    //
    //
    Set<TopicsInProfile> topicList = consumerDefinition.topicList();
    //
    //

    assertThat(topicList).hasSize(1);
    assertThat(topicList).isEqualTo(Set.of(TopicsInProfile.of(Set.of("topicHelloWorld"))));
  }

  @Test
  public void topicList_twoTopics() {
    ClassFor_topicList controller = new ClassFor_topicList();
    Method             method     = findMethod(controller, "method2");

    ConsumerDefinitionOnController consumerDefinition = new ConsumerDefinitionOnController(controller,
                                                                                           method,
                                                                                           new Logger(),
                                                                                           null,
                                                                                           null,
                                                                                           null,
                                                                                           (x, y, z) -> true);

    //
    //
    Set<TopicsInProfile> topicList = consumerDefinition.topicList();
    //
    //

    assertThat(topicList).isEqualTo(Set.of(
      TopicsInProfile.of(Set.of("topicA1", "topicB2"))
    ));
  }

  static class C_Direct {

    @Direct
    @Topic("topic")
    public void methodDirect() {}

    @Topic("topic")
    public void methodKafka() {}

  }

  @Test
  public void isDirect() {
    C_Direct controller   = new C_Direct();
    Method   methodDirect = findMethod(controller, "methodDirect");
    Method   methodKafka  = findMethod(controller, "methodKafka");

    var consumerDefinitionDirect = new ConsumerDefinitionOnController(controller,
                                                                      methodDirect,
                                                                      new Logger(),
                                                                      null,
                                                                      null,
                                                                      null,
                                                                      (x, y, z) -> true);

    var consumerDefinitionKafka = new ConsumerDefinitionOnController(controller,
                                                                     methodKafka,
                                                                     new Logger(),
                                                                     null,
                                                                     null,
                                                                     null,
                                                                     (x, y, z) -> true);

    //
    //
    boolean isDirect_direct = consumerDefinitionDirect.isDirect();
    boolean isDirect_kafka  = consumerDefinitionKafka.isDirect();
    //
    //

    assertThat(isDirect_direct).isTrue();
    assertThat(isDirect_kafka).isFalse();

  }

  public static class C_groupId {

    public String hello() {
      return "quo";
    }

    @Topic("test1")
    @GroupId("status {hello()}")
    @SuppressWarnings("unused")
    public void method1(Box box) {}
  }

  @Test
  public void getGroupId() {
    C_groupId controller = new C_groupId();
    Method    method     = findMethod(controller, "method1");

    ConsumerDefinitionOnController consumerDefinition = new ConsumerDefinitionOnController(controller, method,
                                                                                           new Logger(),
                                                                                           null,
                                                                                           null,
                                                                                           null,
                                                                                           (x, y, z) -> true);

    //
    //
    String groupId = consumerDefinition.getGroupId();
    //
    //

    assertThat(groupId).isEqualTo("status quo");
  }


  static class C_getProfile {
    @Topic("test1")
    @KafkaProfile("h7B41HlfZI")
    @SuppressWarnings("unused")
    public void methodWithProfile(Box box) {}

    @KafkaNotifier
    @Topic("test1")
    @SuppressWarnings("unused")
    public void methodNoProfile(Box box) {}
  }

  @Test
  public void profile_EARLIEST() {
    C_getProfile controller = new C_getProfile();
    Method       method1    = findMethod(controller, "methodWithProfile");
    Method       method2    = findMethod(controller, "methodNoProfile");

    ConsumerDefinitionOnController consumerDefinition1 = new ConsumerDefinitionOnController(controller, method1,
                                                                                            new Logger(),
                                                                                            null,
                                                                                            null,
                                                                                            null,
                                                                                            (x, y, z) -> true);
    ConsumerDefinitionOnController consumerDefinition2 = new ConsumerDefinitionOnController(controller, method2,
                                                                                            new Logger(),
                                                                                            null,
                                                                                            null,
                                                                                            null,
                                                                                            (x, y, z) -> true);

    //
    //
    Profile profile1 = consumerDefinition1.profile();
    Profile profile2 = consumerDefinition2.profile();
    //
    //

    assertThat(profile1.profile).isEqualTo("h7B41HlfZI");
    assertThat(profile2.profile).isEqualTo(null);
  }


  public static class C_topicList__withProfiles {
    @KafkaProfile("2H1Nwk9Iij")
    @TopicListMethod("listTopics")
    public void methodWithProfile(Box box) {}

    @TopicListMethod("listTopics")
    public void methodNoProfile(Box box) {}

    @SuppressWarnings("unused")
    public List<TopicsInProfile> listTopics() {
      return List.of(
        TopicsInProfile.of(Set.of("topic1", "topic2")),
        TopicsInProfile.of(Set.of("topic3", "topic4"), "af7iv8MYf1")
      );
    }
  }

  @Test
  public void topicList__withProfiles() {
    var    controller = new C_topicList__withProfiles();
    Method method1    = findMethod(controller, "methodWithProfile");
    Method method2    = findMethod(controller, "methodNoProfile");

    String[] _A_ = new String[]{"_A_"};
    String[] _B_ = new String[]{"_B_"};

    ConsumerDefinitionOnController consumerDefinition1 = new ConsumerDefinitionOnController(controller, method1,
                                                                                            new Logger(),
                                                                                            () -> _A_[0],
                                                                                            null,
                                                                                            null,
                                                                                            (x, y, z) -> true);
    ConsumerDefinitionOnController consumerDefinition2 = new ConsumerDefinitionOnController(controller, method2,
                                                                                            new Logger(),
                                                                                            () -> _B_[0],
                                                                                            null,
                                                                                            null,
                                                                                            (x, y, z) -> true);
    {
      //
      //
      final Set<TopicsInProfile> topics1 = consumerDefinition1.topicList();
      final Set<TopicsInProfile> topics2 = consumerDefinition2.topicList();
      //
      //

      assertThat(topics1).isEqualTo(Set.of(
        TopicsInProfile.of(Set.of("topic1", "topic2"), "2H1Nwk9Iij"),
        TopicsInProfile.of(Set.of("topic3", "topic4"), "af7iv8MYf1")
      ));
      assertThat(topics2).isEqualTo(Set.of(
        TopicsInProfile.of(Set.of("topic1", "topic2")),
        TopicsInProfile.of(Set.of("topic3", "topic4"), "af7iv8MYf1")
      ));
    }

    {
      //
      //
      final Set<TopicsInProfile> topics1 = consumerDefinition1.prefixedTopicList();
      final Set<TopicsInProfile> topics2 = consumerDefinition2.prefixedTopicList();
      //
      //

      assertThat(topics1).isEqualTo(Set.of(
        TopicsInProfile.of(Set.of("_A_topic1", "_A_topic2"), "2H1Nwk9Iij"),
        TopicsInProfile.of(Set.of("_A_topic3", "_A_topic4"), "af7iv8MYf1")
      ));
      assertThat(topics2).isEqualTo(Set.of(
        TopicsInProfile.of(Set.of("_B_topic1", "_B_topic2")),
        TopicsInProfile.of(Set.of("_B_topic3", "_B_topic4"), "af7iv8MYf1")
      ));
    }
    {
      _A_[0] = "_X_";
      _B_[0] = "_Y_";

      //
      //
      final Set<TopicsInProfile> topics1 = consumerDefinition1.prefixedTopicList();
      final Set<TopicsInProfile> topics2 = consumerDefinition2.prefixedTopicList();
      //
      //

      assertThat(topics1).isEqualTo(Set.of(
        TopicsInProfile.of(Set.of("_X_topic1", "_X_topic2"), "2H1Nwk9Iij"),
        TopicsInProfile.of(Set.of("_X_topic3", "_X_topic4"), "af7iv8MYf1")
      ));
      assertThat(topics2).isEqualTo(Set.of(
        TopicsInProfile.of(Set.of("_Y_topic1", "_Y_topic2")),
        TopicsInProfile.of(Set.of("_Y_topic3", "_Y_topic4"), "af7iv8MYf1")
      ));
    }
  }

}
