package kz.greetgo.kafka.consumer;

import kz.greetgo.kafka.consumer.annotations.ConsumersFolder;
import kz.greetgo.kafka.consumer.annotations.GroupId;
import kz.greetgo.kafka.consumer.annotations.Topic;
import kz.greetgo.kafka.consumer.config.ConsumerConfigDefaults;
import kz.greetgo.kafka.consumer.config.ConsumerConnectSetter;
import kz.greetgo.kafka.consumer.config.ConsumerReactorConfigFactory;
import kz.greetgo.kafka.core.ProducerSynchronizer;
import kz.greetgo.kafka.core.config.EventConfigFileFactoryOverStorage;
import kz.greetgo.kafka.core.config.EventConfigStorageInMem;
import kz.greetgo.kafka.core.logger.Logger;
import kz.greetgo.kafka.model.Box;
import kz.greetgo.kafka.model.ModelKryo;
import kz.greetgo.kafka.util.NetUtil;
import kz.greetgo.strconverter.simple.StrConverterSimple;
import org.testng.SkipException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class ConsumerReactorImplTest {

  String                bootstrapServers;
  ConsumerConnectSetter consumerConnectSetter;

  @BeforeMethod
  public void pingKafka() {
    bootstrapServers = "localhost:9092";

    consumerConnectSetter = (targetConfigMap, params) -> targetConfigMap.put("bootstrap.servers", bootstrapServers);

    if (!NetUtil.canConnectToAnyBootstrapServer(bootstrapServers)) {
      throw new SkipException("326518S6Q2 :: No kafka connection : " + bootstrapServers);
    }
  }

  @ConsumersFolder("top")
  public static class TestController {

    @SuppressWarnings("unused")
    @Topic("test_topic")
    @GroupId("test_topic_wow")
    public void consumer(ModelKryo model) {
      System.out.println("Come from kafka: " + model);
    }

  }

  @Test
  public void startStop() {

    TestController controller = new TestController();

    Logger logger = new Logger();

    var cde = new ConsumerDefinitionExtractor(logger,
                                              null,
                                              c -> c.getClass().getSimpleName() + ".workerCount",
                                              () -> "testHost",
                                              (x, y, z) -> true);

    List<ConsumerDefinitionOnController> consumerDefinitionList = cde.extract(controller);

    assertThat(consumerDefinitionList).hasSize(1);
    var consumerDefinition = consumerDefinitionList.get(0);

    StrConverterSimple strConverter = new StrConverterSimple();
    strConverter.convertRegistry().register(Box.class);
    strConverter.convertRegistry().register(ModelKryo.class);

    EventConfigStorageInMem configStorage = new EventConfigStorageInMem();

    var configFactory = new EventConfigFileFactoryOverStorage(configStorage);

    var reactorConfigFactory = ConsumerReactorConfigFactory.builder()
                                                           .configFactory(configFactory)
                                                           .defaults(profile -> ConsumerConfigDefaults.withDefaults())
                                                           .build();

    var consumerConfig = reactorConfigFactory.getConsumerConfig(consumerDefinition);

    var consumerReactor = ConsumerReactor.builder()
                                         .strConverter(() -> strConverter)
                                         .consumerConnectSetter(consumerConnectSetter)
                                         .logger(logger)
                                         .consumerDefinition(consumerDefinition)
                                         .producerSynchronizer(new ProducerSynchronizer(logger))
                                         .consumerConfig(consumerConfig)
                                         .build();

    consumerReactor.start();

    System.out.println("2834iEFyTv :: consumerReactor = " + consumerReactor);
    configStorage.printCurrentState();

    reactorConfigFactory.close();
  }
}
