package kz.greetgo.kafka.consumer.config;

import kz.greetgo.kafka.util.TestEventConfigFile;
import kz.greetgo.kafka.util.TestHandler;
import org.testng.annotations.Test;

import java.time.Duration;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.entry;

public class FolderConsumerParamsConfigTest {

  @Test
  public void createNewFile() {

    ConsumerConfigDefaults defs = new ConsumerConfigDefaults();
    defs.addDefinition(" Long con.asd.wow.timeout.ms     43125431 ");
    defs.addDefinition(" Int  con.dsa.wow.column.count        457 ");
    defs.addDefinition(" Str  con.asd.wow.name           Co fa me ");
    defs.addDefinition(" Long out.poll.duration.ms           2800 ");

    TestEventConfigFile testConfig = new TestEventConfigFile();
    testConfig.clearCounts();

    FolderConsumerParamsConfig x = new FolderConsumerParamsConfig(testConfig, () -> defs);

    TestHandler testHandler = new TestHandler();
    x.addChangeHandler(testHandler);

    assertThat(testConfig.allCountSum()).isZero();

    assertThat(testHandler.happenCount).isZero();

    Map<String, String> configMap = x.getConfigMap();
    assertThat(configMap).contains(entry("asd.wow.timeout.ms", "43125431"));
    assertThat(configMap).contains(entry("dsa.wow.column.count", "457"));
    assertThat(configMap).contains(entry("asd.wow.name", "Co fa me"));
    assertThat(configMap).hasSize(3);

    assertThat(testConfig.readContent_callCount).isEqualTo(1);
    assertThat(testConfig.writeContent_callCount).isEqualTo(1);
    assertThat(testConfig.allCountSum()).describedAs(testConfig.allCounts()).isEqualTo(2);

    testConfig.clearCounts();

    Duration pollDuration = x.pollDuration();

    assertThat(pollDuration).isEqualTo(Duration.ofMillis(2800));

    assertThat(testConfig.allCountSum()).isZero();

    assertThat(testHandler.happenCount).isZero();

    List<String> lines = testConfig.readLines();
    for (int i = 0; i < lines.size(); i++) {
      final String line = lines.get(i);
      System.out.println("assertThat(lines.get(" + i + ")).isEqualTo(\"" + line + "\");");
    }

    assertThat(lines.get(0)).isEqualTo("");
    assertThat(lines.get(1)).isEqualTo("#");
    assertThat(lines.get(2)).startsWith("# Created at ");
    assertThat(lines.get(3)).isEqualTo("#");
    assertThat(lines.get(4)).isEqualTo("");
    assertThat(lines.get(5)).isEqualTo("con.asd.wow.timeout.ms = 43125431");
    assertThat(lines.get(6)).isEqualTo("con.dsa.wow.column.count = 457");
    assertThat(lines.get(7)).isEqualTo("con.asd.wow.name = Co fa me");
    assertThat(lines.get(8)).isEqualTo("out.poll.duration.ms = 2800");

  }

  @Test
  public void fileAlreadyExists_and_containsAllDefaultParams() {

    ConsumerConfigDefaults defs = new ConsumerConfigDefaults();
    defs.addDefinition(" Long con.asd.wow.timeout.ms     43125431 ");
    defs.addDefinition(" Int  con.dsa.wow.column.count        457 ");
    defs.addDefinition(" Str  con.asd.wow.name           Co fa me ");
    defs.addDefinition(" Long out.poll.duration.ms           2800 ");

    TestEventConfigFile testConfig = new TestEventConfigFile();
    testConfig.addLines("con.asd.wow.timeout.ms     =  77448844");
    testConfig.addLines("con.dsa.wow.column.count   =  11");
    testConfig.addLines("con.dsa.wow.row.count      =  13");
    testConfig.addLines("con.asd.wow.name           =  33 i 77");
    testConfig.addLines("out.poll.duration.ms       =  6166");
    testConfig.addLines("");
    testConfig.addLines("# description line 1");
    testConfig.addLines("# description line 2");
    testConfig.addLines("# description line 3");
    testConfig.addLines("con.asd.wow.name.newParam  =  76535");
    testConfig.clearCounts();

    FolderConsumerParamsConfig x = new FolderConsumerParamsConfig(testConfig, () -> defs);

    TestHandler testHandler = new TestHandler();
    x.addChangeHandler(testHandler);

    assertThat(testConfig.allCountSum()).isZero();

    assertThat(testHandler.happenCount).isZero();

    Map<String, String> configMap = x.getConfigMap();
    assertThat(configMap).contains(entry("asd.wow.timeout.ms", "77448844"));
    assertThat(configMap).contains(entry("dsa.wow.column.count", "11"));
    assertThat(configMap).contains(entry("dsa.wow.row.count", "13"));
    assertThat(configMap).contains(entry("asd.wow.name", "33 i 77"));
    assertThat(configMap).contains(entry("asd.wow.name.newParam", "76535"));
    assertThat(configMap).hasSize(5);

    assertThat(testConfig.readContent_callCount).isEqualTo(1);
    assertThat(testConfig.allCountSum()).isEqualTo(1);

    testConfig.clearCounts();

    Duration pollDuration = x.pollDuration();

    assertThat(pollDuration).isEqualTo(Duration.ofMillis(6166));

    assertThat(testConfig.allCountSum()).isZero();

    assertThat(testHandler.happenCount).isZero();

    List<String> lines = testConfig.readLines();
    for (int i = 0; i < lines.size(); i++) {
      final String line = lines.get(i);
      System.out.println("assertThat(lines.get(" + i + ")).isEqualTo(\"" + line + "\");");
    }

    assertThat(lines.get(0)).isEqualTo("con.asd.wow.timeout.ms     =  77448844");
    assertThat(lines.get(1)).isEqualTo("con.dsa.wow.column.count   =  11");
    assertThat(lines.get(2)).isEqualTo("con.dsa.wow.row.count      =  13");
    assertThat(lines.get(3)).isEqualTo("con.asd.wow.name           =  33 i 77");
    assertThat(lines.get(4)).isEqualTo("out.poll.duration.ms       =  6166");
    assertThat(lines.get(5)).isEqualTo("");
    assertThat(lines.get(6)).isEqualTo("# description line 1");
    assertThat(lines.get(7)).isEqualTo("# description line 2");
    assertThat(lines.get(8)).isEqualTo("# description line 3");
    assertThat(lines.get(9)).isEqualTo("con.asd.wow.name.newParam  =  76535");

  }

  @Test
  public void fileAlreadyExists_and_doesNotContainsAnyDefaultParams() {

    ConsumerConfigDefaults defs = new ConsumerConfigDefaults();
    defs.addDefinition(" Long con.asd.wow.timeout.ms     43125431 ");
    defs.addDefinition(" Int  con.dsa.wow.column.count        457 ");
    defs.addDefinition(" Str  con.asd.wow.name           Co fa me ");
    defs.addDefinition(" Long out.poll.duration.ms           2800 ");
    defs.addDefinition(" Long con.more-param.1                700 ");
    defs.addDefinition(" Long con.more-param.2                701 ");
    defs.addDefinition(" Long con.more-param.3                702 ");

    TestEventConfigFile testConfig = new TestEventConfigFile();
    testConfig.addLines("con.asd.wow.timeout.ms     =  77448844");
    testConfig.addLines("con.dsa.wow.column.count   =  11");
    testConfig.addLines("con.dsa.wow.row.count      =  13");
    testConfig.addLines("con.asd.wow.name           =  33 i 77");
    testConfig.addLines("out.poll.duration.ms       =  6166");
    testConfig.addLines("");
    testConfig.addLines("# description line 1");
    testConfig.addLines("# description line 2");
    testConfig.addLines("# description line 3");
    testConfig.addLines("con.asd.wow.name.newParam  =  76535");
    testConfig.clearCounts();

    FolderConsumerParamsConfig x = new FolderConsumerParamsConfig(testConfig, () -> defs);

    TestHandler testHandler = new TestHandler();
    x.addChangeHandler(testHandler);

    assertThat(testConfig.allCountSum()).isZero();

    assertThat(testHandler.happenCount).isZero();

    Map<String, String> configMap = x.getConfigMap();
    assertThat(configMap).contains(entry("asd.wow.timeout.ms", "77448844"));
    assertThat(configMap).contains(entry("dsa.wow.column.count", "11"));
    assertThat(configMap).contains(entry("dsa.wow.row.count", "13"));
    assertThat(configMap).contains(entry("asd.wow.name", "33 i 77"));
    assertThat(configMap).contains(entry("asd.wow.name.newParam", "76535"));
    assertThat(configMap).contains(entry("more-param.1", "700"));
    assertThat(configMap).contains(entry("more-param.2", "701"));
    assertThat(configMap).contains(entry("more-param.3", "702"));
    assertThat(configMap).hasSize(8);

    assertThat(testConfig.readContent_callCount).isEqualTo(1);
    assertThat(testConfig.writeContent_callCount).isEqualTo(1);
    assertThat(testConfig.allCountSum()).isEqualTo(2);

    testConfig.clearCounts();

    Duration pollDuration = x.pollDuration();

    assertThat(pollDuration).isEqualTo(Duration.ofMillis(6166));

    assertThat(testConfig.allCountSum()).isZero();

    assertThat(testHandler.happenCount).isZero();

    List<String> lines = testConfig.readLines();
    for (int i = 0; i < lines.size(); i++) {
      final String line = lines.get(i);
      System.out.println("assertThat(lines.get(" + i + ")).isEqualTo(\"" + line + "\");");
    }

    assertThat(lines.get(0)).isEqualTo("con.asd.wow.timeout.ms     =  77448844");
    assertThat(lines.get(1)).isEqualTo("con.dsa.wow.column.count   =  11");
    assertThat(lines.get(2)).isEqualTo("con.dsa.wow.row.count      =  13");
    assertThat(lines.get(3)).isEqualTo("con.asd.wow.name           =  33 i 77");
    assertThat(lines.get(4)).isEqualTo("out.poll.duration.ms       =  6166");
    assertThat(lines.get(5)).isEqualTo("");
    assertThat(lines.get(6)).isEqualTo("# description line 1");
    assertThat(lines.get(7)).isEqualTo("# description line 2");
    assertThat(lines.get(8)).isEqualTo("# description line 3");
    assertThat(lines.get(9)).isEqualTo("con.asd.wow.name.newParam  =  76535");
    assertThat(lines.get(10)).isEqualTo("");
    assertThat(lines.get(11)).isEqualTo("#");
    assertThat(lines.get(12)).startsWith("# Appended at ");
    assertThat(lines.get(13)).isEqualTo("#");
    assertThat(lines.get(14)).isEqualTo("");
    assertThat(lines.get(15)).isEqualTo("con.more-param.1 = 700");
    assertThat(lines.get(16)).isEqualTo("");
    assertThat(lines.get(17)).isEqualTo("con.more-param.2 = 701");
    assertThat(lines.get(18)).isEqualTo("");
    assertThat(lines.get(19)).isEqualTo("con.more-param.3 = 702");

  }
}
