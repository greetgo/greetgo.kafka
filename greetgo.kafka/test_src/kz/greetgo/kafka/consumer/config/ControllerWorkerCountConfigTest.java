package kz.greetgo.kafka.consumer.config;

import kz.greetgo.kafka.core.config.ConfigEventType;
import kz.greetgo.kafka.util.TestEventConfigFile;
import kz.greetgo.kafka.util.TestHandler;
import org.testng.annotations.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class ControllerWorkerCountConfigTest {

  @Test
  public void createNewFileWithDefaultValues() {

    TestEventConfigFile testConfig = new TestEventConfigFile();

    testConfig.clearCounts();

    ControllerWorkerCountConfig config = new ControllerWorkerCountConfig(testConfig, ConsumerConfigDefaults::withDefaults);

    TestHandler testHandler = new TestHandler();
    config.addChangeHandler(testHandler);

    assertThat(testHandler.happenCount).isZero();

    assertThat(testConfig.writeContent_callCount).isZero();
    assertThat(testConfig.readContent_callCount).isZero();
    assertThat(testConfig.createdAt_callCount).isZero();
    assertThat(testConfig.lastModifiedAt_callCount).isZero();

    config.register("wow", "hello world\nline 2\nline 3");
    config.register("asd", "status quo");
    config.register("status", null);

    assertThat(testHandler.happenCount).isZero();

    assertThat(testConfig.writeContent_callCount).isZero();
    assertThat(testConfig.readContent_callCount).isZero();
    assertThat(testConfig.createdAt_callCount).isZero();
    assertThat(testConfig.lastModifiedAt_callCount).isZero();

    assertThat(config.getWorkerCount("wow")).isEqualTo(1);
    assertThat(config.getWorkerCount("asd")).isEqualTo(1);
    assertThat(config.getWorkerCount("status")).isEqualTo(1);

    assertThat(testHandler.happenCount).isZero();

    assertThat(testConfig.writeContent_callCount).isEqualTo(1);
    assertThat(testConfig.readContent_callCount).isEqualTo(1);

    config.register("after", null);
    assertThat(config.getWorkerCount("after")).isEqualTo(1);

    assertThat(testConfig.writeContent_callCount).isEqualTo(2);
    assertThat(testConfig.readContent_callCount).isEqualTo(2);

    config.close();

    assertThat(testHandler.happenCount).isZero();

    assertThat(testConfig.writeContent_callCount).isEqualTo(2);
    assertThat(testConfig.readContent_callCount).isEqualTo(2);

    List<String> lines = testConfig.readLines();
    for (int i = 0; i < lines.size(); i++) {
      final String line = lines.get(i);
      System.out.println("assertThat(lines.get(" + i + ")).isEqualTo(\"" + line + "\");");
    }
    System.out.println("assertThat(lines).hasSize(" + lines.size() + ");");

    {
      assertThat(lines.get(0)).isEqualTo("");
      assertThat(lines.get(1)).isEqualTo("#");
      assertThat(lines.get(2)).startsWith("# Created at ");
      assertThat(lines.get(3)).isEqualTo("#");
      assertThat(lines.get(4)).isEqualTo("");
      assertThat(lines.get(5)).isEqualTo("");
      assertThat(lines.get(6)).isEqualTo("# hello world");
      assertThat(lines.get(7)).isEqualTo("# line 2");
      assertThat(lines.get(8)).isEqualTo("# line 3");
      assertThat(lines.get(9)).isEqualTo("wow = 1");
      assertThat(lines.get(10)).isEqualTo("");
      assertThat(lines.get(11)).isEqualTo("# status quo");
      assertThat(lines.get(12)).isEqualTo("asd = 1");
      assertThat(lines.get(13)).isEqualTo("");
      assertThat(lines.get(14)).isEqualTo("status = 1");
      assertThat(lines.get(15)).isEqualTo("");
      assertThat(lines.get(16)).isEqualTo("#");
      assertThat(lines.get(17)).startsWith("# Appended at ");
      assertThat(lines.get(18)).isEqualTo("#");
      assertThat(lines.get(19)).isEqualTo("");
      assertThat(lines.get(20)).isEqualTo("after = 1");
      assertThat(lines).hasSize(21);
    }
  }

  @Test
  public void fileAlreadyExistsWithAllValues() {

    TestEventConfigFile testConfig = new TestEventConfigFile();
    testConfig.addLines("");
    testConfig.addLines("key1 = 31");
    testConfig.addLines("key2 = 171");
    testConfig.addLines("");
    testConfig.addLines("# key 3 comment 1");
    testConfig.addLines("# key 3 comment 2");
    testConfig.addLines("# key 3 comment 3");
    testConfig.addLines("key3 = 11");

    testConfig.clearCounts();

    ControllerWorkerCountConfig x = new ControllerWorkerCountConfig(testConfig, ConsumerConfigDefaults::withDefaults);

    TestHandler testHandler = new TestHandler();
    x.addChangeHandler(testHandler);

    assertThat(testConfig.writeContent_callCount).isZero();
    assertThat(testConfig.readContent_callCount).isZero();
    assertThat(testConfig.createdAt_callCount).isZero();
    assertThat(testConfig.lastModifiedAt_callCount).isZero();

    x.register("key1", "hello world\nline 2\nline 3");
    x.register("key2", "status quo");
    x.register("key3", null);

    assertThat(testHandler.happenCount).isZero();

    assertThat(testConfig.writeContent_callCount).isZero();
    assertThat(testConfig.readContent_callCount).isZero();
    assertThat(testConfig.createdAt_callCount).isZero();
    assertThat(testConfig.lastModifiedAt_callCount).isZero();

    assertThat(x.getWorkerCount("key1")).isEqualTo(31);

    assertThat(testConfig.writeContent_callCount).isEqualTo(0);
    assertThat(testConfig.readContent_callCount).isEqualTo(1);

    assertThat(x.getWorkerCount("key2")).isEqualTo(171);
    assertThat(x.getWorkerCount("key3")).isEqualTo(11);

    assertThat(testConfig.writeContent_callCount).isEqualTo(0);
    assertThat(testConfig.readContent_callCount).isEqualTo(1);

    assertThat(testHandler.happenCount).isZero();

    x.close();

    assertThat(testConfig.writeContent_callCount).isEqualTo(0);
    assertThat(testConfig.readContent_callCount).isEqualTo(1);
    assertThat(testConfig.isClosed()).isTrue();

    List<String> lines = testConfig.readLines();
    for (int i = 0; i < lines.size(); i++) {
      final String line = lines.get(i);
      System.out.println("assertThat(lines.get(" + i + ")).isEqualTo(\"" + line + "\");");
    }

    assertThat(lines.get(0)).isEqualTo("");
    assertThat(lines.get(1)).isEqualTo("key1 = 31");
    assertThat(lines.get(2)).isEqualTo("key2 = 171");
    assertThat(lines.get(3)).isEqualTo("");
    assertThat(lines.get(4)).isEqualTo("# key 3 comment 1");
    assertThat(lines.get(5)).isEqualTo("# key 3 comment 2");
    assertThat(lines.get(6)).isEqualTo("# key 3 comment 3");
    assertThat(lines.get(7)).isEqualTo("key3 = 11");
  }

  @Test
  public void fileAlreadyExists_notAllKeyExists() {

    TestEventConfigFile testConfig = new TestEventConfigFile();
    testConfig.addLines("");
    testConfig.addLines("# key 3 comment 1");
    testConfig.addLines("# key 3 comment 2");
    testConfig.addLines("# key 3 comment 3");
    testConfig.addLines("key4 = 1176");

    testConfig.clearCounts();

    ControllerWorkerCountConfig x = new ControllerWorkerCountConfig(testConfig, ConsumerConfigDefaults::withDefaults);

    TestHandler testHandler = new TestHandler();
    x.addChangeHandler(testHandler);

    assertThat(testHandler.happenCount).isZero();

    assertThat(testConfig.writeContent_callCount).isZero();
    assertThat(testConfig.readContent_callCount).isZero();
    assertThat(testConfig.createdAt_callCount).isZero();
    assertThat(testConfig.lastModifiedAt_callCount).isZero();

    assertThat(testHandler.happenCount).isZero();

    x.register("key1", "hello world\nline 2\nline 3");
    x.register("key2", "status quo");
    x.register("key3", null);
    x.register("key4", null);

    assertThat(testHandler.happenCount).isZero();

    assertThat(testConfig.writeContent_callCount).isZero();
    assertThat(testConfig.readContent_callCount).isZero();
    assertThat(testConfig.createdAt_callCount).isZero();
    assertThat(testConfig.lastModifiedAt_callCount).isZero();

    assertThat(x.getWorkerCount("key1")).isEqualTo(1);
    assertThat(x.getWorkerCount("key2")).isEqualTo(1);
    assertThat(x.getWorkerCount("key3")).isEqualTo(1);
    assertThat(x.getWorkerCount("key4")).isEqualTo(1176);

    assertThat(testHandler.happenCount).isZero();

    assertThat(testConfig.writeContent_callCount).isEqualTo(1);
    assertThat(testConfig.readContent_callCount).isEqualTo(1);

    x.close();

    assertThat(testHandler.happenCount).isZero();

    assertThat(testConfig.writeContent_callCount).isEqualTo(1);
    assertThat(testConfig.readContent_callCount).isEqualTo(1);
    assertThat(testConfig.isClosed()).isTrue();

    List<String> lines = testConfig.readLines();
    for (int i = 0; i < lines.size(); i++) {
      final String line = lines.get(i);
      System.out.println("assertThat(lines.get(" + i + ")).isEqualTo(\"" + line + "\");");
    }

    assertThat(lines.get(0)).isEqualTo("");
    assertThat(lines.get(1)).isEqualTo("# key 3 comment 1");
    assertThat(lines.get(2)).isEqualTo("# key 3 comment 2");
    assertThat(lines.get(3)).isEqualTo("# key 3 comment 3");
    assertThat(lines.get(4)).isEqualTo("key4 = 1176");
    assertThat(lines.get(5)).isEqualTo("");
    assertThat(lines.get(6)).isEqualTo("#");
    assertThat(lines.get(7)).startsWith("# Appended at ");
    assertThat(lines.get(8)).isEqualTo("#");
    assertThat(lines.get(9)).isEqualTo("");
    assertThat(lines.get(10)).isEqualTo("# hello world");
    assertThat(lines.get(11)).isEqualTo("# line 2");
    assertThat(lines.get(12)).isEqualTo("# line 3");
    assertThat(lines.get(13)).isEqualTo("key1 = 1");
    assertThat(lines.get(14)).isEqualTo("");
    assertThat(lines.get(15)).isEqualTo("# status quo");
    assertThat(lines.get(16)).isEqualTo("key2 = 1");
    assertThat(lines.get(17)).isEqualTo("");
    assertThat(lines.get(18)).isEqualTo("key3 = 1");

  }

  @Test
  public void fileAlreadyExists_fileUpdated() {

    TestEventConfigFile testConfig = new TestEventConfigFile();
    testConfig.addLines("");
    testConfig.addLines("# comment of key1");
    testConfig.addLines("key1 = 8811");
    testConfig.addLines("");
    testConfig.addLines("key2 = 78");
    testConfig.addLines("key3 = 718");
    testConfig.addLines("");
    testConfig.addLines("# key 4 comment 1");
    testConfig.addLines("# key 4 comment 2");
    testConfig.addLines("# key 4 comment 3");
    testConfig.addLines("key4 = 761");

    testConfig.clearCounts();

    ControllerWorkerCountConfig x = new ControllerWorkerCountConfig(testConfig, ConsumerConfigDefaults::withDefaults);

    TestHandler testHandler = new TestHandler();
    x.addChangeHandler(testHandler);

    assertThat(testConfig.writeContent_callCount).isZero();
    assertThat(testConfig.readContent_callCount).isZero();
    assertThat(testConfig.createdAt_callCount).isZero();
    assertThat(testConfig.lastModifiedAt_callCount).isZero();

    x.register("key1", "hello world\nline 2\nline 3");
    x.register("key2", "status quo");
    x.register("key3", null);
    x.register("key4", null);

    assertThat(testHandler.happenCount).isZero();

    assertThat(testConfig.writeContent_callCount).isZero();
    assertThat(testConfig.readContent_callCount).isZero();
    assertThat(testConfig.createdAt_callCount).isZero();
    assertThat(testConfig.lastModifiedAt_callCount).isZero();

    assertThat(x.getWorkerCount("key1")).isEqualTo(8811);
    assertThat(x.getWorkerCount("key2")).isEqualTo(78);
    assertThat(x.getWorkerCount("key3")).isEqualTo(718);
    assertThat(x.getWorkerCount("key4")).isEqualTo(761);

    assertThat(testHandler.happenCount).isZero();

    assertThat(testConfig.writeContent_callCount).isEqualTo(0);
    assertThat(testConfig.readContent_callCount).isEqualTo(1);

    testConfig.clearCounts();
    testConfig.replaceLine("key4 = 761", "key4 = 76133");
    testConfig.replaceLine("key2 = 78", "key2 = 7833");
    testConfig.fireEvent(ConfigEventType.UPDATE);

    assertThat(testHandler.happenCount).isEqualTo(1);

    assertThat(testConfig.writeContent_callCount).isEqualTo(0);
    assertThat(testConfig.readContent_callCount).isEqualTo(0);

    assertThat(x.getWorkerCount("key1")).isEqualTo(8811);
    assertThat(x.getWorkerCount("key2")).isEqualTo(7833);
    assertThat(x.getWorkerCount("key3")).isEqualTo(718);
    assertThat(x.getWorkerCount("key4")).isEqualTo(76133);

    assertThat(testConfig.writeContent_callCount).isEqualTo(0);
    assertThat(testConfig.readContent_callCount).isEqualTo(1);
  }
}
