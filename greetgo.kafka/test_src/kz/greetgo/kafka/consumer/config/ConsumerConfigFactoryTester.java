package kz.greetgo.kafka.consumer.config;

import kz.greetgo.kafka.consumer.ConsumerDefinitionExtractor;
import kz.greetgo.kafka.consumer.ConsumerDefinitionOnController;
import kz.greetgo.kafka.consumer.annotations.ConsumerName;
import kz.greetgo.kafka.consumer.annotations.ConsumersFolder;
import kz.greetgo.kafka.consumer.annotations.GroupId;
import kz.greetgo.kafka.consumer.annotations.KafkaDescription;
import kz.greetgo.kafka.consumer.annotations.Topic;
import kz.greetgo.kafka.core.config.ConfigEventHandler;
import kz.greetgo.kafka.core.config.ConfigEventType;
import kz.greetgo.kafka.core.config.EventConfigFileFactory;
import kz.greetgo.kafka.core.config.EventConfigFileFactoryOverStorage;
import kz.greetgo.kafka.core.config.EventConfigStorageZooKeeper;
import kz.greetgo.kafka.core.config.EventFileHandler;
import kz.greetgo.kafka.core.config.ZooConnectParams;
import kz.greetgo.kafka.core.logger.Logger;
import kz.greetgo.util.fui.FUI;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static java.nio.charset.StandardCharsets.UTF_8;

public class ConsumerConfigFactoryTester {

  public static void main(String[] args) {
    new ConsumerConfigFactoryTester().execute();
  }

  @SuppressWarnings("SpellCheckingInspection")
  private void initStorageFui(EventConfigStorageZooKeeper ecs, FUI fui) {
    fui.button("Storage/create-file-asd", () -> {
      ecs.writeContent("asd.txt", "hello world\nbig status".getBytes(UTF_8));
      System.out.println("cwhQ5WH63k :: asd.txt created");
    });
    fui.button("Storage/create-file-dsa", () -> {
      ecs.writeContent("/sinus/wow/too-too/dsa.txt", ("" +
        "США хочет устроить России кровавую кашу и для этого травит Украину на Донбас.\n" +
        "И уже всё как бы сделано, но каша не начинается, потому что начинается массовое\n" +
        "дезертирство в Украинской армии притом среди командного состава, потому что\n" +
        "командиры отлично понимают, что с них спросят - уже есть масса примеров.\n" +
        "Давление со стороны США нарастает, и Зеленский начинает крутиться как юла\n" +
        "потому что то же не хочет отвечать за массовые убийства, вот он и\n" +
        "придумал этот финт со вступлением Украины в НАТО, что мол только у НАТО\n" +
        "есть силы устроить кровавую кашу."
      ).getBytes(UTF_8));
      System.out.println("Cvi735vok5 :: dsa.txt created");
    });

    ecs.ensureLookingFor("sinus/wow/too-too/dsa.txt");

    //noinspection Convert2Lambda
    ecs.addEventHandler(new ConfigEventHandler() {
      @Override
      public void configEventHappened(String path, ConfigEventType type) {
        System.out.println("2yTAnw5ZFb :: Changed " + path + "  ::  type = " + type);
      }
    });

  }

  private void execute() {

    try (var ecs = new EventConfigStorageZooKeeper(getClass().getSimpleName() + "/app-root",
                                                   () -> "localhost:2181",
                                                   ZooConnectParams.builder().build())) {

      FUI fui = new FUI(Paths.get("build").resolve(getClass().getSimpleName()));

      initApplication(fui, ecs);

      System.out.println("EYv4eK3rw6 :: Application start");
      fui.go();

      System.out.println("zbV0uZ52i8 :: Application exited");
    }
  }

  public static class KateController {

    @Topic({"ASD", "DSA"})
    @ConsumerName("WOW")
    @GroupId("super-group")
    public void viewSomeTopics(Object entry) {
      System.out.println("Gn1r0o54to :: entry = " + entry);
    }

    @Topic({"ASD-1", "DSA-1"})
    @ConsumerName("WOW-1")
    @GroupId("super-group-1")
    public void seeSky(Object entry) {
      System.out.println("4OSaX2RxQk :: entry = " + entry);
    }

  }

  public static class BraController {

    @Topic({"ASD", "DSA"})
    @ConsumerName("WOW")
    @GroupId("super-group")
    public void stone(Object entry) {
      System.out.println("j5okGjx5Fm :: entry = " + entry);
    }

    @Topic({"ASD-1", "DSA-1"})
    @ConsumerName("WOW-1")
    @GroupId("super-group-1")
    public void seeSky(Object entry) {
      System.out.println("BlH71bJd9y :: entry = " + entry);
    }

  }

  @ConsumersFolder("troopers")
  public static class CoolController {

    @SuppressWarnings("SpellCheckingInspection")
    @KafkaDescription("" +
      "anyone wants to create an angelic and devlish town. dark and light next to each other.\n" +
      " people would dress up like the elves from the hobbit. it would be awesome.and every morning\n" +
      " someone would play the harp for the light town and an organ for the dark, both in harmony.\n" +
      " bruh i have weird dreams.it wont be like a cult thing but more like a fantasy come true.")
    @ConsumerName("WOW")
    @Topic({"ASD", "DSA"})
    @GroupId("super-group")
    public void goingToMountain(Object entry) {
      System.out.println("kqtvE5APqm :: entry = " + entry);
    }

    @KafkaDescription("" +
      "A dream of mine is to live in a small town in the middle of nowhere.\n" +
      "Everyone gets on well. Money is gold coins. In the middle of a forest\n" +
      "with enchanted looking water & trees. Where everyone is at ease and\n" +
      "believe in myths and creatures. Where everyone can live at peace with\n" +
      "nature & animals. A safe place :) ")
    @Topic({"ASD-1", "DSA-1"})
    @ConsumerName("WOW-1")
    @GroupId("super-group-1")
    public void thinkingAboutLivingForever(Object entry) {
      System.out.println("vYmeDMhTaz :: entry = " + entry);
    }

  }

  private void initApplication(FUI fui, EventConfigStorageZooKeeper ecs) {
    initStorageFui(ecs, fui);

    EventConfigFileFactory ecf = new EventConfigFileFactoryOverStorage(ecs);
    initEcfFui(fui, ecf);

    initConsumerConfigFactory(ecf, fui);
  }

  private void initConsumerConfigFactory(EventConfigFileFactory ecf, FUI fui) {
    var defaults = new ConsumerConfigDefaults();
    defaults.addDefaults();

    //noinspection resource
    ConsumerReactorConfigFactory xx = ConsumerReactorConfigFactory.builder()
                                                                  .configFactory(ecf)
                                                                  .consumerParamsName("params.config")
                                                                  .defaults(profile -> defaults)
                                                                  .build();

    KateController kateController = new KateController();
    BraController  braController  = new BraController();
    CoolController coolController = new CoolController();

    var consumerDefinitionExtractor = new ConsumerDefinitionExtractor(new Logger(),
                                                                      null,
                                                                      c -> c.getClass().getSimpleName() + ".workerCount",
                                                                      () -> "test",
                                                                      (x, y, z) -> true);

    List<ConsumerDefinitionOnController> defList = new ArrayList<>();
    defList.addAll(consumerDefinitionExtractor.extract(kateController));
    defList.addAll(consumerDefinitionExtractor.extract(braController));
    defList.addAll(consumerDefinitionExtractor.extract(coolController));

    var topPrefix = "consumerConfig";

    for (final ConsumerDefinitionOnController consumerDefinition : defList) {

      String prefix = topPrefix + "/"
        + consumerDefinition.getController().getClass().getSimpleName()
        + "-"
        + consumerDefinition.getMethod().getName();

      var consumerConfig = xx.getConsumerConfig(consumerDefinition);

      consumerConfig.onChangeParams(() -> System.out.println(
        "LK2jH8kdoT :: ChangeParams `" + prefix + "` at " + new Date()), null);
      consumerConfig.onChangeWorkerCount(() -> System.out.println(
        "fAR9u78weG :: ChangeWorkerCount `" + prefix + "` at " + new Date()), null);

      fui.button(prefix + "/getConfigMap", () -> {
        var configMap = consumerConfig.getConfigMap(null);
        for (final Map.Entry<String, String> e : configMap.entrySet()) {
          System.out.println("V0M3LPOn9y :: ConfigMap :: " + e.getKey() + " = " + e.getValue());
        }
      });

      fui.button(prefix + "/getWorkerCount", () -> {
        var workerCount = consumerConfig.getWorkerCount(null);
        System.out.println("BKcA5066CH :: workerCount = " + workerCount);
      });
      fui.button(prefix + "/getPollDuration", () -> {
        var pollDuration = consumerConfig.getPollDuration(null);
        System.out.println("11vRXKqZEt :: pollDuration = " + pollDuration);
      });
    }
  }

  private void initEcfFui(FUI fui, EventConfigFileFactory ecf) {

    var configFile = ecf.createEventConfigFile("sal/mini/cop.txt");

    //noinspection Convert2Lambda
    configFile.addEventHandler(new EventFileHandler() {
      @Override
      public void eventHappened(ConfigEventType type) {
        System.out.println("36Xh34ko3X :: File event :: cop.txt :: type = " + type);
      }
    });

    fui.button("ecf/ensureLookingFor", configFile::ensureLookingFor);
    fui.button("ecf/writeContent1", () -> configFile.writeContent("file content ONE".getBytes(UTF_8)));
    fui.button("ecf/writeContent2", () -> configFile.writeContent("file content TWO".getBytes(UTF_8)));
    fui.button("ecf/readContent", () -> {
      var content = configFile.readContent();
      System.out.println("eH7g8o4oMu :: content is " + (content == null ? null : new String(content, UTF_8)));
    });

  }

}
