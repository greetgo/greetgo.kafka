package kz.greetgo.kafka.consumer.custom_consumer;

import kz.greetgo.kafka.consumer.AutoOffsetReset;
import kz.greetgo.kafka.consumer.ConsumerDefinitionCustomBuilder;
import kz.greetgo.kafka.consumer.annotations.TopicContentType;
import kz.greetgo.kafka.core.logger.Logger;
import org.testng.annotations.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class ConsumerDefinitionCustomBuilderTest {

  @Test
  public void build__simple() {

    var definition = new ConsumerDefinitionCustomBuilder()
      .direct(true, true)
      .autoOffsetReset(AutoOffsetReset.EARLIEST)
      .description("des 1")
      .commitOnAll(List.of(IllegalAccessError.class, IllegalAccessException.class))
      .commitOn(UnsatisfiedLinkError.class)
      .folderPath("/folder/path")
      .groupId("asd-001")
      .name("name-002")
      .workerCountFileName("worker-count-file-name-003")
      .topic("topic-004")
      .customConsumer((box, boxMeta) -> System.out.println("ReCxyQ63xQ :: box = " + box))
      .logger(new Logger())
      .topicPrefix(() -> null)
      .build();

    assertThat(definition).isNotNull();

  }

  @Test
  public void build__checkProfile() {

    var definition = new ConsumerDefinitionCustomBuilder()
      .direct(true, true)
      .profile("9e1um6fHd0")
      .autoOffsetReset(AutoOffsetReset.EARLIEST)
      .description("des 1")
      .commitOnAll(List.of(IllegalAccessError.class, IllegalAccessException.class))
      .commitOn(UnsatisfiedLinkError.class)
      .folderPath("/folder/path")
      .groupId("asd-001")
      .name("name-002")
      .workerCountFileName("worker-count-file-name-003")
      .topic("topic-004")
      .customConsumer((box, boxMeta) -> System.out.println("ReCxyQ63xQ :: box = " + box))
      .logger(new Logger())
      .topicPrefix(() -> null)
      .build();

    assertThat(definition).isNotNull();
    assertThat(definition.profile().profile).isEqualTo("9e1um6fHd0");
    assertThat(definition.topicContentType()).isEqualTo(TopicContentType.BOX);

  }

  @Test
  public void build__listBox__checkProfile() {

    var definition = new ConsumerDefinitionCustomBuilder()
      .direct(true, true)
      .profile("u3PnP0nn39")
      .autoOffsetReset(AutoOffsetReset.EARLIEST)
      .description("des 1")
      .folderPath("/folder/path")
      .groupId("asd-001")
      .name("name-002")
      .workerCountFileName("worker-count-file-name-003")
      .topic("topic-004")
      .customListBoxConsumer((boxRecords) -> System.out.println("Leu78779k4 :: box = " + boxRecords))
      .logger(new Logger())
      .topicPrefix(() -> null)
      .build();

    assertThat(definition).isNotNull();
    assertThat(definition.profile().profile).isEqualTo("u3PnP0nn39");
    assertThat(definition.topicContentType()).isEqualTo(TopicContentType.LIST_BOX);
  }

  @Test(expectedExceptions = IllegalArgumentException.class, expectedExceptionsMessageRegExp = "^Kg6QMz8c8h :: you cannot use CommitOn on batched consumers. KafkaConsumer:.*")
  public void build__listBox__commitOn() {

    var definition = new ConsumerDefinitionCustomBuilder()
      .direct(true, true)
      .profile("u3PnP0nn39")
      .autoOffsetReset(AutoOffsetReset.EARLIEST)
      .description("des 1")
      .commitOn(RuntimeException.class)
      .folderPath("/folder/path")
      .groupId("asd-001")
      .name("name-002")
      .workerCountFileName("worker-count-file-name-003")
      .topic("topic-004")
      .customListBoxConsumer((boxRecords) -> System.out.println("Leu78779k4 :: box = " + boxRecords))
      .logger(new Logger())
      .topicPrefix(() -> null)
      .build();

    assertThat(definition).isNotNull();
    assertThat(definition.profile().profile).isEqualTo("u3PnP0nn39");
    assertThat(definition.topicContentType()).isEqualTo(TopicContentType.LIST_BOX);
  }
}
