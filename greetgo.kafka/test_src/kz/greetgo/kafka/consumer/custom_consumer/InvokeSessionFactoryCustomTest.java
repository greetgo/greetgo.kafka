package kz.greetgo.kafka.consumer.custom_consumer;

import kz.greetgo.kafka.consumer.InvokeSession;
import kz.greetgo.kafka.consumer.InvokeSessionFactory;
import kz.greetgo.kafka.consumer.Profile;
import kz.greetgo.kafka.model.Box;
import kz.greetgo.util.RND;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.testng.annotations.Test;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static kz.greetgo.kafka.consumer.RecordUtil.recordWithPartition;
import static kz.greetgo.kafka.consumer.RecordUtil.recordsOf;
import static org.assertj.core.api.Assertions.assertThat;

public class InvokeSessionFactoryCustomTest {

  @Test
  public void customBatchInvoker() {

    AtomicInteger counter = new AtomicInteger(0);

    InvokeSessionFactory customSession = InvokeSessionFactory.builderCustom()
                                                             .customListBoxConsumer(
                                                               (batchBoxRecords -> batchBoxRecords
                                                                 .forEach((record) -> counter.incrementAndGet()))
                                                             )
                                                             .build();

    String topicName = "topic1";
    int    partition = RND.plusInt(1_000);

    ConsumerRecord<byte[], Object> record1 = recordWithPartition(topicName, partition, new Box());
    ConsumerRecord<byte[], Object> record2 = recordWithPartition(topicName, partition, new Box());
    ConsumerRecord<byte[], Object> record3 = recordWithPartition(topicName, partition, new Box());
    ConsumerRecord<byte[], Object> record4 = recordWithPartition(topicName, partition, new Box());

    try (InvokeSession session = customSession.createSession()) {

      //
      //
      session.invoke(recordsOf(List.of(record1, record2, record3, record4)), new Profile(null));
      //
      //

    }

    assertThat(counter.get()).isEqualTo(4);
  }

}
