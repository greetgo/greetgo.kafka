package kz.greetgo.kafka.consumer;

import kz.greetgo.kafka.core.logger.LoggerDestination;
import org.apache.kafka.clients.consumer.ConsumerRecords;

import java.util.Map;
import java.util.function.Supplier;

public class TestLoggerDestinationInteractive implements LoggerDestination {

  @Override
  public void logConsumerErrorInvoking(Throwable throwable, String invokingInfo, Profile profile) {
    System.out.println("********************* logConsumerErrorInvoking");
  }

  @Override
  public void logProducerConfigOnCreating(String producerName, Map<String, Object> configMap) {
    //System.out.println("********************* logProducerConfigOnCreating");
  }

  @Override
  public void logProducerClosed(String producerName, Profile profile) {
    System.out.println("********************* logProducerClosed");
  }

  @Override
  public void logConsumerStartWorker(ConsumerDefinition consumerDefinition, long workerId, Profile workerProfile) {
    System.out.println("********************* logConsumerStartWorker " + consumerDefinition.logDisplay());
  }

  @Override
  public void logConsumerFinishWorker(ConsumerDefinition consumerDefinition, long workerId, Profile workerProfile) {
    System.out.println("********************* logConsumerFinishWorker :: " + consumerDefinition.logDisplay());
  }

  @Override
  public void logConsumerWorkerConfig(ConsumerDefinition consumerDefinition,
                                      long workerId,
                                      Map<String, Object> configMap, Profile workerProfile) {
    System.out.println(
      "********************* logConsumerWorkerConfig :: " + consumerDefinition.logDisplay() + " :: " + configMap);
  }

  @Override
  public void logConsumerIllegalAccessExceptionInvoking(IllegalAccessException e, String invokingInfo) {
    System.out.println("********************* logConsumerIllegalAccessExceptionInvoking");
  }

  @Override
  public void debug(Supplier<String> message) {
    System.out.println("********************* logInfo " + message.get());
  }

  @Override
  public void logConsumerReactorRefresh(ConsumerDefinition consumerDefinition, int currentCount, int needCount, Profile profile) {
    System.out.println(
      "********************* logConsumerReactorRefresh :" + " currentCount = " + currentCount + ", workerCount = " + needCount);
  }

  @Override
  public void logProducerDirectConsumerError(String lineCode, Exception error, ConsumerDefinition consumerDefinition) {
    System.out.println(
      "********************* " + lineCode + " :: logProducerDirectConsumerError : " + consumerDefinition.logDisplay());
  }

  @Override
  public void logConsumerPollExceptionHappened(RuntimeException exception, ConsumerDefinition consumerDefinition, Profile workerProfile) {
    System.out.println("********************* logConsumerPollExceptionHappened");
  }

  @Override
  public void logConsumerCommitSyncExceptionHappened(RuntimeException exception,
                                                     ConsumerDefinition consumerDefinition) {
    System.out.println("********************* logConsumerCommitSyncExceptionHappened");
  }

  @Override
  public void logProducerCreated(String producerName, Profile profile) {
    //System.out.println("********************* logProducerCreated");
  }

  @Override
  public void logProducerValidationError(Throwable error) {
    System.out.println("********************* logProducerValidationError");
  }

  @Override
  public void logProducerAwaitAndGetError(String errorCode, Exception exception) {
    System.out.println("********************* logProducerAwaitAndGetError");
  }

  @Override
  public void logConsumerLogDirectInvokeError(Throwable error) {
    System.out.println("********************* logConsumerLogDirectInvokeError");
  }

  @Override
  public void logConsumerPollZero(ConsumerDefinition consumerDefinition) {
    //System.out.println("********************* logConsumerPollZero");
  }

  @Override
  public void logConsumerPollPerformDelay(ConsumerDefinition consumerDefinition,
                                          long pollPerformDelayMs,
                                          ConsumerRecords<byte[], Object> records) {

    System.out.println("********************* logConsumerPollPerformDelay");
  }

  @Override
  public void logErrorRefreshTopicList(Exception error) {
    //noinspection CallToPrintStackTrace
    error.printStackTrace();
  }
}
