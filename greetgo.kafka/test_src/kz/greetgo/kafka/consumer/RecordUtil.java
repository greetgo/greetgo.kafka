package kz.greetgo.kafka.consumer;

import kz.greetgo.kafka.model.Box;
import lombok.NonNull;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.header.internals.RecordHeaders;
import org.apache.kafka.common.record.TimestampType;

import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.util.stream.Collectors.groupingBy;

public class RecordUtil {
  public static ConsumerRecord<byte[], Object> recordOf(String topic, byte[] key, Box box) {
    return new ConsumerRecord<>(topic, 1, 1, key, box);
  }

  public static <T> ConsumerRecords<byte[], T> recordsOf(@NonNull List<ConsumerRecord<byte[], T>> list) {
    return new ConsumerRecords<>(
      list.stream()
          .collect(groupingBy(rec -> new TopicPartition(rec.topic(), rec.partition())))
    );
  }

  public static ConsumerRecord<byte[], Object> recordWithTimestamp(String topic, Date timestamp, Box box) {
    //noinspection deprecation
    return new ConsumerRecord<>(topic, 1, 1, timestamp.getTime(), TimestampType.CREATE_TIME, 0, 1, 1, new byte[0], box);
  }

  public static ConsumerRecord<byte[], Object> recordWithPartition(String topic, int partition, Box box) {
    return new ConsumerRecord<>(topic, partition, 1, new byte[0], box);
  }

  public static ConsumerRecord<byte[], Object> recordWithHeaders(String topic, Box box, Map<String, String> headers) {
    return new ConsumerRecord<>(topic, 1, 1,
                                ConsumerRecord.NO_TIMESTAMP,
                                TimestampType.NO_TIMESTAMP_TYPE,
                                ConsumerRecord.NULL_SIZE,
                                ConsumerRecord.NULL_SIZE,
                                new byte[0], box,
                                headersFromMap(headers),
                                Optional.empty());
  }

  public static RecordHeaders headersFromMap(Map<String, String> headers) {
    final RecordHeaders recHeaders = new RecordHeaders();

    if (headers != null) {
      for (final Map.Entry<String, String> e : headers.entrySet()) {
        recHeaders.add(e.getKey(), e.getValue().getBytes(StandardCharsets.UTF_8));
      }
    }
    return recHeaders;
  }

  public static ConsumerRecord<byte[], Object> recordWithOffset(String topic, long offset, Box box) {
    return new ConsumerRecord<>(topic, 1, offset, new byte[0], box);
  }
}
