package kz.greetgo.kafka.util;

import kz.greetgo.kafka.core.config.EventConfigFile;
import kz.greetgo.kafka.core.config.EventConfigFileFactory;

import java.util.HashMap;
import java.util.Map;

import static java.util.Objects.requireNonNull;

public class TestEventConfigFileFactory implements EventConfigFileFactory {

  public final Map<String, EventConfigFile> pathMap = new HashMap<>();

  @Override
  public EventConfigFile createEventConfigFile(String path) {
    EventConfigFile ret = pathMap.get(path);
    requireNonNull(ret);
    return ret;
  }

  @Override
  public EventConfigFileFactory cd(String subPath) {
    throw new RuntimeException("juWF3yTPe5 :: this method is no working");
  }

  public boolean isClosed = false;

  @Override
  public void close() {
    isClosed = true;
  }
}
