package kz.greetgo.kafka.util;

import org.testng.annotations.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class LineUtilTest extends LineUtilTest_Parent {

  @Test
  public void calcDelta__hasDelta() {
    List<String> topLines = listOf("# title line 1",
                                   "# title line 2",
                                   "",
                                   "",
                                   "# comment 1",
                                   "key1=value1",
                                   "",
                                   "# comment 2 1",
                                   "# comment 2 2",
                                   "# comment 2 3",
                                   "key2=value2",
                                   "key3=value2",
                                   "key4=value2",
                                   "key5=value2",
                                   "",
                                   "# asd asd",
                                   "# dsa dsa",
                                   "# wow wow",
                                   "key6=value2"
    );

    List<String> lines = listOf("",
                                "key2=value1",
                                "key3=value1",
                                "key4=value1",
                                "key5=value2");

    //
    //
    List<String> result = LineUtil.calcDelta(topLines, lines);
    //
    //

    assert result != null;
    assertThat(result).isNotNull();

    for (final String line : result) {
      System.out.println("W31ko7DacS :: line " + line);
    }

    assertThat(result.get(0)).isEqualTo("# comment 1");
    assertThat(result.get(1)).isEqualTo("key1=value1");
    assertThat(result.get(2)).isEqualTo("");
    assertThat(result.get(3)).isEqualTo("# asd asd");
    assertThat(result.get(4)).isEqualTo("# dsa dsa");
    assertThat(result.get(5)).isEqualTo("# wow wow");
    assertThat(result.get(6)).isEqualTo("key6=value2");

  }

  @Test
  public void calcDelta__noDelta() {
    List<String> topLines = listOf("# title line 1",
                                   "# title line 2",
                                   "",
                                   "",
                                   "# comment 1",
                                   "key1=value1",
                                   "",
                                   "# comment 2 1",
                                   "# comment 2 2",
                                   "# comment 2 3",
                                   "key2=value2",
                                   "key3=value2",
                                   "key4=value2",
                                   "key5=value2",
                                   "",
                                   "# asd asd",
                                   "# dsa dsa",
                                   "# wow wow",
                                   "key6=value2"
    );

    List<String> lines = listOf("",
                                "key2=value1",
                                "key3=value1",
                                "key1=value1",
                                "key6=value1",
                                "key4=value1",
                                "key5=value2");

    //
    //
    List<String> result = LineUtil.calcDelta(topLines, lines);
    //
    //

    assertThat(result).isNull();

  }

  @Test
  public void calcDelta__doubleValue() {
    List<String> topLines = listOf("# title line 1",
                                   "# title line 2",
                                   "",
                                   "",
                                   "# comment 1 1",
                                   "# comment 1 2",
                                   " key1=   value1  =  value2 = value3",
                                   "",
                                   "# comment 2 1",
                                   "# comment 2 2",
                                   "# comment 2 3",
                                   "key2=value2",
                                   "key3=value2",
                                   "key4=value2",
                                   "key5=value2",
                                   "# comment wow",
                                   "key1=value200",
                                   "",
                                   " # asd asd key6 ",
                                   " # dsa dsa key6  ",
                                   " # wow wow key6   ",
                                   "  key6 =  value2  ",
                                   "key7=wow",
                                   "key6=left",
                                   "key7=wow"
    );

    List<String> lines = listOf("",
                                "key2    =value1 = wer",
                                "    key3=value1 === 444",
                                " key7   =value1",
                                "key4=value1",
                                " key5  =value2");

    //
    //
    List<String> result = LineUtil.calcDelta(topLines, lines);
    //
    //

    assert result != null;
    assertThat(result).isNotNull();

    for (int i = 0; i < result.size(); i++) {
      final String line = result.get(i);
      System.out.println("assertThat(result.get(" + i + ")).isEqualTo(\"" + line + "\");");
    }

    assertThat(result.get(0)).isEqualTo("# comment 1 1");
    assertThat(result.get(1)).isEqualTo("# comment 1 2");
    assertThat(result.get(2)).isEqualTo(" key1=   value1  =  value2 = value3");
    assertThat(result.get(3)).isEqualTo("");
    assertThat(result.get(4)).isEqualTo(" # asd asd key6 ");
    assertThat(result.get(5)).isEqualTo(" # dsa dsa key6  ");
    assertThat(result.get(6)).isEqualTo(" # wow wow key6   ");
    assertThat(result.get(7)).isEqualTo("  key6 =  value2  ");

  }
}
