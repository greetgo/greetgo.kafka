package kz.greetgo.kafka.util;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public abstract class LineUtilTest_Parent {

  protected List<String> listOf(String ...textWithLines) {
    return Arrays.stream(textWithLines).collect(Collectors.toList());
  }

}
