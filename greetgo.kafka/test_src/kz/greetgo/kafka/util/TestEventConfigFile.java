package kz.greetgo.kafka.util;

import kz.greetgo.kafka.core.config.ConfigEventType;
import kz.greetgo.kafka.core.config.EventConfigFile;
import kz.greetgo.kafka.core.config.EventFileHandler;
import kz.greetgo.kafka.core.config.EventRegistration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.stream.Collectors.toList;

public class TestEventConfigFile implements EventConfigFile {

  private final ConcurrentHashMap<Long, EventFileHandler> map = new ConcurrentHashMap<>();

  private final AtomicLong nextId = new AtomicLong(1);
  boolean ensureLookingWorking = false;
  public  Date         createdAt      = null;
  public  Date         lastModifiedAt = null;
  private List<String> content        = null;

  public int createdAt_callCount      = 0;
  public int lastModifiedAt_callCount = 0;
  public int readContent_callCount    = 0;
  public int writeContent_callCount   = 0;

  public int allCountSum() {
    return createdAt_callCount + lastModifiedAt_callCount + readContent_callCount + writeContent_callCount;
  }

  @SuppressWarnings("StringBufferReplaceableByString")
  public String allCounts() {
    StringBuilder sb = new StringBuilder();
    sb.append("\n   createdAt_callCount      = ").append(createdAt_callCount);
    sb.append("\n   lastModifiedAt_callCount = ").append(lastModifiedAt_callCount);
    sb.append("\n   readContent_callCount    = ").append(readContent_callCount);
    sb.append("\n   writeContent_callCount   = ").append(writeContent_callCount);
    sb.append("\n");
    return sb.toString();
  }

  @Override
  public EventRegistration addEventHandler(EventFileHandler eventFileHandler) {
    final long id = nextId.getAndIncrement();
    map.put(id, eventFileHandler);
    return () -> map.remove(id);
  }

  public void fireEvent(ConfigEventType type) {
    map.values().forEach(x -> x.eventHappened(type));
  }

  @Override
  public void ensureLookingFor() {
    if (content != null) {
      ensureLookingWorking = true;
    }
  }

  @Override
  public Optional<Date> createdAt() {
    createdAt_callCount++;
    return Optional.ofNullable(createdAt);
  }

  @Override
  public Optional<Date> lastModifiedAt() {
    lastModifiedAt_callCount++;
    return Optional.ofNullable(lastModifiedAt);
  }

  @Override
  public byte[] readContent() {
    readContent_callCount++;
    if (content == null) {
      return null;
    }
    return String.join("\n", content).getBytes(UTF_8);
  }

  @Override
  public void writeContent(byte[] content) {
    writeContent_callCount++;
    if (content == null) {
      this.content         = null;
      createdAt            = null;
      lastModifiedAt       = null;
      ensureLookingWorking = false;
      return;
    }
    Date now = new Date();
    if (this.content == null) {
      createdAt = now;
    }
    this.content   = bytesToList(content);
    lastModifiedAt = now;
  }

  private static List<String> bytesToList(byte[] content) {
    if (content == null) {
      return null;
    }

    return Arrays.stream(new String(content, UTF_8).split("\n")).collect(toList());
  }

  public List<String> readLines() {
    List<String> content = this.content;

    if (content == null) {
      return null;
    }

    return new ArrayList<>(content);
  }

  public List<String> readLinesWithoutSpaces() {
    List<String> content = this.content;

    if (content == null) {
      return null;
    }

    return content
      .stream()
      .map(s -> s.replaceAll("\\s+", ""))
      .collect(toList());

  }

  private boolean isClosed = false;

  @Override
  public void close() {
    map.clear();
    isClosed = true;
  }

  public void addLines(String... lines) {
    if (content != null) {
      content.addAll(Arrays.asList(lines));
      return;
    }
    content = new ArrayList<>(Arrays.asList(lines));
  }

  public void delLines(String... lines) {
    List<String> content = this.content;
    if (content == null) {
      return;
    }

    for (String line : lines) {
      content.remove(line);
    }

  }

  public void clearCounts() {
    createdAt_callCount      = 0;
    lastModifiedAt_callCount = 0;
    readContent_callCount    = 0;
    writeContent_callCount   = 0;
  }

  public boolean isClosed() {
    return isClosed;
  }

  public void replaceLine(String lineFrom, String lineTo) {
    for (int i = 0; i < content.size(); i++) {
      final String line = content.get(i);
      if (line.equals(lineFrom)) {
        content.set(i, lineTo);
        return;
      }
    }
    throw new RuntimeException("ha502hx1OH :: No line [" + lineFrom + "]");
  }
}
