package kz.greetgo.kafka.util;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class GroupIdUtilTest {

  @Test
  public void evalGroupId__simpleValue() {

    //
    //
    var result = GroupIdUtil.evalGroupId("Привет мир hello world", null);
    //
    //

    assertThat(result).isEqualTo("Привет мир hello world");
  }

  @Test
  public void evalGroupId__callMethod__inText() {

    class Controller {
      @SuppressWarnings("unused")
      public String someMethod() {
        return "SomeReturn";
      }
    }

    var controller = new Controller();

    //
    //
    var result = GroupIdUtil.evalGroupId("до-{ someMethod() }-после", controller);
    //
    //

    assertThat(result).isEqualTo("до-SomeReturn-после");
  }


  @Test
  public void evalGroupId__callMethod__manyTimes() {

    class Controller {
      int i = 1;

      @SuppressWarnings("unused")
      public String someMethod1() {
        return "SomeReturn1-" + i++;
      }

      @SuppressWarnings("unused")
      public String someMethod2() {
        return "SomeReturn2-" + i++;
      }
    }

    var controller = new Controller();

    //
    //
    var result = GroupIdUtil.evalGroupId("A { someMethod1() } B { someMethod2 () } C {someMethod1()} D", controller);
    //
    //

    assertThat(result).isEqualTo("A SomeReturn1-1 B SomeReturn2-2 C SomeReturn1-3 D");
  }

  @Test
  public void evalGroupId__callMethod__onlyText() {

    class Controller {
      @SuppressWarnings("unused")
      public String someMethod13() {
        return "WorldTime";
      }
    }

    var controller = new Controller();

    //
    //
    var result = GroupIdUtil.evalGroupId("{someMethod13()}", controller);
    //
    //

    assertThat(result).isEqualTo("WorldTime");
  }

  @Test
  public void evalGroupId__callMethod__leftPlaceInText() {

    class Controller {
      @SuppressWarnings("unused")
      public String someMethod7() {
        return "StatusQuo";
      }
    }

    var controller = new Controller();

    //
    //
    var result = GroupIdUtil.evalGroupId("{someMethod7()}-of-the-world", controller);
    //
    //

    assertThat(result).isEqualTo("StatusQuo-of-the-world");
  }

  @Test
  public void evalGroupId__callMethod__rightPlaceInText() {

    class Controller {
      @SuppressWarnings("unused")
      public String rolling() {
        return "Stone";
      }
    }

    var controller = new Controller();

    //
    //
    var result = GroupIdUtil.evalGroupId("Told to {rolling()}", controller);
    //
    //

    assertThat(result).isEqualTo("Told to Stone");
  }

  @Test
  public void evalGroupId__field() {

    class Controller {
      public String hiField;
    }

    var controller = new Controller();
    controller.hiField = "Sinus";

    //
    //
    var result = GroupIdUtil.evalGroupId("Math {  hiField  } i", controller);
    //
    //

    assertThat(result).isEqualTo("Math Sinus i");
  }

  @Test
  public void evalGroupId__intMethod() {

    class Controller {
      @SuppressWarnings("unused")
      public int intMethod() {
        return 7896;
      }
    }

    var controller = new Controller();

    //
    //
    var result = GroupIdUtil.evalGroupId("Math {  intMethod()  } int", controller);
    //
    //

    assertThat(result).isEqualTo("Math 7896 int");
  }

  @Test
  public void evalGroupId__intField() {

    class Controller {
      public int intField;
    }

    var controller = new Controller();
    controller.intField = 19303;

    //
    //
    var result = GroupIdUtil.evalGroupId("Math {  intField  } int", controller);
    //
    //

    assertThat(result).isEqualTo("Math 19303 int");
  }

}
