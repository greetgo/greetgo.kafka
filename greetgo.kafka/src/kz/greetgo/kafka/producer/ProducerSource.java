package kz.greetgo.kafka.producer;

import kz.greetgo.kafka.consumer.Profile;
import kz.greetgo.kafka.core.logger.Logger;
import kz.greetgo.kafka.util.Handler;
import kz.greetgo.kafka.util.Listener;
import kz.greetgo.strconverter.StrConverter;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.common.serialization.ByteArraySerializer;
import org.apache.kafka.common.serialization.Serializer;

import javax.annotation.Nonnull;
import java.util.Map;

public interface ProducerSource {

  StrConverter getStrConverter();

  Logger logger();

  byte[] extractKey(Object object);

  String author();

  Map<String, String> getConfigFor(String producerName, @Nonnull Profile profile);

  Listener addConfigListener(String producerName, @Nonnull Profile profile, Handler handler);

  <ValueType> Producer<byte[], ValueType> createProducer(String producerName,
                                                         @Nonnull Profile profile,
                                                         ByteArraySerializer keySerializer,
                                                         Serializer<ValueType> valueSerializer);

}
