package kz.greetgo.kafka.producer;

public interface KafkaSending {

  KafkaSending toTopic(String topic);

  KafkaSending toPartition(int partition);

  KafkaSending setTimestamp(Long timestamp);

  KafkaSending setAuthor(String author);

  KafkaSending addHeader(String key, byte[] value);

  KafkaSending addHeader(String key, String value);

  KafkaSending withKey(String keyAsString);

  KafkaSending withKey(byte[] keyAsBytes);

  KafkaSending kafkaId(String kafkaId);

  KafkaSending profile(String profile);

  KafkaFuture go();

  void goWithPortion();

}
