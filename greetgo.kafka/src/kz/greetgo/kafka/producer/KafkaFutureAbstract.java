package kz.greetgo.kafka.producer;

import kz.greetgo.kafka.errors.future.ExecutionExceptionWrapper;
import kz.greetgo.kafka.errors.future.InterruptedExceptionWrapper;
import kz.greetgo.kafka.errors.future.TimeoutExceptionWrapper;
import org.apache.kafka.clients.producer.RecordMetadata;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public abstract class KafkaFutureAbstract implements KafkaFuture {
  protected final Future<RecordMetadata> source;

  protected KafkaFutureAbstract(Future<RecordMetadata> source) {
    this.source = source;
  }

  @Override
  public boolean cancel(boolean mayInterruptIfRunning) {
    return source.cancel(mayInterruptIfRunning);
  }

  @Override
  public boolean isCancelled() {
    return source.isCancelled();
  }

  @Override
  public boolean isDone() {
    return source.isDone();
  }

  @Override
  public RecordMetadata awaitAndGet() {
    return awaitAndGet(true);
  }

  protected abstract void intercept(boolean applyInterceptors);

  @Override
  public RecordMetadata awaitAndGet(boolean applyInterceptors) {
    try {
      var recordMetadata = source.get();
      intercept(applyInterceptors);
      return recordMetadata;
    } catch (InterruptedException e) {
      throw new InterruptedExceptionWrapper(e);
    } catch (ExecutionException e) {
      throw new ExecutionExceptionWrapper(e);
    }
  }

  @Override
  public RecordMetadata get(long timeout, TimeUnit unit) {
    try {
      var recordMetadata = source.get(timeout, unit);
      intercept(true);
      return recordMetadata;
    } catch (InterruptedException e) {
      throw new InterruptedExceptionWrapper(e);
    } catch (ExecutionException e) {
      throw new ExecutionExceptionWrapper(e);
    } catch (TimeoutException e) {
      throw new TimeoutExceptionWrapper(e);
    }
  }
}
