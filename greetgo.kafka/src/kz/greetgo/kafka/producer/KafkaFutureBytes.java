package kz.greetgo.kafka.producer;

import kz.greetgo.kafka.core.ProducerBytesInterceptor;
import org.apache.kafka.clients.producer.RecordMetadata;

import java.util.concurrent.Future;

public class KafkaFutureBytes extends KafkaFutureAbstract {

  private final RecordBytes              record;
  private final ProducerBytesInterceptor interceptor;

  public KafkaFutureBytes(Future<RecordMetadata> source, RecordBytes record, ProducerBytesInterceptor interceptor) {
    super(source);
    this.record      = record;
    this.interceptor = interceptor;
  }

  @Override
  protected void intercept(boolean applyInterceptors) {
    if (applyInterceptors && interceptor != null) {
      interceptor.afterKafkaSent(record);
    }
  }

}
