package kz.greetgo.kafka.producer.config;

import kz.greetgo.kafka.consumer.Profile;
import kz.greetgo.kafka.core.config.EventConfigFileFactory;
import kz.greetgo.kafka.producer.ProducerConfigDefaultsAccess;

import javax.annotation.Nonnull;
import java.util.HashSet;
import java.util.concurrent.ConcurrentHashMap;

import static java.util.Objects.requireNonNull;

public class ProducerReactorConfigFactory implements AutoCloseable {
  private final EventConfigFileFactory       configFactory;
  private final ProducerConfigDefaultsAccess defaults;
  private final String                       configFileExtension;

  private final ConcurrentHashMap<String, ProducerReactorConfig> configMap = new ConcurrentHashMap<>();
  private final Object                                           sync      = new Object();

  public static class Builder {
    private EventConfigFileFactory       configFactory;
    private ProducerConfigDefaultsAccess defaults;
    private String                       configFileExtension = ".config";

    public Builder configFactory(EventConfigFileFactory configFactory) {
      this.configFactory = requireNonNull(configFactory, "H3K4sx1JSe :: configFactory == null");
      return this;
    }

    public Builder defaults(ProducerConfigDefaultsAccess defaults) {
      this.defaults = requireNonNull(defaults, "O0tJkQ1w6m :: defaults == null");
      return this;
    }

    public Builder configFileExtension(String configFileExtension) {
      this.configFileExtension = requireNonNull(configFileExtension, "W0DHu37rzC :: configFileExtension = null");
      return this;
    }

    public ProducerReactorConfigFactory build() {
      return new ProducerReactorConfigFactory(requireNonNull(configFactory),
                                              requireNonNull(defaults),
                                              requireNonNull(configFileExtension));
    }
  }

  public static Builder builder() {
    return new Builder();
  }

  private ProducerReactorConfigFactory(EventConfigFileFactory configFactory,
                                       ProducerConfigDefaultsAccess defaults,
                                       String configFileExtension) {
    this.configFactory       = configFactory;
    this.defaults            = defaults;
    this.configFileExtension = configFileExtension;
  }


  public ProducerReactorConfig getConfig(String producerName, @Nonnull Profile profile) {

    String fileName = producerName + (profile.profile == null ? "" : "_" + profile.profile) + configFileExtension;

    {
      var x = configMap.get(fileName);
      if (x != null) {
        return x;
      }
    }

    synchronized (sync) {
      {
        var x = configMap.get(fileName);
        if (x != null) {
          return x;
        }
      }

      {
        var eventConfigFile = configFactory.createEventConfigFile(fileName);
        var reactorConfig   = new ProducerReactorConfig(eventConfigFile, () -> defaults.get(profile));
        configMap.put(fileName, reactorConfig);
        return reactorConfig;
      }
    }

  }

  @Override
  public void close() throws Exception {
    while (configMap.size() > 0) {
      for (final String key : new HashSet<>(configMap.keySet())) {
        var config = configMap.remove(key);
        if (config != null) {
          config.close();
        }
      }
    }
  }

}
