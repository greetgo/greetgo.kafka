package kz.greetgo.kafka.producer.config;

import java.util.Map;

/**
 * Касс этого интерфейса определяет параметры доступа к кластеру кафки в зависимости от параметров поставщика
 */
public interface ProducerConnectSetter {

  /**
   * Метод, который должен заполнить параметры доступа к кластеру кафки
   *
   * @param targetConfigMap колода, куда нужно отправлять параметры доступа к кластеру кафки
   * @param params          параметры поставщика данных в кафку
   */
  void setConnect(Map<String, Object> targetConfigMap, ProducerConnectParams params);

}
