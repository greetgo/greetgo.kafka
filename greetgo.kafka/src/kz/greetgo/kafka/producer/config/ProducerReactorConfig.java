package kz.greetgo.kafka.producer.config;

import kz.greetgo.kafka.consumer.config.AbstractTextLinesConfig;
import kz.greetgo.kafka.core.config.EventConfigFile;
import kz.greetgo.kafka.util.KeyValue;

import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

public class ProducerReactorConfig extends AbstractTextLinesConfig implements AutoCloseable {

  public static final String CONFIG_MAP_PREFIX = "prod.";

  private final Supplier<ProducerConfigDefaults> defaults;

  public ProducerReactorConfig(EventConfigFile configFile,
                               Supplier<ProducerConfigDefaults> defaults) {
    super(configFile);
    this.defaults = defaults;
  }

  public Map<String, String> getConfigMap() {
    return readData().entrySet()
                     .stream()
                     .filter(x -> x.getKey().startsWith(CONFIG_MAP_PREFIX))
                     .map(x -> KeyValue.of(x.getKey().substring(CONFIG_MAP_PREFIX.length()), x.getValue()))
                     .filter(x -> x.key().trim().length() > 0)
                     .collect(KeyValue.toMap());
  }

  @Override
  protected List<String> createDefaultLines() {
    return defaults.get().toConfigFileLines();
  }

}
