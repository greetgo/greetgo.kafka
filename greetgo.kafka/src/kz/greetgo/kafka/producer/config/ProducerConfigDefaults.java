package kz.greetgo.kafka.producer.config;

import kz.greetgo.kafka.util.KeyValue;

import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class ProducerConfigDefaults {

  private final List<KeyValue> lineList = new ArrayList<>();

  public void append(String key, String value) {
    lineList.add(KeyValue.of(key, value));
  }

  public void append(String definition) {
    var idx = definition.indexOf('=');
    if (idx < 0) {
      throw new RuntimeException("6UAkQ60bxb :: Unknown definition " + definition);
    }
    append(definition.substring(0, idx).trim(), definition.substring(idx + 1).trim());
  }

  private ProducerConfigDefaults() {}

  public static ProducerConfigDefaults empty() {
    return new ProducerConfigDefaults();
  }

  public static ProducerConfigDefaults defaults() {
    var ret = empty();
    ret.appendDefaults();
    return ret;
  }

  public void appendDefaults() {
    //noinspection SpellCheckingInspection
    append("  prod.acks                      =  all      ");
    append("  prod.buffer.memory             =  33554432 ");
    append("  prod.compression.type          =  lz4      ");
    append("  prod.batch.size                =  16384    ");
    append("  prod.connections.max.idle.ms   =  540000   ");
    append("  prod.request.timeout.ms        =  30000    ");
    append("  prod.linger.ms                 =  1        ");

    append("  prod.retries                                 =  2147483647    ");
    append("  prod.max.in.flight.requests.per.connection   =  1             ");
    append("  prod.delivery.timeout.ms                     =  35000         ");
    append("  prod.max.block.ms                            =  25000         ");
  }

  public List<String> toConfigFileLines() {
    return lineList.stream()
                   .map(line -> line.key() + " = " + line.value())
                   .collect(toList());
  }
}
