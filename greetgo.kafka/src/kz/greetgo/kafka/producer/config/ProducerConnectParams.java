package kz.greetgo.kafka.producer.config;

/**
 * Параметры для установки доступа к кафке для поставщика данных
 */
public interface ProducerConnectParams {

  /**
   * Имя поставщика
   */
  String producerName();

  String profile();
}
