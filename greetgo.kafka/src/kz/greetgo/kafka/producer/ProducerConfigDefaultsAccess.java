package kz.greetgo.kafka.producer;

import kz.greetgo.kafka.consumer.Profile;
import kz.greetgo.kafka.producer.config.ProducerConfigDefaults;

public interface ProducerConfigDefaultsAccess {
  ProducerConfigDefaults get(Profile profile);
}
