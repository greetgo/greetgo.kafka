package kz.greetgo.kafka.producer;

import org.apache.kafka.clients.producer.RecordMetadata;

import java.util.concurrent.TimeUnit;

public interface KafkaFuture {

  @SuppressWarnings("unused")
  boolean cancel(boolean mayInterruptIfRunning);

  @SuppressWarnings("unused")
  boolean isCancelled();

  @SuppressWarnings("unused")
  boolean isDone();

  RecordMetadata awaitAndGet();

  RecordMetadata awaitAndGet(boolean applyInterceptors);

  RecordMetadata get(long timeout, TimeUnit unit);

}
