package kz.greetgo.kafka.producer;

import kz.greetgo.kafka.core.ProducerStrInterceptor;
import org.apache.kafka.clients.producer.RecordMetadata;

import java.util.concurrent.Future;

public class KafkaFutureStr extends KafkaFutureAbstract {

  private final RecordStr              record;
  private final ProducerStrInterceptor strInterceptor;

  public KafkaFutureStr(Future<RecordMetadata> source, RecordStr record, ProducerStrInterceptor strInterceptor) {
    super(source);
    this.record         = record;
    this.strInterceptor = strInterceptor;
  }

  @Override
  protected void intercept(boolean applyInterceptors) {
    if (applyInterceptors && strInterceptor != null) {
      strInterceptor.afterKafkaSent(record);
    }
  }

}
