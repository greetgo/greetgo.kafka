package kz.greetgo.kafka.producer;

import kz.greetgo.kafka.consumer.Profile;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.header.Header;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Map;

public class RecordBytes {
  public final String       appTopic;
  public final String       prefixedTopic;
  public final Integer      partition;
  public final Long         timestamp;
  public final byte[]       key;
  public final byte[]       message;
  public final List<Header> headers;
  public final Profile      profile;

  public RecordBytes(String appTopic, String prefixedTopic,
                     Integer partition, Long timestamp, byte[] key,
                     byte[] message, List<Header> headers,
                     @Nonnull Profile profile) {
    this.appTopic      = appTopic;
    this.prefixedTopic = prefixedTopic;
    this.partition     = partition;
    this.timestamp     = timestamp;
    this.key           = key;
    this.message       = message;
    this.headers       = headers;
    this.profile       = profile;
  }

  public ProducerRecord<byte[], byte[]> toProducerRecord() {
    return new ProducerRecord<>(prefixedTopic, partition, timestamp, key, message, headers);
  }

  private int partition() {
    var x = partition;
    return x != null ? x : 0;
  }

  @SuppressWarnings("unused")
  public ConsumerRecords<byte[], byte[]> toConsumerRecords() {
    var topicPartition = new TopicPartition(prefixedTopic, partition());
    var consumerRecord = new ConsumerRecord<>(prefixedTopic, partition(), 0, key, message);
    return new ConsumerRecords<>(Map.of(topicPartition, List.of(consumerRecord)));
  }
}
