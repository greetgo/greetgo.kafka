package kz.greetgo.kafka.producer;

import kz.greetgo.kafka.core.BoxInterceptor;
import org.apache.kafka.clients.producer.RecordMetadata;

import java.util.concurrent.Future;

public class KafkaFutureBox extends KafkaFutureAbstract {

  private final BoxRecord      record;
  private final BoxInterceptor boxInterceptor;

  public KafkaFutureBox(Future<RecordMetadata> source, BoxRecord record, BoxInterceptor boxInterceptor) {
    super(source);
    this.record         = record;
    this.boxInterceptor = boxInterceptor;
  }

  @Override
  protected void intercept(boolean applyInterceptors) {
    if (applyInterceptors && boxInterceptor != null) {
      boxInterceptor.afterKafkaSent(record);
    }
  }

}
