package kz.greetgo.kafka.producer;

import kz.greetgo.kafka.consumer.Profile;
import kz.greetgo.kafka.core.ProducerBytesInterceptor;
import kz.greetgo.kafka.core.ProducerSynchronizer;
import kz.greetgo.kafka.serializer.BytesSerializer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.header.Header;
import org.apache.kafka.common.serialization.Serializer;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.concurrent.Future;
import java.util.function.Supplier;

public class ProducerFacadeBytes extends ProducerFacadeAbstract<byte[]> {

  private final ProducerBytesInterceptor interceptor;

  private ProducerFacadeBytes(String producerName,
                              ProducerSource source,
                              ProducerSynchronizer producerSynchronizer,
                              Supplier<String> topicPrefix,
                              ProducerBytesInterceptor interceptor) {

    super(source, producerName, producerSynchronizer, topicPrefix);
    this.interceptor = interceptor;
  }

  public static @Nonnull ProducerFacadeBytes create(String producerName,
                                                    ProducerSource source,
                                                    ProducerSynchronizer producerSynchronizer,
                                                    Supplier<String> topicPrefix,
                                                    ProducerBytesInterceptor interceptor) {

    return new ProducerFacadeBytes(producerName, source, producerSynchronizer, topicPrefix, interceptor);
  }

  @Override
  protected Serializer<byte[]> createValueSerializer() {
    return new BytesSerializer();
  }

  @Override
  protected KafkaFuture doSend(@Nonnull byte[] message,
                               String toTopic,
                               byte[] withKey,
                               Integer toPartition,
                               Long timestamp,
                               String kafkaId,
                               @Nonnull ArrayList<Header> headers,
                               @Nonnull Profile profile) {
    if (toTopic == null) {
      throw new RuntimeException("LKl6xH3Vh5 :: topic == null");
    }

    byte[] key = withKey != null ? withKey : source.extractKey(message);

    var record = new RecordBytes(toTopic, doPrefixTopic(toTopic), toPartition, timestamp, key, message, headers, profile);

    final Producer<byte[], byte[]> nativeProducer = getNativeProducer(profile);
    final Future<RecordMetadata>   future         = nativeProducer.send(record.toProducerRecord());

    return new KafkaFutureBytes(future, record, interceptor);
  }

}
