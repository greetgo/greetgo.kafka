package kz.greetgo.kafka.producer;

import kz.greetgo.kafka.consumer.Profile;
import kz.greetgo.kafka.core.BoxInterceptor;
import kz.greetgo.kafka.core.ProducerSynchronizer;
import kz.greetgo.kafka.core.logger.LoggerType;
import kz.greetgo.kafka.model.Box;
import kz.greetgo.kafka.serializer.BoxSerializer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.header.Header;
import org.apache.kafka.common.serialization.Serializer;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.concurrent.Future;
import java.util.function.Supplier;

public class ProducerFacadeBox extends ProducerFacadeAbstract<Object> {

  private final BoxInterceptor boxInterceptor;

  private ProducerFacadeBox(String producerName,
                            ProducerSource source,
                            ProducerSynchronizer producerSynchronizer,
                            Supplier<String> topicPrefix,
                            BoxInterceptor boxInterceptor) {

    super(source, producerName, producerSynchronizer, topicPrefix);
    this.boxInterceptor = boxInterceptor;
  }

  public static @Nonnull ProducerFacadeBox create(String producerName,
                                                  ProducerSource source,
                                                  ProducerSynchronizer producerSynchronizer,
                                                  Supplier<String> topicPrefix,
                                                  BoxInterceptor boxInterceptor) {

    return new ProducerFacadeBox(producerName, source, producerSynchronizer, topicPrefix, boxInterceptor);
  }

  @Override
  protected Serializer<Object> createValueSerializer() {
    return new BoxSerializer(source.getStrConverter());
  }

  @Override
  protected KafkaFuture doSend(@Nonnull Object message,
                               String toTopic,
                               byte[] withKey,
                               Integer toPartition,
                               Long timestamp,
                               String kafkaId,
                               @Nonnull ArrayList<Header> headers,
                               @Nonnull Profile profile) {

    if (toTopic == null) {
      throw new RuntimeException("nSzM90vC3V :: topic == null");
    }

    Box box = new Box();
    box.body = message;
    box.id   = kafkaId;

    try {
      box.validate();
    } catch (Throwable throwable) {

      if (source.logger().isShow(LoggerType.LOG_PRODUCER_VALIDATION_ERROR)) {
        source.logger().logProducerValidationError(throwable);
      }

      if (throwable instanceof RuntimeException) {
        throw (RuntimeException) throwable;
      }
      throw new RuntimeException(throwable);

    }

    final byte[] key = withKey != null ? withKey : source.extractKey(message);

    final BoxRecord record = new BoxRecord(toTopic, doPrefixTopic(toTopic), toPartition, timestamp, key, box, headers, profile);

    final Producer<byte[], Object> nativeProducer = getNativeProducer(profile);
    final Future<RecordMetadata>   future         = nativeProducer.send(record.toProducerRecord());

    return new KafkaFutureBox(future, record, boxInterceptor);
  }

}
