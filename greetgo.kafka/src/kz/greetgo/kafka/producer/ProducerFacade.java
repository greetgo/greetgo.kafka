package kz.greetgo.kafka.producer;

import kz.greetgo.kafka.consumer.Profile;
import org.apache.kafka.clients.producer.Producer;

import java.util.Map;

public interface ProducerFacade<RecordType> extends AutoCloseable {

  String name();

  void reset();

  Producer<byte[], RecordType> getNativeProducer(Profile profile);

  @SuppressWarnings("unused")
  Map<String, String> getConfigData(Profile profile);

  KafkaSending sending(RecordType body);

}
