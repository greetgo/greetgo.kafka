package kz.greetgo.kafka.producer;

import kz.greetgo.kafka.consumer.Profile;
import kz.greetgo.kafka.core.ProducerSynchronizer;
import kz.greetgo.kafka.core.logger.LoggerType;
import kz.greetgo.kafka.util.Listener;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.common.header.Header;
import org.apache.kafka.common.serialization.ByteArraySerializer;
import org.apache.kafka.common.serialization.Serializer;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Supplier;

import static java.nio.charset.StandardCharsets.UTF_8;

public abstract class ProducerFacadeAbstract<MessageType> implements ProducerFacade<MessageType> {

  protected final ProducerSource       source;
  protected final String               producerName;
  protected final ProducerSynchronizer producerSynchronizer;
  protected final Supplier<String>     topicPrefix;

  protected final ConcurrentHashMap<Profile, Listener>                      changeListeners   = new ConcurrentHashMap<>();
  protected final ConcurrentHashMap<Profile, Producer<byte[], MessageType>> producerByProfile = new ConcurrentHashMap<>();


  protected ProducerFacadeAbstract(ProducerSource source,
                                   String producerName,
                                   ProducerSynchronizer producerSynchronizer,
                                   Supplier<String> topicPrefix) {
    this.source               = source;
    this.producerName         = producerName;
    this.producerSynchronizer = producerSynchronizer;
    this.topicPrefix          = topicPrefix;
  }

  @Override
  public String name() {
    return producerName;
  }

  public void reset() {
    closeAllProducers();
  }

  private void closeAllProducers() {
    final ArrayList<Map.Entry<Profile, Producer<byte[], MessageType>>> entries = new ArrayList<>(producerByProfile.entrySet());

    for (final Map.Entry<Profile, Producer<byte[], MessageType>> e : entries) {

      final Profile profile  = e.getKey();
      final var     producer = e.getValue();

      //noinspection resource
      producerByProfile.remove(profile);
      producer.close();

      if (source.logger().isShow(LoggerType.LOG_CLOSE_PRODUCER)) {
        source.logger().logProducerClosed(producerName, profile);
      }
    }
  }

  @Override
  public void close() throws Exception {
    closeAllProducers();
    changeListeners.values().forEach(Listener::kill);
  }

  @Override
  public @Nonnull Producer<byte[], MessageType> getNativeProducer(@Nonnull Profile profile) {
    {
      final Producer<byte[], MessageType> p = producerByProfile.get(profile);
      if (p != null) {
        return p;
      }
    }

    synchronized (producerByProfile) {
      {
        final Producer<byte[], MessageType> p = producerByProfile.get(profile);
        if (p != null) {
          return p;
        }
      }
      {
        final Producer<byte[], MessageType> p = createProducer(profile);
        producerByProfile.put(profile, p);
        return p;
      }
    }
  }

  public Map<String, String> getConfigData(Profile profile) {
    return source.getConfigFor(producerName, profile);
  }

  protected abstract Serializer<MessageType> createValueSerializer();

  private @Nonnull Producer<byte[], MessageType> createProducer(@Nonnull Profile profile) {
    ByteArraySerializer     keySerializer   = new ByteArraySerializer();
    Serializer<MessageType> valueSerializer = createValueSerializer();

    Producer<byte[], MessageType> producer    = source.createProducer(producerName, profile, keySerializer, valueSerializer);
    final Listener                listener    = source.addConfigListener(producerName, profile, () -> closeProducer(profile));
    final Listener                oldListener = changeListeners.put(profile, listener);

    if (oldListener != null) {
      oldListener.kill();
    }

    if (source.logger().isShow(LoggerType.LOG_CREATE_PRODUCER)) {
      source.logger().logProducerCreated(producerName, profile);
    }

    return producer;
  }

  private void closeProducer(Profile profile) {
    final Producer<byte[], MessageType> producer = producerByProfile.remove(profile);
    if (producer != null) {
      producer.close();
    }
  }

  protected String doPrefixTopic(String topic) {
    var prefix = topicPrefix.get();
    return prefix == null ? topic : prefix + topic;
  }

  protected abstract KafkaFuture doSend(@Nonnull MessageType message,
                                        String toTopic,
                                        byte[] withKey,
                                        Integer toPartition,
                                        Long timestamp,
                                        String kafkaId,
                                        @Nonnull ArrayList<Header> headers,
                                        @Nonnull Profile profile);

  @Override
  public KafkaSending sending(MessageType message) {
    return new KafkaSending() {

      String toTopic = null;

      @Override
      public KafkaSending toTopic(String topic) {
        toTopic = topic;
        return this;
      }

      Integer toPartition = null;

      @Override
      public KafkaSending toPartition(int partition) {
        toPartition = partition;
        return this;
      }

      Long timestamp = null;

      @Override
      public KafkaSending setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
        return this;
      }

      final ArrayList<Header> headers = new ArrayList<>();

      @Override
      public KafkaSending addHeader(String key, byte[] value) {
        headers.add(new Header() {
          @Override
          public String key() {
            return key;
          }

          @Override
          public byte[] value() {
            return value;
          }
        });
        return this;
      }

      @Override
      public KafkaSending addHeader(String key, String value) {
        return addHeader(key, value == null ? new byte[0] : value.getBytes(UTF_8));
      }

      String author = source.author();

      @Override
      public KafkaSending setAuthor(String author) {
        this.author = author;
        return this;
      }

      byte[] withKey = null;

      @Override
      public KafkaSending withKey(String keyAsString) {
        return withKey(keyAsString.getBytes(UTF_8));
      }

      @Override
      public KafkaSending withKey(byte[] keyAsBytes) {
        withKey = keyAsBytes;
        return this;
      }

      String kafkaId = null;

      @Override
      public KafkaSending kafkaId(String kafkaId) {
        this.kafkaId = kafkaId;
        return this;
      }

      Profile profile = new Profile(null);

      @Override
      public KafkaSending profile(String profile) {
        this.profile = new Profile(profile);
        return this;
      }

      @Override
      public KafkaFuture go() {
        return doSend(message, toTopic, withKey, toPartition, timestamp, kafkaId, headers, profile);
      }

      @Override
      public void goWithPortion() {
        producerSynchronizer.acceptKafkaFuture(go());
      }

    };
  }
}
