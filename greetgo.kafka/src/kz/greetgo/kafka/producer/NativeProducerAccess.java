package kz.greetgo.kafka.producer;

import kz.greetgo.kafka.consumer.Profile;
import org.apache.kafka.clients.producer.Producer;

import javax.annotation.Nonnull;

public interface NativeProducerAccess {
  @Nonnull Producer<byte[], Object> get(@Nonnull Profile profile);
}
