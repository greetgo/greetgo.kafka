package kz.greetgo.kafka.producer;

import kz.greetgo.kafka.consumer.Profile;
import kz.greetgo.kafka.core.ProducerStrInterceptor;
import kz.greetgo.kafka.core.ProducerSynchronizer;
import kz.greetgo.kafka.serializer.StrSerializer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.header.Header;
import org.apache.kafka.common.serialization.Serializer;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.concurrent.Future;
import java.util.function.Supplier;

public class ProducerFacadeStr extends ProducerFacadeAbstract<String> {

  private final ProducerStrInterceptor strInterceptor;

  private ProducerFacadeStr(String producerName,
                            ProducerSource source,
                            ProducerSynchronizer producerSynchronizer,
                            Supplier<String> topicPrefix,
                            ProducerStrInterceptor strInterceptor) {

    super(source, producerName, producerSynchronizer, topicPrefix);
    this.strInterceptor = strInterceptor;
  }

  public static @Nonnull ProducerFacadeStr create(String producerName,
                                                  ProducerSource source,
                                                  ProducerSynchronizer producerSynchronizer,
                                                  Supplier<String> topicPrefix,
                                                  ProducerStrInterceptor strInterceptor) {

    return new ProducerFacadeStr(producerName, source, producerSynchronizer, topicPrefix, strInterceptor);
  }

  @Override
  protected Serializer<String> createValueSerializer() {
    return new StrSerializer();
  }

  @Override
  protected KafkaFuture doSend(@Nonnull String message,
                               String toTopic,
                               byte[] withKey,
                               Integer toPartition,
                               Long timestamp,
                               String kafkaId,
                               @Nonnull ArrayList<Header> headers,
                               @Nonnull Profile profile) {

    if (toTopic == null) {
      throw new RuntimeException("1I8z1gdBrY :: topic == null");
    }

    byte[] key = withKey != null ? withKey : source.extractKey(message);

    var record = new RecordStr(toTopic, doPrefixTopic(toTopic), toPartition, timestamp, key, message, headers, profile);

    final Producer<byte[], String> nativeProducer = getNativeProducer(profile);
    final Future<RecordMetadata>   future         = nativeProducer.send(record.toProducerRecord());

    return new KafkaFutureStr(future, record, strInterceptor);

  }


}
