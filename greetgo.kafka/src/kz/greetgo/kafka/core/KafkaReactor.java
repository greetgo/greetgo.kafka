package kz.greetgo.kafka.core;

import kz.greetgo.kafka.consumer.ConsumerDefinitionCustomBuilder;
import kz.greetgo.kafka.consumer.DynamicReactors;
import kz.greetgo.kafka.core.consumer_filter.ConsumerFilterRegistrar;
import kz.greetgo.kafka.core.logger.LoggerExternal;
import kz.greetgo.kafka.producer.ProducerFacade;

public interface KafkaReactor extends AutoCloseable {

  LoggerExternal logger();

  void startConsumers();

  @SuppressWarnings("unused")
  void activateDirectConsumers();

  void addConsumerDefinitionCustomBuilder(ConsumerDefinitionCustomBuilder consumerDefinitionCustomBuilder);

  @SuppressWarnings("unused")
  ProducerSynchronizer producerSynchronizer();

  ProducerFacade<Object> createProducer(String producerName);

  ProducerFacade<String> createProducerStr(String producerName);

  ProducerFacade<byte[]> createProducerBytes(String producerName);

  boolean isDirect();

  void join();

  @Override
  void close();

  ConsumerFilterRegistrar consumerFilterRegistrar();

  DynamicReactors dynamicConsumers();

  static KafkaReactorBuilder builder() {
    return new KafkaReactorBuilder();
  }

  static KafkaSimulatorBuilder simulatorBuilder() {
    return new KafkaSimulatorBuilder();
  }
}
