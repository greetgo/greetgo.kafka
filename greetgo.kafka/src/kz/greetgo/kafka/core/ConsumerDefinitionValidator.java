package kz.greetgo.kafka.core;

import kz.greetgo.kafka.consumer.ConsumerDefinition;

public interface ConsumerDefinitionValidator {

  void validate(ConsumerDefinition consumerDefinition);

}
