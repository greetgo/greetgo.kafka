package kz.greetgo.kafka.core;

import kz.greetgo.kafka.core.logger.Logger;
import kz.greetgo.kafka.producer.KafkaFuture;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class ProducerSynchronizer {
  private final Logger logger;

  public ProducerSynchronizer(Logger logger) {
    this.logger = logger;
  }

  private class ConsumerContext {
    void close() {
      try {
        for (final KafkaFuture kafkaFuture : futureList) {
          kafkaFuture.awaitAndGet(false);
        }
      } catch (Exception exception) {
        logger.logProducerAwaitAndGetError("JO2wqXe3gh", exception);
      }
    }

    private final Queue<KafkaFuture> futureList = new ConcurrentLinkedQueue<>();

    public void append(KafkaFuture kafkaFuture) {
      futureList.add(kafkaFuture);
    }
  }

  private final ThreadLocal<ConsumerContext> consumerContextHolder = InheritableThreadLocal.withInitial(() -> null);

  public ConsumerPortionInvoking startPortionInvoking() {
    if (consumerContextHolder.get() == null) {
      ConsumerContext consumerContext = new ConsumerContext();
      consumerContextHolder.set(consumerContext);
      return () -> {
        consumerContext.close();
        consumerContextHolder.set(null);
      };
    }

    return () -> {};
  }

  public void acceptKafkaFuture(KafkaFuture kafkaFuture) {
    ConsumerContext consumerContext = consumerContextHolder.get();
    if (consumerContext == null) {
      kafkaFuture.awaitAndGet();
      return;
    }

    consumerContext.append(kafkaFuture);

  }
}
