package kz.greetgo.kafka.core.config;

import org.apache.curator.framework.AuthInfo;
import org.apache.zookeeper.client.ZKClientConfig;

import java.util.List;
import java.util.Map;
import java.util.function.IntSupplier;
import java.util.function.Supplier;

import static java.util.Objects.requireNonNull;

public class ZooConnectParamsBuilder {
  private IntSupplier sessionTimeoutMs        = () -> 10000;
  private IntSupplier connectionTimeoutMs     = () -> 5000;
  private IntSupplier sleepMsBetweenRetriesMs = () -> 700;
  private IntSupplier maxRetries              = () -> 3;

  private Supplier<Map<String, String>> config       = Map::of;
  private Supplier<List<AuthInfo>>      authInfoList = List::of;

  ZooConnectParamsBuilder() {}

  public ZooConnectParamsBuilder maxRetries(IntSupplier maxRetries) {
    this.maxRetries = requireNonNull(maxRetries);
    return this;
  }

  public ZooConnectParamsBuilder config(Supplier<Map<String, String>> config) {
    this.config = config;
    return this;
  }

  public ZooConnectParamsBuilder authInfoList(Supplier<List<AuthInfo>> authInfoList) {
    this.authInfoList = authInfoList;
    return this;
  }

  public ZooConnectParamsBuilder connectionTimeoutMs(IntSupplier connectionTimeoutMs) {
    this.connectionTimeoutMs = requireNonNull(connectionTimeoutMs);
    return this;
  }

  public ZooConnectParamsBuilder sessionTimeoutMs(IntSupplier sessionTimeoutMs) {
    this.sessionTimeoutMs = requireNonNull(sessionTimeoutMs);
    return this;
  }

  public ZooConnectParamsBuilder sleepMsBetweenRetriesMs(IntSupplier sleepMsBetweenRetriesMs) {
    this.sleepMsBetweenRetriesMs = requireNonNull(sleepMsBetweenRetriesMs);
    return this;
  }

  public ZooConnectParams build() {
    return new ZooConnectParams() {
      @Override
      public int sessionTimeoutMs() {
        return sessionTimeoutMs.getAsInt();
      }

      @Override
      public int connectionTimeoutMs() {
        return connectionTimeoutMs.getAsInt();
      }

      @Override
      public int sleepMsBetweenRetriesMs() {
        return sleepMsBetweenRetriesMs.getAsInt();
      }

      @Override
      public ZKClientConfig zkClientConfig() {
        ZKClientConfig ret = new ZKClientConfig();
        config.get().forEach(ret::setProperty);
        return ret;
      }

      @Override
      public List<AuthInfo> authInfoList() {
        return authInfoList.get();
      }

      @Override
      public int maxRetries() {
        return maxRetries.getAsInt();
      }
    };
  }
}
