package kz.greetgo.kafka.core.config;

public interface EventConfigFileFactory extends AutoCloseable {

  EventConfigFile createEventConfigFile(String path);

  EventConfigFileFactory cd(String subPath);

  @Override
  void close();

}
