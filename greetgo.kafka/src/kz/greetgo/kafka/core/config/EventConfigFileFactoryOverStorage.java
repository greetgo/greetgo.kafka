package kz.greetgo.kafka.core.config;

public class EventConfigFileFactoryOverStorage implements EventConfigFileFactory {

  private final EventConfigStorage storage;
  private final String             topPath;

  public EventConfigFileFactoryOverStorage(EventConfigStorage storage) {
    this(storage, null);
  }

  public EventConfigFileFactoryOverStorage(EventConfigStorage storage, String topPath) {
    this.storage = storage;
    this.topPath = topPath;
  }

  @Override
  public EventConfigFile createEventConfigFile(String path) {
    return new EventConfigFileFromStorage(connect(path), storage);
  }

  private String connect(String path) {
    if (topPath == null) {
      return path;
    }
    if (path == null) {
      return topPath;
    }

    //noinspection SuspiciousNameCombination
    String left = topPath;
    while (left.endsWith("/")) {
      left = left.substring(0, left.length() - 1);
    }

    String right = path;
    while (right.startsWith("/")) {
      right = right.substring(1);
    }

    return left + '/' + right;
  }

  @Override
  public EventConfigFileFactory cd(String subPath) {
    return new EventConfigFileFactoryOverStorage(storage, connect(subPath));
  }

  @Override
  public void close() {
    storage.close();
  }
}
