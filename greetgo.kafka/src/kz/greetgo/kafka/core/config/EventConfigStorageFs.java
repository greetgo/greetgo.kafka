package kz.greetgo.kafka.core.config;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.util.Arrays;
import java.util.Date;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;

import static java.util.Objects.requireNonNull;

public class EventConfigStorageFs extends EventConfigStorageAbstract {

  public static class Builder {
    private Path rootDir;

    public Builder rootDir(Path rootDir) {
      this.rootDir = requireNonNull(rootDir);
      return this;
    }

    public Builder rootDir(String rootDirPath) {
      this.rootDir = Paths.get(requireNonNull(rootDirPath));
      return this;
    }

    public EventConfigStorageFs build() {
      return new EventConfigStorageFs(requireNonNull(rootDir, "rootDir"));
    }
  }

  private final Path rootDir;

  private EventConfigStorageFs(Path rootDir) {
    this.rootDir = rootDir;
  }

  @Override
  public Optional<Date> createdAt(String path) {
    try {
      Path     fullPath = rootDir.resolve(path);
      var      attrs    = Files.readAttributes(fullPath, BasicFileAttributes.class);
      FileTime fileTime = attrs.creationTime();
      return Optional.of(new Date(fileTime.toMillis()));
    } catch (java.nio.file.NoSuchFileException e) {
      return Optional.empty();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private Optional<Date> readLastModifiedAt(Path fullPath) {
    try {
      var      attrs    = Files.readAttributes(fullPath, BasicFileAttributes.class);
      FileTime fileTime = attrs.lastModifiedTime();
      return Optional.of(new Date(fileTime.toMillis()));
    } catch (java.nio.file.NoSuchFileException e) {
      return Optional.empty();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public Optional<Date> lastModifiedAt(String path) {
    return readLastModifiedAt(rootDir.resolve(path));
  }

  private byte[] readContentOrNull(Path fullPath) {
    try {
      return Files.readAllBytes(fullPath);
    } catch (java.nio.file.NoSuchFileException e) {
      return null;
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public byte[] readContent(String path) {
    var fullPath    = rootDir.resolve(path);
    var content     = readContentOrNull(fullPath);
    var lookingFile = lookingForPaths.get(fullPath);
    if (lookingFile != null) {
      lookingFile.content.set(content);
      lookingFile.lastModified.set(readLastModifiedAt(fullPath).orElse(null));
    }
    return content;
  }

  @Override
  public void writeContent(String path, byte[] content) {
    Path fullPath = rootDir.resolve(path);
    if (content == null) {
      if (Files.exists(fullPath)) {
        try {
          Files.delete(fullPath);
        } catch (IOException e) {
          throw new RuntimeException(e);
        }
      }

      var lookingFile = lookingForPaths.get(fullPath);
      if (lookingFile != null) {
        lookingFile.content.set(null);
        lookingFile.lastModified.set(null);
      }
      return;
    }

    {
      fullPath.toFile().getParentFile().mkdirs();
      try {
        Files.write(fullPath, content);
      } catch (IOException e) {
        throw new RuntimeException(e);
      }

      var lookingFile = lookingForPaths.get(fullPath);
      if (lookingFile != null) {
        lookingFile.content.set(content);
        lookingFile.lastModified.set(readLastModifiedAt(fullPath).orElse(null));
      }
    }
  }

  private class LookingFile {
    final String                  path;
    final Path                    fullPath;
    final AtomicReference<byte[]> content      = new AtomicReference<>(null);
    final AtomicReference<Date>   lastModified = new AtomicReference<>(null);

    private LookingFile(String path, Path fullPath) {
      this.path     = path;
      this.fullPath = fullPath;
      content.set(readContentOrNull(fullPath));
      lastModified.set(readLastModifiedAt(fullPath).orElse(null));
    }

    public void ping() {
      Date lastModifiedNew = readLastModifiedAt(fullPath).orElse(null);
      Date lastModifiedOld = lastModified.get();
      lastModified.set(lastModifiedNew);

      if (lastModifiedNew == null && lastModifiedOld == null) {
        return;
      }

      if (lastModifiedNew == null) {
        fireConfigEventHandler(path, ConfigEventType.DELETE);
        return;
      }
      if (lastModifiedOld == null) {
        fireConfigEventHandler(path, ConfigEventType.CREATE);
        return;
      }

      byte[] contentOld = content.get();
      byte[] contentNew = readContentOrNull(fullPath);

      if (Arrays.equals(contentOld, contentNew)) {
        return;
      }

      content.set(contentNew);
      fireConfigEventHandler(path, ConfigEventType.UPDATE);
    }
  }

  private final ConcurrentHashMap<Path, LookingFile> lookingForPaths = new ConcurrentHashMap<>();

  @Override
  public void ensureLookingFor(String path) {
    var rootPath = rootDir.resolve(path);
    lookingForPaths.putIfAbsent(rootPath, new LookingFile(path, rootPath));
  }

  public void ping() {
    lookingForPaths.values().forEach(LookingFile::ping);
  }
}
