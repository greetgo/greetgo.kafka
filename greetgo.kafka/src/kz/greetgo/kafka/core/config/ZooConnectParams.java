package kz.greetgo.kafka.core.config;

import org.apache.curator.framework.AuthInfo;
import org.apache.zookeeper.client.ZKClientConfig;

import java.util.List;

public interface ZooConnectParams {
  int sessionTimeoutMs();

  int connectionTimeoutMs();

  int sleepMsBetweenRetriesMs();

  int maxRetries();

  ZKClientConfig zkClientConfig();

  List<AuthInfo> authInfoList();

  static ZooConnectParamsBuilder builder() {
    return new ZooConnectParamsBuilder();
  }
}
