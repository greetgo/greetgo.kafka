package kz.greetgo.kafka.core;

import kz.greetgo.kafka.producer.RecordStr;

public interface ProducerStrInterceptor {
  void afterKafkaSent(RecordStr boxRecord);
}
