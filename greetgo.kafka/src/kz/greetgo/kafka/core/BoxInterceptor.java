package kz.greetgo.kafka.core;

import kz.greetgo.kafka.producer.BoxRecord;

public interface BoxInterceptor {
  void afterKafkaSent(BoxRecord boxRecord);
}
