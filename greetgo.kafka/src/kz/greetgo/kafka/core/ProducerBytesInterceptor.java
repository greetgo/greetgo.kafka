package kz.greetgo.kafka.core;

import kz.greetgo.kafka.producer.RecordBytes;

public interface ProducerBytesInterceptor {
  void afterKafkaSent(RecordBytes boxRecord);
}
