package kz.greetgo.kafka.core.logger;

import kz.greetgo.kafka.consumer.ConsumerDefinition;
import kz.greetgo.kafka.consumer.Profile;
import org.apache.kafka.clients.consumer.ConsumerRecords;

import java.util.Map;
import java.util.function.Supplier;

public class LoggerDestinationMessageBridge implements LoggerDestination {

  private final LogMessageAcceptor acceptor;

  private LoggerDestinationMessageBridge(LogMessageAcceptor acceptor) {
    this.acceptor = acceptor;
  }

  public static LoggerDestinationMessageBridge of(LogMessageAcceptor acceptor) {
    return new LoggerDestinationMessageBridge(acceptor);
  }

  @Override
  public void logProducerConfigOnCreating(String producerName, Map<String, Object> configMap) {
    if (acceptor.isInfoEnabled()) {
      StringBuilder sb = new StringBuilder();
      sb.append("eaq327lA56 :: Created producer `").append(producerName).append("` with config:");
      configMap
        .entrySet()
        .stream()
        .sorted(Map.Entry.comparingByKey())
        .forEachOrdered(e ->
                          sb.append("\n    ").append(e.getKey()).append(" = `").append(e.getValue()).append("`")
        );
      acceptor.info(sb.toString());
    }
  }

  @Override
  public void logProducerClosed(String producerName, Profile profile) {
    if (acceptor.isInfoEnabled()) {
      acceptor.info("eg3VjG8LJ2 :: Closed producer `" + producerName + "` on " + profile);
    }
  }

  @Override
  public void logProducerCreated(String producerName, Profile profile) {
    if (acceptor.isInfoEnabled()) {
      acceptor.info("H07lNf5CWz :: Created producer `" + producerName + "` on " + profile);
    }
  }

  @Override
  public void logConsumerStartWorker(ConsumerDefinition consumerDefinition, long workerId, Profile workerProfile) {
    if (acceptor.isInfoEnabled()) {
      acceptor.info("Qd7yBcyWxr :: Started consumer worker `" + consumerDefinition.logDisplay() + "`"
                      + " with id = " + workerId
                      + " on " + workerProfile
                      + " in thread " + Thread.currentThread().getName());
    }
  }

  @Override
  public void logConsumerFinishWorker(ConsumerDefinition consumerDefinition, long workerId, Profile workerProfile) {
    if (acceptor.isInfoEnabled()) {
      acceptor.info("CP7mAW9YKx :: Finished consumer worker `" + consumerDefinition.logDisplay() + "`"
                      + " with id = " + workerId
                      + " in " + workerProfile);
    }
  }

  @Override
  public void debug(Supplier<String> message) {
    if (acceptor.isDebugEnabled()) {
      acceptor.debug(message.get());
    }
  }

  @Override
  public void logConsumerErrorInvoking(Throwable throwable, String consumerName, Profile profile) {
    acceptor.error("2GAwFLdp66 :: Error in invoking custom consumer: " + consumerName + " " + profile, throwable);
  }

  @Override
  public void logConsumerWorkerConfig(ConsumerDefinition consumerDefinition,
                                      long workerId, Map<String, Object> configMap, Profile workerProfile) {

    if (!acceptor.isInfoEnabled()) {
      return;
    }

    StringBuilder sb = new StringBuilder();

    sb.append("2Xl54Xw3U8 :: Consumer worker config: consumer = `").append(consumerDefinition.logDisplay()).append("`")
      .append(", workerId = `").append(workerId).append("` ").append(workerProfile).append("\n");

    configMap
      .entrySet()
      .stream()
      .sorted(Map.Entry.comparingByKey())
      .forEachOrdered(e ->
                        sb.append("\n    ").append(e.getKey()).append(" = `").append(e.getValue()).append("`")
      );

    acceptor.info(sb.toString());

  }

  @Override
  public void logProducerDirectConsumerError(String lineCode, Exception error, ConsumerDefinition consumerDefinition) {
    acceptor.error(lineCode + " :: DIRECT Consumer = " + consumerDefinition.logDisplay(), error);
  }

  @Override
  public void logErrorRefreshTopicList(Exception error) {
    acceptor.error("4UjjGRH7K5 :: Refresh topic list", error);
  }

  @Override
  public void logConsumerIllegalAccessExceptionInvoking(IllegalAccessException e, String invokerInfo) {

    //noinspection StringBufferReplaceableByString
    StringBuilder sb = new StringBuilder();
    sb.append("yv36s1a1yn :: IllegalAccessException invoking ").append(invokerInfo);

    acceptor.error(sb.toString(), e);
  }

  @Override
  public void logConsumerPollExceptionHappened(RuntimeException exception,
                                               ConsumerDefinition consumerDefinition,
                                               Profile workerProfile) {

    acceptor.error("3FsKY038mh :: Poll exception in consumer " + consumerDefinition.logDisplay()
                     + " in " + workerProfile, exception);

  }

  @Override
  public void logConsumerCommitSyncExceptionHappened(RuntimeException exception,
                                                     ConsumerDefinition consumerDefinition) {

    acceptor.error("In0PflFViW :: CommitSync exception in consumer " + consumerDefinition.logDisplay(), exception);

  }

  @Override
  public void logProducerValidationError(Throwable error) {
    acceptor.error("XxLY0m1mzb :: Producer validation error", error);
  }

  @Override
  public void logProducerAwaitAndGetError(String errorCode, Exception exception) {
    acceptor.error(errorCode + " :: Producer AwaitAndGet Error", exception);
  }

  @Override
  public void logConsumerLogDirectInvokeError(Throwable error) {
    acceptor.error("Consumer ERROR", error);
  }

  @Override
  public void logConsumerReactorRefresh(ConsumerDefinition consumerDefinition, int currentCount, int needCount, Profile profile) {
    if (!acceptor.isInfoEnabled()) {
      return;
    }

    acceptor.info("BO55WO1GEV :: Refresh consumer count = "
                    + currentCount + " --> " + needCount + "; " + consumerDefinition.logDisplay() + " of " + profile);
  }

  @Override
  public void logConsumerPollZero(ConsumerDefinition consumerDefinition) {
    if (!acceptor.isInfoEnabled()) {
      return;
    }

    acceptor.info("U1HoFbkMtu :: Poll zero " + consumerDefinition.logDisplay());
  }

  @Override
  public void logConsumerPollPerformDelay(ConsumerDefinition consumerDefinition,
                                          long pollPerformDelayMs,
                                          ConsumerRecords<byte[], Object> records) {

    if (!acceptor.isInfoEnabled()) {
      return;
    }

    acceptor.info("nWeJWP1bx7 :: Poll delay " + pollPerformDelayMs + " ms,"
                    + " recordCount=" + (records == null ? "(records==null)" : "" + records.count())
                    + " : " + consumerDefinition.logDisplay());
  }
}
