package kz.greetgo.kafka.core.logger;

import kz.greetgo.kafka.consumer.ConsumerDefinition;
import kz.greetgo.kafka.consumer.Profile;
import org.apache.kafka.clients.consumer.ConsumerRecords;

import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Predicate;

import static java.util.Objects.requireNonNull;
import static java.util.Objects.requireNonNullElse;

public class Logger implements LoggerExternal {

  private LoggerDestination destination = null;

  @Override
  public void setDestination(LoggerDestination destination) {
    this.destination = destination;
  }

  @Override
  public void setDestination(LogMessageAcceptor acceptor) {
    if (acceptor == null) {
      destination = null;
    } else {
      destination = LoggerDestinationMessageBridge.of(acceptor);
    }
  }

  private static final Predicate<LoggerType> SHOW_ALL = x -> true;

  private final AtomicReference<Predicate<LoggerType>> loggerTypeFilter = new AtomicReference<>(SHOW_ALL);

  @Override
  public void setLoggerTypeFilter(Predicate<LoggerType> loggerTypeFilter) {
    this.loggerTypeFilter.set(requireNonNullElse(loggerTypeFilter, SHOW_ALL));
  }

  public boolean isShow(LoggerType loggerType) {
    if (destination == null) {
      return false;
    }
    {
      return loggerTypeFilter.get().test(loggerType);
    }
  }

  public void logProducerConfigOnCreating(String producerName, Map<String, Object> configMap) {
    LoggerDestination d = this.destination;
    if (d != null) {
      d.logProducerConfigOnCreating(producerName, configMap);
    }
  }

  public void logProducerClosed(String producerName, Profile profile) {
    LoggerDestination d = this.destination;
    if (d != null) {
      d.logProducerClosed(producerName, profile);
    }
  }

  public void logConsumerStartWorker(ConsumerDefinition consumerDefinition, long workerId, Profile workerProfile) {
    LoggerDestination d = this.destination;
    if (d != null) {
      d.logConsumerStartWorker(consumerDefinition, workerId, workerProfile);
    }
  }

  public void logConsumerFinishWorker(ConsumerDefinition consumerDefinition, long workerId, Profile workerProfile) {
    LoggerDestination d = this.destination;
    if (d != null) {
      d.logConsumerFinishWorker(consumerDefinition, workerId, workerProfile);
    }
  }

  public void logConsumerErrorInvoking(Throwable throwable, String consumerName, Profile profile) {
    if (errorSkipper.test(throwable)) {
      return;
    }

    LoggerDestination d = this.destination;
    if (d != null) {
      d.logConsumerErrorInvoking(throwable, consumerName, profile);
    }
  }

  public void logConsumerWorkerConfig(ConsumerDefinition consumerDefinition,
                                      long workerId,
                                      Map<String, Object> configMap,
                                      Profile workerProfile) {

    LoggerDestination d = this.destination;
    if (d != null) {
      d.logConsumerWorkerConfig(consumerDefinition, workerId, configMap, workerProfile);
    }

  }

  public void logConsumerIllegalAccessExceptionInvoking(IllegalAccessException e, String invokingInfo) {

    LoggerDestination d = this.destination;
    if (d != null) {
      d.logConsumerIllegalAccessExceptionInvoking(e, invokingInfo);
    }

  }

  public void logConsumerReactorRefresh(ConsumerDefinition consumerDefinition, int currentCount, int needCount, Profile profile) {
    LoggerDestination d = this.destination;
    if (d != null) {
      d.logConsumerReactorRefresh(consumerDefinition, currentCount, needCount, profile);
    }
  }

  public void logConsumerPollExceptionHappened(RuntimeException exception, ConsumerDefinition consumerDefinition, Profile workerProfile) {
    LoggerDestination d = this.destination;
    if (d != null) {
      d.logConsumerPollExceptionHappened(exception, consumerDefinition, workerProfile);
    }
  }

  public void logConsumerLogDirectInvokeError(Throwable error) {
    if (errorSkipper.test(error)) {
      return;
    }

    LoggerDestination d = this.destination;
    if (d != null) {
      d.logConsumerLogDirectInvokeError(error);
    }

  }

  public void logConsumerCommitSyncExceptionHappened(RuntimeException exception,
                                                     ConsumerDefinition consumerDefinition) {
    LoggerDestination d = this.destination;
    if (d != null) {
      d.logConsumerCommitSyncExceptionHappened(exception, consumerDefinition);
    }
  }

  public void logProducerCreated(String producerName, Profile profile) {
    LoggerDestination d = this.destination;
    if (d != null) {
      d.logProducerCreated(producerName, profile);
    }
  }

  public void logProducerValidationError(Throwable error) {
    LoggerDestination d = this.destination;
    if (d != null) {
      d.logProducerValidationError(error);
    }
  }

  public void logProducerAwaitAndGetError(String errorCode, Exception exception) {
    LoggerDestination d = this.destination;
    if (d != null) {
      d.logProducerAwaitAndGetError(errorCode, exception);
    }
  }

  private Predicate<Throwable> errorSkipper = x -> false;

  public void setErrorSkipper(Predicate<Throwable> errorSkipper) {
    this.errorSkipper = requireNonNull(errorSkipper, "3n5Qk0id57 :: errorSkipper");
  }

  public void logProducerDirectConsumerError(String lineCode, Exception error, ConsumerDefinition consumerDefinition) {
    LoggerDestination d = this.destination;
    if (d != null) {
      d.logProducerDirectConsumerError(lineCode, error, consumerDefinition);
    }
  }

  public void logConsumerPollZero(ConsumerDefinition consumerDefinition) {
    LoggerDestination d = this.destination;
    if (d != null) {
      d.logConsumerPollZero(consumerDefinition);
    }
  }

  public void logConsumerPollPerformDelay(ConsumerDefinition consumerDefinition,
                                          long pollPerformDelayMs, ConsumerRecords<byte[], Object> records) {

    LoggerDestination d = this.destination;
    if (d != null) {
      d.logConsumerPollPerformDelay(consumerDefinition, pollPerformDelayMs, records);
    }
  }

  public void logErrorRefreshTopicList(Exception error) {
    LoggerDestination d = this.destination;
    if (d != null) {
      d.logErrorRefreshTopicList(error);
    }
  }
}
