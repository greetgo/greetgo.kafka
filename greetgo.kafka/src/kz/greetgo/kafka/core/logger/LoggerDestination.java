package kz.greetgo.kafka.core.logger;

import kz.greetgo.kafka.consumer.ConsumerDefinition;
import kz.greetgo.kafka.consumer.Profile;
import org.apache.kafka.clients.consumer.ConsumerRecords;

import java.util.Map;
import java.util.function.Supplier;

public interface LoggerDestination {

  void logProducerConfigOnCreating(String producerName, Map<String, Object> configMap);

  void logProducerClosed(String producerName, Profile profile);

  void logConsumerStartWorker(ConsumerDefinition consumerDefinition, long workerId, Profile workerProfile);

  void logConsumerFinishWorker(ConsumerDefinition consumerDefinition, long workerId, Profile workerProfile);

  void logConsumerErrorInvoking(Throwable throwable, String consumerName, Profile profile);

  void logConsumerWorkerConfig(ConsumerDefinition consumerDefinition, long workerId, Map<String, Object> configMap, Profile workerProfile);

  void logConsumerIllegalAccessExceptionInvoking(IllegalAccessException e, String invokingInfo);

  @SuppressWarnings("unused")
  void debug(Supplier<String> message);

  void logConsumerLogDirectInvokeError(Throwable error);

  void logConsumerReactorRefresh(ConsumerDefinition consumerDefinition, int currentCount, int needCount, Profile profile);

  void logConsumerPollExceptionHappened(RuntimeException exception, ConsumerDefinition consumerDefinition, Profile workerProfile);

  void logConsumerCommitSyncExceptionHappened(RuntimeException exception, ConsumerDefinition consumerDefinition);

  void logProducerCreated(String producerName, Profile profile);

  void logProducerValidationError(Throwable error);

  void logProducerAwaitAndGetError(String errorCode, Exception exception);

  void logProducerDirectConsumerError(String lineCode, Exception error, ConsumerDefinition consumerDefinition);

  void logConsumerPollZero(ConsumerDefinition consumerDefinition);

  void logConsumerPollPerformDelay(ConsumerDefinition consumerDefinition,
                                   long pollPerformDelayMs, ConsumerRecords<byte[], Object> records);

  void logErrorRefreshTopicList(Exception error);
}
