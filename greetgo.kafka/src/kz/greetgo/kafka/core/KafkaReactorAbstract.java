package kz.greetgo.kafka.core;

import kz.greetgo.kafka.consumer.ConsumerActivateFilter;
import kz.greetgo.kafka.consumer.ConsumerDefinition;
import kz.greetgo.kafka.consumer.ConsumerDefinitionCustom;
import kz.greetgo.kafka.consumer.ConsumerDefinitionCustomBuilder;
import kz.greetgo.kafka.consumer.ConsumerDefinitionExtractor;
import kz.greetgo.kafka.consumer.ConsumerDefinitionOnController;
import kz.greetgo.kafka.consumer.config.ConsumerConnectParams;
import kz.greetgo.kafka.consumer.config.ConsumerConnectSetter;
import kz.greetgo.kafka.core.consumer_filter.ConsumerFilterManager;
import kz.greetgo.kafka.core.consumer_filter.ConsumerFilterRegistrar;
import kz.greetgo.kafka.core.logger.Logger;
import kz.greetgo.kafka.core.logger.LoggerExternal;
import kz.greetgo.kafka.producer.BoxRecord;
import kz.greetgo.kafka.producer.ProducerFacade;
import kz.greetgo.kafka.producer.ProducerFacadeBox;
import kz.greetgo.kafka.producer.ProducerFacadeBytes;
import kz.greetgo.kafka.producer.ProducerFacadeStr;
import kz.greetgo.kafka.producer.ProducerSource;
import kz.greetgo.kafka.producer.RecordBytes;
import kz.greetgo.kafka.producer.RecordStr;
import kz.greetgo.kafka.producer.config.ProducerConnectParams;
import kz.greetgo.kafka.producer.config.ProducerConnectSetter;
import kz.greetgo.strconverter.StrConverter;
import org.checkerframework.checker.nullness.qual.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Supplier;

public abstract class KafkaReactorAbstract implements KafkaReactor {
  Supplier<String> authorSupplier;
  Supplier<String> consumerHostId;
  Supplier<String> topicPrefix = () -> null;
  Supplier<String> bootstrapServers;

  protected ConsumerConnectSetter consumerConnectSetter = new ConsumerConnectSetter() {
    @Override
    public void setConnect(Map<String, Object> targetConfigMap, ConsumerConnectParams params) {
      targetConfigMap.put("bootstrap.servers", bootstrapServers.get());
    }
  };

  protected ProducerConnectSetter producerConnectSetter = new ProducerConnectSetter() {
    @Override
    public void setConnect(Map<String, Object> targetConfigMap, ProducerConnectParams params) {
      targetConfigMap.put("bootstrap.servers", bootstrapServers.get());
    }
  };

  Function<Object, String> controllerToWorkerCountFileName = c -> c.getClass().getSimpleName() + ".workerCount";

  protected final Logger                                logger;
  protected final ProducerSynchronizer                  producerSynchronizer;
  final           List<ConsumerDefinitionValidator>     consumerDefinitionValidatorList = new ArrayList<>();
  private final   List<ConsumerDefinitionCustomBuilder> additionalDcBuilderList         = new ArrayList<>();

  List<Object>           controllerList;
  Supplier<StrConverter> strConverter;

  private final ConsumerFilterManager consumerFilterManager = new ConsumerFilterManager();

  @Override
  public ConsumerFilterRegistrar consumerFilterRegistrar() {
    return consumerFilterManager;
  }

  @Override
  public void addConsumerDefinitionCustomBuilder(ConsumerDefinitionCustomBuilder consumerDefinitionCustomBuilder) {
    additionalDcBuilderList.add(consumerDefinitionCustomBuilder);
  }

  protected List<ConsumerDefinition> consumerDefinitionList;

  protected KafkaReactorAbstract(Logger logger) {
    this.logger          = logger;
    producerSynchronizer = new ProducerSynchronizer(logger);
  }

  @Override
  public LoggerExternal logger() {
    return logger;
  }

  protected abstract ProducerSource getProducerSource();

  @Override
  public ProducerSynchronizer producerSynchronizer() {
    return producerSynchronizer;
  }

  private final BoxInterceptor boxInterceptor = this::afterKafkaSent;

  private final ProducerStrInterceptor   producerStrInterceptor   = this::afterKafkaSentStr;
  private final ProducerBytesInterceptor producerBytesInterceptor = this::afterKafkaSentBytes;

  protected abstract void afterKafkaSent(BoxRecord boxRecord);

  protected void afterKafkaSentStr(RecordStr recordStr) {
    // override it
  }

  protected void afterKafkaSentBytes(RecordBytes recordStr) {
    // override it
  }

  @Override
  public ProducerFacade<Object> createProducer(String producerName) {
    return ProducerFacadeBox.create(producerName,
                                    getProducerSource(),
                                    producerSynchronizer,
                                    () -> topicPrefix.get(),
                                    boxInterceptor);
  }

  @Override
  public ProducerFacade<String> createProducerStr(String producerName) {
    return ProducerFacadeStr.create(producerName,
                                    getProducerSource(),
                                    producerSynchronizer,
                                    () -> topicPrefix.get(),
                                    producerStrInterceptor);
  }

  @Override
  public ProducerFacade<byte[]> createProducerBytes(String producerName) {
    return ProducerFacadeBytes.create(producerName,
                                      getProducerSource(),
                                      producerSynchronizer,
                                      () -> topicPrefix.get(),
                                      producerBytesInterceptor);
  }

  protected void accumulateConsumerDefinitionList() {

    consumerDefinitionList = new ArrayList<>();

    var cde = new ConsumerDefinitionExtractor(logger,
                                              () -> topicPrefix.get(),
                                              controllerToWorkerCountFileName,
                                              consumerHostId,
                                              consumerFilterManager);

    final ConsumerActivateFilter caf = getConsumerActivateFilter();

    for (Object controller : controllerList) {
      for (final ConsumerDefinitionOnController def : cde.extract(controller)) {
        if (caf.activateFromController(def)) {
          consumerDefinitionList.add(def);
        }
      }
    }

    for (final ConsumerDefinitionCustomBuilder consumerDcBuilder : additionalDcBuilderList) {
      final ConsumerDefinitionCustom def = prepareConsumerDefinition(consumerDcBuilder).build();
      if (caf.activateCustom(def)) {
        consumerDefinitionList.add(def);
      }
    }

    for (final ConsumerDefinitionValidator cdv : consumerDefinitionValidatorList) {
      consumerDefinitionList.forEach(cdv::validate);
    }
  }

  protected abstract ConsumerActivateFilter getConsumerActivateFilter();

  protected ConsumerDefinitionCustomBuilder prepareConsumerDefinition(@NonNull ConsumerDefinitionCustomBuilder consumerDcBuilder) {
    return consumerDcBuilder.logger(logger)
                            .topicPrefix(topicPrefix)
                            .consumerFilter(consumerFilterManager);
  }

}
