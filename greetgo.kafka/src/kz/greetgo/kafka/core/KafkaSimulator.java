package kz.greetgo.kafka.core;

import kz.greetgo.kafka.consumer.ConsumerActivateFilter;
import kz.greetgo.kafka.consumer.ConsumerDefinition;
import kz.greetgo.kafka.consumer.ConsumerDefinitionCustom;
import kz.greetgo.kafka.consumer.ConsumerDefinitionCustomBuilder;
import kz.greetgo.kafka.consumer.DynamicReactor;
import kz.greetgo.kafka.consumer.DynamicReactorAdding;
import kz.greetgo.kafka.consumer.DynamicReactorAddingImpl;
import kz.greetgo.kafka.consumer.DynamicReactors;
import kz.greetgo.kafka.consumer.InvokeResult;
import kz.greetgo.kafka.consumer.InvokeSession;
import kz.greetgo.kafka.consumer.InvokeSessionFactory;
import kz.greetgo.kafka.consumer.Profile;
import kz.greetgo.kafka.core.logger.Logger;
import kz.greetgo.kafka.errors.ConsumerInvocationException;
import kz.greetgo.kafka.model.Box;
import kz.greetgo.kafka.model.BoxHolder;
import kz.greetgo.kafka.producer.BoxRecord;
import kz.greetgo.kafka.producer.ProducerSource;
import kz.greetgo.kafka.util.BoxUtil;
import kz.greetgo.kafka.util.ConsumerActivateFilterUtil;
import kz.greetgo.kafka.util.Handler;
import kz.greetgo.kafka.util.KeyUtil;
import kz.greetgo.kafka.util.Listener;
import kz.greetgo.kafka.util.PushFilterOnControllerClasses;
import kz.greetgo.strconverter.StrConverter;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.Cluster;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.record.TimestampType;
import org.apache.kafka.common.serialization.ByteArraySerializer;
import org.apache.kafka.common.serialization.Serializer;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Predicate;
import java.util.function.Supplier;

import static java.util.Collections.singletonList;
import static java.util.Collections.synchronizedList;
import static java.util.stream.Collectors.toList;
import static kz.greetgo.kafka.util.GenericUtil.longNullAsZero;

@SuppressWarnings("unused")
public class KafkaSimulator extends KafkaReactorAbstract {

  Supplier<Cluster> clusterSupplier;

  public KafkaSimulator(Logger logger) {
    super(logger);
  }

  @Override
  public void close() {}

  @Override
  public void join() {}

  private final AtomicBoolean isDirect = new AtomicBoolean(true);

  @Override
  public boolean isDirect() {
    return isDirect.get();
  }

  public void setDirect(boolean isDirect) {
    this.isDirect.set(isDirect);
  }

  @Override
  protected ProducerSource getProducerSource() {
    return producerSource;
  }

  @SuppressWarnings("rawtypes")
  private final ConcurrentHashMap<String, MockProducerHolder> producers = new ConcurrentHashMap<>();

  private final ProducerSource producerSource = new ProducerSource() {

    @Override
    public Logger logger() {
      return logger;
    }

    @Override
    public StrConverter getStrConverter() {
      return strConverter.get();
    }

    @Override
    public byte[] extractKey(Object object) {
      return KeyUtil.extractKey(object);
    }

    @Override
    public String author() {
      return authorSupplier == null ? null : authorSupplier.get();
    }

    @Override
    public Map<String, String> getConfigFor(String producerName, @Nonnull Profile profile) {
      return new HashMap<>();
    }

    @Override
    public Listener addConfigListener(String producerName, @Nonnull Profile profile, Handler handler) {
      return () -> {};
    }

    @Override
    public <valueType> Producer<byte[], valueType> createProducer(String producerName,
                                                                  @Nonnull Profile profile,
                                                                  ByteArraySerializer keySerializer,
                                                                  Serializer<valueType> valueSerializer) {

      if (producers.containsKey(producerName)) {
        throw new RuntimeException("aUjVA0J486 :: Producer with name = " + producerName
                                     + " already created. Please select another name");
      }

      {
        //noinspection rawtypes,unchecked
        var x = new MockProducerHolder(producerName, keySerializer, valueSerializer, clusterSupplier.get());
        producers.put(x.getProducerName(), x);
        //noinspection unchecked
        return x.getProducer();
      }

    }
  };

  public void push(Class<?>... controllerClass) {
    push(PushFilterOnControllerClasses.on(controllerClass));
  }

  public void push(PushFilter pushFilter) {
    //noinspection rawtypes
    for (MockProducerHolder producer : producers.values()) {
      producer.getProducer().flush();
      //noinspection unchecked
      List<ProducerRecord<byte[], Object>> history = producer.getProducer().history();
      producer.getProducer().clear();

      for (ProducerRecord<byte[], Object> record : history) {
        pushRecord(record, producer, pushFilter);
      }
    }
  }

  private final List<ConsumerRecord<byte[], Object>> pushedRecords = synchronizedList(new ArrayList<>());

  @SuppressWarnings("rawtypes")
  private void pushRecord(ProducerRecord<byte[], Object> r, MockProducerHolder producer, PushFilter pushFilter) {

    //noinspection unchecked
    TopicPartition topicPartition = producer.topicPartition(r);

    ConsumerRecord<byte[], Object> consumerRecord = new ConsumerRecord<>(
      topicPartition.topic(), topicPartition.partition(), 1L,
      longNullAsZero(r.timestamp()), TimestampType.CREATE_TIME, 1, 1, r.key(), serialization(r.value()), r.headers(), Optional.empty()
    );

    List<ConsumerDefinition> consumerDefinitionList = new ArrayList<>();
    {
      var x = this.consumerDefinitionList;
      if (x != null) {
        consumerDefinitionList.addAll(x);
      }
    }
    consumerDefinitionList.addAll(this.customConsumerDefinitionList);

    dynamicReactorMap.values().stream().map(x -> x.definition).forEach(consumerDefinitionList::add);

    if (consumerDefinitionList.size() > 0) {

      Map<TopicPartition, List<ConsumerRecord<byte[], Object>>> map = new HashMap<>();
      map.put(topicPartition, singletonList(consumerRecord));

      ConsumerRecords<byte[], Object> singleList = new ConsumerRecords<>(map);

      for (ConsumerDefinition consumerDefinition : consumerDefinitionList) {

        if (!pushFilter.canPush(consumerDefinition)) {
          continue;
        }

        InvokeSessionFactory invokeSessionFactory = consumerDefinition.getInvokeSessionFactory();

        try (InvokeSession invokeSession = invokeSessionFactory.createSession()) {

          // TODO pompei здесь нужно перекинуть профили (KafkaSimulator)
          InvokeResult invokeResult = invokeSession.invoke(singleList, new Profile(null));
          if (!invokeResult.needToCommit()) {
            throw new ConsumerInvocationException("6x292NmZmS :: Cannot invoke consumer "
                                                    + consumerDefinition.logDisplay()
                                                    + " of record " + r.value(),
                                                  invokeResult.invocationError());
          }
        }
      }

    }


    pushedRecords.add(consumerRecord);
  }

  private Object serialization(Object value) {
    StrConverter strConverter = this.strConverter.get();

    String str = strConverter.toStr(value);

    return strConverter.fromStr(str);
  }

  public void clearAllProducers() {
    //noinspection rawtypes
    for (MockProducerHolder producer : producers.values()) {
      producer.getProducer().clear();
    }
  }

  public void clearPushed() {
    pushedRecords.clear();
  }

  public List<ConsumerRecord<byte[], Box>> allPushed() {
    List<ConsumerRecord<byte[], Box>> ret = new ArrayList<>();

    for (final ConsumerRecord<byte[], Object> r : pushedRecords) {
      if (r.value() instanceof Box) {
        //noinspection unchecked
        ret.add((ConsumerRecord<byte[], Box>) (Object) r);
      }
    }

    return ret;
  }

  public <T> List<BoxHolder<T>> pushedOf(Class<T> aClass) {

    return allPushed().stream()
                      .map(rec -> BoxUtil.hold(rec.value(), aClass))
                      .filter(Optional::isPresent)
                      .map(Optional::get)
                      .collect(toList());

  }


  private enum InitVariants {
    STARTED_CONSUMERS,
    ACTIVATED_DIRECT_CONSUMERS,
  }

  private InitVariants initiated = null;

  @Override
  public void startConsumers() {
    if (initiated != null) {
      throw new RuntimeException("mXF535wem0 :: Already initiated with " + initiated);
    }
    initiated = InitVariants.STARTED_CONSUMERS;
    accumulateConsumerDefinitionList();
  }

  @Override
  public void activateDirectConsumers() {
    if (initiated != null) {
      throw new RuntimeException("z4FRU1kuH6 :: Already initiated with " + initiated);
    }
    initiated = InitVariants.ACTIVATED_DIRECT_CONSUMERS;
    accumulateConsumerDefinitionList();
  }

  private final List<ConsumerDefinitionCustom> customConsumerDefinitionList = new ArrayList<>();

  @Override
  public void addConsumerDefinitionCustomBuilder(ConsumerDefinitionCustomBuilder consumerDefinitionCustomBuilder) {
    customConsumerDefinitionList.add(
      consumerDefinitionCustomBuilder.logger(new Logger())
                                     .topicPrefix(topicPrefix)
                                     .build()
    );
  }

  public void clearCustomConsumerDefinitionList() {
    customConsumerDefinitionList.clear();
  }

  @Override
  protected void afterKafkaSent(BoxRecord boxRecord) {}

  private static class DynamicReactorHolder implements DynamicReactor {

    private final long                     id;
    private final ConsumerDefinitionCustom definition;

    public DynamicReactorHolder(long id, ConsumerDefinitionCustom definition) {
      this.id         = id;
      this.definition = definition;
    }

    @Override
    public long id() {
      return id;
    }

    @Override
    public ConsumerDefinition definition() {
      return definition;
    }

    @Override
    public boolean isWorking() {
      return true;
    }

    @Override
    public void stop() {}

    @Override
    public void join() {}

    @Override
    public void refreshTopicList() {}
  }

  private final ConcurrentHashMap<Long, DynamicReactorHolder> dynamicReactorMap    = new ConcurrentHashMap<>();
  private final AtomicLong                                    dynamicReactorIdNext = new AtomicLong(1);

  @Override
  public DynamicReactors dynamicConsumers() {
    return new DynamicReactors() {

      private DynamicReactor add(ConsumerDefinitionCustomBuilder consumerDefinitionCustomBuilder) {
        long                     id         = dynamicReactorIdNext.getAndIncrement();
        ConsumerDefinitionCustom definition = consumerDefinitionCustomBuilder.build();
        DynamicReactorHolder     holder     = new DynamicReactorHolder(id, definition);
        dynamicReactorMap.put(id, holder);
        return holder;
      }

      @Override
      public DynamicReactorAdding adding() {
        return new DynamicReactorAddingImpl(this::add);
      }

      @Override
      public Collection<DynamicReactor> reactors() {
        return dynamicReactorMap.values().stream().map(DynamicReactor.class::cast).collect(toList());
      }

      @Override
      public DynamicReactor stopById(long id) {
        return dynamicReactorMap.remove(id);
      }

      @Override
      public Collection<DynamicReactor> stopByFilter(Predicate<DynamicReactor> filter) {
        return dynamicReactorMap.values()
                                .stream()
                                .filter(filter)
                                .map(DynamicReactor::id)
                                .map(dynamicReactorMap::remove)
                                .filter(Objects::nonNull)
                                .collect(toList());
      }
    };
  }

  @Override
  protected ConsumerActivateFilter getConsumerActivateFilter() {
    return ConsumerActivateFilterUtil.ALWAYS_TRUE;
  }
}
