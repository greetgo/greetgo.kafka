package kz.greetgo.kafka.core;

import kz.greetgo.kafka.consumer.ConsumerActivateFilter;
import kz.greetgo.kafka.consumer.ConsumerDefinitionOnController;
import kz.greetgo.kafka.consumer.config.ConsumerConnectSetter;
import kz.greetgo.kafka.consumer.config.ConsumerReactorConfigFactory;
import kz.greetgo.kafka.core.logger.Logger;
import kz.greetgo.kafka.producer.config.ProducerConnectSetter;
import kz.greetgo.kafka.producer.config.ProducerReactorConfigFactory;
import kz.greetgo.kafka.util.CachedBoolSupplier;
import kz.greetgo.strconverter.StrConverter;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.BooleanSupplier;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.LongSupplier;
import java.util.function.Predicate;
import java.util.function.Supplier;

import static java.util.Objects.requireNonNull;
import static kz.greetgo.kafka.util.Fnn.fnn;

public class KafkaReactorBuilder {

  private final Logger logger = new Logger();

  private final List<Object> controllerList = new ArrayList<>();

  private Supplier<StrConverter> strConverter;
  private Supplier<String>       bootstrapServers;
  private ConsumerConnectSetter  consumerConnectSetter;
  private ProducerConnectSetter  producerConnectSetter;
  private Supplier<String>       authorSupplier;
  private Supplier<String>       consumerHostId       = () -> null;
  private Supplier<String>       topicPrefix          = () -> null;
  private Supplier<String>       consumerThreadPrefix = () -> "kafka-consumer-";
  private ConsumerActivateFilter consumerActivateFilter;

  private Function<Object, String> controllerToWorkerCountFileName
    = controller -> controller.getClass().getSimpleName() + ".workerCount";

  private ConsumerReactorConfigFactory consumerConfigFactory;
  private ConsumerReactorConfigFactory dynamicConsumerConfigFactory;
  private ProducerReactorConfigFactory producerConfigFactory;

  private LongSupplier refreshTopicListMillis = () -> 15000;

  private final List<ConsumerDefinitionValidator> consumerDefinitionValidatorList = new ArrayList<>();

  private BooleanSupplier consumersEnabledSupplier = () -> true;

  KafkaReactorBuilder() {}

  public KafkaReactorBuilder onLogger(@NonNull Consumer<Logger> loggerConsumer) {
    loggerConsumer.accept(logger);
    return this;
  }

  @SuppressWarnings("unused")
  public KafkaReactorBuilder loggerErrorSkipper(@NonNull Predicate<Throwable> errorSkipper) {
    logger.setErrorSkipper(errorSkipper);
    return this;
  }

  @SuppressWarnings("UnusedReturnValue")
  public KafkaReactorBuilder addController(Object controller) {
    controllerList.add(requireNonNull(controller));
    return this;
  }

  public KafkaReactorBuilder consumerActivateFilter(ConsumerActivateFilter consumerActivateFilter) {
    this.consumerActivateFilter = consumerActivateFilter;
    return this;
  }

  public KafkaReactorBuilder consumersEnabledSupplier(BooleanSupplier enabledSupplier) {
    return consumersEnabledSupplier(3000, enabledSupplier);
  }

  public KafkaReactorBuilder consumersEnabledSupplier(long callDurationMs, BooleanSupplier enabledSupplier) {
    if (enabledSupplier == null) {
      this.consumersEnabledSupplier = () -> true;
    } else {
      this.consumersEnabledSupplier = callDurationMs <= 0 ? enabledSupplier : new CachedBoolSupplier(callDurationMs, enabledSupplier);
    }
    return this;
  }

  public KafkaReactorBuilder refreshTopicListMillis(LongSupplier refreshTopicListMillis) {
    this.refreshTopicListMillis = requireNonNull(refreshTopicListMillis);
    return this;
  }

  public KafkaReactorBuilder addControllers(Collection<?> controllers) {
    if (controllers != null) {
      controllers.forEach(this::addController);
    }
    return this;
  }

  public KafkaReactorBuilder strConverter(Supplier<StrConverter> strConverter) {
    this.strConverter = requireNonNull(strConverter);
    return this;
  }

  public KafkaReactorBuilder consumerHostId(Supplier<String> consumerHostId) {
    this.consumerHostId = requireNonNull(consumerHostId);
    return this;
  }

  public KafkaReactorBuilder consumerConfigFactory(ConsumerReactorConfigFactory consumerConfigFactory) {
    this.consumerConfigFactory = consumerConfigFactory;
    return this;
  }

  public KafkaReactorBuilder dynamicConsumerConfigFactory(ConsumerReactorConfigFactory consumerConfigFactory) {
    this.dynamicConsumerConfigFactory = consumerConfigFactory;
    return this;
  }

  public KafkaReactorBuilder producerConfigFactory(ProducerReactorConfigFactory producerConfigFactory) {
    this.producerConfigFactory = producerConfigFactory;
    return this;
  }

  public KafkaReactorBuilder bootstrapServers(Supplier<String> bootstrapServers) {
    this.bootstrapServers = bootstrapServers;
    return this;
  }

  public KafkaReactorBuilder consumerConnectSetter(ConsumerConnectSetter consumerConnectSetter) {
    this.consumerConnectSetter = consumerConnectSetter;
    return this;
  }

  public KafkaReactorBuilder producerConnectSetter(ProducerConnectSetter producerConnectSetter) {
    this.producerConnectSetter = producerConnectSetter;
    return this;
  }

  public KafkaReactorBuilder authorSupplier(Supplier<String> authorSupplier) {
    this.authorSupplier = authorSupplier;
    return this;
  }

  public KafkaReactorBuilder consumerDefinitionValidator(ConsumerDefinitionValidator consumerDefinitionValidator) {
    consumerDefinitionValidatorList.add(consumerDefinitionValidator);
    return this;
  }

  public KafkaReactorBuilder makeConsumerDescriptionMandatory() {
    consumerDefinitionValidatorList.add(consumerDefinition -> {
      if (consumerDefinition instanceof final ConsumerDefinitionOnController x) {
        if (x.getDescription() == null) {
          throw new RuntimeException("EoH1cZzmA8 :: No description in consumer " + x.logDisplay());
        }
      }
    });
    return this;
  }

  public KafkaReactorBuilder topicPrefix(Supplier<String> topicPrefix) {
    this.topicPrefix = topicPrefix;
    return this;
  }

  public KafkaReactorBuilder consumerThreadPrefix(Supplier<String> consumerThreadPrefix) {
    this.consumerThreadPrefix = consumerThreadPrefix;
    return this;
  }

  @SuppressWarnings("unused")
  public KafkaReactorBuilder controllerToWorkerCountFileName(Function<Object, String> controllerToWorkerCountFileName) {
    this.controllerToWorkerCountFileName = requireNonNull(controllerToWorkerCountFileName);
    return this;
  }

  public KafkaReactor build() {

    var reactor = new KafkaReactorImpl(logger);

    if (bootstrapServers == null && consumerConnectSetter == null) {
      throw new RuntimeException("MqLUt04o9a :: You MUST defined one of `bootstrapServers` OR `consumerConnectSetter`");
    }
    if (bootstrapServers == null && producerConnectSetter == null) {
      throw new RuntimeException("8VT3qv19ev :: You MUST defined one of `bootstrapServers` OR `producerConnectSetter`");
    }

    if (bootstrapServers != null) {
      reactor.bootstrapServers = bootstrapServers;
    }
    if (consumerConnectSetter != null) {
      reactor.consumerConnectSetter = consumerConnectSetter;
    }
    if (producerConnectSetter != null) {
      reactor.producerConnectSetter = producerConnectSetter;
    }

    reactor.strConverter                 = requireNonNull(strConverter);
    reactor.consumerHostId               = requireNonNull(consumerHostId);
    reactor.topicPrefix                  = requireNonNull(topicPrefix);
    reactor.controllerList               = requireNonNull(controllerList);
    reactor.consumerConfigFactory        = requireNonNull(consumerConfigFactory);
    reactor.dynamicConsumerConfigFactory = requireNonNull(fnn(dynamicConsumerConfigFactory, consumerConfigFactory));
    reactor.producerConfigFactory        = requireNonNull(producerConfigFactory);
    reactor.authorSupplier               = requireNonNull(authorSupplier);
    reactor.consumerThreadPrefix         = requireNonNull(consumerThreadPrefix);
    reactor.refreshTopicListMillis       = requireNonNull(refreshTopicListMillis);

    reactor.consumerActivateFilter = consumerActivateFilter;//Can be NULL

    reactor.consumerDefinitionValidatorList.addAll(consumerDefinitionValidatorList);
    reactor.controllerToWorkerCountFileName = controllerToWorkerCountFileName;

    reactor.consumersEnabledSupplier = consumersEnabledSupplier;

    return reactor;

  }

}
