package kz.greetgo.kafka.core;

import kz.greetgo.kafka.core.logger.Logger;
import kz.greetgo.kafka.core.logger.LoggerExternal;
import kz.greetgo.strconverter.StrConverter;
import org.apache.kafka.common.Cluster;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;

import static java.util.Objects.requireNonNull;

public class KafkaSimulatorBuilder {

  private final Logger logger = new Logger();

  private final List<Object> controllerList = new ArrayList<>();

  private Supplier<StrConverter> strConverter;
  private Supplier<String>       authorSupplier;
  private Supplier<String>       consumerHostId  = () -> null;
  private Supplier<Cluster>      clusterSupplier = Cluster::empty;

  KafkaSimulatorBuilder() {}

  @SuppressWarnings("unused")
  public KafkaSimulatorBuilder onLogger(Consumer<LoggerExternal> loggerConsumer) {
    loggerConsumer.accept(logger);
    return this;
  }

  public KafkaSimulatorBuilder addController(Object controller) {
    controllerList.add(requireNonNull(controller));
    return this;
  }

  @SuppressWarnings("unused")
  public KafkaSimulatorBuilder addControllers(List<?> controllerList) {
    if (controllerList != null) {
      controllerList.forEach(this::addController);
    }
    return this;
  }

  public KafkaSimulatorBuilder strConverter(Supplier<StrConverter> strConverter) {
    this.strConverter = requireNonNull(strConverter);
    return this;
  }

  public KafkaSimulatorBuilder consumerHostId(Supplier<String> consumerHostId) {
    this.consumerHostId = requireNonNull(consumerHostId);
    return this;
  }

  public KafkaSimulatorBuilder authorSupplier(Supplier<String> authorSupplier) {
    this.authorSupplier = requireNonNull(authorSupplier);
    return this;
  }

  @SuppressWarnings("unused")
  public KafkaSimulatorBuilder clusterSupplier(Supplier<Cluster> clusterSupplier) {
    this.clusterSupplier = requireNonNull(clusterSupplier);
    return this;
  }

  @SuppressWarnings("unused")
  public KafkaSimulatorBuilder loggerErrorSkipper(Predicate<Throwable> errorSkipper) {
    logger.setErrorSkipper(errorSkipper);
    return this;
  }

  public KafkaSimulator build() {
    var simulator = new KafkaSimulator(logger);

    simulator.strConverter    = requireNonNull(strConverter);
    simulator.consumerHostId  = requireNonNull(consumerHostId);
    simulator.controllerList  = requireNonNull(controllerList);
    simulator.authorSupplier  = requireNonNull(authorSupplier);
    simulator.clusterSupplier = requireNonNull(clusterSupplier);

    return simulator;
  }

}
