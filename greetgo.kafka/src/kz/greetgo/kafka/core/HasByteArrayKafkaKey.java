package kz.greetgo.kafka.core;

/**
 * Результат должен быть уникальным идентификатором
 * Пример кода:
 * <pre><code>
 *  public class KafkaObject implements HasByteArrayKafkaKey {
 *    public String id;
 *    @Override public byte[] extractByteArrayKafkaKey() {
 *      return id.getBytes(UTF_8);
 *    }
 *  }
 * </code></pre>
 */
public interface HasByteArrayKafkaKey {
  byte[] extractByteArrayKafkaKey();
}
