package kz.greetgo.kafka.core;

import kz.greetgo.kafka.consumer.ConsumerActivateFilter;
import kz.greetgo.kafka.consumer.ConsumerDefinition;
import kz.greetgo.kafka.consumer.ConsumerDefinitionCustom;
import kz.greetgo.kafka.consumer.ConsumerDefinitionCustomBuilder;
import kz.greetgo.kafka.consumer.ConsumerReactor;
import kz.greetgo.kafka.consumer.ConsumerReactorBuilder;
import kz.greetgo.kafka.consumer.DynamicReactor;
import kz.greetgo.kafka.consumer.DynamicReactorAdding;
import kz.greetgo.kafka.consumer.DynamicReactorAddingImpl;
import kz.greetgo.kafka.consumer.DynamicReactors;
import kz.greetgo.kafka.consumer.Profile;
import kz.greetgo.kafka.consumer.config.ConsumerReactorConfigFactory;
import kz.greetgo.kafka.core.logger.Logger;
import kz.greetgo.kafka.core.logger.LoggerType;
import kz.greetgo.kafka.producer.BoxRecord;
import kz.greetgo.kafka.producer.ProducerSource;
import kz.greetgo.kafka.producer.config.ProducerConnectParams;
import kz.greetgo.kafka.producer.config.ProducerReactorConfigFactory;
import kz.greetgo.kafka.util.ConsumerActivateFilterUtil;
import kz.greetgo.kafka.util.Handler;
import kz.greetgo.kafka.util.KeyUtil;
import kz.greetgo.kafka.util.Listener;
import kz.greetgo.strconverter.StrConverter;
import lombok.NonNull;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.common.serialization.ByteArraySerializer;
import org.apache.kafka.common.serialization.Serializer;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BooleanSupplier;
import java.util.function.LongSupplier;
import java.util.function.Predicate;
import java.util.function.Supplier;

import static java.util.stream.Collectors.toList;

public class KafkaReactorImpl extends KafkaReactorAbstract {

  ConsumerReactorConfigFactory consumerConfigFactory;
  ConsumerReactorConfigFactory dynamicConsumerConfigFactory;
  ProducerReactorConfigFactory producerConfigFactory;
  LongSupplier                 refreshTopicListMillis;
  Supplier<String>             consumerThreadPrefix;
  ConsumerActivateFilter       consumerActivateFilter;

  private final   AtomicReference<RefreshStaticListThread> refreshTopicListThread = new AtomicReference<>(null);
  private final   List<ConsumerReactor>                    consumerReactorList    = Collections.synchronizedList(new ArrayList<>());
  private final   List<ConsumerDefinition>                 directDefinitionList   = new ArrayList<>();
  protected final ThreadLocal<Boolean>                     isDirectThreadLocal    = ThreadLocal.withInitial(() -> false);

  BooleanSupplier consumersEnabledSupplier = () -> true;

  KafkaReactorImpl(Logger logger) {
    super(logger);
  }

  private class RefreshStaticListThread extends Thread {
    final AtomicBoolean localWorking = new AtomicBoolean(true);

    private RefreshStaticListThread() {
      start();
    }

    @Override
    public void run() {
      while (working.get() && localWorking.get()) {

        try {
          Thread.sleep(refreshTopicListMillis.getAsLong());
        } catch (InterruptedException e) {
          return;
        }

        try {
          new ArrayList<>(consumerReactorList).forEach(ConsumerReactor::refreshTopicList);
          dynamicReactors.reactors().forEach(DynamicReactor::refreshTopicList);
        } catch (Exception error) {
          logger.logErrorRefreshTopicList(error);
        }
      }
    }
  }

  @Override
  protected ConsumerActivateFilter getConsumerActivateFilter() {
    var f = consumerActivateFilter;
    return f != null ? f : ConsumerActivateFilterUtil.ALWAYS_TRUE;
  }

  @Override
  public boolean isDirect() {
    return isDirectThreadLocal.get();
  }

  private enum InitVariant {
    STARTED_CONSUMERS,
    ACTIVATED_DIRECT_CONSUMERS
  }

  private InitVariant initiated;

  private final AtomicBoolean working = new AtomicBoolean(true);

  @Override
  public void startConsumers() {
    if (initiated != null) {
      throw new RuntimeException("VkHD4j4DnF :: Reactor already initiated with " + initiated);
    }
    initiated = InitVariant.STARTED_CONSUMERS;

    accumulateConsumerDefinitionList();

    for (final ConsumerDefinition consumerDefinition : consumerDefinitionList) {
      if (!consumerDefinition.ignoreKafka()) {
        consumerConfigFactory.getConsumerConfig(consumerDefinition);
      }
    }

    for (ConsumerDefinition consumerDefinition : consumerDefinitionList) {

      if (consumerDefinition.isDirect()) {
        directDefinitionList.add(consumerDefinition);
      }
      if (consumerDefinition.ignoreKafka()) {
        continue;
      }

      ConsumerReactor reactor = prepareReactorBuilder(consumerDefinition, consumerConfigFactory).build();

      reactor.start();

      consumerReactorList.add(reactor);
    }

    dynamicReactorMap.values().forEach(LocalDynConsumer::start);

    {
      RefreshStaticListThread oldThread = refreshTopicListThread.getAndSet(new RefreshStaticListThread());
      if (oldThread != null) {
        oldThread.localWorking.set(false);
      }
    }
  }

  private ConsumerReactorBuilder prepareReactorBuilder(@NonNull ConsumerDefinition consumerDefinition,
                                                       @NonNull ConsumerReactorConfigFactory consumerConfigFactory) {
    return ConsumerReactor.builder()
                          .logger(logger)
                          .producerSynchronizer(producerSynchronizer)
                          .strConverter(strConverter)
                          .consumerConnectSetter(consumerConnectSetter)
                          .consumerDefinition(consumerDefinition)
                          .consumerConfig(consumerConfigFactory.getConsumerConfig(consumerDefinition))
                          .consumerThreadPrefix(consumerThreadPrefix)
                          .addExternalWorkingSupplierAND(consumersEnabledSupplier);
  }

  @Override
  public void activateDirectConsumers() {
    if (initiated != null) {
      throw new RuntimeException("2YMtX1PqkJ :: Reactor already initiated with " + initiated);
    }
    initiated = InitVariant.ACTIVATED_DIRECT_CONSUMERS;

    accumulateConsumerDefinitionList();
    for (ConsumerDefinition consumerDefinition : consumerDefinitionList) {
      if (consumerDefinition.isDirect()) {
        directDefinitionList.add(consumerDefinition);
      }
    }
  }

  @Override
  protected void afterKafkaSent(BoxRecord boxRecord) {

    isDirectThreadLocal.set(true);
    try {

      for (final ConsumerDefinition consumerDefinition : directDefinitionList) {
        try (var session = consumerDefinition.getInvokeSessionFactory().createSession()) {
          try {

            // TODO pompei проверить, что здесь корректно фильтруется по профилю
            if (consumerDefinition.prefixedTopicList()
                                  .stream()
                                  .filter(x -> boxRecord.profile.equals(x.profile))
                                  .flatMap(x -> x.topics.stream())
                                  .anyMatch(t -> t.equals(boxRecord.prefixedTopic))) {

              session.invoke(boxRecord.toConsumerRecords(), boxRecord.profile);
            }

          } catch (Exception e) {
            if (logger.isShow(LoggerType.LOG_PRODUCER_DIRECT_CONSUMER_ERROR)) {
              logger.logProducerDirectConsumerError("VpJ3Dl9570", e, consumerDefinition);
            }
          }
        }
      }

    } finally {
      isDirectThreadLocal.set(false);
    }
  }

  @Override
  public ProducerSource getProducerSource() {
    return producerSource;
  }

  private final ProducerSource producerSource = new ProducerSource() {

    @Override
    public Logger logger() {
      return logger;
    }

    @Override
    public StrConverter getStrConverter() {
      return strConverter.get();
    }

    @Override
    public byte[] extractKey(Object object) {
      return KeyUtil.extractKey(object);
    }

    @Override
    public String author() {
      return authorSupplier == null ? null : authorSupplier.get();
    }

    @Override
    public Map<String, String> getConfigFor(String producerName, @Nonnull Profile profile) {
      return producerConfigFactory.getConfig(producerName, profile).getConfigMap();
    }

    @Override
    public Listener addConfigListener(String producerName, @Nonnull Profile profile, Handler handler) {
      return producerConfigFactory.getConfig(producerName, profile).addChangeHandler(handler);
    }

    @Override
    public <ValueType> Producer<byte[], ValueType> createProducer(String producerName,
                                                                  @Nonnull Profile profile,
                                                                  ByteArraySerializer keySerializer,
                                                                  Serializer<ValueType> valueSerializer) {

      Map<String, Object> configMap = new HashMap<>(getConfigFor(producerName, profile));

      producerConnectSetter.setConnect(configMap, new ProducerConnectParams() {
        @Override
        public String producerName() {
          return producerName;
        }

        @Override
        public String profile() {
          return profile.profile;
        }
      });

      if (logger.isShow(LoggerType.SHOW_PRODUCER_CONFIG)) {
        logger.logProducerConfigOnCreating(producerName, configMap);
      }
      return new KafkaProducer<>(configMap, keySerializer, valueSerializer);
    }

  };

  @Override
  public DynamicReactors dynamicConsumers() {
    return dynamicReactors;
  }

  interface LocalDynConsumer extends DynamicReactor {
    void start();
  }

  private final ConcurrentHashMap<Long, LocalDynConsumer> dynamicReactorMap = new ConcurrentHashMap<>();
  private final AtomicLong                                dynamicIdNext     = new AtomicLong(1);
  private final DynamicReactors                           dynamicReactors   = new DynamicReactors() {

    private DynamicReactor add(ConsumerDefinitionCustomBuilder consumerDefinitionCustomBuilder) {
      final long id = dynamicIdNext.getAndIncrement();

      ConsumerDefinitionCustom consumerDefinition = prepareConsumerDefinition(consumerDefinitionCustomBuilder).build();

      ConsumerReactor reactor = prepareReactorBuilder(consumerDefinition, dynamicConsumerConfigFactory)
        .addExternalWorkingSupplierAND(() -> working.get() && dynamicReactorMap.containsKey(id))
        .build();

      LocalDynConsumer dynamicReactor = new LocalDynConsumer() {
        @Override
        public long id() {
          return id;
        }

        @Override
        public ConsumerDefinition definition() {
          return reactor.getConsumerDefinition();
        }

        @Override
        public void refreshTopicList() {
          reactor.refreshTopicList();
        }

        @Override
        public void join() {
          reactor.join();
        }

        @Override
        public void start() {
          reactor.start();
        }

        @Override
        public void stop() {
          reactor.stop();
        }

        @Override
        public boolean isWorking() {
          return reactor.isWorking();
        }
      };

      dynamicReactorMap.put(id, dynamicReactor);

      if (initiated == InitVariant.STARTED_CONSUMERS) {
        reactor.start();
      }

      return dynamicReactor;
    }

    @Override
    public DynamicReactorAdding adding() {
      return new DynamicReactorAddingImpl(this::add);
    }

    @Override
    public Collection<DynamicReactor> reactors() {
      return dynamicReactorMap.values().stream().map(DynamicReactor.class::cast).collect(toList());
    }

    @Override
    public DynamicReactor stopById(long id) {
      return dynamicReactorMap.remove(id);
    }

    @Override
    public Collection<DynamicReactor> stopByFilter(Predicate<DynamicReactor> filter) {
      List<Long> removingIds = dynamicReactorMap.values()
                                                .stream()
                                                .filter(filter)
                                                .map(DynamicReactor::id)
                                                .toList();
      return removingIds.stream()
                        .map(this::stopById)
                        .filter(Objects::nonNull)
                        .collect(toList());
    }
  };

  @Override
  public void close() {
    working.set(false);
    consumerReactorList.forEach(ConsumerReactor::stop);
  }

  @Override
  public void join() {
    consumerReactorList.forEach(ConsumerReactor::join);
    dynamicReactorMap.values().forEach(DynamicReactor::join);
  }

}
