package kz.greetgo.kafka.core.consumer_filter;

import kz.greetgo.kafka.consumer.ConsumerFilter;

public interface ConsumerFilterRegistrar {

  CustomFilterRegistration registerForLocalThread(ConsumerFilter consumerFilter);

  CustomFilterRegistration registerForAllThreads(ConsumerFilter consumerFilter);

}
