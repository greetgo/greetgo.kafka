package kz.greetgo.kafka.core.consumer_filter;

public interface CustomFilterRegistration extends AutoCloseable {
  @Override
  void close();
}
