package kz.greetgo.kafka.core.consumer_filter;

import kz.greetgo.kafka.consumer.ConsumerDefinition;
import kz.greetgo.kafka.consumer.ConsumerFilter;
import kz.greetgo.kafka.model.Box;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;

public class ConsumerFilterManager implements ConsumerFilterRegistrar, ConsumerFilter {

  private final AtomicLong nextId = new AtomicLong(1L);

  private final ThreadLocal<ConcurrentHashMap<Long, ConsumerFilter>>
    localThreadFilters = ThreadLocal.withInitial(ConcurrentHashMap::new);

  @Override
  public CustomFilterRegistration registerForLocalThread(ConsumerFilter consumerFilter) {
    requireNonNull(consumerFilter, "g6VB5dX1WS :: consumerFilter == null");

    var workingMap = localThreadFilters.get();

    long registerId = nextId.getAndIncrement();
    workingMap.put(registerId, consumerFilter);

    return () -> workingMap.remove(registerId);
  }

  private final ConcurrentHashMap<Long, ConsumerFilter> allThreadsFilters = new ConcurrentHashMap<>();

  @Override
  public CustomFilterRegistration registerForAllThreads(ConsumerFilter consumerFilter) {
    requireNonNull(consumerFilter, "k4Wh3UYi1o :: consumerFilter == null");

    long registerId = nextId.getAndIncrement();
    allThreadsFilters.put(registerId, consumerFilter);

    return () -> allThreadsFilters.remove(registerId);
  }

  @Override
  public boolean isInFilter(ConsumerDefinition consumerDefinition, Box box, byte[] recordKey) {
    if (!isInFilters(localThreadFilters.get(), consumerDefinition, box, recordKey)) {
      return false;
    }
    return isInFilters(allThreadsFilters, consumerDefinition, box, recordKey);
  }

  private static boolean isInFilters(ConcurrentHashMap<Long, ConsumerFilter> allThreadsFilters,
                                     ConsumerDefinition consumerDefinition,
                                     Box box,
                                     byte[] recordKey) {

    var list = allThreadsFilters.entrySet()
                                .stream()
                                .sorted(Map.Entry.comparingByKey())
                                .map(Map.Entry::getValue)
                                .collect(toList());

    for (final ConsumerFilter consumerFilter : list) {
      if (!consumerFilter.isInFilter(consumerDefinition, box, recordKey)) {
        return false;
      }
    }

    return true;
  }
}
