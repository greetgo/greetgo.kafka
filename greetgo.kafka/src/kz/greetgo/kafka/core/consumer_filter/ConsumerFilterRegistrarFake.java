package kz.greetgo.kafka.core.consumer_filter;

import kz.greetgo.kafka.consumer.ConsumerFilter;

public class ConsumerFilterRegistrarFake implements ConsumerFilterRegistrar {

  @Override
  public CustomFilterRegistration registerForLocalThread(ConsumerFilter consumerFilter) {
    return () -> {};
  }

  @Override
  public CustomFilterRegistration registerForAllThreads(ConsumerFilter consumerFilter) {
    return () -> {};
  }
}
