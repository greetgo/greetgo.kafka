package kz.greetgo.kafka.core;

import kz.greetgo.kafka.consumer.ConsumerDefinition;
import kz.greetgo.kafka.consumer.ConsumerDefinitionOnController;

public interface PushFilter {

  boolean canPush(ConsumerDefinition consumerDefinition);

}
