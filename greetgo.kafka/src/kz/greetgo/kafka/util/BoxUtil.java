package kz.greetgo.kafka.util;

import kz.greetgo.kafka.model.Box;
import kz.greetgo.kafka.model.BoxHolder;

import java.util.Optional;

public class BoxUtil {

  public static <T> Optional<BoxHolder<T>> hold(Box box, Class<T> aClass) {
    if (box == null) {
      return Optional.empty();
    }

    if (aClass.isInstance(box.body)) {
      return Optional.of(() -> {
        //noinspection unchecked
        return (T) box.body;
      });
    }

    return Optional.empty();
  }

}
