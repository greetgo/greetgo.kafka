package kz.greetgo.kafka.util;

import lombok.NonNull;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

import static java.util.Objects.requireNonNull;

public class GenericUtil {
  public static boolean isOfClass(Type someType, Class<?> aClass) {
    if (someType == aClass) {
      return true;
    }

    if (someType instanceof final ParameterizedType type) {
      if (type.getRawType() == aClass) {
        return true;
      }
    }

    return false;
  }

  public static @NonNull Type[] extractGenericParameterTypes(@NonNull Method method) {
    final Class<?> declaringClass = method.getDeclaringClass();
    if (!declaringClass.getSimpleName().contains("$$SpringCGLIB$$")) {
      return method.getGenericParameterTypes();
    }
    final Class<?> superclass = declaringClass.getSuperclass();
    final Method   superMethod;
    try {
      superMethod = superclass.getMethod(method.getName(), method.getParameterTypes());
    } catch (NoSuchMethodException e) {
      return method.getGenericParameterTypes();
    }

    return superMethod.getGenericParameterTypes();
  }

  public static Class<?> extractListParameter(Type type) {
    if (type instanceof ParameterizedType pt) {
      if (pt.getRawType() == List.class && pt.getActualTypeArguments().length == 1) {
        final Type argType = pt.getActualTypeArguments()[0];
        return extractClass(argType);
      }
    }
    return null;
  }

  public static Class<?> extractClass(Type someType) {
    if (someType instanceof Class) {
      return (Class<?>) someType;
    }

    if (someType instanceof final ParameterizedType type) {
      if (type.getRawType() instanceof Class) {
        return (Class<?>) type.getRawType();
      }
    }

    throw new IllegalArgumentException("Cannot extract class from " + someType);
  }

  public static long longNullAsZero(Long value) {
    return value == null ? 0L : value;
  }

  public static @NonNull String infoOf(Object controller, Method method) {
    requireNonNull(controller, "Zub7bmzGC9 :: controller");
    requireNonNull(method, "cv6Vz6Q7AM :: method");
    return "{method " + method.getName() + " of " + controller.getClass() + " :: " + controller + '}';
  }

  public static Object classOrNull(@NonNull Class<?> aClass, Object value) {
    return aClass.isInstance(value) ? value : null;
  }
}
