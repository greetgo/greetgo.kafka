package kz.greetgo.kafka.util;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class HandlerList {
  private final ConcurrentHashMap<Long, Handler> handlerMap = new ConcurrentHashMap<>();
  private final AtomicLong                       nextId     = new AtomicLong(1);

  public void fire() {
    handlerMap.values().forEach(Handler::handle);
  }

  public Listener addHandler(Handler handler) {
    long id = nextId.getAndIncrement();
    handlerMap.put(id, handler);
    return () -> handlerMap.remove(id);
  }

}
