package kz.greetgo.kafka.util;

public class Fnn {

  @SafeVarargs
  public static <T> T fnn(T... tt) {
    for (final T t : tt) {
      if (t != null) {
        return t;
      }
    }
    return null;
  }

}
