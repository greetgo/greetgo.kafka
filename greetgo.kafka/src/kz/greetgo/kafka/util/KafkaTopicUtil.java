package kz.greetgo.kafka.util;

import kz.greetgo.kafka.consumer.Profile;
import kz.greetgo.kafka.consumer.TopicsInProfile;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class KafkaTopicUtil {

  public static Set<TopicsInProfile> callAndPrepareResult(Profile defaultProfile,
                                                          Method listTopicMethod,
                                                          Object controller) {

    final Object methodCallResult;
    try {
      methodCallResult = listTopicMethod.invoke(controller);
    } catch (InvocationTargetException e) {
      throw new RuntimeException("lo21fT5gJ5 :: InvocationTargetException", e.getCause());
    } catch (IllegalAccessException e) {
      throw new RuntimeException(e);
    }

    if (methodCallResult instanceof TopicsInProfile) {
      var tip = (TopicsInProfile) methodCallResult;
      return tip.profile.profile != null ? Set.of(tip) : Set.of(TopicsInProfile.of(tip.topics, defaultProfile));
    }

    if (methodCallResult instanceof CharSequence) {
      return Set.of(TopicsInProfile.of(Set.of(methodCallResult.toString()), defaultProfile));
    }

    if (methodCallResult instanceof Collection) {
      @SuppressWarnings("unchecked")
      Collection<Object> collection = (Collection<Object>) methodCallResult;

      Set<TopicsInProfile> topics = new HashSet<>();
      for (final Object topic : collection) {
        if (topic instanceof CharSequence) {
          topics.add(TopicsInProfile.of(Set.of(topic.toString()), defaultProfile));
          continue;
        }
        if (topic instanceof TopicsInProfile) {
          var tip = (TopicsInProfile) topic;
          if (tip.profile.profile != null) {
            topics.add(tip);
          } else {
            topics.add(TopicsInProfile.of(tip.topics, defaultProfile));
          }
        }
      }

      return TopicsInProfile.distinct(topics);
    }

    throw new RuntimeException("KnhORYqB21 :: Unknown methodCallResult = " + methodCallResult);
  }

  public static Map<Profile, Set<String>> topicsToMap(Set<TopicsInProfile> prefixedTopics) {
    Map<Profile, Set<String>> newMap = new HashMap<>();
    for (final TopicsInProfile tip : prefixedTopics) {
      Profile profile = tip.profile;
      newMap.computeIfAbsent(profile, p -> new HashSet<>()).addAll(tip.topics);
    }

    Map<Profile, Set<String>> newMap2 = new HashMap<>();
    for (final Map.Entry<Profile, Set<String>> e : newMap.entrySet()) {
      newMap2.put(e.getKey(), Set.copyOf(e.getValue()));
    }

    return newMap2;
  }

}
