package kz.greetgo.kafka.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class LineUtil {
  public static List<String> calcDelta(List<String> topLines, List<String> lines) {
    Set<String> linesKeys = new HashSet<>();

    for (final String line : lines) {
      String trimmedLine = line.trim();
      if (trimmedLine.isEmpty() || trimmedLine.startsWith("#")) {
        continue;
      }
      int idx = line.indexOf("=");
      if (idx < 0) {
        continue;
      }
      linesKeys.add(line.substring(0, idx).trim());
    }

    Set<String>  existsKeys = new HashSet<>();
    List<String> delta      = new ArrayList<>();
    List<String> current    = null;

    for (final String line : topLines) {
      String trimmedLine = line.trim();
      if (trimmedLine.isEmpty()) {
        current = null;
        continue;
      }
      if (trimmedLine.startsWith("#")) {
        if (current == null) {
          current = new ArrayList<>();
        }
        current.add(line);
        continue;
      }
      int idx = line.indexOf('=');
      if (idx < 0) {
        current = null;
        continue;
      }
      String key = line.substring(0, idx).trim();
      if (linesKeys.contains(key)) {
        continue;
      }
      if (existsKeys.contains(key)) {
        continue;
      }
      existsKeys.add(key);

      if (current == null) {
        current = new ArrayList<>();
      }
      current.add(line);
      if (delta.size() > 0) {
        delta.add("");
      }
      delta.addAll(current);
      current = null;
    }

    return delta.isEmpty() ? null : delta;
  }


}
