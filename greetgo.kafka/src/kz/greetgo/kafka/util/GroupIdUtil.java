package kz.greetgo.kafka.util;

public class GroupIdUtil {

  public static String evalGroupId(String groupIdValue, Object controller) {

    if (groupIdValue == null) {
      return null;
    }

    int index = 0;

    StringBuilder ret = new StringBuilder();

    while (index < groupIdValue.length()) {
      var i1 = groupIdValue.indexOf('{', index);
      if (i1 < 0) {
        break;
      }

      var i2 = groupIdValue.indexOf('}', i1);
      if (i2 < 0) {
        break;
      }

      ret.append(groupIdValue, index, i1);
      ret.append(eval(controller, groupIdValue.substring(i1 + 1, i2)));

      index = i2 + 1;
    }

    ret.append(groupIdValue.substring(index));

    return ret.toString();
  }

  private static String eval(Object controller, String expr) {
    try {

      var trimmedExpr = expr.trim();

      if (trimmedExpr.endsWith("()")) {
        var methodName = trimmedExpr.substring(0, trimmedExpr.length() - 2).trim();

        var method = controller.getClass().getMethod(methodName);
        return convertToStr(method.invoke(controller));
      }

      var field = controller.getClass().getField(trimmedExpr);
      return convertToStr(field.get(controller));

    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  private static String convertToStr(Object value) {

    if (value == null) {
      return "";
    }

    if (value instanceof String) {
      return (String) value;
    }

    return "" + value;
  }

}
