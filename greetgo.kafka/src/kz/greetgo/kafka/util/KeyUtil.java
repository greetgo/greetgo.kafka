package kz.greetgo.kafka.util;

import kz.greetgo.kafka.core.HasByteArrayKafkaKey;
import kz.greetgo.kafka.core.HasStrKafkaKey;
import kz.greetgo.kafka.errors.CannotExtractKeyFrom;

import javax.annotation.Nonnull;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class KeyUtil {

  public static byte[] extractKey(Object object) {
    if (object instanceof HasByteArrayKafkaKey) {
      return ((HasByteArrayKafkaKey) object).extractByteArrayKafkaKey();
    }
    if (object instanceof HasStrKafkaKey) {
      String str = ((HasStrKafkaKey) object).extractStrKafkaKey();
      if (str == null) {
        return new byte[0];
      }
      return str.getBytes(StandardCharsets.UTF_8);
    }
    if (object instanceof CharSequence) {
      return sha1sum(object.toString().getBytes(StandardCharsets.UTF_8));
    }
    if (object instanceof byte[]) {
      return sha1sum((byte[]) object);
    }
    throw new CannotExtractKeyFrom("BH09y23Tx9", object);
  }

  private static byte[] sha1sum(@Nonnull byte[] bytes) {
    try {
      return MessageDigest.getInstance("SHA-1").digest(bytes);
    } catch (NoSuchAlgorithmException e) {
      throw new RuntimeException(e);
    }
  }

}
