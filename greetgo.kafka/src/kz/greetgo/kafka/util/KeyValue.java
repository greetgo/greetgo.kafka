package kz.greetgo.kafka.util;

import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class KeyValue {
  private final String key;
  private final String value;

  public KeyValue(String key, String value) {
    this.key   = key;
    this.value = value;
  }

  public static KeyValue of(String key, String value) {
    return new KeyValue(key, value);
  }

  public String key() {
    return key;
  }

  public String value() {
    return value;
  }

  public static Collector<KeyValue, ?, Map<String, String>> toMap() {
    return Collectors.toMap(KeyValue::key, KeyValue::value, (a, b) -> b);
  }

  @Override
  public String toString() {
    return "KeyValue{" + key + " = " + value + '}';
  }
}
