package kz.greetgo.kafka.util;

import lombok.NonNull;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.BooleanSupplier;
import java.util.function.LongSupplier;

public class CachedBoolSupplier implements BooleanSupplier {

  private final long            readDurationMs;
  private final BooleanSupplier durableSupplier;
  private final LongSupplier    nowSource;
  private final AtomicLong      lastSupplierCall = new AtomicLong(0);
  private final AtomicBoolean   cachedValue      = new AtomicBoolean();

  public CachedBoolSupplier(long readDurationMs, @NonNull BooleanSupplier durableSupplier) {
    this(readDurationMs, durableSupplier, System::currentTimeMillis);
  }

  public CachedBoolSupplier(long readDurationMs, @NonNull BooleanSupplier durableSupplier, @NonNull LongSupplier nowSource) {
    this.readDurationMs  = readDurationMs;
    this.durableSupplier = durableSupplier;
    this.nowSource       = nowSource;
  }

  @Override
  public boolean getAsBoolean() {
    final long nowMs      = nowSource.getAsLong();
    final long lastCallMs = lastSupplierCall.get();

    if (lastCallMs > 0 && nowMs - lastCallMs < readDurationMs) {
      return cachedValue.get();
    }

    lastSupplierCall.set(nowMs);

    {
      final boolean value = durableSupplier.getAsBoolean();
      cachedValue.set(value);
      return value;
    }
  }
}
