package kz.greetgo.kafka.util;

import kz.greetgo.kafka.consumer.ConsumerActivateFilter;
import kz.greetgo.kafka.consumer.ConsumerDefinitionCustom;
import kz.greetgo.kafka.consumer.ConsumerDefinitionOnController;

public class ConsumerActivateFilterUtil {
  public static final ConsumerActivateFilter ALWAYS_TRUE = new ConsumerActivateFilter() {
    @Override
    public boolean activateFromController(ConsumerDefinitionOnController definition) {
      return true;
    }

    @Override
    public boolean activateCustom(ConsumerDefinitionCustom definition) {
      return true;
    }
  };
}
