package kz.greetgo.kafka.util;

import java.util.stream.Stream;

public class StreamUtil {

  @SafeVarargs
  public static <T> Stream<T> concat(Stream<T> first, Stream<T> ...rest) {
    if (rest.length == 0) {
      return first;
    }

    var ret = Stream.concat(first, rest[0]);

    for (int i = 1; i < rest.length; i++) {
      ret = Stream.concat(ret, rest[i]);
    }

    return ret;
  }

}
