package kz.greetgo.kafka.util;

import kz.greetgo.kafka.consumer.ConsumerDefinitionOnController;
import kz.greetgo.kafka.core.PushFilter;

public class PushFilterOnControllerClasses {

  public static PushFilter on(Class<?>... controllerClass) {
    return consumerDefinition -> {

      if (!(consumerDefinition instanceof ConsumerDefinitionOnController)) {
        return true;
      }

      {
        var controller = ((ConsumerDefinitionOnController) consumerDefinition).getController();

        for (Class<?> aClass : controllerClass) {
          if (aClass.isInstance(controller)) {
            return true;
          }
        }
      }

      return false;
    };
  }

}
