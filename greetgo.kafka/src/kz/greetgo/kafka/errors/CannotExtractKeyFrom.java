package kz.greetgo.kafka.errors;

public class CannotExtractKeyFrom extends RuntimeException {
  public CannotExtractKeyFrom(String placeId, Object object) {
    super(placeId + " :: " + (object == null ? "object == null" : "object.class = " + object.getClass()));
  }
}
