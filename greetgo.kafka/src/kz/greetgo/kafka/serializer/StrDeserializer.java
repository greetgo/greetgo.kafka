package kz.greetgo.kafka.serializer;

import org.apache.kafka.common.serialization.Deserializer;

import java.nio.charset.StandardCharsets;

public class StrDeserializer implements Deserializer<Object> {
  @Override
  public Object deserialize(String topic, byte[] data) {
    return data == null ? null : new String(data, StandardCharsets.UTF_8);
  }
}
