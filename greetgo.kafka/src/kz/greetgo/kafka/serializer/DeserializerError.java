package kz.greetgo.kafka.serializer;

public class DeserializerError extends RuntimeException {
  public final byte[] data;

  public DeserializerError(Exception e, byte[] data) {
    super(e);
    this.data = data;
  }
}
