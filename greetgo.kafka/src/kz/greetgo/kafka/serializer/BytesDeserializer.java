package kz.greetgo.kafka.serializer;

import org.apache.kafka.common.serialization.Deserializer;

public class BytesDeserializer implements Deserializer<Object> {
  @Override
  public Object deserialize(String topic, byte[] data) {
    return data;
  }
}
