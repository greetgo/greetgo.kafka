package kz.greetgo.kafka.serializer;

import org.apache.kafka.common.serialization.Serializer;

public class BytesSerializer implements Serializer<byte[]> {

  @Override
  public byte[] serialize(String topic, byte[] data) {
    return data;
  }

}
