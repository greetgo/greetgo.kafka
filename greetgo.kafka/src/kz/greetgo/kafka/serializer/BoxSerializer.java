package kz.greetgo.kafka.serializer;

import kz.greetgo.strconverter.StrConverter;
import org.apache.kafka.common.serialization.Serializer;

import static java.nio.charset.StandardCharsets.UTF_8;

public class BoxSerializer implements Serializer<Object> {

  private final StrConverter strConverter;

  public BoxSerializer(StrConverter strConverter) {
    this.strConverter = strConverter;
  }

  @Override
  public byte[] serialize(String topic, Object data) {

    String str = strConverter.toStr(data);

    return str.getBytes(UTF_8);

  }
}
