package kz.greetgo.kafka.serializer;

import kz.greetgo.kafka.model.Box;
import kz.greetgo.strconverter.StrConverter;
import org.apache.kafka.common.serialization.Deserializer;

import java.nio.charset.StandardCharsets;

public class BoxDeserializer implements Deserializer<Object> {

  private final StrConverter strConverter;

  public BoxDeserializer(StrConverter strConverter) {
    this.strConverter = strConverter;
  }

  @Override
  public Object deserialize(String topic, byte[] data) {

    if (data == null) {
      return null;
    }

    try {

      String str = new String(data, StandardCharsets.UTF_8);

      Object ret = strConverter.fromStr(str);

      return ret instanceof Box ? ret : null;

    } catch (Exception e) {
      return new DeserializerError(e, data);
    }
  }

}
