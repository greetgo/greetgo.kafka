package kz.greetgo.kafka.serializer;

import org.apache.kafka.common.serialization.Serializer;

import java.nio.charset.StandardCharsets;

public class StrSerializer implements Serializer<String> {

  @Override
  public byte[] serialize(String topic, String data) {
    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);
  }

}
