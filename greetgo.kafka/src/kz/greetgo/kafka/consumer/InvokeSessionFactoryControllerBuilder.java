package kz.greetgo.kafka.consumer;

import kz.greetgo.kafka.consumer.annotations.KafkaCommitOn;
import kz.greetgo.kafka.consumer.annotations.TopicContentType;
import kz.greetgo.kafka.core.logger.Logger;
import kz.greetgo.kafka.util.AnnotationUtil;
import kz.greetgo.kafka.util.GenericUtil;
import lombok.NonNull;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Supplier;

import static java.util.Objects.requireNonNull;
import static kz.greetgo.kafka.util.AnnotationUtil.getAnnotation;
import static kz.greetgo.kafka.util.GenericUtil.extractListParameter;
import static kz.greetgo.kafka.util.GenericUtil.isOfClass;

public class InvokeSessionFactoryControllerBuilder {

  final Object           controller;
  final Method           method;
  final Logger           logger;
  final BoxFilter        boxFilter;
  final Supplier<String> topicPrefix;

  public InvokeSessionFactoryControllerBuilder(Object controller,
                                               Method method,
                                               Logger logger,
                                               BoxFilter boxFilter,
                                               Supplier<String> topicPrefix) {

    this.controller  = requireNonNull(controller, "wz6NF2lB8o :: controller == null");
    this.method      = requireNonNull(method, "nC5a0z8Lzk :: method == null");
    this.logger      = requireNonNull(logger, "gT11yHPpFL :: logger == null");
    this.boxFilter   = requireNonNull(boxFilter, "5aAoiDzs48 :: boxFilter == null");
    this.topicPrefix = topicPrefix != null ? topicPrefix : () -> null;
  }

  public InvokeSessionFactory build() {

    Class<?>[] tmpCommitOn = new Class<?>[0];
    {
      KafkaCommitOn commitOn = getAnnotation(method, KafkaCommitOn.class);
      if (commitOn != null) {
        tmpCommitOn = commitOn.value();
      }
    }

    final Class<?>[]       commitOn             = tmpCommitOn;
    final TopicContentType topicContentType     = TopicContentTypeReader.readFrom(method);
    final Type[]           parameterTypes       = GenericUtil.extractGenericParameterTypes(method);
    final Annotation[][]   parameterAnnotations = AnnotationUtil.getParameterAnnotations(method);

    assert parameterTypes.length == parameterAnnotations.length;

    if (isBatchMethodTypes(parameterTypes)) {

      if (commitOn.length != 0) {
        throw new IllegalArgumentException("768Jd40Q5w :: you cannot use " + KafkaCommitOn.class.getSimpleName() + " annotation with batched consumers. ConsumerName: " + controller.getClass().getSimpleName());
      }

      return createBatchInvokeSessionFactory(parameterTypes, parameterAnnotations, topicContentType, commitOn, topicPrefix);
    }

    return createSingleInvokeSessionFactory(parameterTypes, parameterAnnotations, topicContentType, commitOn);
  }

  private boolean isBatchMethodTypes(Type[] parameterTypes) {
    if (parameterTypes.length != 1) {
      return false;
    }

    final Type firstType = parameterTypes[0];

    if (firstType == List.class) {
      throw new RuntimeException("tiCb7asRlj :: List MUST have argument type: controller class  "
                                   + controller.getClass().getSimpleName() + ", method: " + method);
    }

    if (isOfClass(firstType, ConsumerRecords.class)) {
      return true;
    }

    if (firstType instanceof ParameterizedType pt) {
      if (pt.getRawType() instanceof Class<?>) {
        return true;
      }
    }
    return false;
  }

  private @NonNull SingleInvokeSessionFactoryImpl createSingleInvokeSessionFactory(Type[] parameterTypes,
                                                                                   Annotation[][] parameterAnnotations,
                                                                                   TopicContentType topicContentType,
                                                                                   Class<?>[] commitOn) {

    final boolean[]              canAcceptDeserializerError = new boolean[]{false};
    final ParameterReaderOptions parameterReaderOptions     = () -> canAcceptDeserializerError[0] = true;
    final int                    parametersCount            = parameterTypes.length;
    final ParameterValueReader[] parameterValueReaders      = new ParameterValueReader[parametersCount];

    for (int i = 0; i < parametersCount; i++) {
      parameterValueReaders[i] = ParameterValueReaderUtil.createParameterValueReader(parameterTypes[i],
                                                                                     parameterAnnotations[i],
                                                                                     topicContentType,
                                                                                     parameterReaderOptions,
                                                                                     topicPrefix);
    }

    Class<?> tmpGettingBodyClass = null;
    for (ParameterValueReader parameterValueReader : parameterValueReaders) {
      Class<?> aClass = parameterValueReader.gettingBodyClass();
      if (aClass != null) {
        tmpGettingBodyClass = aClass;
      }
    }

    final Class<?> gettingBodyClass = tmpGettingBodyClass;

    SingleInvoker invoker = new SingleInvokerImpl(this,
                                                  canAcceptDeserializerError,
                                                  parametersCount,
                                                  parameterValueReaders,
                                                  gettingBodyClass,
                                                  topicContentType,
                                                  commitOn);

    return new SingleInvokeSessionFactoryImpl(logger, invoker);
  }

  private @NonNull InvokeSessionFactory createBatchInvokeSessionFactory(Type[] parameterTypes,
                                                                        Annotation[][] parameterAnnotations,
                                                                        TopicContentType topicContentType,
                                                                        Class<?>[] commitOn,
                                                                        Supplier<String> topicPrefix) {

    BiFunction<ConsumerRecords<byte[], Object>, Profile, Object[]>
      recordsToListConverter = createRecordsToListConvertor(parameterTypes,
                                                            parameterAnnotations,
                                                            topicContentType,
                                                            topicPrefix);

    return new BatchInvokeSessionFactory(new BatchInvokerImpl(controller, method, recordsToListConverter, commitOn, logger));
  }

  private static @NonNull BiFunction<ConsumerRecords<byte[], Object>, Profile, Object[]>

  createRecordsToListConvertor(Type[] parameterTypes,
                               @SuppressWarnings("unused") Annotation[][] parameterAnnotations2,
                               @NonNull TopicContentType topicContentType,
                               @NonNull Supplier<String> topicPrefix) {

    final Type firstParameterType = parameterTypes[0];

    if (isOfClass(firstParameterType, ConsumerRecords.class)) {
      return (consumerRecords, profile) -> new Object[]{consumerRecords};
    }

    final Class<?> listParamClass = extractListParameter(firstParameterType);

    if (listParamClass == null) {
      throw new RuntimeException("a74X2t76iu :: Умею работать только со списками");
    }

    final BiFunction<ConsumerRecord<byte[], Object>, Profile, Object>
      recordConverter = RecordToClassConverter.createRecordToClassConverter(listParamClass, topicPrefix, topicContentType);

    return (ConsumerRecords<byte[], Object> consumerRecords, Profile profile) -> {

      List<Object> list = new ArrayList<>();

      for (final ConsumerRecord<byte[], Object> record : consumerRecords) {
        final Object object = recordConverter.apply(record, profile);
        if (object != null) {
          list.add(object);
        }
      }

      return new Object[]{list};
    };

  }

}
