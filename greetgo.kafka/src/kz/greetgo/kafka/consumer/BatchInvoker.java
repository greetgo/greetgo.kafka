package kz.greetgo.kafka.consumer;

import org.apache.kafka.clients.consumer.ConsumerRecords;

import javax.annotation.Nonnull;

public interface BatchInvoker {

  InvokeResult invoke(@Nonnull ConsumerRecords<byte[], Object> records,
                      @Nonnull InvokeSessionContext context,
                      @Nonnull Profile profile);

}
