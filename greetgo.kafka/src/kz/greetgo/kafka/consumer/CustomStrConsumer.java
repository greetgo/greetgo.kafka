package kz.greetgo.kafka.consumer;

public interface CustomStrConsumer {

  void execute(String recordStr, BoxMeta boxMeta) throws Exception;

}
