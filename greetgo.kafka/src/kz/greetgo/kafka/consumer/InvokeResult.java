package kz.greetgo.kafka.consumer;

import lombok.NonNull;

public interface InvokeResult {

  boolean needToCommit();

  Throwable invocationError();

  static @NonNull InvokeResult of(boolean needToCommit, Throwable invocationError) {
    return new InvokeResult() {
      @Override
      public boolean needToCommit() {
        return needToCommit;
      }

      @Override
      public Throwable invocationError() {
        return invocationError;
      }
    };
  }

  static @NonNull InvokeResult ok() {
    return of(true, null);
  }
}
