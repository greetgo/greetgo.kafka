package kz.greetgo.kafka.consumer;

public interface DynamicReactor {
  long id();

  ConsumerDefinition definition();

  boolean isWorking();

  void refreshTopicList();

  @SuppressWarnings("unused")
  void stop();

  void join();
}
