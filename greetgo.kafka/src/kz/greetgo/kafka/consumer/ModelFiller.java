package kz.greetgo.kafka.consumer;

import lombok.NonNull;
import org.apache.kafka.clients.consumer.ConsumerRecord;

public interface ModelFiller {
  boolean fill(@NonNull Object target,
               ConsumerRecord<byte[], Object> record,
               Profile profile) throws Exception;
}
