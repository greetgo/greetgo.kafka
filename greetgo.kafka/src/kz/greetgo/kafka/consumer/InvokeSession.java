package kz.greetgo.kafka.consumer;

import org.apache.kafka.clients.consumer.ConsumerRecords;

public interface InvokeSession extends AutoCloseable {

  InvokeResult invoke(ConsumerRecords<byte[], Object> records, Profile profile);

  @Override
  void close();
}
