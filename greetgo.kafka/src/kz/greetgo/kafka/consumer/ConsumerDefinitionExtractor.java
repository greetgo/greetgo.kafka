package kz.greetgo.kafka.consumer;

import kz.greetgo.kafka.consumer.annotations.Topic;
import kz.greetgo.kafka.consumer.annotations.TopicListMethod;
import kz.greetgo.kafka.core.logger.Logger;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

import static java.util.Objects.requireNonNull;
import static kz.greetgo.kafka.util.AnnotationUtil.getAnnotation;

public class ConsumerDefinitionExtractor {

  public final  Logger                   logger;
  public final  Supplier<String>         consumerHostId;
  private final Supplier<String>         topicPrefix;
  private final Function<Object, String> controllerToWorkerCountFileName;
  private final ConsumerFilter           consumerFilter;


  public ConsumerDefinitionExtractor(Logger logger,
                                     Supplier<String> topicPrefix,
                                     Function<Object, String> controllerToWorkerCountFileName,
                                     Supplier<String> consumerHostId,
                                     ConsumerFilter consumerFilter) {

    this.logger                          = requireNonNull(logger, "PmUmkLm3Pg");
    this.topicPrefix                     = topicPrefix != null ? topicPrefix : () -> null;
    this.controllerToWorkerCountFileName = requireNonNull(controllerToWorkerCountFileName, "k98Yi33y9W");
    this.consumerHostId                  = requireNonNull(consumerHostId, "aDttPcjFQ3");
    this.consumerFilter                  = requireNonNull(consumerFilter, "WMCi8hGhqR");
  }

  public List<ConsumerDefinitionOnController> extract(Object controller) {

    List<ConsumerDefinitionOnController> ret = new ArrayList<>();

    for (Method method : controller.getClass().getMethods()) {

      Topic           topic           = getAnnotation(method, Topic.class);
      TopicListMethod topicListMethod = getAnnotation(method, TopicListMethod.class);

      if (topic == null && topicListMethod == null) {
        continue;
      }

      ret.add(new ConsumerDefinitionOnController(controller,
                                                 method,
                                                 logger,
                                                 topicPrefix,
                                                 consumerHostId.get(),
                                                 controllerToWorkerCountFileName.apply(controller),
                                                 consumerFilter));
    }

    return ret;
  }

}
