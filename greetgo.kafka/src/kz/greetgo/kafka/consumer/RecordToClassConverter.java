package kz.greetgo.kafka.consumer;

import kz.greetgo.kafka.consumer.annotations.DataRef;
import kz.greetgo.kafka.consumer.annotations.HeadersRef;
import kz.greetgo.kafka.consumer.annotations.KafkaId;
import kz.greetgo.kafka.consumer.annotations.KafkaKeyRef;
import kz.greetgo.kafka.consumer.annotations.Offset;
import kz.greetgo.kafka.consumer.annotations.Partition;
import kz.greetgo.kafka.consumer.annotations.Timestamp;
import kz.greetgo.kafka.consumer.annotations.TopicContentType;
import kz.greetgo.kafka.consumer.annotations.TopicRef;
import kz.greetgo.kafka.model.Box;
import lombok.NonNull;
import lombok.SneakyThrows;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.header.Header;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Supplier;

public class RecordToClassConverter {
  public static @NonNull BiFunction<ConsumerRecord<byte[], Object>, Profile, Object>

  createRecordToClassConverter(@NonNull Class<?> listParamClass,
                               @NonNull Supplier<String> topicPrefix,
                               @NonNull TopicContentType topicContentType) {

    if (topicContentType == TopicContentType.BOX) {
      if (listParamClass == Box.class) {
        return (rec, profile) -> rec.value() instanceof Box ? rec.value() : null;
      }
      if (listParamClass == Object.class) {
        return (rec, profile) -> rec.value() instanceof Box box ? box.body : null;
      }
    }
    if (topicContentType == TopicContentType.STR) {
      if (listParamClass == String.class) {
        return (rec, profile) -> rec.value() instanceof String str ? str : null;
      }
    }
    if (topicContentType == TopicContentType.BYTES) {
      if (listParamClass == byte[].class) {
        return (rec, profile) -> rec.value() instanceof byte[] bb ? bb : null;
      }
    }

    final ModelCreator modelCreator = createModelCreator(listParamClass, topicPrefix, topicContentType);

    final List<ModelFiller> fillers = new ArrayList<>();

    appendFillers(fillers, listParamClass, topicPrefix, topicContentType);

    return (record, profile) -> {
      try {

        final Object object = modelCreator.create(record, profile);

        if (object == null) {
          return null;
        }

        for (final ModelFiller filler : fillers) {
          if (filler.fill(object, record, profile)) {
            return null;
          }
        }

        return object;

      } catch (Exception e) {
        throw e instanceof RuntimeException r ? r : new RuntimeException(e);
      }
    };
  }

  private static void appendFillers(@NonNull List<ModelFiller> fillers,
                                    @NonNull Class<?> listParamClass,
                                    @NonNull Supplier<String> topicPrefix,
                                    @NonNull TopicContentType topicContentType) {

    FIELDS:
    for (final Field field : listParamClass.getFields()) {

      if (field.getAnnotation(DataRef.class) != null) {
        final Class<?> fieldType = field.getType();

        if (topicContentType == TopicContentType.STR) {
          if (fieldType != String.class) {
            throw new RuntimeException("1iMHt1om7s :: Для TopicContentType == STR поле должно иметь тип String");
          }
          fillers.add((target, record, profile) -> {
            final Object value = record.value();
            if (!(value instanceof String str)) {
              return true;
            }
            field.set(target, str);
            return false;
          });
          continue FIELDS;
        }

        if (topicContentType == TopicContentType.BYTES) {
          if (fieldType != String.class) {
            throw new RuntimeException("mP0yzKDdYt :: Для TopicContentType == BYTES поле должно иметь тип byte[]");
          }
          fillers.add((target, record, profile) -> {
            final Object value = record.value();
            if (!(value instanceof byte[] bytes)) {
              return true;
            }
            field.set(target, bytes);
            return false;
          });
          continue FIELDS;
        }

        if (topicContentType == TopicContentType.BOX) {
          fillers.add((target, record, profile) -> {
            final Object value = record.value();

            if (!(value instanceof Box box)) {
              return true;
            }

            if (!fieldType.isInstance(box.body)) {
              return true;
            }

            field.set(target, box.body);
            return false;
          });
          continue FIELDS;
        }

        throw new RuntimeException("InZkPwHl1m :: Unknown topicContentType = " + topicContentType);
      }

      if (field.getAnnotation(TopicRef.class) != null) {
        final Class<?> type = field.getType();
        if (type == String.class) {
          fillers.add((target, record, profile) -> {
            final String topic = calculateTopic(record.topic(), topicPrefix);
            field.set(target, topic);
            return false;
          });
          continue FIELDS;
        }
        throw new RuntimeException("Eer1mfc20Q :: Cannot apply annotation @TopicRef to field with type " + type + ". Please use String");
      }

      if (field.getAnnotation(KafkaId.class) != null) {
        final Class<?> type = field.getType();
        if (type == String.class) {
          fillers.add((target, record, profile) -> {
            final Object value = record.value();
            if (!(value instanceof Box box)) {
              return true;
            }
            field.set(target, box.id);
            return false;
          });
          continue FIELDS;
        }
        throw new RuntimeException("kmBeiL9l8a :: Cannot apply annotation @KafkaId to field with type " + type + ". Please use String");
      }

      if (field.getAnnotation(Partition.class) != null) {
        final Class<?> type = field.getType();
        if (type == int.class || type == Integer.class) {
          fillers.add((target, record, profile) -> {
            final int partition = record.partition();
            field.set(target, partition);
            return false;
          });
          continue FIELDS;
        }
        throw new RuntimeException("sfnQKiw8dJ :: Cannot apply annotation @Partition to field with type " + type + "."
                                     + " Please use int or Integer");
      }

      if (field.getAnnotation(Offset.class) != null) {
        final Class<?> type = field.getType();
        if (type == long.class || type == Long.class) {
          fillers.add((target, record, profile) -> {
            final long offset = record.offset();
            field.set(target, offset);
            return false;
          });
          continue FIELDS;
        }
        throw new RuntimeException("LwzL2BSqFs :: Cannot apply annotation @Offset to field with type " + type + "."
                                     + " Please use long or Long");
      }

      if (field.getAnnotation(KafkaKeyRef.class) != null) {
        final Class<?> type = field.getType();
        if (type == String.class) {
          fillers.add((target, record, profile) -> {
            final byte[] keyBytes = record.key();
            field.set(target, new String(keyBytes, StandardCharsets.UTF_8));
            return false;
          });
          continue FIELDS;
        }
        if (type == byte[].class) {
          fillers.add((target, record, profile) -> {
            final byte[] keyBytes = record.key();
            field.set(target, keyBytes);
            return false;
          });
          continue FIELDS;
        }
        throw new RuntimeException("CgzQmIHjaX :: Cannot apply annotation @KafkaKeyRef to field with type " + type + "."
                                     + " Please use String or byte[]");
      }

      if (field.getAnnotation(HeadersRef.class) != null) {
        final Class<?> type = field.getType();
        if (type == Map.class) {
          fillers.add((target, record, profile) -> {
            Map<String, String> headMap = new HashMap<>();
            for (final Header header : record.headers()) {
              headMap.put(header.key(), new String(header.value(), StandardCharsets.UTF_8));
            }
            field.set(target, headMap);
            return false;
          });
          continue FIELDS;
        }
        throw new RuntimeException("STCfjEJA6D :: Cannot apply annotation @HeadersRef to field with type " + type + "."
                                     + " Please use Map<String, String>");
      }

      if (field.getAnnotation(Timestamp.class) != null) {
        final Class<?> type = field.getType();
        if (type == Date.class) {
          fillers.add((target, record, profile) -> {
            final long timestampLong = record.timestamp();
            field.set(target, new Date(timestampLong));
            return false;
          });
          continue FIELDS;
        }
        if (type == long.class || type == Long.class) {
          fillers.add((target, record, profile) -> {
            final long timestampLong = record.timestamp();
            field.set(target, timestampLong);
            return false;
          });
          continue FIELDS;
        }
        throw new RuntimeException("G7ExOhfUrm :: Cannot apply annotation @Timestamp to field with type " + type + "."
                                     + " Please use java.util.Date or long or Long");
      }
    }

  }

  @SneakyThrows
  private static @NonNull ModelCreator createModelCreator(@NonNull Class<?> listParamClass,
                                                          @SuppressWarnings("unused") Supplier<String> topicPrefix,
                                                          @SuppressWarnings("unused") TopicContentType topicContentType) {

    final Constructor<?> constructor = listParamClass.getConstructor();
    //noinspection Convert2Lambda
    return new ModelCreator() {
      @Override
      public @SneakyThrows Object create(@NonNull ConsumerRecord<byte[], Object> record,
                                         @NonNull Profile profile) {
        return constructor.newInstance();
      }
    };
  }

  public static String calculateTopic(String topicFromRecord, @NonNull Supplier<String> topicPrefix) {
    if (topicFromRecord == null) {
      return null;
    }

    final String prefix = topicPrefix.get();

    if (prefix == null) {
      return topicFromRecord;
    }

    if (topicFromRecord.startsWith(prefix)) {
      return topicFromRecord.substring(prefix.length());
    }

    return topicFromRecord;
  }
}
