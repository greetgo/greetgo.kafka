package kz.greetgo.kafka.consumer;

import java.util.Date;

public interface BoxMeta {
  Date getTime();

  byte[] getKey();

  String fromTopic();

  int fromPartition();

  long offset();

  Profile profile();
}
