package kz.greetgo.kafka.consumer;

public interface ConsumerReactor {

  boolean isWorking();

  ConsumerDefinition getConsumerDefinition();

  ConsumerReactor stop();

  void start();

  void join();

  void refreshTopicList();

  static ConsumerReactorBuilder builder() {
    return new ConsumerReactorBuilder();
  }

}
