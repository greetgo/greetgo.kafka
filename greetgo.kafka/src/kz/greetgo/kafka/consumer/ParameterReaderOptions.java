package kz.greetgo.kafka.consumer;

public interface ParameterReaderOptions {
  void acceptDeserializerError();
}
