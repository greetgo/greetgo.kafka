package kz.greetgo.kafka.consumer;

import kz.greetgo.kafka.model.Box;

public interface ConsumerFilter {

  boolean isInFilter(ConsumerDefinition consumerDefinition, Box box, byte[] recordKey);

}
