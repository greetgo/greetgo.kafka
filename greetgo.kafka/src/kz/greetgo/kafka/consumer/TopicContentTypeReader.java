package kz.greetgo.kafka.consumer;

import kz.greetgo.kafka.consumer.annotations.TopicContent;
import kz.greetgo.kafka.consumer.annotations.TopicContentType;

import java.lang.reflect.Method;

import static kz.greetgo.kafka.util.AnnotationUtil.getAnnotation;

public class TopicContentTypeReader {

  public static TopicContentType readFrom(Method method) {
    TopicContent tc = getAnnotation(method, TopicContent.class);
    return tc == null ? TopicContentType.BOX : tc.value();
  }

}
