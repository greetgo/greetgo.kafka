package kz.greetgo.kafka.consumer;

import lombok.NonNull;
import org.apache.kafka.clients.consumer.ConsumerRecord;

public interface ModelCreator {
  Object create(@NonNull ConsumerRecord<byte[], Object> record, @NonNull Profile profile);
}
