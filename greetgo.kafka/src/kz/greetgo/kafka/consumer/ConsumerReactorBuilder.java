package kz.greetgo.kafka.consumer;

import kz.greetgo.kafka.consumer.config.ConsumerConnectSetter;
import kz.greetgo.kafka.consumer.config.ConsumerReactorConfig;
import kz.greetgo.kafka.core.ProducerSynchronizer;
import kz.greetgo.kafka.core.logger.Logger;
import kz.greetgo.strconverter.StrConverter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.BooleanSupplier;
import java.util.function.Supplier;

import static java.util.Objects.requireNonNull;

public class ConsumerReactorBuilder {

  private Logger                 logger;
  private ProducerSynchronizer   producerSynchronizer;
  private Supplier<StrConverter> strConverter;
  private ConsumerDefinition     consumerDefinition;
  private Supplier<String>       consumerThreadPrefix = () -> "kafka-consumer-";
  private ConsumerReactorConfig  consumerConfig;
  private ConsumerConnectSetter  consumerConnectSetter;

  private final List<BooleanSupplier> externalWorkingSupplierList = Collections.synchronizedList(new ArrayList<>());

  ConsumerReactorBuilder() {}

  public ConsumerReactorBuilder consumerConfig(ConsumerReactorConfig consumerConfig) {
    this.consumerConfig = requireNonNull(consumerConfig);
    return this;
  }

  public ConsumerReactorBuilder logger(Logger logger) {
    this.logger = requireNonNull(logger);
    return this;
  }

  public ConsumerReactorBuilder producerSynchronizer(ProducerSynchronizer producerSynchronizer) {
    this.producerSynchronizer = requireNonNull(producerSynchronizer);
    return this;
  }

  public ConsumerReactorBuilder strConverter(Supplier<StrConverter> strConverter) {
    this.strConverter = requireNonNull(strConverter);
    return this;
  }

  public ConsumerReactorBuilder consumerDefinition(ConsumerDefinition consumerDefinition) {
    this.consumerDefinition = requireNonNull(consumerDefinition);
    return this;
  }

  public ConsumerReactorBuilder consumerConnectSetter(ConsumerConnectSetter consumerConnectSetter) {
    this.consumerConnectSetter = consumerConnectSetter;
    return this;
  }

  public ConsumerReactorBuilder consumerThreadPrefix(Supplier<String> consumerThreadPrefix) {
    this.consumerThreadPrefix = requireNonNull(consumerThreadPrefix);
    return this;
  }

  public ConsumerReactorBuilder addExternalWorkingSupplierAND(BooleanSupplier workingSupplier) {
    if (workingSupplier != null) {
      externalWorkingSupplierList.add(workingSupplier);
    }
    return this;
  }

  public ConsumerReactor build() {
    var reactor = new ConsumerReactorImpl();

    reactor.logger                = requireNonNull(logger);
    reactor.producerSynchronizer  = requireNonNull(producerSynchronizer);
    reactor.strConverter          = requireNonNull(strConverter);
    reactor.consumerDefinition    = requireNonNull(consumerDefinition);
    reactor.consumerConnectSetter = requireNonNull(consumerConnectSetter);
    reactor.consumerConfig        = requireNonNull(consumerConfig);
    reactor.consumerThreadPrefix  = requireNonNull(consumerThreadPrefix);

    reactor.externalWorkingSupplier = () -> {
      for (final BooleanSupplier bs : externalWorkingSupplierList) {
        if (!bs.getAsBoolean()) {
          return false;
        }
      }
      return true;
    };

    return reactor;
  }

}
