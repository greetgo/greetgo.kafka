package kz.greetgo.kafka.consumer;

import kz.greetgo.kafka.consumer.config.ConsumerConnectParams;
import kz.greetgo.kafka.consumer.config.ConsumerConnectSetter;
import kz.greetgo.kafka.consumer.config.ConsumerReactorConfig;
import kz.greetgo.kafka.core.ConsumerPortionInvoking;
import kz.greetgo.kafka.core.ProducerSynchronizer;
import kz.greetgo.kafka.core.logger.Logger;
import kz.greetgo.kafka.serializer.BoxDeserializer;
import kz.greetgo.kafka.serializer.BytesDeserializer;
import kz.greetgo.kafka.serializer.StrDeserializer;
import kz.greetgo.kafka.util.KafkaTopicUtil;
import kz.greetgo.kafka.util.Listener;
import kz.greetgo.strconverter.StrConverter;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.ByteArrayDeserializer;
import org.apache.kafka.common.serialization.Deserializer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.BooleanSupplier;
import java.util.function.Supplier;

import static java.util.Objects.requireNonNull;
import static kz.greetgo.kafka.core.logger.LoggerType.LOG_CONSUMER_COMMIT_SYNC_EXCEPTION_HAPPENED;
import static kz.greetgo.kafka.core.logger.LoggerType.LOG_CONSUMER_FINISH_WORKER;
import static kz.greetgo.kafka.core.logger.LoggerType.LOG_CONSUMER_POLL_EXCEPTION_HAPPENED;
import static kz.greetgo.kafka.core.logger.LoggerType.LOG_CONSUMER_POLL_PERFORM_DELAY;
import static kz.greetgo.kafka.core.logger.LoggerType.LOG_CONSUMER_POLL_ZERO;
import static kz.greetgo.kafka.core.logger.LoggerType.LOG_CONSUMER_REACTOR_REFRESH;
import static kz.greetgo.kafka.core.logger.LoggerType.LOG_START_CONSUMER_WORKER;
import static kz.greetgo.kafka.core.logger.LoggerType.SHOW_CONSUMER_WORKER_CONFIG;

public class ConsumerReactorImpl implements ConsumerReactor {

  Logger                 logger;
  ProducerSynchronizer   producerSynchronizer;
  Supplier<StrConverter> strConverter;
  ConsumerDefinition     consumerDefinition;
  ConsumerReactorConfig  consumerConfig;
  Supplier<String>       consumerThreadPrefix;
  BooleanSupplier        externalWorkingSupplier;
  ConsumerConnectSetter  consumerConnectSetter;

  private final AtomicBoolean                   working   = new AtomicBoolean(true);
  private final ConcurrentHashMap<Long, Worker> workers   = new ConcurrentHashMap<>();
  private final AtomicLong                      nextValue = new AtomicLong(1);

  private final ConcurrentHashMap<Profile, Set<String>> currentPrefixedTopics = new ConcurrentHashMap<>();

  private final ConcurrentLinkedQueue<Listener>     listenerQueue       = new ConcurrentLinkedQueue<>();
  private final ConcurrentHashMap<Profile, Integer> profileListeningMap = new ConcurrentHashMap<>();
  private final Object                              sync                = new Object();

  private void ensureListenProfileConfig(Profile profile) {
    if (!working.get()) {
      return;
    }

    if (profileListeningMap.containsKey(profile)) {
      return;
    }
    synchronized (sync) {
      if (profileListeningMap.containsKey(profile)) {
        return;
      }
      listenerQueue.add(consumerConfig.onChangeWorkerCount(() -> correctWorkersCount(profile), profile));
      listenerQueue.add(consumerConfig.onChangeParams(() -> restartAllWorkerForProfile(profile), profile));
      profileListeningMap.put(profile, 1);
    }
  }

  private void killAllListeners() {
    while (true) {
      final Listener listener = listenerQueue.poll();
      if (listener == null) {
        return;
      }
      listener.kill();
    }
  }

  @Override
  public void refreshTopicList() {

    if (!isWorking()) {
      currentPrefixedTopics.clear();
      return;
    }

    final Map<Profile, Set<String>> newTopics = KafkaTopicUtil.topicsToMap(consumerDefinition.prefixedTopicList());
    if (currentPrefixedTopics.equals(newTopics)) {
      return;
    }
    currentPrefixedTopics.clear();
    currentPrefixedTopics.putAll(newTopics);
    currentPrefixedTopics.keySet().forEach(this::ensureListenProfileConfig);

    restartAllWorkers();
  }

  private Deserializer<Object> createValueDeserializer() {
    return switch (consumerDefinition.topicContentType()) {
      case BOX, LIST_BOX -> new BoxDeserializer(strConverter.get());
      case STR -> new StrDeserializer();
      case BYTES -> new BytesDeserializer();
    };
  }

  private class Worker extends Thread {
    private final long        id = nextValue.getAndIncrement();
    private final Profile     workerProfile;
    private final Set<String> prefixedTopics;

    private Worker(Profile workerProfile) {
      requireNonNull(workerProfile, "37t58W6jtS :: workerProfile == null");
      this.workerProfile  = workerProfile;
      this.prefixedTopics = currentPrefixedTopics.get(workerProfile);
    }

    private boolean isInCircle() {
      return (prefixedTopics != null && prefixedTopics.size() > 0) && (isWorking() && workers.containsKey(id));
    }

    @Override
    public void run() {
      try {

        Thread.currentThread().setName(consumerThreadPrefix.get() + consumerDefinition.logDisplay() + "-" + id);

        if (logger.isShow(LOG_START_CONSUMER_WORKER)) {
          logger.logConsumerStartWorker(consumerDefinition, id, workerProfile);
        }

        Map<String, Object> configMap = new HashMap<>(consumerConfig.getConfigMap(workerProfile));
        configMap.put("auto.offset.reset", consumerDefinition.getAutoOffsetReset().name().toLowerCase());
        configMap.put("group.id", consumerDefinition.getGroupId());
        configMap.put("enable.auto.commit", consumerDefinition.isAutoCommit() ? "true" : "false");

        consumerConnectSetter.setConnect(configMap, new ConsumerConnectParams() {
          @Override
          public Profile profile() {
            return workerProfile;
          }

          @Override
          public Collection<String> nativeTopics() {
            return prefixedTopics;
          }
        });

        if (logger.isShow(SHOW_CONSUMER_WORKER_CONFIG)) {
          logger.logConsumerWorkerConfig(consumerDefinition, id, configMap, workerProfile);
        }

        ByteArrayDeserializer forKey               = new ByteArrayDeserializer();
        Deserializer<Object>  forValue             = createValueDeserializer();
        InvokeSessionFactory  invokeSessionFactory = consumerDefinition.getInvokeSessionFactory();

        try (InvokeSession invokeSession = invokeSessionFactory.createSession()) {

          OUT:
          while (isInCircle()) {

            try (KafkaConsumer<byte[], Object> consumer = new KafkaConsumer<>(configMap, forKey, forValue)) {
              consumer.subscribe(prefixedTopics);

              INNER:
              while (isInCircle()) {

                final ConsumerRecords<byte[], Object> records;

                try {
                  records = consumer.poll(consumerConfig.getPollDuration(workerProfile));

                  if (records.count() == 0) {
                    if (logger.isShow(LOG_CONSUMER_POLL_ZERO)) {
                      logger.logConsumerPollZero(consumerDefinition);
                    }
                    continue INNER;
                  }

                } catch (RuntimeException exception) {
                  if (logger.isShow(LOG_CONSUMER_POLL_EXCEPTION_HAPPENED)) {
                    logger.logConsumerPollExceptionHappened(exception, consumerDefinition, workerProfile);
                  }
                  working.set(false);
                  continue OUT;
                }

                long startPollPerforming = System.currentTimeMillis();
                try {

                  try (ConsumerPortionInvoking ignore = producerSynchronizer.startPortionInvoking()) {
                    if (!invokeSession.invoke(records, workerProfile).needToCommit()) {
                      continue OUT;
                    }
                  }

                  try {
                    consumer.commitSync();
                  } catch (RuntimeException exception) {
                    if (logger.isShow(LOG_CONSUMER_COMMIT_SYNC_EXCEPTION_HAPPENED)) {
                      logger.logConsumerCommitSyncExceptionHappened(exception, consumerDefinition);
                    }
                    continue OUT;
                  }


                } finally {
                  long pollPerformDelay = System.currentTimeMillis() - startPollPerforming;
                  if (logger.isShow(LOG_CONSUMER_POLL_PERFORM_DELAY)) {
                    logger.logConsumerPollPerformDelay(consumerDefinition, pollPerformDelay, records);
                  }
                }
              }
            }

          }

        }

      } finally {

        workers.remove(id);

        if (logger.isShow(LOG_CONSUMER_FINISH_WORKER)) {
          logger.logConsumerFinishWorker(consumerDefinition, id, workerProfile);
        }

      }

    }

  }

  @Override
  public boolean isWorking() {
    return working.get() && externalWorkingSupplier.getAsBoolean();
  }

  ConsumerReactorImpl() {}

  @Override
  public ConsumerDefinition getConsumerDefinition() {
    return consumerDefinition;
  }

  @Override
  public ConsumerReactor stop() {
    working.set(false);
    killAllListeners();
    return this;
  }

  private final AtomicBoolean started = new AtomicBoolean(false);

  /**
   * Start reactor
   */
  @Override
  public void start() {
    if (!working.get()) {
      return;
    }

    if (!started.compareAndSet(false, true)) {
      return;
    }

    correctWorkersCountForAllProfiles();
  }

  /**
   * Waits for all consumers to finish their work.
   * <p>
   * You can call this method after called {@link #stop()} to wait for all consumer finished their work.
   */
  @Override
  public void join() {

    while (workers.size() > 0) {

      final ArrayList<Worker> list = new ArrayList<>(workers.values());

      for (final Worker worker : list) {
        try {
          worker.join();
        } catch (InterruptedException e) {
          return;
        }
      }

    }
  }

  private void restartAllWorkers() {
    if (!isWorking()) {
      return;
    }

    workers.clear();
    correctWorkersCountForAllProfiles();
  }

  private void restartAllWorkerForProfile(Profile profile) {
    List<Long> toDelete = new ArrayList<>();
    for (Map.Entry<Long, Worker> e : workers.entrySet()) {
      if (Objects.equals(profile, e.getValue().workerProfile) && e.getValue().isInCircle()) {
        toDelete.add(e.getKey());
      }
    }
    toDelete.forEach(workers::remove);

    correctWorkersCount(profile);
  }

  private void correctWorkersCountForAllProfiles() {
    if (!isWorking()) {
      return;
    }

    currentPrefixedTopics.keySet().forEach(this::correctWorkersCount);
  }

  private void correctWorkersCount(Profile profile) {
    if (!isWorking()) {
      return;
    }

    requireNonNull(profile, "mmG8FjXqj2 :: profile == null");

    Set<Long> toDelete = new HashSet<>();

    int currentCount = 0;

    for (Map.Entry<Long, Worker> e : workers.entrySet()) {

      if (!Objects.equals(profile, e.getValue().workerProfile)) {
        continue;
      }

      if (!e.getValue().isInCircle()) {
        toDelete.add(e.getKey());
      } else {
        currentCount++;
      }
    }

    toDelete.forEach(workers::remove);

    int needCount = consumerConfig.getWorkerCount(profile);
    ensureListenProfileConfig(profile);

    if (logger.isShow(LOG_CONSUMER_REACTOR_REFRESH)) {
      logger.logConsumerReactorRefresh(consumerDefinition, currentCount, needCount, profile);
    }

    if (needCount > currentCount) {
      appendWorker(profile, needCount - currentCount);
    } else if (currentCount > needCount) {
      removeWorkers(currentCount - needCount, profile);
    }
  }

  private void appendWorker(Profile profile, int count) {
    for (int i = 0; i < count; i++) {
      Worker worker = new Worker(profile);
      workers.put(worker.id, worker);
      worker.start();
    }
  }

  private void removeWorkers(int countToRemove, Profile profile) {
    int removed = 0;

    INFINITY_CIRCLE:
    while (removed < countToRemove) {

      for (final Map.Entry<Long, Worker> e : workers.entrySet()) {
        final Worker worker = e.getValue();
        if (profile.equals(worker.workerProfile) && worker.isInCircle()) {
          workers.remove(e.getKey());
          removed++;

          continue INFINITY_CIRCLE;
        }
      }

      break INFINITY_CIRCLE;
    }
  }

}
