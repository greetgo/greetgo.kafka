package kz.greetgo.kafka.consumer;

public class ConsumerDirectStatus {
  public final boolean isDirect;
  public final boolean ignoreKafka;

  private ConsumerDirectStatus(boolean isDirect, boolean ignoreKafka) {
    this.isDirect    = isDirect;
    this.ignoreKafka = ignoreKafka;
  }

  public static ConsumerDirectStatus of(boolean isDirect, boolean ignoreKafka) {
    return new ConsumerDirectStatus(isDirect, ignoreKafka);
  }
}
