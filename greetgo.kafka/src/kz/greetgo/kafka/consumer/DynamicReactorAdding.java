package kz.greetgo.kafka.consumer;

import java.util.Collection;

public interface DynamicReactorAdding {

  DynamicReactorAdding name(String name);

  DynamicReactorAdding topic(String topic);

  DynamicReactorAdding profile(String profile);

  DynamicReactorAdding autoOffsetReset(AutoOffsetReset autoOffsetReset);

  DynamicReactorAdding customConsumer(CustomBoxConsumer customConsumer);

  DynamicReactorAdding customStrConsumer(CustomStrConsumer customStrConsumer);

  DynamicReactorAdding customBytesConsumer(CustomBytesConsumer customBytesConsumer);

  DynamicReactorAdding customListBoxConsumer(CustomListBoxConsumer customBytesConsumer);

  @SuppressWarnings("unused")
  DynamicReactorAdding commitOn(Class<?> errorClass);

  @SuppressWarnings("unused")
  DynamicReactorAdding commitOnAll(Collection<Class<?>> errorClassAll);

  DynamicReactorAdding groupId(String groupId);

  @SuppressWarnings("unused")
  DynamicReactorAdding folderPath(String folderPath);

  @SuppressWarnings("unused")
  DynamicReactorAdding workerCountFileName(String workerCountFileName);

  DynamicReactorAdding description(String description);

  @SuppressWarnings("unused")
  DynamicReactorAdding consumerFilter(ConsumerFilter consumerFilter);

  @SuppressWarnings("unused")
  DynamicReactorAdding direct(boolean direct, boolean ignoreKafka);

  DynamicReactor add();

}
