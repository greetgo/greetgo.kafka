package kz.greetgo.kafka.consumer.annotations;

import kz.greetgo.kafka.consumer.config.ConsumerConnectParams;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Defined consumer controller method profile. Profile defines connections to kafka.
 * Different profiles define different kafka broker clusters.
 * <p>
 * This annotation contains only one method, witch returns profile name as string.
 * This string will be returned from method {@link ConsumerConnectParams#profile()} to detect kafka cluster to define connect data
 */
@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface KafkaProfile {
  String value();
}
