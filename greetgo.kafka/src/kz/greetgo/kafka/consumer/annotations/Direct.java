package kz.greetgo.kafka.consumer.annotations;

import kz.greetgo.kafka.consumer.ConsumerDirectStatus;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks consumer method to be direct called from producer outside kafka
 */
@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Direct {
  /**
   * Whether to process the given record through kafka
   *
   * @return if it returns true, then this entry will be processed by kafka, otherwise - no
   */
  boolean ignoreKafka() default false;

  /**
   * Specifies the name of the method in the controller,
   * the results of which will determine the parameters of the direct call to the consumer.
   * <p>
   * If method name is empty, then make this consumer is direct and use field {@link #ignoreKafka()} to detect ignoring kafka.
   *
   * @return the name of the method in the controller that should return the {@link ConsumerDirectStatus} object.
   * This object will define the recipient's processing options.
   */
  String fromMethod() default "";
}
