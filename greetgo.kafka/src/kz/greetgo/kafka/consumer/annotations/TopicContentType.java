package kz.greetgo.kafka.consumer.annotations;

/**
 * Describe method of reading data from topic
 */
public enum TopicContentType {

  /**
   * Topic contents Box objects.
   */
  BOX,

  /**
   * Topic contents string in UTF-8 format
   */
  STR,

  /**
   * Topic contents any bytes
   */
  BYTES,

  /**
   * Topic contents list of Box objects.
   */
  LIST_BOX
}
