package kz.greetgo.kafka.consumer.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks consumer controller method to define name of another method (topic-list-method) in annotation method `value()`,
 * <p>
 * Topic-list-method will be called to define list of topics to listen by consumer method marked by this annotation.
 * <p>
 * Topic-list-method will be regularly call to check, does list ot topics change. If it is,
 * then system restarts consumer mechanism of this consumer method to use new topics.
 * <p>
 * Topic-list-method must return one of the following types:
 * <ul>
 *   <li>Collection&lt;String&gt;</li>
 * </ul>
 */
@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface TopicListMethod {

  /**
   * @return a name of consumer controller class method, witch will be called to defined list of topics to subscribe to.
   */
  String value();

}
