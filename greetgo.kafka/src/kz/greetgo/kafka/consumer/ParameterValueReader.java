package kz.greetgo.kafka.consumer;

import org.apache.kafka.clients.consumer.ConsumerRecord;

import javax.annotation.Nonnull;

public interface ParameterValueReader {
  Object read(@Nonnull ConsumerRecord<byte[], Object> record,
              @Nonnull InvokeSessionContext invokeSessionContext,
              @Nonnull Profile profile);

  default Class<?> gettingBodyClass() {
    return null;
  }

}
