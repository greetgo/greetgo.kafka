package kz.greetgo.kafka.consumer;

public interface CustomBytesConsumer {

  void execute(byte[] recordBytes, BoxMeta boxMeta) throws Exception;

}
