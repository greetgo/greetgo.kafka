package kz.greetgo.kafka.consumer;

import kz.greetgo.kafka.consumer.annotations.HeadersRef;
import kz.greetgo.kafka.consumer.annotations.KafkaId;
import kz.greetgo.kafka.consumer.annotations.Offset;
import kz.greetgo.kafka.consumer.annotations.Partition;
import kz.greetgo.kafka.consumer.annotations.ProfileRef;
import kz.greetgo.kafka.consumer.annotations.Timestamp;
import kz.greetgo.kafka.consumer.annotations.TopicContentType;
import kz.greetgo.kafka.consumer.annotations.TopicRef;
import kz.greetgo.kafka.errors.IllegalParameterType;
import kz.greetgo.kafka.model.Box;
import kz.greetgo.kafka.serializer.DeserializerError;
import kz.greetgo.kafka.util.GenericUtil;
import lombok.NonNull;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.header.Header;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

import static kz.greetgo.kafka.util.GenericUtil.extractClass;
import static kz.greetgo.kafka.util.GenericUtil.isOfClass;

public class ParameterValueReaderUtil {

  public static @NonNull ParameterValueReader createParameterValueReader(@NonNull Type parameterType,
                                                                         Annotation[] parameterAnnotations,
                                                                         TopicContentType topicContentType,
                                                                         ParameterReaderOptions parameterReaderOptions,
                                                                         Supplier<String> topicPrefix) {

    for (Annotation annotation : parameterAnnotations) {

      if (annotation instanceof Partition) {
        if (!isOfClass(parameterType, int.class) && !isOfClass(parameterType, Integer.class)) {
          throw new IllegalParameterType("Parameter with @Partition must be `int` or `Integer`");
        }

        return (record, invokeSessionContext, profile) -> record.partition();
      }

      if (annotation instanceof Offset) {
        if (!isOfClass(parameterType, long.class) && !isOfClass(parameterType, Long.class)) {
          throw new IllegalParameterType("Parameter with @Offset must be `long` or `Long`");
        }

        return (record, invokeSessionContext, profile) -> record.offset();
      }

      if (annotation instanceof TopicRef) {
        if (!isOfClass(parameterType, String.class)) {
          throw new IllegalParameterType("Parameter with @" + TopicRef.class.getSimpleName() + " must be `String`");
        }

        return (record, invokeSessionContext, profile) -> RecordToClassConverter.calculateTopic(record.topic(), topicPrefix);
      }

      if (annotation instanceof Timestamp) {
        if (isOfClass(parameterType, Date.class)) {
          return (record, invokeSessionContext, profile) -> new Date(record.timestamp());
        }
        if (isOfClass(parameterType, long.class) || isOfClass(parameterType, Long.class)) {
          return (record, invokeSessionContext, profile) -> record.timestamp();
        }

        throw new IllegalParameterType("Parameter with @Offset must be `long` or `Long` or `java.util.Date`");
      }

      if (annotation instanceof KafkaId) {
        if (!isOfClass(parameterType, String.class)) {
          throw new IllegalParameterType("Parameter with @" + KafkaId.class.getSimpleName() + " must be `String`");
        }

        return (record, invokeSessionContext, profile) -> record.value() instanceof Box ? ((Box) record.value()).id : null;
      }

      if (annotation instanceof ProfileRef) {
        if (isOfClass(parameterType, String.class)) {
          return (record, invokeSessionContext, profile) -> profile.profile;
        }

        if (isOfClass(parameterType, Profile.class)) {
          return (record, invokeSessionContext, profile) -> profile;
        }

        throw new IllegalParameterType("Parameter with @" + ProfileRef.class.getSimpleName()
                                         + " must be `String` or `" + Profile.class.getSimpleName() + "`");
      }

      if (annotation instanceof HeadersRef) {
        return (record, invokeSessionContext, profile) -> {
          Map<String, String> ret = new HashMap<>();
          for (final Header header : record.headers()) {
            final String key   = header.key();
            final byte[] value = header.value();
            ret.put(key, value == null ? "" : new String(value, StandardCharsets.UTF_8));
          }
          return ret;
        };
      }

    }

    if (isOfClass(parameterType, DeserializerError.class)) {
      parameterReaderOptions.acceptDeserializerError();
      return (record, invokeSessionContext, profile) -> GenericUtil.classOrNull(DeserializerError.class, record.value());
    }

    return switch (topicContentType) {
      case BOX, LIST_BOX -> {
        if (isOfClass(parameterType, Box.class)) {
          yield (record, invokeSessionContext, profile) -> GenericUtil.classOrNull(Box.class, record.value());
        }
        yield new ParameterValueReader() {
          @Override
          public Object read(@NonNull ConsumerRecord<byte[], Object> record,
                             @NonNull InvokeSessionContext invokeSessionContext,
                             @NonNull Profile profile) {
            Object recordValue = record.value();
            return recordValue instanceof Box ? ((Box) recordValue).body : null;
          }

          @Override
          public Class<?> gettingBodyClass() {
            return extractClass(parameterType);
          }
        };
      }
      case STR -> new ParameterValueReader() {
        @Override
        public Object read(@NonNull ConsumerRecord<byte[], Object> record,
                           @NonNull InvokeSessionContext invokeSessionContext,
                           @NonNull Profile profile) {
          Object recordValue = record.value();
          if (recordValue == null) {
            return null;
          }
          if (recordValue instanceof String) {
            return recordValue;
          }
          return recordValue.toString();
        }

        @Override
        public Class<?> gettingBodyClass() {
          return extractClass(parameterType);
        }
      };
      case BYTES -> new ParameterValueReader() {
        @Override
        public Object read(@NonNull ConsumerRecord<byte[], Object> record,
                           @NonNull InvokeSessionContext invokeSessionContext,
                           @NonNull Profile profile) {
          return record.value();
        }

        @Override
        public Class<?> gettingBodyClass() {
          return extractClass(parameterType);
        }
      };
    };


  }

}
