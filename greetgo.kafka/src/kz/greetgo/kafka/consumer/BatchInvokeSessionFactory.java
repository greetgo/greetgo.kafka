package kz.greetgo.kafka.consumer;

import lombok.NonNull;
import org.apache.kafka.clients.consumer.ConsumerRecords;

public class BatchInvokeSessionFactory implements InvokeSessionFactory {

  private final BatchInvoker batchInvoker;

  public BatchInvokeSessionFactory(@NonNull BatchInvoker batchInvoker) {
    this.batchInvoker = batchInvoker;
  }

  @Override
  public InvokeSession createSession() {
    final InvokeSessionContext context = new InvokeSessionContext();

    return new InvokeSession() {
      @Override
      public InvokeResult invoke(ConsumerRecords<byte[], Object> records, Profile profile) {
        return batchInvoker.invoke(records, context, profile);
      }

      @Override
      public void close() {
        context.close();
      }
    };
  }
}
