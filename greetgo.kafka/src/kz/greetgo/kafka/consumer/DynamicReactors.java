package kz.greetgo.kafka.consumer;

import java.util.Collection;
import java.util.function.Predicate;

/**
 * Collection of dynamic consumer reactors
 */
public interface DynamicReactors {

  /**
   * Starts to create and add a new reactor with specified consumer definition
   *
   * @return dynamic reactor adding mechanism
   */
  DynamicReactorAdding adding();

  /**
   * Returns all working reactors
   *
   * @return all working reactors
   */
  Collection<DynamicReactor> reactors();

  /**
   * Stops reactor by id
   *
   * @param id id of reactor
   * @return stopped reactor or null, if such id does not exist
   */
  DynamicReactor stopById(long id);

  /**
   * Stop reactors by filter
   *
   * @param filter filter for reactors to stop
   * @return stopped reactors
   */
  @SuppressWarnings("UnusedReturnValue")
  Collection<DynamicReactor> stopByFilter(Predicate<DynamicReactor> filter);

}
