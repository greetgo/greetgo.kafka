package kz.greetgo.kafka.consumer;

import kz.greetgo.kafka.model.Box;

public interface BoxFilter {

  boolean isInFilter(Box box, byte[] recordKey);

}
