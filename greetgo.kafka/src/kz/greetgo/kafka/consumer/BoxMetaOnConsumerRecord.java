package kz.greetgo.kafka.consumer;

import org.apache.kafka.clients.consumer.ConsumerRecord;

import javax.annotation.Nonnull;
import java.util.Date;

class BoxMetaOnConsumerRecord implements BoxMeta {
  private final ConsumerRecord<byte[], Object> record;
  private final Profile                        profile;

  public BoxMetaOnConsumerRecord(@Nonnull ConsumerRecord<byte[], Object> record, @Nonnull Profile profile) {
    this.record  = record;
    this.profile = profile;
  }

  @Override
  public Date getTime() {
    return new Date(record.timestamp());
  }

  @Override
  public byte[] getKey() {
    return record.key();
  }

  @Override
  public String fromTopic() {
    return record.topic();
  }

  @Override
  public int fromPartition() {
    return record.partition();
  }

  @Override
  public long offset() {
    return record.offset();
  }

  @Override
  public Profile profile() {
    return profile;
  }
}
