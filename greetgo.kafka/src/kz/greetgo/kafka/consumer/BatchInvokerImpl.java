package kz.greetgo.kafka.consumer;

import kz.greetgo.kafka.core.logger.Logger;
import kz.greetgo.kafka.producer.KafkaFuture;
import kz.greetgo.kafka.util.GenericUtil;
import org.apache.kafka.clients.consumer.ConsumerRecords;

import javax.annotation.Nonnull;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.function.BiFunction;

import static kz.greetgo.kafka.core.logger.LoggerType.LOG_CONSUMER_ERROR_INVOKING;
import static kz.greetgo.kafka.core.logger.LoggerType.LOG_CONSUMER_ILLEGAL_ACCESS_EXCEPTION_INVOKING_METHOD;

public class BatchInvokerImpl implements BatchInvoker {

  private final Object                                                         controller;
  private final Method                                                         method;
  private final BiFunction<ConsumerRecords<byte[], Object>, Profile, Object[]> recordsToListConverter;
  private final Class<?>[]                                                     commitOn;
  private final Logger                                                         logger;

  public BatchInvokerImpl(Object controller,
                          Method method,
                          BiFunction<ConsumerRecords<byte[], Object>, Profile, Object[]> recordsToListConverter,
                          Class<?>[] commitOn, Logger logger) {
    this.controller             = controller;
    this.method                 = method;
    this.recordsToListConverter = recordsToListConverter;
    this.commitOn               = commitOn;
    this.logger                 = logger;
  }

  @Override
  public InvokeResult invoke(@Nonnull ConsumerRecords<byte[], Object> records,
                             @Nonnull InvokeSessionContext context,
                             @Nonnull Profile profile) {
    final Object[] arguments = recordsToListConverter.apply(records, profile);

    try {
      context.kafkaFutures.clear();
      method.invoke(controller, arguments);
      context.kafkaFutures.forEach(KafkaFuture::awaitAndGet);
      context.kafkaFutures.clear();
    } catch (IllegalAccessException e) {
      if (logger.isShow(LOG_CONSUMER_ILLEGAL_ACCESS_EXCEPTION_INVOKING_METHOD)) {
        logger.logConsumerIllegalAccessExceptionInvoking(e, GenericUtil.infoOf(controller, method));
      }
      return InvokeResult.of(false, e);
    } catch (InvocationTargetException e) {
      Throwable error = e.getTargetException();

      if (logger.isShow(LOG_CONSUMER_ERROR_INVOKING)) {
        logger.logConsumerErrorInvoking(error, GenericUtil.infoOf(controller, method), profile);
      }

      return InvokeResult.of(false, error);
    }

    return InvokeResult.ok();
  }

}
