package kz.greetgo.kafka.consumer;

import kz.greetgo.kafka.core.logger.Logger;
import kz.greetgo.kafka.model.Box;
import org.apache.kafka.clients.consumer.ConsumerRecord;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static java.util.Objects.requireNonNull;
import static kz.greetgo.kafka.core.logger.LoggerType.LOG_CONSUMER_ERROR_INVOKING;

public class InvokeSessionFactoryCustomBuilder {
  private       CustomBoxConsumer     customBoxConsumer;
  private       CustomStrConsumer     customStrConsumer;
  private       CustomBytesConsumer   customBytesConsumer;
  private       CustomListBoxConsumer customListBoxConsumer;
  private       Logger                logger;
  private       String                consumerName;
  private final List<Class<?>>        commitOnList = new ArrayList<>();
  private       BoxFilter             boxFilter    = (box, key) -> true;

  public InvokeSessionFactoryCustomBuilder customBoxConsumer(CustomBoxConsumer customBoxConsumer) {
    this.customBoxConsumer = customBoxConsumer;
    return this;
  }

  public InvokeSessionFactoryCustomBuilder customStrConsumer(CustomStrConsumer customStrConsumer) {
    this.customStrConsumer = customStrConsumer;
    return this;
  }

  public InvokeSessionFactoryCustomBuilder customBytesConsumer(CustomBytesConsumer customBytesConsumer) {
    this.customBytesConsumer = customBytesConsumer;
    return this;
  }

  public InvokeSessionFactoryCustomBuilder customListBoxConsumer(CustomListBoxConsumer customListBoxConsumer) {
    this.customListBoxConsumer = customListBoxConsumer;
    return this;
  }

  @SuppressWarnings("unused")
  public InvokeSessionFactoryCustomBuilder consumerName(String consumerName) {
    this.consumerName = consumerName;
    return this;
  }

  public InvokeSessionFactoryCustomBuilder logger(Logger logger) {
    this.logger = logger;
    return this;
  }

  @SuppressWarnings("UnusedReturnValue")
  public InvokeSessionFactoryCustomBuilder commitOn(Class<?> aClass) {
    commitOnList.add(aClass);
    return this;
  }

  public InvokeSessionFactoryCustomBuilder commitOnAll(Collection<Class<?>> aClassCollection) {
    aClassCollection.forEach(this::commitOn);
    return this;
  }

  public InvokeSessionFactoryCustomBuilder boxFilter(BoxFilter boxFilter) {
    this.boxFilter = requireNonNull(boxFilter, "36kb80b3tF :: boxFilter == null");
    return this;
  }

  public InvokeSessionFactory build() {

    if (customBoxConsumer == null && customStrConsumer == null && customBytesConsumer == null && customListBoxConsumer == null) {
      throw new RuntimeException("5WGhRI4RUN :: Consumer function is undefined. Please, define consumer function");
    }

    requireNonNull(boxFilter, "WmXnTYGmSl :: boxFilter");

    return customListBoxConsumer != null ? batchInvoker() : singleInvoker();
  }

  private InvokeSessionFactory singleInvoker() {

    return new SingleInvokeSessionFactoryImpl(logger, new SingleInvoker() {
      @Override
      public boolean isInFilter(@Nonnull ConsumerRecord<byte[], Object> record) {
        Object x = record.value();
        return x instanceof Box ? boxFilter.isInFilter((Box) x, record.key()) : true;
      }

      @Override
      public InvokeResult invoke(@Nonnull ConsumerRecord<byte[], Object> record,
                                 @Nonnull InvokeSessionContext context,
                                 @Nonnull Profile profile) {

        try {
          final Object message = record.value();
          final var    boxMeta = new BoxMetaOnConsumerRecord(record, profile);

          {
            final CustomBoxConsumer c = customBoxConsumer;
            if (c != null && message instanceof Box) {
              c.execute((Box) message, boxMeta);
              return InvokeResult.ok();
            }
          }
          {
            final CustomStrConsumer c = customStrConsumer;
            if (c != null && message instanceof CharSequence) {
              c.execute(message.toString(), boxMeta);
              return InvokeResult.ok();
            }
          }
          {
            final CustomBytesConsumer c = customBytesConsumer;
            if (c != null && message instanceof byte[]) {
              c.execute((byte[]) message, boxMeta);
              return InvokeResult.ok();
            }
          }

          throw new RuntimeException("9wfq5yLzGK :: Consumer function is not defined");
        } catch (Exception e) {

          if (logger.isShow(LOG_CONSUMER_ERROR_INVOKING)) {
            logger.logConsumerErrorInvoking(e, consumerName, profile);
          }

          for (final Class<?> errorClass : commitOnList) {
            if (errorClass.isInstance(e)) {
              return InvokeResult.of(true, e);
            }
          }
          return InvokeResult.of(false, e);
        }

      }
    });

  }

  private InvokeSessionFactory batchInvoker() {

    return new BatchInvokeSessionFactory(

      (records, context, profile) -> {

        List<BatchBoxRecord> boxes = new ArrayList<>();

        for (ConsumerRecord<byte[], Object> record : records) {

          final Object message = record.value();
          if (!(message instanceof Box box)) {
            continue;
          }

          boxes.add(BatchBoxRecord.of(box, new BoxMetaOnConsumerRecord(record, profile)));
        }

        if (boxes.isEmpty()) {
          InvokeResult.ok();
        }

        try {
          customListBoxConsumer.execute(boxes);
        } catch (Exception e) {

          if (logger.isShow(LOG_CONSUMER_ERROR_INVOKING)) {
            logger.logConsumerErrorInvoking(e, consumerName, profile);
          }

          return InvokeResult.of(false, e);
        }

        return InvokeResult.ok();
      }

    );

  }

}
