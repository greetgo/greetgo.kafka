package kz.greetgo.kafka.consumer;

import java.util.Collection;
import java.util.function.Function;

public class DynamicReactorAddingImpl implements DynamicReactorAdding {

  private final ConsumerDefinitionCustomBuilder                           b;
  private final Function<ConsumerDefinitionCustomBuilder, DynamicReactor> addFunction;

  public DynamicReactorAddingImpl(Function<ConsumerDefinitionCustomBuilder, DynamicReactor> addFunction) {
    this.addFunction = addFunction;
    b                = ConsumerDefinition.customBuilder()
                                         .topicPrefix(null)
                                         .autoOffsetReset(AutoOffsetReset.EARLIEST);
  }

  @Override
  public DynamicReactorAdding name(String name) {
    b.name(name);
    return this;
  }

  @Override
  public DynamicReactorAdding topic(String topic) {
    b.topic(topic);
    return this;
  }

  @Override
  public DynamicReactorAdding profile(String profile) {
    b.profile(profile);
    return this;
  }

  @Override
  public DynamicReactorAdding autoOffsetReset(AutoOffsetReset autoOffsetReset) {
    b.autoOffsetReset(autoOffsetReset);
    return this;
  }

  @Override
  public DynamicReactorAdding customConsumer(CustomBoxConsumer customConsumer) {
    b.customConsumer(customConsumer);
    return this;
  }

  @Override
  public DynamicReactorAdding customStrConsumer(CustomStrConsumer customStrConsumer) {
    b.customStrConsumer(customStrConsumer);
    return this;
  }

  @Override
  public DynamicReactorAdding customBytesConsumer(CustomBytesConsumer customBytesConsumer) {
    b.customBytesConsumer(customBytesConsumer);
    return this;
  }

  @Override
  public DynamicReactorAdding customListBoxConsumer(CustomListBoxConsumer customListBoxConsumer) {
    b.customListBoxConsumer(customListBoxConsumer);
    return this;
  }

  @Override
  public DynamicReactorAdding commitOn(Class<?> errorClass) {
    b.commitOn(errorClass);
    return this;
  }

  @Override
  public DynamicReactorAdding commitOnAll(Collection<Class<?>> errorClassAll) {
    b.commitOnAll(errorClassAll);
    return this;
  }

  @Override
  public DynamicReactorAdding groupId(String groupId) {
    b.groupId(groupId);
    return this;
  }

  @Override
  public DynamicReactorAdding folderPath(String folderPath) {
    b.folderPath(folderPath);
    return this;
  }

  @Override
  public DynamicReactorAdding workerCountFileName(String workerCountFileName) {
    b.workerCountFileName(workerCountFileName);
    return this;
  }

  @Override
  public DynamicReactorAdding description(String description) {
    b.description(description);
    return this;
  }

  @Override
  public DynamicReactorAdding consumerFilter(ConsumerFilter consumerFilter) {
    b.consumerFilter(consumerFilter);
    return this;
  }

  @Override
  public DynamicReactorAdding direct(boolean direct, boolean ignoreKafka) {
    b.direct(direct, ignoreKafka);
    return this;
  }

  @Override
  public DynamicReactor add() {
    return addFunction.apply(b);
  }
}
