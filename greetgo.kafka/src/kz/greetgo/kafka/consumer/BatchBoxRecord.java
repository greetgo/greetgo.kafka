package kz.greetgo.kafka.consumer;

import kz.greetgo.kafka.model.Box;
import lombok.NonNull;

public class BatchBoxRecord {

  public Box box;

  public BoxMeta boxMeta;

  public static BatchBoxRecord of(@NonNull Box box, @NonNull BoxMeta boxMeta) {
    BatchBoxRecord batchBoxRecord = new BatchBoxRecord();
    batchBoxRecord.box     = box;
    batchBoxRecord.boxMeta = boxMeta;

    return batchBoxRecord;
  }

}
