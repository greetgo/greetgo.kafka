package kz.greetgo.kafka.consumer;

import kz.greetgo.kafka.core.logger.Logger;
import kz.greetgo.kafka.producer.KafkaFuture;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static kz.greetgo.kafka.core.logger.LoggerType.CONSUMER_LOG_DIRECT_INVOKE_ERROR;

public class SingleInvokeSessionFactoryImpl implements InvokeSessionFactory {

  private final SingleInvoker invoker;
  private final Logger        logger;

  public SingleInvokeSessionFactoryImpl(Logger logger, SingleInvoker invoker) {
    this.invoker = invoker;
    this.logger  = logger;
  }

  @Override
  public InvokeSession createSession() {
    return new InvokeSession() {

      private final InvokeSessionContext context = new InvokeSessionContext();

      @Override
      public InvokeResult invoke(ConsumerRecords<byte[], Object> records, Profile profile) {
        boolean   needToCommit    = true;
        Throwable lastInvokeError = null;

        List<KafkaFuture> kafkaFutures = new ArrayList<>();

        for (ConsumerRecord<byte[], Object> record : records) {

          if (!invoker.isInFilter(record)) {
            continue;
          }

          context.kafkaFutures.clear();

          InvokeResult invokeResult = invoker.invoke(record, context, profile);

          if (!invokeResult.needToCommit()) {
            needToCommit = false;
          }

          {
            Throwable error = invokeResult.invocationError();
            if (error != null) {
              lastInvokeError = error;
              if (logger.isShow(CONSUMER_LOG_DIRECT_INVOKE_ERROR)) {
                logger.logConsumerLogDirectInvokeError(error);
              }
            }
          }

          kafkaFutures.addAll(context.kafkaFutures);

          context.kafkaFutures.clear();

        }

        kafkaFutures.stream().filter(Objects::nonNull).forEach(KafkaFuture::awaitAndGet);

        return InvokeResult.of(needToCommit, lastInvokeError);
      }

      @Override
      public void close() {
        context.close();
      }
    };
  }
}
