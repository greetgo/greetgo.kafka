package kz.greetgo.kafka.consumer;

import kz.greetgo.kafka.model.Box;

public interface CustomBoxConsumer {

  void execute(Box box, BoxMeta boxMeta) throws Exception;

}
