package kz.greetgo.kafka.consumer;

import kz.greetgo.kafka.consumer.annotations.TopicContentType;
import kz.greetgo.kafka.core.logger.Logger;

import java.util.List;
import java.util.Set;
import java.util.function.Supplier;

import static java.util.Objects.requireNonNull;

public class ConsumerDefinitionCustom implements ConsumerDefinition {

  private final String               name;
  private final List<String>         topicList;
  private final String               folderPath;
  private final String               workerCountFileName;
  private final AutoOffsetReset      autoOffsetReset;
  private final String               groupId;
  private final String               description;
  private final boolean              isDirect;
  private final boolean              ignoreKafka;
  private final TopicContentType     topicContentType;
  private final Profile              profile;
  private final Supplier<String>     topicPrefix;
  private final InvokeSessionFactory invokeSessionFactory;

  public ConsumerDefinitionCustom(CustomBoxConsumer customConsumer,
                                  CustomStrConsumer customStrConsumer,
                                  CustomBytesConsumer customBytesConsumer,
                                  CustomListBoxConsumer customListBoxConsumer,
                                  String name,
                                  List<String> topicList,
                                  String folderPath,
                                  String workerCountFileName,
                                  AutoOffsetReset autoOffsetReset,
                                  String groupId,
                                  String description,
                                  Logger logger,
                                  Supplier<String> topicPrefix,
                                  List<Class<?>> commitOnList,
                                  boolean isDirect,
                                  boolean ignoreKafka,
                                  ConsumerFilter consumerFilter,
                                  TopicContentType topicContentType,
                                  Profile profile) {

    boolean isBatched = false;
    {
      int countDefined = 0;
      if (customConsumer != null) {
        countDefined++;
      }
      if (customStrConsumer != null) {
        countDefined++;
      }
      if (customBytesConsumer != null) {
        countDefined++;
      }
      if (customListBoxConsumer != null) {
        isBatched = true;
        countDefined++;
      }
      if (countDefined == 0) {
        throw new RuntimeException("7gi0h81nt0 :: One consumer MUST be defined");
      }
      if (countDefined > 2) {
        throw new RuntimeException("S376K9O0RK :: Only one consumer MAY be defined");
      }
    }

    if (isBatched && !commitOnList.isEmpty()) {
      throw new IllegalArgumentException("Kg6QMz8c8h :: you cannot use CommitOn on batched consumers. KafkaConsumer: " + name);
    }

    this.name                = name;
    this.topicList           = List.copyOf(topicList);
    this.topicContentType    = topicContentType;
    this.folderPath          = folderPath;
    this.workerCountFileName = workerCountFileName;
    this.autoOffsetReset     = autoOffsetReset;
    this.groupId             = groupId;
    this.description         = description;
    this.isDirect            = isDirect;
    this.ignoreKafka         = ignoreKafka;
    this.profile             = requireNonNull(profile, "6jTRI9BVB3 :: profile == null");
    this.topicPrefix         = topicPrefix != null ? topicPrefix : () -> null;

    var consumerDefinition = this;

    BoxFilter boxFilter = (box, recordKey) -> consumerFilter.isInFilter(consumerDefinition, box, recordKey);

    invokeSessionFactory = InvokeSessionFactory.builderCustom()
                                               .commitOnAll(commitOnList)
                                               .customBoxConsumer(customConsumer)
                                               .customStrConsumer(customStrConsumer)
                                               .customBytesConsumer(customBytesConsumer)
                                               .customListBoxConsumer(customListBoxConsumer)
                                               .logger(logger)
                                               .boxFilter(boxFilter)
                                               .build();
  }

  @Override
  public Object id() {
    return this;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public String getFolderPath() {
    return folderPath;
  }

  @Override
  public InvokeSessionFactory getInvokeSessionFactory() {
    return invokeSessionFactory;
  }

  @Override
  public String logDisplay() {
    return "CUSTOM[" + name + "]";
  }

  @Override
  public AutoOffsetReset getAutoOffsetReset() {
    return autoOffsetReset;
  }

  @Override
  public String getGroupId() {
    final String prefix = topicPrefix.get();
    return prefix == null ? groupId : prefix + groupId;
  }

  @Override
  public String getDescription() {
    return description;
  }

  @Override
  public boolean isAutoCommit() {
    return false;
  }

  @Override
  public String getConsumerName() {
    return name;
  }

  @Override
  public Set<TopicsInProfile> topicList() {
    return Set.of(TopicsInProfile.of(topicList, profile));
  }

  @Override
  public Set<TopicsInProfile> prefixedTopicList() {
    return TopicsInProfile.prefixTopics(topicPrefix.get(), topicList());
  }

  @Override
  public String workerCountFileName() {
    return workerCountFileName;
  }

  @Override
  public boolean isDirect() {
    return isDirect;
  }

  @Override
  public boolean ignoreKafka() {
    return ignoreKafka;
  }

  @Override
  public TopicContentType topicContentType() {
    return topicContentType;
  }

  @Override
  public Profile profile() {
    return profile;
  }
}
