package kz.greetgo.kafka.consumer.config;

import kz.greetgo.kafka.consumer.annotations.KafkaProfile;

import java.util.Map;

/**
 * Объект, добавляющий параметры доступа к кафке.
 * <p>
 * Если доступ простой, то нужно добавить только:
 * <pre>
 *    bootstrap.servers
 * </pre>
 * <p>
 * Иначе нужно добавлять кучу параметров доступа.
 */
public interface ConsumerConnectSetter {

  /**
   * В этом методе нужно добавить параметры доступа к кафке
   *
   * @param targetConfigMap коллекция, в которую нужно добавлять параметры доступа. При простом доступе нужно добавить только
   *                        <code>bootstrap.servers</code>
   * @param params          параметры для доступа к кафке. Среди них стоит обратить на профиль доступа.
   *                        Если он null, то доступ по умолчанию. Профиль в получателях определяется аннотацией {@link KafkaProfile}
   */
  void setConnect(Map<String, Object> targetConfigMap, ConsumerConnectParams params);

}
