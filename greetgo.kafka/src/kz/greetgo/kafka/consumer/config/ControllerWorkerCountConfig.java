package kz.greetgo.kafka.consumer.config;

import kz.greetgo.kafka.core.config.EventConfigFile;
import kz.greetgo.kafka.util.StreamUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ControllerWorkerCountConfig extends AbstractTextLinesConfig implements AutoCloseable {
  private final Supplier<ConsumerConfigDefaults> defaults;

  public ControllerWorkerCountConfig(EventConfigFile configFile, Supplier<ConsumerConfigDefaults> defaults) {
    super(configFile);
    this.defaults = defaults;
  }

  public int getWorkerCount(String methodName) {
    String value = readData(methodName).get(methodName);
    try {
      return value == null ? 0 : Integer.parseInt(value);
    } catch (NumberFormatException ignore) {
      return 0;
    }
  }

  private final List<WorkCountRecord> registeredRecords = new ArrayList<>();

  public synchronized void register(String methodName, String description) {
    if (registeredRecords.stream().noneMatch(r -> Objects.equals(methodName, r.methodName))) {
      registeredRecords.add(new WorkCountRecord(methodName, description, getDefaultWorkCount()));
    }
  }

  private int getDefaultWorkCount() {
    return defaults.get().getIntParam("out.worker.count", 1);
  }

  @Override
  protected synchronized List<String> createDefaultLines() {
    return registeredRecords.stream()
                            .flatMap(ControllerWorkerCountConfig::convertRecordToLines)
                            .collect(Collectors.toList());
  }

  private static Stream<String> convertRecordToLines(WorkCountRecord record) {
    return StreamUtil.concat(
      Stream.of(""),
      record.descriptionAsLinesStream().map(x -> "# " + x),
      Stream.of(record.methodName + " = " + record.defaultValue)
    );
  }

}
