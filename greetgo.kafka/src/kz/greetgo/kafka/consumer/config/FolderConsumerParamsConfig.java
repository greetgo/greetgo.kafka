package kz.greetgo.kafka.consumer.config;

import kz.greetgo.kafka.core.config.EventConfigFile;
import kz.greetgo.kafka.util.KeyValue;

import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

public class FolderConsumerParamsConfig extends AbstractTextLinesConfig implements AutoCloseable {

  public static final String POLL_DURATION_KEY = "out.poll.duration.ms";
  public static final String CONFIG_MAP_PREFIX = "con.";

  private final Supplier<ConsumerConfigDefaults> defaults;

  public FolderConsumerParamsConfig(EventConfigFile configFile, Supplier<ConsumerConfigDefaults> defaults) {
    super(configFile);
    this.defaults = defaults;
  }

  public Map<String, String> getConfigMap() {
    return readData().entrySet()
                     .stream()
                     .filter(x -> x.getKey().startsWith(CONFIG_MAP_PREFIX))
                     .map(x -> KeyValue.of(x.getKey().substring(CONFIG_MAP_PREFIX.length()), x.getValue()))
                     .filter(x -> x.key().trim().length() > 0)
                     .collect(KeyValue.toMap());
  }

  public Duration pollDuration() {
    String str = readData(POLL_DURATION_KEY).get(POLL_DURATION_KEY);
    if (str == null) {
      return Duration.ZERO;
    }
    try {
      return Duration.ofMillis(Long.parseLong(str));
    } catch (NumberFormatException ignore) {
      return Duration.ZERO;
    }
  }

  @Override
  protected List<String> createDefaultLines() {
    return defaults.get().toConfigFileLines();
  }

}
