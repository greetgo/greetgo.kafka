package kz.greetgo.kafka.consumer.config;

import kz.greetgo.kafka.consumer.ParameterValueValidator;
import kz.greetgo.kafka.consumer.ParameterValueValidatorInt;
import kz.greetgo.kafka.consumer.ParameterValueValidatorLong;
import kz.greetgo.kafka.consumer.ParameterValueValidatorStr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.Collections.unmodifiableMap;
import static java.util.Objects.requireNonNull;

public class ConsumerConfigDefaults {
  private final        List<ParameterDefinition>            parameterDefinitionList = new ArrayList<>();
  private final        Map<String, ParameterDefinition>     parameterDefinitionMap  = new HashMap<>();
  private static final Map<String, ParameterValueValidator> validatorMap;

  static {
    Map<String, ParameterValueValidator> vMap = new HashMap<>();
    vMap.put("Long", new ParameterValueValidatorLong());
    vMap.put("Int", new ParameterValueValidatorInt());
    vMap.put("Str", new ParameterValueValidatorStr());
    //noinspection Java9CollectionFactory
    validatorMap = unmodifiableMap(vMap);
  }

  public static ConsumerConfigDefaults withDefaults() {
    ConsumerConfigDefaults consumerConfigDefaults = new ConsumerConfigDefaults();
    consumerConfigDefaults.addDefaults();
    return consumerConfigDefaults;
  }

  public void addDefaults() {
    addDefinition(" Long   con.auto.commit.interval.ms           1000  ");
    addDefinition(" Long   con.fetch.min.bytes                      1  ");
    addDefinition(" Long   con.max.partition.fetch.bytes      1048576  ");
    addDefinition(" Long   con.connections.max.idle.ms         540000  ");
    addDefinition(" Long   con.default.api.timeout.ms           60000  ");
    addDefinition(" Long   con.fetch.max.bytes               52428800  ");
    addDefinition(" Long   con.retry.backoff.ms                  2000  ");

    addDefinition(" Long   con.session.timeout.ms               10000  ");
    addDefinition(" Long   con.heartbeat.interval.ms             3000  ");
    addDefinition(" Long   con.max.poll.interval.ms           3000000  ");
    addDefinition(" Long   con.max.poll.records                   500  ");

    addDefinition(" Long   con.receive.buffer.bytes             65536  ");
    addDefinition(" Long   con.request.timeout.ms               30000  ");
    addDefinition(" Long   con.send.buffer.bytes               131072  ");
    addDefinition(" Long   con.fetch.max.wait.ms                  500  ");

    addDefinition(" Int out.poll.duration.ms  1800  ");
    addDefinition(" Int out.worker.count         1  ");
  }

  public int getIntParam(String key, int valueOnAbsent) {
    final ParameterDefinition parameterDefinition = parameterDefinitionMap.get(key);
    if (parameterDefinition == null) {
      return valueOnAbsent;
    }
    return parameterDefinition.validator.getIntValue(parameterDefinition.defaultValue, "" + valueOnAbsent);
  }

  public void addDefinition(String definitionStr) {
    String trimmedDefStr = definitionStr.trim();

    int idx1 = trimmedDefStr.indexOf(' ');
    if (idx1 < 0) {
      throw new RuntimeException("4HmXzZgRRT :: Illegal definition str = [" + definitionStr + "]");
    }

    String validatorStr = trimmedDefStr.substring(0, idx1);

    String restStr2 = trimmedDefStr.substring(idx1 + 1).trim();

    int idx2 = restStr2.indexOf(' ');
    if (idx2 < 0) {
      throw new RuntimeException("32s3t7NqhS :: Illegal definition str = [" + definitionStr + "]");
    }

    String parameterName = restStr2.substring(0, idx2);

    String defaultValue = restStr2.substring(idx2 + 1).trim();

    ParameterValueValidator validator = requireNonNull(validatorMap.get(validatorStr),
                                                       "29N38wqS98 :: validatorStr = [" + validatorStr + "]");

    ParameterDefinition definition = new ParameterDefinition(parameterName, defaultValue, validator);
    parameterDefinitionList.add(definition);
    parameterDefinitionMap.put(definition.parameterName, definition);
  }

  public List<ParameterDefinition> parameterDefinitionList() {
    return parameterDefinitionList;
  }

  private static final
  ParameterDefinition DEFAULT_PARAMETER_DEFINITION = new ParameterDefinition(null, "",
                                                                             new ParameterValueValidatorStr());

  public ParameterDefinition getDefinition(String parameterName) {
    requireNonNull(parameterName);

    {
      ParameterDefinition pd = parameterDefinitionMap.get(parameterName);
      if (pd != null) {
        return pd;
      }
    }

    return DEFAULT_PARAMETER_DEFINITION;
  }

  public List<String> toConfigFileLines() {
    return parameterDefinitionList.stream()
                                  .map(ParameterDefinition::toConfigFileLine)
                                  .collect(Collectors.toList());
  }

}
