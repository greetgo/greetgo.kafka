package kz.greetgo.kafka.consumer.config;

import kz.greetgo.kafka.consumer.Profile;
import kz.greetgo.kafka.consumer.TopicsInProfile;
import kz.greetgo.kafka.consumer.annotations.KafkaProfile;
import kz.greetgo.kafka.consumer.annotations.TopicListMethod;

import java.util.Collection;

/**
 * Параметры получателя данных из кафки, которые могут понадобиться, чтобы определить параметры доступа к кафке.
 */
public interface ConsumerConnectParams {

  /**
   * Профиль доступа к кафке.
   * <p>
   * Если он null, то доступ по умолчанию.
   * <p>
   * Профиль в получателях определяется аннотацией {@link KafkaProfile}.
   * <p>
   * Также профиль может определяться методом, указанным в аннотации {@link TopicListMethod}.
   * Если этот метод будет возвращать объекты {@link TopicsInProfile}
   */
  Profile profile();

  /**
   * Предоставляет список топиков, на который настраивается считыватель. Префикс уже указан - т.е. это те названия,
   * которые непосредственно будут содержаться в кафке
   */
  @SuppressWarnings("unused")
  Collection<String> nativeTopics();

}
