package kz.greetgo.kafka.consumer.config;

import kz.greetgo.kafka.core.config.ConfigEventType;
import kz.greetgo.kafka.core.config.EventConfigFile;
import kz.greetgo.kafka.core.config.EventRegistration;
import kz.greetgo.kafka.util.Handler;
import kz.greetgo.kafka.util.HandlerList;
import kz.greetgo.kafka.util.Listener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.stream.Collectors.toList;
import static kz.greetgo.kafka.util.LineUtil.calcDelta;

public abstract class AbstractTextLinesConfig implements AutoCloseable {
  protected final EventConfigFile configFile;

  protected AbstractTextLinesConfig(EventConfigFile configFile) {
    this.configFile = configFile;
  }

  protected final HandlerList changeHandlers = new HandlerList();

  public Listener addChangeHandler(Handler handler) {
    return changeHandlers.addHandler(handler);
  }

  @Override
  public void close() {
    configFile.close();
  }

  protected void writeLines(List<String> lines) {
    configFile.writeContent(String.join("\n", lines).getBytes(UTF_8));
  }

  protected List<String> readLines() {
    byte[] content = configFile.readContent();
    if (content == null) {
      return null;
    }
    return Arrays.stream(new String(content, UTF_8).split("\n")).collect(toList());
  }

  protected final AtomicReference<EventRegistration> changeEventRegistration = new AtomicReference<>(null);

  protected void ensureChangeListener() {
    if (changeEventRegistration.get() != null) {
      return;
    }
    synchronized (this) {
      if (changeEventRegistration.get() != null) {
        return;
      }
      configFile.ensureLookingFor();
      {
        var er = changeEventRegistration.getAndSet(configFile.addEventHandler(this::happenedFileEvent));
        if (er != null) {
          er.unregister();
        }
      }
    }
  }

  private void happenedFileEvent(ConfigEventType type) {
    if (type == ConfigEventType.DELETE) {
      return;
    }

    savedDatum.set(null);

    changeHandlers.fire();
  }

  protected abstract List<String> createDefaultLines();

  private static class Datum {
    final Map<String, String> data       = new HashMap<>();
    final Set<String>         existsKeys = new HashSet<>();
  }

  private final AtomicReference<Datum> savedDatum = new AtomicReference<>();

  protected Map<String, String> readData() {
    return readData(null);
  }

  protected Map<String, String> readData(String keyHaveToBeExist) {
    {
      Datum datum = savedDatum.get();
      if (datum != null && (keyHaveToBeExist == null || datum.existsKeys.contains(keyHaveToBeExist))) {
        return datum.data;
      }
    }

    ensureChangeListener();

    List<String> defaultLines = createDefaultLines();
    List<String> lines        = readLines();

    if (lines == null) {
      lines = new ArrayList<>();
      {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        lines.add("");
        lines.add("#");
        lines.add("# Created at " + sdf.format(new Date()));
        lines.add("#");
        lines.add("");
      }

      lines.addAll(defaultLines);

      return extractData_Save_WriteLines(keyHaveToBeExist, lines, true);
    }

    List<String> deltaLines = calcDelta(defaultLines, lines);

    if (deltaLines == null) {
      return extractData_Save_WriteLines(keyHaveToBeExist, lines, false);
    }

    {
      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

      lines.add("");
      lines.add("#");
      lines.add("# Appended at " + sdf.format(new Date()));
      lines.add("#");
      lines.add("");
    }

    lines.addAll(deltaLines);

    return extractData_Save_WriteLines(keyHaveToBeExist, lines, true);
  }

  private Map<String, String> extractData_Save_WriteLines(String key, List<String> lines, boolean linesChanged) {
    Objects.requireNonNull(lines, "4FRWHsCnLI :: lines == null");
    Datum datum = linesToDatum(lines);

    if (key != null && !datum.existsKeys.contains(key)) {
      lines.add("");
      lines.add("#");
      lines.add("# Unknown parameter");
      lines.add(key);
      writeLines(lines);
      savedDatum.set(datum);
      return datum.data;
    }

    if (linesChanged) {
      writeLines(lines);
    }
    savedDatum.set(datum);
    return datum.data;
  }

  private static Datum linesToDatum(List<String> lines) {

    final Datum datum = new Datum();

    for (final String line : lines) {
      if (line == null) {
        continue;
      }
      String trimmedLine = line.trim();
      if (trimmedLine.startsWith("#")) {
        continue;
      }

      int idx = line.indexOf("=");
      if (idx < 0) {
        datum.existsKeys.add(trimmedLine);
      } else {
        String key   = line.substring(0, idx).trim();
        String value = line.substring(idx + 1).trim();
        datum.existsKeys.add(key);
        datum.data.put(key, value);
      }
    }

    return datum;
  }
}
