package kz.greetgo.kafka.consumer.config;

import kz.greetgo.kafka.consumer.Profile;
import kz.greetgo.kafka.util.Handler;
import kz.greetgo.kafka.util.Listener;

import java.time.Duration;
import java.util.Map;

public interface ConsumerReactorConfig {

  Listener onChangeParams(Handler handler, Profile profile);

  Listener onChangeWorkerCount(Handler handler, Profile profile);

  int getWorkerCount(Profile profile);

  Duration getPollDuration(Profile profile);

  Map<String, String> getConfigMap(Profile profile);

}
