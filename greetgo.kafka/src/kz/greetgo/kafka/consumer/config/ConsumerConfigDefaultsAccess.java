package kz.greetgo.kafka.consumer.config;

import kz.greetgo.kafka.consumer.Profile;

public interface ConsumerConfigDefaultsAccess {
  ConsumerConfigDefaults get(Profile profile);
}
