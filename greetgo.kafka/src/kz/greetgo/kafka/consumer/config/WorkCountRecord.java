package kz.greetgo.kafka.consumer.config;

import java.util.Arrays;
import java.util.stream.Stream;

public class WorkCountRecord {
  public final String methodName;
  public final String description;
  public final int    defaultValue;

  public WorkCountRecord(String methodName, String description, int defaultValue) {
    this.methodName   = methodName;
    this.description  = description;
    this.defaultValue = defaultValue;
  }

  public Stream<String> descriptionAsLinesStream() {
    String x = description;
    return x == null ? Stream.of() : Arrays.stream(x.split("\n")).sequential();
  }
}
