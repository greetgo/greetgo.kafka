package kz.greetgo.kafka.consumer.config;

import kz.greetgo.kafka.consumer.ConsumerDefinition;
import kz.greetgo.kafka.consumer.Profile;
import kz.greetgo.kafka.core.config.EventConfigFileFactory;
import kz.greetgo.kafka.util.Handler;
import kz.greetgo.kafka.util.Listener;

import java.time.Duration;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

import static java.util.Objects.requireNonNull;
import static kz.greetgo.kafka.util.KafkaStrUtil.concatPath;

public class ConsumerReactorConfigFactory implements AutoCloseable {
  private final Object sync = new Object();

  private final EventConfigFileFactory       configFactory;
  private final ConsumerConfigDefaultsAccess defaults;
  private final String                       consumerParamsName;
  private final String                       profileConfigPrefix;

  private final ConcurrentHashMap<Object, ConsumerReactorConfig>               consumerConfigMap              = new ConcurrentHashMap<>();
  private final ConcurrentHashMap<PathProfileKey, FolderConsumerParamsConfig>  folderConsumerParamsConfigMap  = new ConcurrentHashMap<>();
  private final ConcurrentHashMap<PathProfileKey, ControllerWorkerCountConfig> controllerWorkerCountConfigMap = new ConcurrentHashMap<>();

  public static Builder builder() {
    return new Builder();
  }

  public static class Builder {
    EventConfigFileFactory       configFactory;
    ConsumerConfigDefaultsAccess defaults;
    String                       consumerParamsName  = "params.config";
    String                       profileConfigPrefix = "external/";

    public Builder configFactory(EventConfigFileFactory configFactory) {
      this.configFactory = requireNonNull(configFactory);
      return this;
    }

    public Builder defaults(ConsumerConfigDefaultsAccess defaults) {
      this.defaults = requireNonNull(defaults);
      return this;
    }

    public Builder consumerParamsName(String consumerParamsName) {
      this.consumerParamsName = requireNonNull(consumerParamsName);
      return this;
    }

    public Builder profileConfigPrefix(String profileConfigPrefix) {
      this.profileConfigPrefix = profileConfigPrefix;
      return this;
    }

    private Builder() {}

    public ConsumerReactorConfigFactory build() {
      return new ConsumerReactorConfigFactory(requireNonNull(configFactory),
                                              requireNonNull(defaults),
                                              requireNonNull(consumerParamsName),
                                              profileConfigPrefix);
    }
  }

  private ConsumerReactorConfigFactory(EventConfigFileFactory configFactory,
                                       ConsumerConfigDefaultsAccess defaults,
                                       String consumerParamsName,
                                       String profileConfigPrefix) {
    this.configFactory       = configFactory;
    this.defaults            = defaults;
    this.consumerParamsName  = consumerParamsName;
    this.profileConfigPrefix = profileConfigPrefix;
  }

  public ConsumerReactorConfig getConsumerConfig(ConsumerDefinition consumerDefinition) {
    {
      ConsumerReactorConfig consumerConfig = consumerConfigMap.get(consumerDefinition.id());
      if (consumerConfig != null) {
        return consumerConfig;
      }
    }
    synchronized (sync) {
      {
        ConsumerReactorConfig consumerConfig = consumerConfigMap.get(consumerDefinition.id());
        if (consumerConfig != null) {
          return consumerConfig;
        }
      }

      {
        ConsumerReactorConfig consumerConfig = createConsumerConfig(consumerDefinition);
        consumerConfigMap.put(consumerDefinition.id(), consumerConfig);
        return consumerConfig;
      }
    }
  }

  private ConsumerReactorConfig createConsumerConfig(ConsumerDefinition consumerDefinition) {
    return new ConsumerReactorConfig() {
      @Override
      public Listener onChangeParams(Handler handler, Profile profile) {
        return getFolderConsumerParamsConfig(consumerDefinition, profile).addChangeHandler(handler);
      }

      @Override
      public Listener onChangeWorkerCount(Handler handler, Profile profile) {
        return getControllerWorkerCountConfig(consumerDefinition, profile).addChangeHandler(handler);
      }

      @Override
      public Map<String, String> getConfigMap(Profile profile) {
        return getFolderConsumerParamsConfig(consumerDefinition, profile).getConfigMap();
      }

      @Override
      public int getWorkerCount(Profile profile) {
        return getControllerWorkerCountConfig(consumerDefinition, profile).getWorkerCount(consumerDefinition.getName());
      }

      @Override
      public Duration getPollDuration(Profile profile) {
        return getFolderConsumerParamsConfig(consumerDefinition, profile).pollDuration();
      }
    };
  }

  private static final class PathProfileKey {
    public final String  path;
    public final Profile profile;

    private PathProfileKey(String path, Profile profile) {
      this.path    = requireNonNull(path, "p5Pj2Y9Ur2");
      this.profile = requireNonNull(profile, "TuyBBvm6H3");
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }
      final PathProfileKey that = (PathProfileKey) o;
      return Objects.equals(path, that.path) && Objects.equals(profile, that.profile);
    }

    @Override
    public int hashCode() {
      return Objects.hash(path, profile);
    }
  }

  private FolderConsumerParamsConfig getFolderConsumerParamsConfig(ConsumerDefinition consumerDefinition,
                                                                   Profile profile) {
    String         consumerParamsName = this.consumerParamsName;
    String         folderPath         = consumerDefinition.getFolderPath();
    String         configPath         = concatPath(folderPath, profile.withPrefix(profileConfigPrefix), consumerParamsName);
    PathProfileKey key                = new PathProfileKey(configPath, profile);

    {
      FolderConsumerParamsConfig x = folderConsumerParamsConfigMap.get(key);
      if (x != null) {
        return x;
      }
    }
    synchronized (sync) {
      {
        FolderConsumerParamsConfig x = folderConsumerParamsConfigMap.get(key);
        if (x != null) {
          return x;
        }
      }
      {
        FolderConsumerParamsConfig x = new FolderConsumerParamsConfig(configFactory.createEventConfigFile(configPath),
                                                                      () -> defaults.get(profile));
        folderConsumerParamsConfigMap.put(key, x);
        return x;
      }
    }
  }

  private ControllerWorkerCountConfig getControllerWorkerCountConfig(ConsumerDefinition consumerDefinition, Profile profile) {
    final ControllerWorkerCountConfig c = extractControllerWorkerCountConfig(consumerDefinition, profile);
    c.register(consumerDefinition.getName(), consumerDefinition.getDescription());
    return c;
  }

  private ControllerWorkerCountConfig extractControllerWorkerCountConfig(ConsumerDefinition consumerDefinition,
                                                                         Profile profile) {

    String         workerCountFileName = consumerDefinition.workerCountFileName();
    String         folderPath          = consumerDefinition.getFolderPath();
    String         configPath          = concatPath(folderPath, profile.withPrefix(profileConfigPrefix), workerCountFileName);
    PathProfileKey key                 = new PathProfileKey(configPath, profile);

    {
      ControllerWorkerCountConfig c = controllerWorkerCountConfigMap.get(key);
      if (c != null) {
        return c;
      }
    }
    synchronized (sync) {
      {
        ControllerWorkerCountConfig c = controllerWorkerCountConfigMap.get(key);
        if (c != null) {
          return c;
        }
      }
      {
        ControllerWorkerCountConfig c = new ControllerWorkerCountConfig(configFactory.createEventConfigFile(configPath),
                                                                        () -> defaults.get(profile));
        controllerWorkerCountConfigMap.put(key, c);
        return c;
      }
    }
  }

  @Override
  public void close() {
    boolean hasRemoves = true;
    while (hasRemoves) {
      hasRemoves = false;
      {
        PathProfileKey key = folderConsumerParamsConfigMap.keySet().stream().findAny().orElse(null);
        if (key != null) {
          hasRemoves = true;
          FolderConsumerParamsConfig removed = folderConsumerParamsConfigMap.remove(key);
          if (removed != null) {
            removed.close();
          }
        }
      }
      {
        PathProfileKey key = controllerWorkerCountConfigMap.keySet().stream().findAny().orElse(null);
        if (key != null) {
          hasRemoves = true;
          ControllerWorkerCountConfig removed = controllerWorkerCountConfigMap.remove(key);
          if (removed != null) {
            removed.close();
          }
        }
      }
    }
  }
}
