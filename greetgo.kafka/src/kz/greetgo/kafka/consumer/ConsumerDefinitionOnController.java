package kz.greetgo.kafka.consumer;

import kz.greetgo.kafka.consumer.annotations.ConsumerName;
import kz.greetgo.kafka.consumer.annotations.ConsumersFolder;
import kz.greetgo.kafka.consumer.annotations.Direct;
import kz.greetgo.kafka.consumer.annotations.GroupId;
import kz.greetgo.kafka.consumer.annotations.KafkaDescription;
import kz.greetgo.kafka.consumer.annotations.KafkaLatest;
import kz.greetgo.kafka.consumer.annotations.KafkaNotifier;
import kz.greetgo.kafka.consumer.annotations.KafkaProfile;
import kz.greetgo.kafka.consumer.annotations.Topic;
import kz.greetgo.kafka.consumer.annotations.TopicContentType;
import kz.greetgo.kafka.consumer.annotations.TopicListMethod;
import kz.greetgo.kafka.core.logger.Logger;
import kz.greetgo.kafka.util.GroupIdUtil;

import javax.annotation.Nonnull;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Supplier;

import static kz.greetgo.kafka.util.AnnotationUtil.getAnnotation;
import static kz.greetgo.kafka.util.KafkaTopicUtil.callAndPrepareResult;

public class ConsumerDefinitionOnController implements ConsumerDefinition {

  private final Id                   id;
  private final Object               controller;
  private final Method               method;
  private final String               folderPath;
  private final InvokeSessionFactory invokeSessionFactory;
  private final String               groupId;
  private final String               description;
  private final String               consumerName;
  private final ConsumerDirectStatus consumerDirectStatus;
  private final Profile              profile;
  private final Supplier<String>     topicPrefix;

  private final List<String>                   topicList;
  private final Supplier<Set<TopicsInProfile>> topicDynamicSupplier;

  private final AutoOffsetReset autoOffsetReset;
  private final String          workerCountFileName;

  private final TopicContentType topicContentType;

  @Override
  public String toString() {
    return getClass().getSimpleName() + '{' +
           controller.getClass() + '#' + method.getName() +
           ", folderPath=" + folderPath +
           ", groupId=" + groupId +
           '}';
  }

  public static class Id {
    public final Class<?> controllerClass;
    public final Method   method;

    private Id(Class<?> controllerClass, Method method) {
      this.controllerClass = controllerClass;
      this.method          = method;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }
      final Id id = (Id) o;
      return Objects.equals(controllerClass, id.controllerClass) && Objects.equals(method, id.method);
    }

    @Override
    public int hashCode() {
      return Objects.hash(controllerClass, method);
    }
  }

  @Override
  public Object id() {
    return id;
  }

  @Override
  public String getName() {
    return method.getName();
  }

  public ConsumerDefinitionOnController(@Nonnull Object controller,
                                        @Nonnull Method method,
                                        Logger logger,
                                        Supplier<String> topicPrefix,
                                        String consumerHostId,
                                        String workerCountFileName,
                                        ConsumerFilter consumerFilter) {

    this.workerCountFileName = workerCountFileName;
    this.id                  = new Id(controller.getClass(), method);
    this.controller          = controller;
    this.method              = method;
    this.topicPrefix         = topicPrefix != null ? topicPrefix : () -> null;

    final ConsumerFilter consumerFilterUse = consumerFilter != null ? consumerFilter : (x, y, z) -> true;

    {
      ConsumersFolder consumersFolder = getAnnotation(controller.getClass(), ConsumersFolder.class);
      folderPath = consumersFolder == null ? null : consumersFolder.value();
    }

    {
      KafkaDescription kd = getAnnotation(method, KafkaDescription.class);
      description = kd == null ? null : kd.value();
    }

    consumerDirectStatus = extractConsumerDirectStatus(getAnnotation(method, Direct.class), controller);

    {
      String tmpConsumerName = method.getName();
      {
        ConsumerName annotation = getAnnotation(method, ConsumerName.class);
        if (annotation != null) {
          tmpConsumerName = annotation.value();
        }
      }
      consumerName = tmpConsumerName;
    }

    var consumerDefinition = this;

    BoxFilter boxFilter = (box, recordKey) -> consumerFilterUse.isInFilter(consumerDefinition, box, recordKey);

    invokeSessionFactory = InvokeSessionFactory.builderController(controller, method, logger, boxFilter, topicPrefix).build();

    final boolean hasKafkaLatest = getAnnotation(method, KafkaLatest.class) != null;

    {
      autoOffsetReset = getAnnotation(method, KafkaNotifier.class) != null
        ? AutoOffsetReset.LATEST
        :
        (
          hasKafkaLatest ? AutoOffsetReset.LATEST : AutoOffsetReset.EARLIEST
        );
    }

    {
      final String  tmpGroupId;
      final GroupId annotation = getAnnotation(method, GroupId.class);
      if (annotation != null) {
        tmpGroupId = GroupIdUtil.evalGroupId(annotation.value(), controller);
      } else {
        tmpGroupId = method.getName();
      }

      groupId = autoOffsetReset == AutoOffsetReset.EARLIEST || hasKafkaLatest ? tmpGroupId : tmpGroupId + consumerHostId;
    }

    {
      Topic topic = getAnnotation(method, Topic.class);
      if (topic == null) {
        topicList = List.of();
      } else {
        topicList = Arrays.stream(topic.value()).toList();
      }
    }

    {
      KafkaProfile ann = getAnnotation(method, KafkaProfile.class);
      profile = new Profile(ann == null ? null : ann.value());
    }

    {
      TopicListMethod topicListMethod = getAnnotation(method, TopicListMethod.class);
      if (topicListMethod == null) {
        topicDynamicSupplier = Set::of;
      } else {
        final Method listTopicMethod;
        try {
          listTopicMethod = controller.getClass().getMethod(topicListMethod.value());
        } catch (NoSuchMethodException e) {
          throw new RuntimeException("1DjZAwUFaI", e);
        }

        topicDynamicSupplier = () -> callAndPrepareResult(profile, listTopicMethod, controller);
      }
    }

    topicContentType = TopicContentTypeReader.readFrom(method);
  }

  private static @Nonnull ConsumerDirectStatus extractConsumerDirectStatus(Direct direct, Object controller) {
    if (direct == null) {
      return ConsumerDirectStatus.of(false, false);
    }

    if (direct.fromMethod().isEmpty()) {
      return ConsumerDirectStatus.of(true, direct.ignoreKafka());
    }

    try {
      final Method method = controller.getClass().getMethod(direct.fromMethod());
      final Object result = method.invoke(controller);

      if (result instanceof ConsumerDirectStatus) {
        return (ConsumerDirectStatus) result;
      }

      throw new RuntimeException("HHtvY6ONj9 :: Method " + controller.getClass().getSimpleName() + "." + method.getName() + "()"
                                 + " MUST returns " + ConsumerDirectStatus.class.getSimpleName() + ". Full name of method: " + method);
    } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public String getFolderPath() {
    return folderPath;
  }

  public Object getController() {
    return controller;
  }

  public Method getMethod() {
    return method;
  }

  @Override
  public InvokeSessionFactory getInvokeSessionFactory() {
    return invokeSessionFactory;
  }

  @Override
  public String getDescription() {
    return description;
  }

  /**
   * @return строка для описания в логах этого кон-сю-ме-ра
   */
  @Override
  public String logDisplay() {

    StringBuilder sb = new StringBuilder();
    if (folderPath != null) {
      sb.append(folderPath).append("/");
    }
    sb.append(cutAfter$$(controller.getClass().getSimpleName()));
    sb.append('.');

    {
      String consumerName = getConsumerName();
      if (Objects.equals(consumerName, method.getName())) {
        sb.append('[').append(consumerName).append(']');
      } else {
        sb.append(method.getName()).append('[').append(consumerName).append(']');
      }
    }

    return sb.toString();

  }

  private static String cutAfter$$(String str) {
    if (str == null) {
      return null;
    }

    var idx = str.indexOf("$$");
    if (idx < 0) {
      return str;
    }

    return str.substring(0, idx);
  }

  @Override
  public AutoOffsetReset getAutoOffsetReset() {
    return autoOffsetReset;
  }

  @Override
  public String getGroupId() {
    final String prefix = topicPrefix.get();
    return prefix == null ? groupId : prefix + groupId;
  }

  @Override
  public boolean isAutoCommit() {
    return false;
  }

  @Override
  public String getConsumerName() {
    return consumerName;
  }

  @Override
  public Set<TopicsInProfile> topicList() {

    final Set<TopicsInProfile> topics = new HashSet<>();
    topics.add(TopicsInProfile.of(topicList, profile));
    topics.addAll(topicDynamicSupplier.get());

    return TopicsInProfile.distinct(topics);
  }

  @Override
  public Set<TopicsInProfile> prefixedTopicList() {
    return TopicsInProfile.prefixTopics(topicPrefix.get(), topicList());
  }

  @Override
  public String workerCountFileName() {
    return workerCountFileName;
  }

  @Override
  public boolean isDirect() {
    return consumerDirectStatus.isDirect;
  }

  @Override
  public boolean ignoreKafka() {
    return consumerDirectStatus.ignoreKafka;
  }

  @Override
  public TopicContentType topicContentType() {
    return topicContentType;
  }

  @Override
  public Profile profile() {
    return profile;
  }
}
