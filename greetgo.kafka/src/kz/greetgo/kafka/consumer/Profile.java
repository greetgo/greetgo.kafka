package kz.greetgo.kafka.consumer;

import java.util.Objects;

public class Profile {
  public final String profile;

  public Profile(String profile) {
    this.profile = profile;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    final Profile profile1 = (Profile) o;
    return Objects.equals(profile, profile1.profile);
  }

  @Override
  public int hashCode() {
    return Objects.hash(profile);
  }

  @Override
  public String toString() {
    return "Profile{" + (profile == null ? "<NULL>" : profile) + "}";
  }

  public String withPrefix(String prefix) {
    if (profile == null) {
      return null;
    }
    if (prefix == null) {
      return profile;
    }
    return prefix + profile;
  }
}
