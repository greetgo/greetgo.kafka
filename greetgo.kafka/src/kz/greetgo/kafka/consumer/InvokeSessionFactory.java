package kz.greetgo.kafka.consumer;

import kz.greetgo.kafka.core.logger.Logger;
import lombok.NonNull;

import java.lang.reflect.Method;
import java.util.function.Supplier;

public interface InvokeSessionFactory {
  InvokeSession createSession();

  static @NonNull InvokeSessionFactoryControllerBuilder builderController(Object controller,
                                                                          Method method,
                                                                          Logger logger,
                                                                          BoxFilter boxFilter,
                                                                          Supplier<String> topicPrefix) {

    return new InvokeSessionFactoryControllerBuilder(controller, method, logger, boxFilter, topicPrefix);
  }

  static @NonNull InvokeSessionFactoryCustomBuilder builderCustom() {
    return new InvokeSessionFactoryCustomBuilder();
  }
}
