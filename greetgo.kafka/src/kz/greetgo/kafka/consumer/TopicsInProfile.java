package kz.greetgo.kafka.consumer;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Stream;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toUnmodifiableSet;

public class TopicsInProfile {
  public final Set<String> topics;
  public final Profile     profile;

  private TopicsInProfile(Collection<String> topics, Profile profile) {
    this.topics  = Set.copyOf(requireNonNull(topics, "v3wxo9flNI :: topics==null"));
    this.profile = requireNonNull(profile, "Zg1k5z831V :: profile == null");
  }

  private TopicsInProfile(Stream<String> topics, Profile profile) {
    this.topics  = requireNonNull(topics, "lfb9OI97f9 :: topics==null").collect(toUnmodifiableSet());
    this.profile = requireNonNull(profile, "nNN37N9len :: profile == null");
  }

  public static TopicsInProfile of(Collection<String> topics, String profile) {
    return new TopicsInProfile(topics, new Profile(profile));
  }

  public static TopicsInProfile of(Collection<String> topics) {
    return new TopicsInProfile(topics, new Profile(null));
  }

  public static TopicsInProfile of(Collection<String> topics, Profile profile) {
    return new TopicsInProfile(topics, profile);
  }

  public static TopicsInProfile of(Stream<String> topics, String profile) {
    return new TopicsInProfile(topics, new Profile(profile));
  }

  public static TopicsInProfile of(Stream<String> topics, Profile profile) {
    return new TopicsInProfile(topics, profile);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    final TopicsInProfile that = (TopicsInProfile) o;
    return Objects.equals(topics, that.topics) && Objects.equals(profile, that.profile);
  }

  @Override
  public int hashCode() {
    return Objects.hash(topics, profile);
  }

  public static Set<TopicsInProfile> distinct(Collection<TopicsInProfile> source) {
    Map<Profile, Set<String>> profileWithTopics   = new HashMap<>();
    Set<String>               topicsInNullProfile = new HashSet<>();

    for (final TopicsInProfile tip : source) {
      if (tip.profile == null) {
        topicsInNullProfile.addAll(tip.topics);
      } else {
        profileWithTopics.computeIfAbsent(tip.profile, k -> new HashSet<>())
                         .addAll(tip.topics);
      }
    }

    Set<TopicsInProfile> ret = new HashSet<>();

    if (topicsInNullProfile.size() > 0) {
      ret.add(TopicsInProfile.of(topicsInNullProfile));
    }

    for (final Map.Entry<Profile, Set<String>> e : profileWithTopics.entrySet()) {
      final Set<String> topics = e.getValue();
      if (topics.size() > 0) {
        ret.add(TopicsInProfile.of(topics, e.getKey()));
      }
    }

    return Set.copyOf(ret);
  }

  public static Set<TopicsInProfile> prefixTopics(String topicPrefix, Set<TopicsInProfile> source) {
    if (topicPrefix == null || topicPrefix.isBlank()) {
      return source;
    }

    return source.stream().map(t -> t.prefixTopicWith(topicPrefix)).collect(toUnmodifiableSet());
  }

  private TopicsInProfile prefixTopicWith(String topicPrefix) {
    if (topicPrefix == null || topics.isEmpty()) {
      return this;
    }
    return TopicsInProfile.of(topics.stream().map(s -> topicPrefix + s), profile);
  }

  @Override
  public String toString() {
    return "TopicsInProfile{["
      + topics.stream().sorted().collect(joining(" "))
      + "] in " + profile + '}';
  }
}
