package kz.greetgo.kafka.consumer;

import java.util.List;

public interface CustomListBoxConsumer {

  void execute(List<BatchBoxRecord> batchBoxRecords) throws Exception;

}
