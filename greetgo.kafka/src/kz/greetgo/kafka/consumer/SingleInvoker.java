package kz.greetgo.kafka.consumer;

import org.apache.kafka.clients.consumer.ConsumerRecord;

import javax.annotation.Nonnull;

public interface SingleInvoker {

  InvokeResult invoke(@Nonnull ConsumerRecord<byte[], Object> record,
                      @Nonnull InvokeSessionContext context,
                      @Nonnull Profile profile);

  boolean isInFilter(@Nonnull ConsumerRecord<byte[], Object> record);

}
