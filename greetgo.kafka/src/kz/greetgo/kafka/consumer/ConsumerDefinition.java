package kz.greetgo.kafka.consumer;

import kz.greetgo.kafka.consumer.annotations.TopicContentType;

import java.util.Set;

public interface ConsumerDefinition {
  Object id();

  String getName();

  /**
   * @return Папка, где лежит кон-сю-мер. Слэши как разделитель. Не должна начинаться и заканчиваться со слэшем.
   * Может быть null - это значит, что папка корневая
   */
  String getFolderPath();

  InvokeSessionFactory getInvokeSessionFactory();

  String logDisplay();

  AutoOffsetReset getAutoOffsetReset();

  String getGroupId();

  String getDescription();

  boolean isAutoCommit();

  String getConsumerName();

  Set<TopicsInProfile> topicList();

  Set<TopicsInProfile> prefixedTopicList();

  String workerCountFileName();

  static ConsumerDefinitionCustomBuilder customBuilder() {
    return new ConsumerDefinitionCustomBuilder();
  }

  boolean isDirect();

  boolean ignoreKafka();

  TopicContentType topicContentType();

  Profile profile();
}
