package kz.greetgo.kafka.consumer;

import kz.greetgo.kafka.consumer.annotations.TopicContentType;
import kz.greetgo.kafka.core.logger.Logger;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Supplier;

import static java.util.Objects.requireNonNull;
import static kz.greetgo.kafka.util.Fnn.fnn;

public class ConsumerDefinitionCustomBuilder {

  private CustomBoxConsumer     customConsumer;
  private CustomStrConsumer     customStrConsumer;
  private CustomBytesConsumer   customBytesConsumer;
  private CustomListBoxConsumer customListBoxConsumer;

  private final List<String> topicList = new ArrayList<>();

  private String           groupId;
  private String           name;
  private String           folderPath;
  private String           workerCountFileName;
  private AutoOffsetReset  autoOffsetReset;
  private String           description;
  private Logger           logger;
  private boolean          isDirect;
  private boolean          ignoreKafka;
  private ConsumerFilter   consumerFilter = (x, y, z) -> true;
  private Supplier<String> topicPrefix;
  private boolean          topicPrefixGot = false;
  private Profile          profile        = new Profile(null);

  private final List<Class<?>> commitOnClassList = new ArrayList<>();

  public ConsumerDefinitionCustomBuilder logger(Logger logger) {
    this.logger = logger;
    return this;
  }

  public ConsumerDefinitionCustomBuilder customConsumer(CustomBoxConsumer customConsumer) {
    this.customConsumer = requireNonNull(customConsumer, "2whl73P1fl :: customConsumer == null");
    return this;
  }

  public void customStrConsumer(CustomStrConsumer customStrConsumer) {
    this.customStrConsumer = requireNonNull(customStrConsumer, "EhXb7lak33 :: customStrConsumer == null");
  }

  public void customBytesConsumer(CustomBytesConsumer customBytesConsumer) {
    this.customBytesConsumer = requireNonNull(customBytesConsumer, "oDiP55IznR :: customBytesConsumer == null");
  }

  public ConsumerDefinitionCustomBuilder customListBoxConsumer(CustomListBoxConsumer customListBoxConsumer) {
    this.customListBoxConsumer = requireNonNull(customListBoxConsumer, "LfLZdObXUd :: customListBoxConsumer == null");
    return this;
  }

  public ConsumerDefinitionCustomBuilder topicPrefix(Supplier<String> topicPrefix) {
    this.topicPrefix = topicPrefix != null ? topicPrefix : () -> null;
    topicPrefixGot   = true;
    return this;
  }

  public ConsumerDefinitionCustomBuilder commitOn(Class<?> errorClass) {
    commitOnClassList.add(errorClass);
    return this;
  }

  public ConsumerDefinitionCustomBuilder commitOnAll(Collection<Class<?>> errorClassAll) {
    errorClassAll.forEach(this::commitOn);
    return this;
  }

  public ConsumerDefinitionCustomBuilder topic(String topic) {
    topicList.add(topic);
    return this;
  }

  public ConsumerDefinitionCustomBuilder groupId(String groupId) {
    this.groupId = groupId;
    return this;
  }

  public ConsumerDefinitionCustomBuilder name(String name) {
    this.name = name;
    return this;
  }

  public ConsumerDefinitionCustomBuilder profile(String profile) {
    this.profile = new Profile(profile);
    return this;
  }

  public ConsumerDefinitionCustomBuilder folderPath(String folderPath) {
    this.folderPath = folderPath;
    return this;
  }

  public ConsumerDefinitionCustomBuilder workerCountFileName(String workerCountFileName) {
    this.workerCountFileName = workerCountFileName;
    return this;
  }

  public ConsumerDefinitionCustomBuilder autoOffsetReset(AutoOffsetReset autoOffsetReset) {
    this.autoOffsetReset = autoOffsetReset;
    return this;
  }

  public ConsumerDefinitionCustomBuilder description(String description) {
    this.description = description;
    return this;
  }

  public ConsumerDefinitionCustomBuilder consumerFilter(ConsumerFilter consumerFilter) {
    this.consumerFilter = requireNonNull(consumerFilter, "i94aySq67t :: consumerFilter == null");
    return this;
  }

  public ConsumerDefinitionCustomBuilder direct(boolean direct, boolean ignoreKafka) {
    this.isDirect    = direct;
    this.ignoreKafka = ignoreKafka;
    return this;
  }

  public ConsumerDefinitionCustom build() {
    String          name                = requireNonNull(this.name, "name");
    String          workerCountFileName = fnn(this.workerCountFileName, name + ".workerCount");
    AutoOffsetReset autoOffsetReset     = requireNonNull(this.autoOffsetReset, "autoOffsetReset");
    String          groupId             = fnn(this.groupId, name);

    if (!topicPrefixGot) {
      throw new RuntimeException("fBQhDe1MrY :: Define topicPrefix");
    }

    return new ConsumerDefinitionCustom(customConsumer,
                                        customStrConsumer,
                                        customBytesConsumer,
                                        customListBoxConsumer,
                                        name,
                                        topicList,
                                        folderPath,
                                        workerCountFileName,
                                        autoOffsetReset,
                                        groupId,
                                        description,
                                        logger,
                                        topicPrefix,
                                        commitOnClassList,
                                        isDirect,
                                        ignoreKafka,
                                        consumerFilter,
                                        findTopicContentType(),
                                        profile);
  }

  private @Nonnull TopicContentType findTopicContentType() {
    TopicContentType topicContentType = null;
    {
      int consumerCount = 0;
      if (customConsumer != null) {
        consumerCount++;
        topicContentType = TopicContentType.BOX;
      }
      if (customStrConsumer != null) {
        consumerCount++;
        topicContentType = TopicContentType.STR;
      }
      if (customBytesConsumer != null) {
        consumerCount++;
        topicContentType = TopicContentType.BYTES;
      }
      if (customListBoxConsumer != null) {
        consumerCount++;
        topicContentType = TopicContentType.LIST_BOX;
      }
      if (topicContentType == null) {
        throw new RuntimeException("tbr15pZPuI :: No consumer function");
      }
      if (consumerCount > 1) {
        throw new RuntimeException("Y4drcYN7ja :: Too many consumer functions");
      }
    }
    return topicContentType;
  }

}
