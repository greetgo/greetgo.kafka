package kz.greetgo.kafka.consumer;

public interface ConsumerActivateFilter {

  boolean activateFromController(ConsumerDefinitionOnController definition);

  boolean activateCustom(ConsumerDefinitionCustom definition);

}
