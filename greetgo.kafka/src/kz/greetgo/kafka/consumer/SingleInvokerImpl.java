package kz.greetgo.kafka.consumer;

import kz.greetgo.kafka.consumer.annotations.TopicContentType;
import kz.greetgo.kafka.model.Box;
import kz.greetgo.kafka.serializer.DeserializerError;
import kz.greetgo.kafka.util.GenericUtil;
import lombok.NonNull;
import org.apache.kafka.clients.consumer.ConsumerRecord;

import java.lang.reflect.InvocationTargetException;

import static kz.greetgo.kafka.core.logger.LoggerType.LOG_CONSUMER_ERROR_INVOKING;
import static kz.greetgo.kafka.core.logger.LoggerType.LOG_CONSUMER_ILLEGAL_ACCESS_EXCEPTION_INVOKING_METHOD;

class SingleInvokerImpl implements SingleInvoker {
  private final InvokeSessionFactoryControllerBuilder owner;

  private final boolean[]              canAcceptDeserializerError;
  private final int                    parametersCount;
  private final ParameterValueReader[] parameterValueReaders;
  private final Class<?>               gettingBodyClass;
  private final TopicContentType       topicContentType;
  private final Class<?>[]             commitOn;

  public SingleInvokerImpl(final InvokeSessionFactoryControllerBuilder owner,
                           boolean[] canAcceptDeserializerError,
                           int parametersCount,
                           ParameterValueReader[] parameterValueReaders,
                           Class<?> gettingBodyClass,
                           TopicContentType topicContentType,
                           Class<?>[] commitOn) {
    this.owner                      = owner;
    this.canAcceptDeserializerError = canAcceptDeserializerError;
    this.parametersCount            = parametersCount;
    this.parameterValueReaders      = parameterValueReaders;
    this.gettingBodyClass           = gettingBodyClass;
    this.topicContentType           = topicContentType;
    this.commitOn                   = commitOn;
  }

  @Override
  public InvokeResult invoke(@NonNull ConsumerRecord<byte[], Object> record,
                             @NonNull InvokeSessionContext context,
                             @NonNull Profile profile) {

    if (!canAcceptDeserializerError[0] && record.value() instanceof DeserializerError) {
      return InvokeResult.ok();
    }

    Object[] parameters = new Object[parametersCount];

    for (int i = 0; i < parametersCount; i++) {
      parameters[i] = parameterValueReaders[i].read(record, context, profile);
    }

    return invokeMethod(parameters, profile);
  }

  private boolean isInFilterBox(@NonNull ConsumerRecord<byte[], Object> record) {
    Object recordValue = record.value();

    if (recordValue == null) {
      return false;
    }

    if (!(recordValue instanceof final Box box)) {
      return false;
    }

    if (gettingBodyClass == null) {
      return owner.boxFilter.isInFilter(box, record.key());
    }

    if (gettingBodyClass == Box.class) {
      return owner.boxFilter.isInFilter(box, record.key());
    }
    if (!gettingBodyClass.isInstance(box.body)) {
      return false;
    }

    return owner.boxFilter.isInFilter(box, record.key());
  }

  private boolean isInFilterStr(@NonNull ConsumerRecord<byte[], Object> record) {
    return record.value() instanceof String;
  }

  private boolean isInFilterBytes(@NonNull ConsumerRecord<byte[], Object> record) {
    return record.value() instanceof byte[];
  }

  @Override
  public boolean isInFilter(@NonNull ConsumerRecord<byte[], Object> record) {
    if (record.value() instanceof DeserializerError) {
      return true;
    }
    return switch (topicContentType) {
      case BOX -> isInFilterBox(record);
      case STR -> isInFilterStr(record);
      case BYTES -> isInFilterBytes(record);
      case LIST_BOX -> throw new IllegalArgumentException("JDvoAkaWMs :: list box content type is inappropriate for SingleInvoker");
    };
  }

  private InvokeResult invokeMethod(Object[] parameters, Profile profile) {
    try {
      owner.method.invoke(owner.controller, parameters);
      return InvokeResult.ok();
    } catch (IllegalAccessException e) {
      if (owner.logger.isShow(LOG_CONSUMER_ILLEGAL_ACCESS_EXCEPTION_INVOKING_METHOD)) {
        owner.logger.logConsumerIllegalAccessExceptionInvoking(e, GenericUtil.infoOf(owner.controller, owner.method));
      }
      return InvokeResult.of(false, e);
    } catch (InvocationTargetException e) {
      Throwable error = e.getTargetException();
      if (owner.logger.isShow(LOG_CONSUMER_ERROR_INVOKING)) {
        owner.logger.logConsumerErrorInvoking(error, GenericUtil.infoOf(owner.controller, owner.method), profile);
      }

      for (Class<?> aClass : commitOn) {
        if (aClass.isInstance(error)) {
          return InvokeResult.of(true, error);
        }
      }

      return InvokeResult.of(false, error);
    }
  }

}
