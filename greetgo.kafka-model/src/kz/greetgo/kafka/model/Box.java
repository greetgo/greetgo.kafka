package kz.greetgo.kafka.model;

import java.util.Collection;
import java.util.stream.Stream;

import static java.util.stream.Collectors.joining;

public class Box {

  /**
   * Kafka id
   */
  public String id;

  /**
   * Body of message
   */
  public Object body;

  @Override
  public String toString() {
    return Stream.of(
                   new Object[]{"id", id},
                   new Object[]{"body", body}
                 )
                 .filter(e -> e[1] != null)
                 .map(e -> e[0] + "=" + e[1])
                 .collect(joining(", ", getClass().getSimpleName() + "{", "}"));
  }

  public static void validateBody(Object body) throws Throwable {

    if (body == null) {
      throw new NullPointerException(Box.class.getName() + ".body == null");
    }

    if (body instanceof KafkaValidator) {
      ((KafkaValidator) body).validateKafka();
      return;
    }

    if (body instanceof Collection) {
      //noinspection rawtypes
      for (Object object : ((Collection) body)) {
        validateIt(object);
      }
      return;
    }
  }

  public void validate() throws Throwable {
    validateBody(body);
  }

  private static void validateIt(Object object) throws Throwable {
    if (object instanceof KafkaValidator) {
      ((KafkaValidator) object).validateKafka();
    }
  }

}
