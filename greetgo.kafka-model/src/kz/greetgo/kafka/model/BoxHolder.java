package kz.greetgo.kafka.model;

public interface BoxHolder<T> {

  T body();

}
