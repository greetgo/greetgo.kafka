#!/bin/sh

cd "$(dirname "$0")" || exit 13

docker compose down

docker run --rm -i \
  -v "$HOME/volumes/greetgo.kafka:/volumes" \
  busybox:1.31.0 \
  find /volumes/ -maxdepth 1 -mindepth 1 -exec rm -rf {} \;

docker compose up -d
