package kz.greetgo.kafka_test_app.app002;

import kz.greetgo.kafka.consumer.annotations.KafkaDescription;
import kz.greetgo.kafka.consumer.annotations.Topic;
import kz.greetgo.kafka.model.Box;

public class GroundConsumer {

  @Topic("stones")
  @KafkaDescription("Этим методом кидают камни")
  public void stoneBlowing(Box box) {
    System.out.println("FMlzQKPUv6 :: stoneBlowing: box = " + box);
  }

  @Topic("stones")
  @KafkaDescription("Здесь камни ловят")
  public void catchingStones(Box box) {
    System.out.println("Je5h1hoWtt :: catchingStones: box = " + box);
  }

}
