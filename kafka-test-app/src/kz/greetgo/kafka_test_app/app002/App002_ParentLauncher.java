package kz.greetgo.kafka_test_app.app002;

import kz.greetgo.kafka.consumer.config.ConsumerReactorConfig;
import kz.greetgo.kafka.util.Listener;
import kz.greetgo.util.fui.FUI;

import java.time.Duration;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;

public abstract class App002_ParentLauncher {
  protected final ConcurrentLinkedQueue<Listener> listeners = new ConcurrentLinkedQueue<>();

  @SuppressWarnings("SameParameterValue")
  protected void interact(FUI fui, String methodName, ConsumerReactorConfig consumerConfig) {

    fui.button(methodName + "/onChangeParams",
               () -> listeners.add(consumerConfig.onChangeParams(
                 () -> System.out.println("a94exQ0s1T :: onChangeParams for " + methodName), null)));
    fui.button(methodName + "/onChangeWorkerCount",
               () -> listeners.add(consumerConfig.onChangeWorkerCount(
                 () -> System.out.println("R0y1bgLJdi :: " + methodName + ".workerCount = " + consumerConfig.getWorkerCount(null)), null)));

    fui.button(methodName + "/getCountAndDuration", () -> {
      final int      workerCount  = consumerConfig.getWorkerCount(null);
      final Duration pollDuration = consumerConfig.getPollDuration(null);
      System.out.println("iKvd7gQvE9 :: workerCount = " + workerCount + ", pollDuration = " + pollDuration.toMillis());
    });

    fui.button(methodName + "/getConfigMap", () -> {
      System.out.println("A9HH4gNlP7 :: configMap");
      consumerConfig.getConfigMap(null)
                    .entrySet()
                    .stream()
                    .sorted(Map.Entry.comparingByKey())
                    .forEach(e -> System.out.println("    " + e.getKey() + " = " + e.getValue()));
    });

  }

  protected void printHelp() {
    final String c1 = SkyConsumer.class.getSimpleName();
    final String c2 = GroundConsumer.class.getSimpleName();
    final String c3 = RiverConsumer.class.getSimpleName();

    System.out.println();
    System.out.println();
    System.out.println();
    System.out.println("BVcuKL8jYk :: See folder `someMethods`");
    System.out.println("                  - delete `createAllConfigFiles.btn`");
    System.out.println("                  - delete all `onChangeParams.btn` and `onChangeWorkerCount.btn` in dirs `m_*/`");
    System.out.println("                  - then delete `getConfigMap.btn` and `getCountAndDuration.btn`");
    System.out.println("           ");
    System.out.println("           What must be happened:");
    System.out.println("                in Zookeeper (http://localhost:13010/) must be created:");
    System.out.println("                    ");
    System.out.println("                    /app-consumer-reactor-config/consumers/" + c1 + ".config");
    System.out.println("                    /app-consumer-reactor-config/consumers/" + c2 + ".config");
    System.out.println("                    /app-consumer-reactor-config/consumers/" + c3 + ".config");
    System.out.println("                    /app-consumer-reactor-config/consumers/params.config");
    System.out.println("                    ");
    System.out.println("                Changing this files you MUST see messages in terminal `onChangeParams` or `onChangeWorkerCount`");
    System.out.println("      ");
    System.out.println("      You can delete `onChangeParams.btn` and/or `onChangeWorkerCount.btn` to see its values");
    System.out.println();
    System.out.println();
    System.out.println();
  }

  protected void killAllListeners() {
    while (true) {
      final Listener listener = listeners.poll();
      if (listener == null) {
        return;
      }
      listener.kill();
    }
  }

  protected enum ChangeSnowConsumers {
    SETUP_NULL_SIN_12, CLEAR,
    ADD_NULL_12, ADD_SIN_12, ADD_COS_12, ADD_NULL_1, ADD_SIN_1, ADD_COS_1,
    DEL_NULL_12, DEL_SIN_12, DEL_COS_12, DEL_NULL_1, DEL_SIN_1, DEL_COS_1,
  }


  protected void changeSnowConsumers(ChangeSnowConsumers variant, WinterConsumer winterConsumer) {

    switch (variant) {
      case SETUP_NULL_SIN_12:
        winterConsumer.setSnowTopics(WinterConsumer.SETUP);
        return;

      case CLEAR:
        winterConsumer.setSnowTopics(Set.of());
        return;

      case ADD_NULL_12:
        winterConsumer.addSnowTopics(WinterConsumer.NULL_12);
        return;
      case ADD_SIN_12:
        winterConsumer.addSnowTopics(WinterConsumer.SIN_12);
        return;
      case ADD_COS_12:
        winterConsumer.addSnowTopics(WinterConsumer.COS_12);
        return;

      case ADD_NULL_1:
        winterConsumer.addSnowTopics(WinterConsumer.NULL_1);
        return;
      case ADD_SIN_1:
        winterConsumer.addSnowTopics(WinterConsumer.SIN_1);
        return;
      case ADD_COS_1:
        winterConsumer.addSnowTopics(WinterConsumer.COS_1);
        return;


      case DEL_NULL_12:
        winterConsumer.delSnowTopics(WinterConsumer.NULL_12);
        return;
      case DEL_SIN_12:
        winterConsumer.delSnowTopics(WinterConsumer.SIN_12);
        return;
      case DEL_COS_12:
        winterConsumer.delSnowTopics(WinterConsumer.COS_12);
        return;

      case DEL_NULL_1:
        winterConsumer.delSnowTopics(WinterConsumer.NULL_1);
        return;
      case DEL_SIN_1:
        winterConsumer.delSnowTopics(WinterConsumer.SIN_1);
        return;
      case DEL_COS_1:
        winterConsumer.delSnowTopics(WinterConsumer.COS_1);
        return;

    }

    throw new RuntimeException("BpYH26bKH3 :: Unknown " + ChangeSnowConsumers.class.getSimpleName() + " = " + variant);
  }
}
