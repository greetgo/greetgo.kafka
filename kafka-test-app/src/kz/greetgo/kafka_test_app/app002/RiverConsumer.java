package kz.greetgo.kafka_test_app.app002;

import kz.greetgo.kafka.consumer.annotations.ConsumersFolder;
import kz.greetgo.kafka.consumer.annotations.KafkaDescription;
import kz.greetgo.kafka.consumer.annotations.Topic;
import kz.greetgo.kafka.model.Box;

@ConsumersFolder("status/river")
public class RiverConsumer {

  @Topic("water")
  @KafkaDescription("Течение реки Волги")
  public void blowVolga(Box box) {
    System.out.println("k999kQx79s :: blowVolga: box = " + box);
  }

}
