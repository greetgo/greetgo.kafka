package kz.greetgo.kafka_test_app.app002;

import kz.greetgo.kafka.consumer.annotations.KafkaDescription;
import kz.greetgo.kafka.consumer.annotations.Topic;
import kz.greetgo.kafka.model.Box;

public class SkyConsumer {

  @Topic("clouds")
  @KafkaDescription("Этим методом можно считывать облака на небе")
  public void cloudsReading(Box box) {
    System.out.println("FMlzQKPUv6 :: cloudsReading: box = " + box);
  }

  @Topic("stars")
  @KafkaDescription("Этим методом читаются звёзды на небе.\nИ когда загорается новая звезда\nслучается грандиозный праздник!!!")
  public void starsLighting(Box box) {
    System.out.println("Je5h1hoWtt :: otherMethod: box = " + box);
  }

}
