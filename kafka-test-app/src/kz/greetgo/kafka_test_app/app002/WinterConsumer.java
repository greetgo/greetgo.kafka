package kz.greetgo.kafka_test_app.app002;

import kz.greetgo.kafka.consumer.TopicsInProfile;
import kz.greetgo.kafka.consumer.annotations.TopicListMethod;
import kz.greetgo.kafka.model.Box;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

public class WinterConsumer {

  public static final TopicsInProfile NULL_12 = TopicsInProfile.of(Set.of("SNOW1", "SNOW2"));
  public static final TopicsInProfile SIN_12  = TopicsInProfile.of(Set.of("SNOW_SIN1", "SNOW_SIN2"), "SIN");
  public static final TopicsInProfile COS_12  = TopicsInProfile.of(Set.of("SNOW_COS1", "SNOW_COS2"), "COS");

  public static final TopicsInProfile NULL_1 = TopicsInProfile.of(Set.of("SNOW1"));
  public static final TopicsInProfile SIN_1  = TopicsInProfile.of(Set.of("SNOW_SIN1"), "SIN");
  public static final TopicsInProfile COS_1  = TopicsInProfile.of(Set.of("SNOW_COS1"), "COS");

  public static final Set<TopicsInProfile> SETUP = Set.of(NULL_12, SIN_12);

  public final AtomicReference<Set<TopicsInProfile>> snowTopics = new AtomicReference<>(SETUP);

  @TopicListMethod("snowTopics")
  public void snow(Box box) {
    System.out.println("2oaR14Fzy1 :: snow " + box);
  }

  @SuppressWarnings("unused")
  public Set<TopicsInProfile> snowTopics() {
    return snowTopics.get();
  }

  public void setSnowTopics(Set<TopicsInProfile> snowTopics) {
    this.snowTopics.set(snowTopics);
  }

  public void addSnowTopics(TopicsInProfile tip) {
    final Set<TopicsInProfile> current = snowTopics.get();
    Set<TopicsInProfile>       newSet  = new HashSet<>();
    if (current != null) {
      newSet.addAll(current);
    }
    newSet.add(tip);
    snowTopics.set(Set.copyOf(newSet));
  }

  public void delSnowTopics(TopicsInProfile tip) {
    final Set<TopicsInProfile> current = snowTopics.get();
    Set<TopicsInProfile>       newSet  = new HashSet<>();
    if (current != null) {
      newSet.addAll(current);
    }
    newSet.remove(tip);
    snowTopics.set(Set.copyOf(newSet));
  }
}
