package kz.greetgo.kafka_test_app.config;

import java.util.List;

import static kz.greetgo.kafka_test_app.util.ReflectUtil.getAllStaticFinalFieldNames;

public interface Topics {

  List<String> ALL = getAllStaticFinalFieldNames(Topics.class, f -> !"ALL".equals(f.getName()));

  String CLIENT = "CLIENT";

  String CONTACT = "CONTACT";

}
