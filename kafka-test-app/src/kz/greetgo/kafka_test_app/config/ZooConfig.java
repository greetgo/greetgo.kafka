package kz.greetgo.kafka_test_app.config;

public interface ZooConfig {
  String zooServers();
}
