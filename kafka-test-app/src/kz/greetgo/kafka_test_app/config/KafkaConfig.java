package kz.greetgo.kafka_test_app.config;

import java.util.Map;

public interface KafkaConfig {
  String kafkaServers();

  Map<String, String> params();
}
