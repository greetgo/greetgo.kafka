package kz.greetgo.kafka_test_app.app_common;

import kz.greetgo.kafka_test_app.config.KafkaConfig;
import kz.greetgo.kafka_test_app.config.ZooConfig;

import java.util.Map;

public class StaticConfigFactory {

  public static ZooConfig zooConfig() {
    return () -> "localhost:13019";
  }

  public static KafkaConfig kfConfig() {
    return new KafkaConfig() {
      @Override
      public String kafkaServers() {
        return "localhost:13011,localhost:13012,localhost:13013,localhost:13014";
      }

      @Override
      public Map<String, String> params() {
        return Map.of();
      }
    };
  }

  public static KafkaConfig kfOtherConfig() {
    return new KafkaConfig() {
      @Override
      public String kafkaServers() {
        return "localhost:13111";
      }

      @Override
      public Map<String, String> params() {
        return Map.of(
          "sasl.jaas.config",
          "org.apache.kafka.common.security.plain.PlainLoginModule" +
            " required serviceName=\"Kafka\" username=\"alice\" password=\"alice-secret\";"

          , "sasl.mechanism", "PLAIN"

          , "ssl.truststore.location", System.getenv("PWD") + "/kafka-test-app/docker-compose-env/cert/files/client.truststore.jks"

          , "ssl.truststore.password", "111222"

          , "security.protocol", "SASL_SSL"
        );
      }
    };
  }

}
