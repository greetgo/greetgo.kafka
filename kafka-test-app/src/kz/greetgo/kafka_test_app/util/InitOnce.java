package kz.greetgo.kafka_test_app.util;

import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Supplier;

public class InitOnce<T> {
  private final Supplier<T> creator;

  public InitOnce(Supplier<T> creator) {
    this.creator = creator;
  }

  private final AtomicReference<T> holder = new AtomicReference<>(null);

  public T get() {
    {
      T t = holder.get();
      if (t != null) {
        return t;
      }
    }
    synchronized (holder) {
      {
        T t = holder.get();
        if (t != null) {
          return t;
        }
      }
      {
        T t = creator.get();
        holder.set(t);
        return t;
      }
    }
  }
}
