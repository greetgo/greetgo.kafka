package kz.greetgo.kafka_test_app.util;

import kz.greetgo.kafka.consumer.config.ConsumerConfigDefaults;
import kz.greetgo.kafka.core.logger.LoggerExternal;
import kz.greetgo.kafka.core.logger.LoggerType;
import kz.greetgo.kafka.producer.config.ProducerConfigDefaults;
import kz.greetgo.kafka_test_app.app001.KafkaLogMessageAcceptor;

import java.util.HashMap;
import java.util.Map;

import static java.util.stream.Collectors.toSet;

public class ReactorUtil {
  public static String controllerToName(Object controller) {
    var name = controller.getClass().getSimpleName();
    {
      var idx = name.indexOf("$$");
      if (idx >= 0) {
        name = name.substring(0, idx);
      }
    }
    {
      var idx = name.indexOf("Consumer");
      if (idx >= 0) {
        name = name.substring(0, idx);
      }
    }
    return name + ".workerCount";
  }

  public static void prepareLoggers(LoggerExternal logger) {
    logger.setDestination(new KafkaLogMessageAcceptor());

    Map<LoggerType, Boolean> logMap = new HashMap<>();


    logMap.put( /* Это должно быть всегда включено */ LoggerType.LOG_CONSUMER_POLL_EXCEPTION_HAPPENED, true);

    logMap.put(LoggerType.SHOW_PRODUCER_CONFIG, true);
    logMap.put(LoggerType.SHOW_CONSUMER_WORKER_CONFIG, false);
    logMap.put(LoggerType.LOG_CLOSE_PRODUCER, false);
    logMap.put(LoggerType.LOG_CONSUMER_ERROR_INVOKING, true);
    logMap.put(LoggerType.CONSUMER_LOG_DIRECT_INVOKE_ERROR, true);
    logMap.put(LoggerType.LOG_CONSUMER_ILLEGAL_ACCESS_EXCEPTION_INVOKING_METHOD, true);
    logMap.put(LoggerType.LOG_START_CONSUMER_WORKER, true);
    logMap.put(LoggerType.LOG_CONSUMER_REACTOR_REFRESH, true);
    logMap.put(LoggerType.LOG_PRODUCER_DIRECT_CONSUMER_ERROR, true);
    logMap.put(LoggerType.LOG_CONSUMER_FINISH_WORKER, true);

    logger.setShowLoggerTypes(
      logMap.entrySet()
            .stream()
            .filter(Map.Entry::getValue)
            .map(Map.Entry::getKey)
            .collect(toSet())
    );
  }

  public static ProducerConfigDefaults createProducerConfigDefaults() {
    var producerDefs = ProducerConfigDefaults.empty();
    {
      //noinspection SpellCheckingInspection
      producerDefs.append(" prod.acks                         =    all      ");
      producerDefs.append(" prod.buffer.memory                =    33554432 ");
      producerDefs.append(" prod.compression.type             =    lz4      ");
      producerDefs.append(" prod.batch.size                   =    16384    ");
      producerDefs.append(" prod.connections.max.idle.ms      =    540000   ");
      producerDefs.append(" prod.request.timeout.ms           =    30000    ");
      producerDefs.append(" prod.linger.ms                    =    1        ");
      producerDefs.append(" prod.retries                               =  2147483647   ");
      producerDefs.append(" prod.max.in.flight.requests.per.connection =  1            ");
      producerDefs.append(" prod.delivery.timeout.ms                   =  35000        ");
      producerDefs.append(" prod.max.block.ms                          =  25000        ");
    }
    return producerDefs;
  }


  public static ProducerConfigDefaults createProducerConfigDefaultsOther() {
    var producerDefs = ProducerConfigDefaults.empty();
    {
      //noinspection SpellCheckingInspection
      producerDefs.append(" prod.acks                         =    all      ");
      producerDefs.append(" prod.buffer.memory                =    33554432 ");
      producerDefs.append(" prod.compression.type             =    lz4      ");
      producerDefs.append(" prod.batch.size                   =    16384    ");
      producerDefs.append(" prod.connections.max.idle.ms      =    540001   ");
      producerDefs.append(" prod.request.timeout.ms           =    30001    ");
      producerDefs.append(" prod.linger.ms                    =    1        ");
      producerDefs.append(" prod.retries                               =  2147483647   ");
      producerDefs.append(" prod.max.in.flight.requests.per.connection =  1            ");
      producerDefs.append(" prod.delivery.timeout.ms                   =  35001        ");
      producerDefs.append(" prod.max.block.ms                          =  25001        ");
    }
    return producerDefs;
  }

  public static ConsumerConfigDefaults createConsumerConfigDefaults() {
    ConsumerConfigDefaults consumerDefs = new ConsumerConfigDefaults();
    {
      consumerDefs.addDefinition(" Long   con.auto.commit.interval.ms           1000  ");
      consumerDefs.addDefinition(" Long   con.fetch.min.bytes                      1  ");
      consumerDefs.addDefinition(" Long   con.max.partition.fetch.bytes    141943040  ");
      consumerDefs.addDefinition(" Long   con.connections.max.idle.ms         540000  ");
      consumerDefs.addDefinition(" Long   con.default.api.timeout.ms           60000  ");
      consumerDefs.addDefinition(" Long   con.fetch.max.bytes               52428800  ");
      consumerDefs.addDefinition(" Long   con.retry.backoff.ms                  2000  ");

      consumerDefs.addDefinition(" Long   con.session.timeout.ms               45000  ");
      consumerDefs.addDefinition(" Long   con.heartbeat.interval.ms             5000  ");
      consumerDefs.addDefinition(" Long   con.max.poll.interval.ms           3000000  ");
      consumerDefs.addDefinition(" Long   con.max.poll.records                   500  ");

      consumerDefs.addDefinition(" Long   con.receive.buffer.bytes             65536  ");
      consumerDefs.addDefinition(" Long   con.request.timeout.ms               30000  ");
      consumerDefs.addDefinition(" Long   con.send.buffer.bytes               131072  ");
      consumerDefs.addDefinition(" Long   con.fetch.max.wait.ms                  500  ");

      consumerDefs.addDefinition(" Int out.worker.count        1  ");
      consumerDefs.addDefinition(" Int out.poll.duration.ms  800  ");
    }
    return consumerDefs;
  }

  public static ConsumerConfigDefaults createConsumerConfigDefaultsOther() {
    ConsumerConfigDefaults consumerDefs = new ConsumerConfigDefaults();
    {
      consumerDefs.addDefinition(" Long   con.auto.commit.interval.ms           1001  ");
      consumerDefs.addDefinition(" Long   con.fetch.min.bytes                      1  ");
      consumerDefs.addDefinition(" Long   con.max.partition.fetch.bytes    141943041  ");
      consumerDefs.addDefinition(" Long   con.connections.max.idle.ms         540001  ");
      consumerDefs.addDefinition(" Long   con.default.api.timeout.ms           60001  ");
      consumerDefs.addDefinition(" Long   con.fetch.max.bytes               52428801  ");
      consumerDefs.addDefinition(" Long   con.retry.backoff.ms                  2001  ");

      consumerDefs.addDefinition(" Long   con.session.timeout.ms               45001  ");
      consumerDefs.addDefinition(" Long   con.heartbeat.interval.ms             5001  ");
      consumerDefs.addDefinition(" Long   con.max.poll.interval.ms           3000001  ");
      consumerDefs.addDefinition(" Long   con.max.poll.records                   501  ");

      consumerDefs.addDefinition(" Long   con.receive.buffer.bytes             65536  ");
      consumerDefs.addDefinition(" Long   con.request.timeout.ms               30001  ");
      consumerDefs.addDefinition(" Long   con.send.buffer.bytes               131072  ");
      consumerDefs.addDefinition(" Long   con.fetch.max.wait.ms                  501  ");

      consumerDefs.addDefinition(" Int out.worker.count        2  ");
      consumerDefs.addDefinition(" Int out.poll.duration.ms  800  ");
    }
    return consumerDefs;
  }
}
