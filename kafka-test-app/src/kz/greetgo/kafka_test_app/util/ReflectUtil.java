package kz.greetgo.kafka_test_app.util;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class ReflectUtil {

  public static List<String> getAllStaticFinalFieldNames(Class<?> aClass, Predicate<Field> filter) {
    return Arrays.stream(aClass.getFields())
                 .filter(f -> Modifier.isStatic(f.getModifiers()))
                 .filter(f -> Modifier.isFinal(f.getModifiers()))
                 .filter(f -> isFieldNameRight(f.getName()))
                 .filter(filter)
                 .map(Field::getName)
                 .collect(Collectors.toList());
  }


  private static boolean isFieldNameRight(String fieldName) {
    if (fieldName.startsWith("__$") && fieldName.endsWith("$__")) {
      return false;
    }
    return true;
  }

}
