package kz.greetgo.kafka_test_app.app003;

import kz.greetgo.kafka.consumer.BatchBoxRecord;
import kz.greetgo.kafka.consumer.ConsumerDirectStatus;
import kz.greetgo.kafka.consumer.CustomBoxConsumer;
import kz.greetgo.kafka.consumer.DynamicReactors;
import kz.greetgo.kafka.consumer.Profile;
import kz.greetgo.kafka.consumer.TopicsInProfile;
import kz.greetgo.kafka.consumer.annotations.DataRef;
import kz.greetgo.kafka.consumer.annotations.Direct;
import kz.greetgo.kafka.consumer.annotations.GroupId;
import kz.greetgo.kafka.consumer.annotations.KafkaDescription;
import kz.greetgo.kafka.consumer.annotations.KafkaLatest;
import kz.greetgo.kafka.consumer.annotations.KafkaProfile;
import kz.greetgo.kafka.consumer.annotations.ProfileRef;
import kz.greetgo.kafka.consumer.annotations.Topic;
import kz.greetgo.kafka.consumer.annotations.TopicListMethod;
import kz.greetgo.kafka.consumer.annotations.TopicRef;
import kz.greetgo.kafka.core.KafkaReactor;
import kz.greetgo.kafka.model.Box;
import kz.greetgo.kafka.producer.KafkaFuture;
import kz.greetgo.kafka.producer.ProducerFacade;
import kz.greetgo.kafka.serializer.DeserializerError;
import kz.greetgo.kafka_test_app.model.SpaceStar;
import kz.greetgo.kafka_test_app.model.SpaceStone;
import kz.greetgo.util.RND;
import kz.greetgo.util.fui.BoolAccessor;
import kz.greetgo.util.fui.FUI;
import kz.greetgo.util.fui.IntAccessor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.IntSupplier;
import java.util.function.Supplier;

import static java.nio.charset.StandardCharsets.UTF_8;

@SuppressWarnings("FieldCanBeLocal")
@Slf4j
public class SpaceConsumer {
  private final FUI                              fui;
  private final Supplier<ProducerFacade<Object>> producer;
  private final String                           cmdFolder;
  private final BoolAccessor                     viewStoneFromMain;
  private final BoolAccessor                     viewStoneFromOther;
  private final BoolAccessor                     viewStars;
  private final BoolAccessor                     viewStarsFromAnotherFolder;
  private final BoolAccessor                     viewStarRunProcess;
  private final BoolAccessor                     viewStarsAndDeserializerErrors;
  private final BoolAccessor                     viewDirectStoneFromOther;
  private final BoolAccessor                     readStones;
  private final BoolAccessor                     readStars;
  private final BoolAccessor                     readFromMain;
  private final BoolAccessor                     readFromOther;
  private final BoolAccessor                     showDetectReadTopics;
  private final IntAccessor                      delayStarRunProcess;

  private final BoolAccessor sendStarsAndStones;
  private final IntAccessor  sendStarsPortion;
  private final IntAccessor  sendStonesPortion;
  private final IntAccessor  sendStarsTimeoutMs;
  private final IntAccessor  sendStonesTimeoutMs;
  private final Thread       sendStarsThread;
  private final Thread       sendStonesThread;

  public final SpaceConsumerAnotherFolder fromAnotherFolder;

  public static final String TOPIC_STONES = "STONES";
  public static final String TOPIC_STARS  = "STARS";

  public SpaceConsumer(@NonNull FUI fui, @NonNull Supplier<ProducerFacade<Object>> producer) {
    this.fui                       = fui;
    this.producer                  = producer;
    cmdFolder                      = getClass().getSimpleName();
    viewStoneFromMain              = fui.entryBool(cmdFolder + "/viewStonesFromMain", true);
    viewStoneFromOther             = fui.entryBool(cmdFolder + "/viewStonesFromOther", true);
    viewDirectStoneFromOther       = fui.entryBool(cmdFolder + "/viewDirectStoneFromOther", true);
    viewStars                      = fui.entryBool(cmdFolder + "/viewStars", false);
    viewStarsFromAnotherFolder     = fui.entryBool(cmdFolder + "/viewStarsFromAnotherFolder", true);
    viewStarRunProcess             = fui.entryBool(cmdFolder + "/starRunProcess-view", true);
    viewStarsAndDeserializerErrors = fui.entryBool(cmdFolder + "/viewStarsAndDeserializerErrors", true);
    delayStarRunProcess            = fui.entryInt(cmdFolder + "/starRunProcess-delay-s", 0);

    fromAnotherFolder = new SpaceConsumerAnotherFolder(viewStarsFromAnotherFolder, viewStarRunProcess, delayStarRunProcess);

    readFromMain  = fui.entryBool(cmdFolder + "/read/From_MAIN", false);
    readFromOther = fui.entryBool(cmdFolder + "/read/From_OTHER", false);
    readStones    = fui.entryBool(cmdFolder + "/read/Stones", true);
    readStars     = fui.entryBool(cmdFolder + "/read/Stars", true);

    showDetectReadTopics = fui.entryBool(cmdFolder + "/read/__Show_Detect__Read__Topics__", false);

    final String permanently = cmdFolder + "/permanently-send-stars-and-stones";
    sendStarsAndStones = fui.entryBool(permanently + "/activated", false);

    sendStarsPortion  = fui.entryInt(permanently + "/sendStarsPortion", 10);
    sendStonesPortion = fui.entryInt(permanently + "/sendStonesPortion", 10);

    sendStarsTimeoutMs  = fui.entryInt(permanently + "/sendStarsTimeoutMs", 2000);
    sendStonesTimeoutMs = fui.entryInt(permanently + "/sendStonesTimeoutMs", 2000);

    sendStarsThread  = new Thread(this::permanentlySendingStars);
    sendStonesThread = new Thread(this::permanentlySendingStones);
  }


  public void sendStarsToMain(int clientCount) {
    sendSpaceStars(clientCount, null);
  }

  public void sendStonesToMain(int clientCount) {
    sendSpaceStones(clientCount, null);
  }

  public void sendStarsToOther(int clientCount) {
    sendSpaceStars(clientCount, "other");
  }

  public void sendStonesToOther(int clientCount) {
    sendSpaceStones(clientCount, "other");
  }

  private void sendSpaceStones(int clientCount, String profile) {
    List<KafkaFuture> futures = new ArrayList<>();

    for (int i = 0; i < clientCount; i++) {
      final SpaceStone c = new SpaceStone();
      c.id   = RND.str(10) + "-" + i;
      c.name = RND.str(17);

      futures.add(producer.get()
                          .sending(c)
                          .profile(profile)
                          .toTopic(TOPIC_STONES)
                          .go());
    }

    futures.forEach(KafkaFuture::awaitAndGet);
  }

  private void sendSpaceStars(int clientCount, String profile) {
    List<KafkaFuture> futures = new ArrayList<>();

    for (int i = 0; i < clientCount; i++) {
      final SpaceStar c = new SpaceStar();
      c.id   = RND.str(10) + "-" + i;
      c.name = RND.str(17);

      futures.add(producer.get()
                          .sending(c)
                          .profile(profile)
                          .toTopic(TOPIC_STARS)
                          .go());
    }

    futures.forEach(KafkaFuture::awaitAndGet);
  }

  public void initInterface(KafkaReactor kafkaReactor) {

    final IntAccessor count = fui.entryInt(cmdFolder + "/count", 1);

    fui.button(cmdFolder + "/sendStonesToMain", () -> sendStonesToMain(count.get()));
    fui.button(cmdFolder + "/sendStonesToOther", () -> sendStonesToOther(count.get()));

    fui.button(cmdFolder + "/sendStarsToMain", () -> sendStarsToMain(count.get()));
    fui.button(cmdFolder + "/sendStarsToOther", () -> sendStarsToOther(count.get()));

    fui.button(cmdFolder + "/sendStarsToOtherWithAdditionalParams", () -> sendStarsToOtherWithAdditionalParams(count.get()));

    final BoolAccessor dynamicGroupId = fui.entryBool(cmdFolder + "/dynamicConsumer-groupId", false);
    fui.button(cmdFolder + "/dynamicConsumer/activate-box", () -> toggleDynamicConsumer(kafkaReactor.dynamicConsumers(), dynamicGroupId));
    fui.button(cmdFolder + "/dynamicConsumer/activate-list-box", () -> toggleDynamicListConsumer(kafkaReactor.dynamicConsumers()));

    sendStarsThread.start();
    sendStonesThread.start();
  }

  private void sendStarsToOtherWithAdditionalParams(int count) {
    List<KafkaFuture> futures = new ArrayList<>();

    for (int i = 0; i < count; i++) {
      final SpaceStar c = new SpaceStar();
      c.id   = RND.str(10) + "-" + i;
      c.name = RND.str(17);

      futures.add(producer.get()
                          .sending(c)
                          .kafkaId(RND.str(10))
                          .addHeader("cool-phrase", "Hello World")
                          .addHeader("small", "Status Quo")
                          .withKey(RND.str(11))
                          .toPartition((i + 1) % 48)
                          .profile("other")
                          .toTopic(TOPIC_STARS)
                          .go());
    }

    futures.forEach(KafkaFuture::awaitAndGet);
  }

  private void toggleDynamicListConsumer(DynamicReactors dynamicReactors) {

    long id = dynamicReactors.adding()
                             .name("dynamic-list")
                             .topic(TOPIC_STARS)
                             .customListBoxConsumer(this::comeIntoListConsumer)
                             .add()
                             .id();

    final long oldId = dynamicReactorId.getAndSet(id);

    if (oldId > 0) {
      dynamicReactors.stopById(id);
      log.info("3J3Z44IDa9 :: Stop dynamic consumer with id = " + id);
      return;
    }

  }

  private void comeIntoListConsumer(List<BatchBoxRecord> batchBoxRecords) {
    log.info("9mb0mjqHpu :: Come message into list consumer in thread " + Thread.currentThread().getName() + ", \n" +
               "List size: " + batchBoxRecords.size());

    for (BatchBoxRecord batchBoxRecord : batchBoxRecords) {
      log.info(batchBoxRecord.box.body + "");
    }

    log.info("T32s9z5V2g :: end processing list of boxes");
  }

  private void toggleDynamicConsumer(DynamicReactors dynamicReactors, BoolAccessor dynamicGroupId) {

    {
      final long id = dynamicReactorId.getAndSet(0);

      if (id > 0) {
        dynamicReactors.stopById(id);
        log.info("3J3Z44IDa9 :: Stop dynamic consumer with id = " + id);
        return;
      }
    }

    {
      final CustomBoxConsumer customConsumer = (box, boxMeta) -> log.info(
        "xetHL0769K :: Come dynamic: from topic: " + boxMeta.fromTopic() + " " + boxMeta.profile()
          + " partition " + boxMeta.fromPartition()
          + " offset " + boxMeta.offset()
          + " " + box
          + " in thread " + Thread.currentThread().getName()
      );

      final long id = dynamicReactors.adding()
                                     .profile("other")
                                     .name("test")
                                     .topic(TOPIC_STARS)
                                     .topic(TOPIC_STONES)
                                     .description("dyn cons")
                                     .groupId(dynamicGroupId.is() ? "dynamic-one" : "dynamic-two")
                                     .customConsumer(customConsumer)
                                     .add()
                                     .id();

      log.info("4PDL1XSWlo :: Start dynamic consumer with id = " + id);

      if (dynamicReactorId.compareAndSet(0, id)) {
        return;
      }

      dynamicReactors.stopById(id);
      log.info("6eh7HfP1EW :: Stop dynamic consumer with id = " + id);
    }
  }

  private final AtomicLong dynamicReactorId = new AtomicLong(0);

  public static class SpaceStoneHolder {
    @DataRef
    public SpaceStone spaceStone;
  }

  @Topic(TOPIC_STONES)
  @KafkaDescription("readStonesFromMain")
  public void readStonesFromMain(List<SpaceStoneHolder> list) {
    if (viewStoneFromMain.is()) {
      final List<SpaceStone> spaceStones = list.stream().map(x -> x.spaceStone).toList();
      log.info("gL446v2gBb :: KAFKA_MAIN: Come space stone {}", spaceStones);
    }
  }

  @Special
  @Topic(TOPIC_STONES)
  @KafkaDescription("specialReadStonesFromMain")
  public void specialStoneReader(SpaceStone spaceStone) {
    log.info("slLQD6Q5DX :: KAFKA_MAIN: SPECIAL come space stone " + spaceStone);
  }

  @Topic(TOPIC_STONES)
  @KafkaProfile("other")
  public void readStonesFromOther(SpaceStone spaceStone) {
    if (viewStoneFromOther.is()) {
      log.info("Dl1hlBdqDs :: KAFKA_OTHER: Come space stone {}", spaceStone);
    }
  }

  @TopicListMethod("detectReadTopics")
  public void read(Box box,
                   DeserializerError deserializerError,
                   @TopicRef String fromTopic, @ProfileRef Profile profile) {

    if (deserializerError != null) {
      System.err.println("1kVpY915ux :: ERROR");
      deserializerError.printStackTrace(System.err);
      return;
    }
    log.info("KlJ06xC234 :: fromTopic `{}` {} read {}", fromTopic, profile, box.body);
  }

  @SuppressWarnings("unused")
  public Set<TopicsInProfile> detectReadTopics() {
    Set<TopicsInProfile> ret = new HashSet<>();

    Set<String> topics = new HashSet<>();

    if (readStars.is()) {
      topics.add(TOPIC_STARS);
    }
    if (readStones.is()) {
      topics.add(TOPIC_STONES);
    }

    if (readFromMain.is()) {
      ret.add(TopicsInProfile.of(topics));
    }

    if (readFromOther.is()) {
      ret.add(TopicsInProfile.of(topics, "other"));
    }

    if (showDetectReadTopics.is()) {
      log.info("a3UNC08QGK :: detectReadTopics returns " + ret);
    }

    return ret;
  }

  @Direct(fromMethod = "detectDirect")
  @Topic(TOPIC_STONES)
  @KafkaProfile("other")
  public void directReadStonesFromOther(SpaceStone spaceStone) {
    if (viewDirectStoneFromOther.is()) {
      log.info("6gAvGW1Q1V :: DIRECT: Come space stone " + spaceStone + " from thread " + Thread.currentThread().getName());
    }
  }


  @SuppressWarnings("unused")
  public ConsumerDirectStatus detectDirect() {
    return ConsumerDirectStatus.of(true, false);
  }

  @Topic({TOPIC_STONES, TOPIC_STARS})
  public void readStars(SpaceStar star) {
    if (viewStars.is()) {
      log.info("e1ee5gDJ19 :: read star " + star);
    }
  }

  @Topic({TOPIC_STONES, TOPIC_STARS})
  public void readStarsAndErrors(SpaceStar star, DeserializerError error) {
    if (viewStarsAndDeserializerErrors.is()) {
      if (error != null) {
        log.info("DI1CZev3AJ :: readStarsAndErrors : errorData: " + new String(error.data, UTF_8));
      } else {
        log.info("69sg1ylW2D :: read star: " + star);
      }
    }
  }

  public void stopWorking() {
    working.set(false);
    sendStarsThread.interrupt();
    sendStonesThread.interrupt();
  }

  private final AtomicBoolean working = new AtomicBoolean(true);

  private void permanentlySendingStars() {
    sendingCircle(sendStarsTimeoutMs::getInt,
                  printErrToStdErr(this::sendStarsNow),
                  () -> log.info("B8un0VGMCq :: permanentlySendingStars STOPPED"));
  }

  private void permanentlySendingStones() {
    sendingCircle(sendStonesTimeoutMs::getInt,
                  printErrToStdErr(this::sendStonesNow),
                  () -> log.info("aFV5HN016P :: permanentlySendingStones STOPPED"));
  }

  Runnable printErrToStdErr(Runnable runnable) {
    return () -> {
      try {
        runnable.run();
      } catch (RuntimeException e) {
        e.printStackTrace(System.err);
      }
    };
  }

  private void sendingCircle(IntSupplier timeout, Runnable sendNow, Runnable printStopMessage) {
    while (working.get()) {
      try {
        Thread.sleep(timeout.getAsInt());
      } catch (InterruptedException e) {
        break;
      }

      if (!working.get()) {
        break;
      }

      if (!sendStarsAndStones.is()) {
        continue;
      }

      sendNow.run();

    }

    printStopMessage.run();
  }

  private void sendStonesNow() {
    final Integer portion = sendStonesPortion.get();
    if (portion != null && portion > 0) {
      List<KafkaFuture> futures = new ArrayList<>();
      for (int i = 0; i < portion && working.get(); i++) {
        final SpaceStone c = new SpaceStone();
        c.id   = RND.str(10) + "-" + i;
        c.name = RND.str(17);

        futures.add(producer.get()
                            .sending(c)
                            .kafkaId(RND.str(10))
                            .withKey(RND.str(11))
                            .toTopic(TOPIC_STONES)
                            .go());
      }
      futures.forEach(KafkaFuture::awaitAndGet);
    }
  }

  private void sendStarsNow() {
    final Integer portion = sendStarsPortion.get();
    if (portion != null && portion > 0) {
      List<KafkaFuture> futures = new ArrayList<>();
      for (int i = 0; i < portion && working.get(); i++) {
        final SpaceStar c = new SpaceStar();
        c.id   = RND.str(10) + "-" + i;
        c.name = RND.str(17);

        futures.add(producer.get()
                            .sending(c)
                            .kafkaId(RND.str(10))
                            .withKey(RND.str(11))
                            .toTopic(TOPIC_STARS)
                            .go());
      }
      futures.forEach(KafkaFuture::awaitAndGet);
    }
  }

  int count = 0;

  @Topic(TOPIC_STONES)
  @KafkaLatest
  @GroupId("usingLatestAnnotation4")
  public void usingLatestAnnotation(SpaceStone spaceStone) {
    log.info("3emmNl11xb :: KAFKA_OTHER: Come space stone {} - count = {}", spaceStone, ++count);
  }

}
