package kz.greetgo.kafka_test_app.app003;

import kz.greetgo.kafka.consumer.annotations.ConsumersFolder;
import kz.greetgo.kafka.consumer.annotations.Topic;
import kz.greetgo.kafka_test_app.model.SpaceStar;
import kz.greetgo.util.fui.BoolAccessor;
import kz.greetgo.util.fui.IntAccessor;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import static kz.greetgo.kafka_test_app.app003.SpaceConsumer.TOPIC_STARS;

@Slf4j
@ConsumersFolder("other-folder")
public class SpaceConsumerAnotherFolder {
  private final BoolAccessor viewStarsFromAnotherFolder;
  private final BoolAccessor viewStarRunProcess;
  private final IntAccessor  delayStarRunProcess;


  public SpaceConsumerAnotherFolder(@NonNull BoolAccessor viewStarsFromAnotherFolder,
                                    @NonNull BoolAccessor viewStarRunProcess,
                                    @NonNull IntAccessor delayStarRunProcess) {
    this.viewStarsFromAnotherFolder = viewStarsFromAnotherFolder;
    this.viewStarRunProcess         = viewStarRunProcess;
    this.delayStarRunProcess        = delayStarRunProcess;
  }

  @Topic(TOPIC_STARS)
  public void readStarsFromAnotherFolder(SpaceStar star) {
    if (viewStarsFromAnotherFolder.is()) {
      log.info("IazHK1WRcA :: read star from another folder " + star);
    }
  }

  @SneakyThrows
  @Topic(TOPIC_STARS)
  public void starRunProcess(SpaceStar star) {
    if (viewStarRunProcess.is()) {
      log.info("FiFT1X88yV :: start  process " + star);
    }

    Thread.sleep(delayStarRunProcess.get() * 1000);

    if (viewStarRunProcess.is()) {
      log.info("TFQm1jPQuN :: finish process " + star);
    }
  }
}
