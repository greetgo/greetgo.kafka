package kz.greetgo.kafka_test_app.app003;

import kz.greetgo.kafka.producer.ProducerFacade;
import kz.greetgo.util.fui.FUI;
import lombok.NonNull;

import java.util.concurrent.atomic.AtomicReference;

public class InKafkaMigrationRegister {
  private final AtomicReference<ProducerFacade<String>> producerStr;

  public InKafkaMigrationRegister(AtomicReference<ProducerFacade<String>> producerStr, @NonNull FUI fui) {
    this.producerStr = producerStr;

    String prefix = getClass().getSimpleName();

    fui.button(prefix + "/add-message-1", this::addMessage01);

  }

  private void addMessage01() {

    String c1 = """
             {
                  "recordId" : "r1",
                  "externalId" : "aaa1",
                  "boCode" : "CLIENT",
                  "fields" : [ {
                    "code" : "SURNAME",
                    "apiValue" : "Иванов"
                  }, {
                    "code" : "NAME",
                    "apiValue" : "Иван"
                  } ]
                }
      """;
    String c2 = """
             {
                  "recordId" : "r2",
                  "externalId" : "aaa2",
                  "boCode" : "CLIENT",
                  "fields" : [ {
                    "code" : "SURNAME",
                    "apiValue" : "Петров"
                  }, {
                    "code" : "NAME",
                    "apiValue" : "Константин"
                  } ]
                }
      """;
    String c3 = """
             {
                  "recordId" : "r3",
                  "externalId" : "aaa2",
                  "boCode" : "CLIENT",
                  "fields" : [ {
                    "code" : "COMMENT",
                    "apiValue" : "Начинается новый день"
                  } ]
                }
      """;

    String c4 = """
      {
        "recordId": "r8",
        "externalId": "gg2",
        "boCode": "CLIENT_GROUP",
        "fields": [
          {
            "code": "GROUP_NAME",
            "apiValue": "Вторая группа"
          }
        ],
        "boFields": [
          {
            "fieldCode": "CLIENTS",
            "boiInput": {
              "boCode": "CLIENT",
              "externalId": "aaa1"
            }
          },
          {
            "fieldCode": "CLIENTS",
            "boiInput": {
              "boCode": "CLIENT",
              "externalId": "aaa2"
            }
          }
        ]
      }
      """;

    producerStr.get()
               .sending(c4)
               .toTopic("IN_MIGRATION")
               .profile(null)
               .go()
               .awaitAndGet();

    System.out.println("KHvd0KZz2s :: Отправил JSON");
  }

  public void onExitApplication() {
    System.out.println("LI5theirFz :: onExitApplication");
  }
}
