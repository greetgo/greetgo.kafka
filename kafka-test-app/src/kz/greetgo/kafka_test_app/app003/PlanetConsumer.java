package kz.greetgo.kafka_test_app.app003;

import kz.greetgo.kafka.consumer.BoxMeta;
import kz.greetgo.kafka.consumer.DynamicReactors;
import kz.greetgo.kafka.consumer.Profile;
import kz.greetgo.kafka.consumer.TopicsInProfile;
import kz.greetgo.kafka.consumer.annotations.KafkaProfile;
import kz.greetgo.kafka.consumer.annotations.Offset;
import kz.greetgo.kafka.consumer.annotations.Partition;
import kz.greetgo.kafka.consumer.annotations.ProfileRef;
import kz.greetgo.kafka.consumer.annotations.Timestamp;
import kz.greetgo.kafka.consumer.annotations.Topic;
import kz.greetgo.kafka.consumer.annotations.TopicContent;
import kz.greetgo.kafka.consumer.annotations.TopicContentType;
import kz.greetgo.kafka.consumer.annotations.TopicListMethod;
import kz.greetgo.kafka.core.KafkaReactor;
import kz.greetgo.kafka.producer.KafkaFuture;
import kz.greetgo.kafka.producer.ProducerFacade;
import kz.greetgo.util.RND;
import kz.greetgo.util.fui.BoolAccessor;
import kz.greetgo.util.fui.FUI;
import kz.greetgo.util.fui.IntAccessor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Supplier;

import static java.nio.charset.StandardCharsets.UTF_8;

@Slf4j
public class PlanetConsumer {
  private final FUI                              fui;
  private final Supplier<ProducerFacade<String>> producerStr;
  private final Supplier<ProducerFacade<byte[]>> producerBytes;
  private final String                           cmdFolder;
  private final BoolAccessor                     viewMarsMessagesFromMain;
  private final BoolAccessor                     viewMarsMessagesFromOther;
  private final BoolAccessor                     readSaturnFromMain;
  private final BoolAccessor                     readSaturnFromOther;
  final         IntAccessor                      totalSend;
  final         IntAccessor                      portion;

  public PlanetConsumer(@NonNull FUI fui, Supplier<ProducerFacade<String>> producerStr, Supplier<ProducerFacade<byte[]>> producerBytes) {
    this.fui           = fui;
    this.producerStr   = producerStr;
    this.producerBytes = producerBytes;
    cmdFolder          = getClass().getSimpleName();

    viewMarsMessagesFromMain  = fui.entryBool(cmdFolder + "/viewMarsMessagesFromMain", true);
    viewMarsMessagesFromOther = fui.entryBool(cmdFolder + "/viewMarsMessagesFromOther", true);

    String readerFolder = cmdFolder + "/reader";
    readSaturnFromMain  = fui.entryBool(readerFolder + "/readSaturnFromMain", false);
    readSaturnFromOther = fui.entryBool(readerFolder + "/readSaturnFromOther", false);

    totalSend = fui.entryInt(cmdFolder + "/count/total-send", 1);
    portion   = fui.entryInt(cmdFolder + "/count/portion", 1000);
  }

  public void initInterface(KafkaReactor kafkaReactor) {

    fui.button(cmdFolder + "/sendMessageToMarsOverMain", () -> sendMessageToMars(TOPIC_MARS, null));
    fui.button(cmdFolder + "/sendMessageToSaturnOverMain", () -> sendMessageToMars(TOPIC_SATURN, null));
    fui.button(cmdFolder + "/sendMessageToMarsOverOther", () -> sendMessageToMars(TOPIC_MARS, "other"));
    fui.button(cmdFolder + "/sendMessageToSaturnOverOther", () -> sendMessageToMars(TOPIC_SATURN, "other"));

    fui.button(cmdFolder + "/bytes/sendMessageToMarsOverMain", () -> sendMessageToMars__bytes(TOPIC_MARS, null));
    fui.button(cmdFolder + "/bytes/sendMessageToSaturnOverMain", () -> sendMessageToMars__bytes(TOPIC_SATURN, null));
    fui.button(cmdFolder + "/bytes/sendMessageToMarsOverOther", () -> sendMessageToMars__bytes(TOPIC_MARS, "other"));
    fui.button(cmdFolder + "/bytes/sendMessageToSaturnOverOther", () -> sendMessageToMars__bytes(TOPIC_SATURN, "other"));

    fui.button(cmdFolder + "/dynamic/activate-str", () -> activateDynamicStrConsumer(kafkaReactor.dynamicConsumers()));
    fui.button(cmdFolder + "/dynamic/activate-bytes", () -> activateDynamicBytesConsumer(kafkaReactor.dynamicConsumers()));
    fui.button(cmdFolder + "/dynamic/deactivate", () -> deactivateDynamicConsumer(kafkaReactor.dynamicConsumers()));

  }

  private final AtomicLong activatedDynamicConsumerId = new AtomicLong(0);

  private void deactivateDynamicConsumer(DynamicReactors dynamicReactors) {
    final long oldId = activatedDynamicConsumerId.getAndSet(0);
    if (oldId > 0) {
      dynamicReactors.stopById(oldId);
    }
  }

  private void activateDynamicBytesConsumer(@NonNull DynamicReactors dynamicReactors) {
    final long id = dynamicReactors.adding()
                                   .name("dynamic-bytes")
                                   .topic(TOPIC_MARS)
                                   .topic(TOPIC_SATURN)
                                   .customBytesConsumer(this::comeIntoBytesConsumer)
                                   .add()
                                   .id();

    final long oldId = activatedDynamicConsumerId.getAndSet(id);

    if (oldId > 0) {
      dynamicReactors.stopById(oldId);
    }
  }

  private void comeIntoBytesConsumer(byte[] recordBytes, BoxMeta boxMeta) {
    log.info("tW1myZ7k7Y :: Come message into bytes consumer: " + new String(recordBytes, UTF_8)
               + " in thread " + Thread.currentThread().getName());
  }

  private void activateDynamicStrConsumer(@NonNull DynamicReactors dynamicReactors) {
    final long id = dynamicReactors.adding()
                                   .name("dynamic-bytes")
                                   .topic(TOPIC_MARS)
                                   .topic(TOPIC_SATURN)
                                   .customStrConsumer(this::comeIntoStrConsumer)
                                   .add()
                                   .id();

    final long oldId = activatedDynamicConsumerId.getAndSet(id);

    if (oldId > 0) {
      dynamicReactors.stopById(oldId);
    }
  }

  private void comeIntoStrConsumer(String recordStr, BoxMeta boxMeta) {
    log.info("nmh22p2QA5 :: Come message into str consumer: " + recordStr
               + " in thread " + Thread.currentThread().getName());
  }

  private static final String TOPIC_MARS   = "MARS";
  private static final String TOPIC_SATURN = "SATURN";

  private void sendMessageToMars(String topic, String profile) {
    final List<KafkaFuture> futureList = new ArrayList<>();
    final int               totalSend  = this.totalSend.getInt();
    final int               portion    = this.portion.getInt();

    for (int i = 0; i < totalSend; i++) {
      futureList.add(producerStr.get()
                                .sending("Message " + i + " to " + topic + ": " + RND.str(10))
                                .toTopic(topic)
                                .profile(profile)
                                .go());

      if (futureList.size() >= portion) {
        futureList.forEach(KafkaFuture::awaitAndGet);
        futureList.clear();
      }
    }
    futureList.forEach(KafkaFuture::awaitAndGet);
  }

  private void sendMessageToMars__bytes(String topic, String profile) {
    final List<KafkaFuture> futureList = new ArrayList<>();
    final int               totalSend  = this.totalSend.getInt();
    final int               portion    = this.portion.getInt();

    for (int i = 0; i < totalSend; i++) {
      futureList.add(producerBytes.get()
                                  .sending(("Using bytes message " + i + " to " + topic + ": " + RND.str(10)).getBytes())
                                  .toTopic(topic)
                                  .profile(profile)
                                  .go());
      if (futureList.size() >= portion) {
        futureList.forEach(KafkaFuture::awaitAndGet);
        futureList.clear();
      }
    }
    futureList.forEach(KafkaFuture::awaitAndGet);
  }

  @Topic(TOPIC_MARS)
  @TopicContent(TopicContentType.STR)
  public void comeMessageToMars(String message, @Partition int partition, @Offset long offset, @Timestamp Date ts) {
    if (viewMarsMessagesFromMain.is()) {
      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
      log.info("Mz1tr7Qb5e :: Profile(main) From partition: " + partition + ", offset: " + offset + ", ts: " + sdf.format(ts)
                 + " Come message to Mars: [" + message + "] in thread " + Thread.currentThread().getName());
    }
  }

  @Topic(TOPIC_MARS)
  @KafkaProfile("other")
  @TopicContent(TopicContentType.STR)
  public void comeMessageToMarsFromOther(String message, @Partition int partition, @Offset long offset, @Timestamp Date ts) {
    if (viewMarsMessagesFromOther.is()) {
      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
      log.info("q40udeT8XC :: Profile(other) From partition: " + partition + ", offset: " + offset + ", ts: " + sdf.format(ts)
                 + " Come message to Mars: [" + message + "] in thread " + Thread.currentThread().getName());
    }
  }

  @TopicListMethod("detectTopics")
  @TopicContent(TopicContentType.STR)
  public void readSaturnMessages(String message,
                                 @Partition int partition, @Offset long offset, @Timestamp Date ts,
                                 @ProfileRef Profile profile) {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    log.info("2SF8Y93J7Z :: " + profile + " From partition: " + partition + ", offset: " + offset + ", ts: " + sdf.format(ts)
               + " Read message for Saturn: [" + message + "] in thread " + Thread.currentThread().getName());
  }

  @SuppressWarnings("unused")
  public List<TopicsInProfile> detectTopics() {
    List<TopicsInProfile> ret = new ArrayList<>();

    if (readSaturnFromMain.is()) {
      ret.add(TopicsInProfile.of(Set.of(TOPIC_SATURN), new Profile(null)));
    }

    if (readSaturnFromOther.is()) {
      ret.add(TopicsInProfile.of(Set.of(TOPIC_SATURN), new Profile("other")));
    }

    return ret;
  }
}
