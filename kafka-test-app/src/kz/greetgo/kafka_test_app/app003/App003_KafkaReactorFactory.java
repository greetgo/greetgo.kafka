package kz.greetgo.kafka_test_app.app003;

import kz.greetgo.kafka.consumer.ConsumerActivateFilter;
import kz.greetgo.kafka.consumer.ConsumerDefinitionCustom;
import kz.greetgo.kafka.consumer.ConsumerDefinitionOnController;
import kz.greetgo.kafka.consumer.Profile;
import kz.greetgo.kafka.consumer.config.ConsumerConfigDefaults;
import kz.greetgo.kafka.consumer.config.ConsumerConfigDefaultsAccess;
import kz.greetgo.kafka.consumer.config.ConsumerConnectSetter;
import kz.greetgo.kafka.consumer.config.ConsumerReactorConfigFactory;
import kz.greetgo.kafka.core.KafkaReactor;
import kz.greetgo.kafka.core.config.EventConfigFileFactory;
import kz.greetgo.kafka.core.config.EventConfigFileFactoryOverStorage;
import kz.greetgo.kafka.core.config.EventConfigStorageZooKeeper;
import kz.greetgo.kafka.core.config.ZooConnectParams;
import kz.greetgo.kafka.core.logger.LogMessageAcceptor;
import kz.greetgo.kafka.core.logger.Logger;
import kz.greetgo.kafka.core.logger.LoggerDestinationMessageBridge;
import kz.greetgo.kafka.producer.ProducerConfigDefaultsAccess;
import kz.greetgo.kafka.producer.ProducerFacade;
import kz.greetgo.kafka.producer.config.ProducerConfigDefaults;
import kz.greetgo.kafka.producer.config.ProducerConnectSetter;
import kz.greetgo.kafka.producer.config.ProducerReactorConfigFactory;
import kz.greetgo.kafka_test_app.config.KafkaConfig;
import kz.greetgo.kafka_test_app.config.ZooConfig;
import kz.greetgo.kafka_test_app.util.InitOnce;
import kz.greetgo.kafka_test_app.util.ReactorUtil;
import kz.greetgo.strconverter.StrConverter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.util.Collection;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BooleanSupplier;
import java.util.function.Consumer;

@Slf4j
public class App003_KafkaReactorFactory implements AutoCloseable {
  private final ZooConfig                        zooConfig;
  private final KafkaConfig                      kfConfig;
  private final KafkaConfig                      kfOtherConfig;
  private final StrConverter                     strConverter;
  private final Collection<Object>               consumers;
  private final Consumer<ProducerFacade<Object>> producer;
  private final Consumer<ProducerFacade<String>> producerStr;
  private final Consumer<ProducerFacade<byte[]>> producerBytes;
  private final BooleanSupplier                  enabledSupplier;

  public App003_KafkaReactorFactory(ZooConfig zooConfig,
                                    KafkaConfig kfConfig,
                                    KafkaConfig kfOtherConfig,
                                    StrConverter strConverter,
                                    Collection<Object> consumers,
                                    Consumer<ProducerFacade<Object>> producer,
                                    Consumer<ProducerFacade<String>> producerStr,
                                    Consumer<ProducerFacade<byte[]>> producerBytes,
                                    BooleanSupplier enabledSupplier) {

    this.zooConfig       = zooConfig;
    this.kfConfig        = kfConfig;
    this.kfOtherConfig   = kfOtherConfig;
    this.strConverter    = strConverter;
    this.consumers       = consumers;
    this.producer        = producer;
    this.producerStr     = producerStr;
    this.producerBytes   = producerBytes;
    this.enabledSupplier = enabledSupplier;
  }

  public final AtomicReference<String> topicPrefix = new AtomicReference<>("P_");

  private final InitOnce<KafkaReactor> kafkaReactor = new InitOnce<>(this::create);

  public KafkaReactor kafkaReactor() {
    return kafkaReactor.get();
  }

  private final ConcurrentLinkedQueue<AutoCloseable> deferList = new ConcurrentLinkedQueue<>();

  @Override
  public void close() {
    deferList.forEach(x -> {
      try {
        x.close();
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
    });

    kafkaReactor.get().join();
  }

  private <T extends AutoCloseable> T defer(T t) {
    deferList.add(t);
    return t;
  }


  private KafkaReactor create() {

    ConsumerConfigDefaults       consumerDefs  = ReactorUtil.createConsumerConfigDefaults();
    ConsumerConfigDefaults       consumerDefs2 = ReactorUtil.createConsumerConfigDefaultsOther();
    ConsumerConfigDefaultsAccess cda           = profile -> "other".equals(profile.profile) ? consumerDefs2 : consumerDefs;

    ProducerConfigDefaults       producerDefs      = ReactorUtil.createProducerConfigDefaults();
    ProducerConfigDefaults       producerDefsOther = ReactorUtil.createProducerConfigDefaultsOther();
    ProducerConfigDefaultsAccess pda               = profile -> "other".equals(profile.profile) ? producerDefsOther : producerDefs;

    var configStorage = new EventConfigStorageZooKeeper("app003",
                                                        zooConfig::zooServers,
                                                        ZooConnectParams.builder().build());

    EventConfigFileFactory top = defer(new EventConfigFileFactoryOverStorage(configStorage));


    ConsumerReactorConfigFactory consumerRCF = defer(ConsumerReactorConfigFactory.builder()
                                                                                 .configFactory(top.cd("consumers"))
                                                                                 .defaults(cda)
                                                                                 .profileConfigPrefix("external_kafka_clusters/")
                                                                                 .build());

    ConsumerReactorConfigFactory consumerRCF2 = defer(ConsumerReactorConfigFactory.builder()
                                                                                  .configFactory(top.cd("dynamic-consumers"))
                                                                                  .profileConfigPrefix("external_kafka_clusters/")
                                                                                  .defaults(cda)
                                                                                  .build());

    ProducerReactorConfigFactory producerRCF = defer(ProducerReactorConfigFactory.builder()
                                                                                 .configFactory(top.cd("producers"))
                                                                                 .defaults(pda)
                                                                                 .configFileExtension(".conf")
                                                                                 .build());

    return defer(KafkaReactor.builder()
                             .strConverter(() -> strConverter)
                             .producerConfigFactory(producerRCF)
                             .consumerConfigFactory(consumerRCF)
                             .dynamicConsumerConfigFactory(consumerRCF2)
                             .consumerConnectSetter(createConsumerConnectSetter())
                             .producerConnectSetter(createProducerConnectSetter())
                             .authorSupplier(() -> "SharonStone")
                             .consumerHostId(() -> "mini-host")
                             .topicPrefix(topicPrefix::get)
                             .addControllers(consumers)
                             .onLogger(ReactorUtil::prepareLoggers)
                             .controllerToWorkerCountFileName(ReactorUtil::controllerToName)
                             .refreshTopicListMillis(() -> 5000)
                             .onLogger(this::configureLogger)
                             .consumerActivateFilter(consumerActivateFilter)
                             .consumersEnabledSupplier(enabledSupplier)
                             .build());
  }

  private final ConsumerActivateFilter consumerActivateFilter = new ConsumerActivateFilter() {
    @Override
    public boolean activateFromController(@NonNull ConsumerDefinitionOnController definition) {
      final boolean ret = definition.getMethod().getAnnotation(Special.class) == null;
      log.info("JG3DI9g8pO :: activateFromController(" + definition.logDisplay() + ") returns " + ret);
      return ret;
    }

    @Override
    public boolean activateCustom(ConsumerDefinitionCustom definition) {
      return true;
    }
  };


  private void configureLogger(@NonNull Logger logger) {
    logger.setDestination(LoggerDestinationMessageBridge.of(new LogMessageAcceptor() {
      @Override
      public boolean isInfoEnabled() {
        return log.isInfoEnabled();
      }

      @Override
      public void info(String message) {
        log.info(message);
      }

      @Override
      public boolean isDebugEnabled() {
        return log.isDebugEnabled();
      }

      @Override
      public void debug(String message) {
        log.debug(message);
      }

      @Override
      public void error(String message, Throwable throwable) {
        log.error(message, throwable);
      }
    }));
  }

  private ConsumerConnectSetter createConsumerConnectSetter() {
    return (targetConfigMap, params) -> {
      final Profile profile = params.profile();

      if (profile.profile == null) {
        targetConfigMap.put("bootstrap.servers", kfConfig.kafkaServers());
        targetConfigMap.putAll(kfConfig.params());
        return;
      }

      if ("other".equals(profile.profile)) {
        targetConfigMap.put("bootstrap.servers", kfOtherConfig.kafkaServers());
        targetConfigMap.putAll(kfOtherConfig.params());
        return;
      }

      throw new RuntimeException("XJkr6QUO5W :: ConsumerConnectSetter: Unknown profile = `" + profile + "`");
    };
  }

  private ProducerConnectSetter createProducerConnectSetter() {
    return (targetConfigMap, params) -> {
      final String profile = params.profile();

      if (profile == null) {
        targetConfigMap.put("bootstrap.servers", kfConfig.kafkaServers());
        targetConfigMap.putAll(kfConfig.params());
        return;
      }

      if ("other".equals(profile)) {
        targetConfigMap.put("bootstrap.servers", kfOtherConfig.kafkaServers());
        targetConfigMap.putAll(kfOtherConfig.params());
        return;
      }

      throw new RuntimeException("xmLZmYYT2q :: ProducerConnectSetter: Unknown profile = `" + profile + "`");
    };
  }

  public KafkaReactor start() {
    final KafkaReactor kafkaReactor = kafkaReactor();

    this.producer.accept(defer(kafkaReactor.createProducer("main")));
    this.producerStr.accept(defer(kafkaReactor.createProducerStr("str")));
    this.producerBytes.accept(defer(kafkaReactor.createProducerBytes("bytes")));

    kafkaReactor.startConsumers();

    return kafkaReactor;
  }

}
