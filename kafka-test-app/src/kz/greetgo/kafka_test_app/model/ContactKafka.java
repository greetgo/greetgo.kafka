package kz.greetgo.kafka_test_app.model;

import kz.greetgo.kafka.core.HasStrKafkaKey;

public class ContactKafka implements HasStrKafkaKey {
  public String id;
  public String clientId;
  public String phoneNumber;

  @Override
  public String extractStrKafkaKey() {
    return clientId;
  }

  @Override
  public String toString() {
    return "ContactKafka{" +
      "id='" + id + '\'' +
      ", clientId='" + clientId + '\'' +
      ", phoneNumber='" + phoneNumber + '\'' +
      '}';
  }

}
