package kz.greetgo.kafka_test_app.model;

import kz.greetgo.kafka.core.HasStrKafkaKey;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ClientKafka implements HasStrKafkaKey {
  public String id;
  public String surname;
  public String name;
  public String patronymic;

  public List<Contact> contactList = new ArrayList<>();

  public List<Contact> contactList() {
    var x = contactList;
    return x != null ? x : (contactList = new ArrayList<>());
  }

  @Override
  public String extractStrKafkaKey() {
    return id;
  }

  @Override
  public String toString() {
    return "Client{" +
      "id='" + id + '\'' +
      ", surname='" + surname + '\'' +
      ", name='" + name + '\'' +
      ", patronymic='" + patronymic + '\'' +
      ", contactCount=" + Optional.ofNullable(contactList).map(List::size).orElse(0) +
      '}';
  }

}
