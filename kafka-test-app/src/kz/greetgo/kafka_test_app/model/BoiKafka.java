package kz.greetgo.kafka_test_app.model;

import kz.greetgo.kafka.core.HasStrKafkaKey;

public class BoiKafka implements HasStrKafkaKey {
  public String topic;
  public String id;
  public String label1;
  public String label2;
  public String label3;

  @Override
  public String extractStrKafkaKey() {
    return id;
  }

  @Override
  public String toString() {
    return "BoiKafka{" +
      "topic='" + topic + '\'' +
      ", id='" + id + '\'' +
      ", label1='" + label1 + '\'' +
      ", label2='" + label2 + '\'' +
      ", label3='" + label3 + '\'' +
      '}';
  }
}
