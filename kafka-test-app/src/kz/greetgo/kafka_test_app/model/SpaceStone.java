package kz.greetgo.kafka_test_app.model;

import kz.greetgo.kafka.core.HasStrKafkaKey;

public class SpaceStone implements HasStrKafkaKey {
  public String id;
  public String name;

  @Override
  public String toString() {
    return "SpaceStone{" +
      "id='" + id + '\'' +
      ", name='" + name + '\'' +
      '}';
  }

  @Override
  public String extractStrKafkaKey() {
    return id;
  }
}
