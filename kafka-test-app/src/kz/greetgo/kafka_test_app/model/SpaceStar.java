package kz.greetgo.kafka_test_app.model;

import kz.greetgo.kafka.core.HasStrKafkaKey;

public class SpaceStar implements HasStrKafkaKey {
  public String id;
  public String name;

  @Override
  public String extractStrKafkaKey() {
    return id;
  }

  @Override
  public String toString() {
    return "SpaceStar{id='" + id + "', name='" + name + "'}";
  }
}
