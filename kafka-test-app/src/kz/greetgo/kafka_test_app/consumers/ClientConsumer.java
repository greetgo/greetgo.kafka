package kz.greetgo.kafka_test_app.consumers;

import kz.greetgo.kafka.consumer.annotations.GroupId;
import kz.greetgo.kafka.consumer.annotations.KafkaDescription;
import kz.greetgo.kafka.consumer.annotations.Topic;
import kz.greetgo.kafka.producer.ProducerFacade;
import kz.greetgo.kafka_test_app.model.ClientKafka;
import kz.greetgo.kafka_test_app.model.Contact;
import kz.greetgo.kafka_test_app.model.ContactKafka;

import java.util.List;
import java.util.function.Supplier;

import static kz.greetgo.kafka_test_app.config.Topics.CLIENT;
import static kz.greetgo.kafka_test_app.config.Topics.CONTACT;

public class ClientConsumer {

  private final Supplier<ProducerFacade> producer;

  public ClientConsumer(Supplier<ProducerFacade> producer) {
    this.producer = producer;
  }

  public List<ClientKafka> comeClientList = null;

  @Topic(CLIENT)
  @KafkaDescription("Вычитываем клиентов")
  @GroupId("client_to_contact_2")
  public void clientToContact(ClientKafka client) {
    {
      var x = comeClientList;
      if (x != null) {
        x.add(client);
      }
    }

    for (final Contact contact : client.contactList()) {

      ContactKafka k = new ContactKafka();
      k.id          = contact.id;
      k.phoneNumber = contact.phoneNumber;
      k.clientId    = client.id;

      producer.get()
              .sending(k)
              .toTopic(CONTACT)
              .goWithPortion();
    }

  }

}
