package kz.greetgo.kafka_test_app.consumers;

import kz.greetgo.kafka.consumer.annotations.GroupId;
import kz.greetgo.kafka.consumer.annotations.KafkaDescription;
import kz.greetgo.kafka.consumer.annotations.TopicContent;
import kz.greetgo.kafka.consumer.annotations.TopicListMethod;
import kz.greetgo.kafka.consumer.annotations.TopicRef;

import java.util.Set;

import static kz.greetgo.kafka.consumer.annotations.TopicContentType.STR;

public class StringConsumer {

  @SuppressWarnings("unused")
  public Set<String> strTopics() {
    return Set.of("str1", "str2");
  }

  @TopicContent(STR)
  @KafkaDescription("Считаем строки")
  @GroupId("string_reader")
  @TopicListMethod("strTopics")
  public void countBoi(String str, @TopicRef String topic) {
    System.out.println("JfPOWe2R4h :: From topic `" + topic + "` come string: " + str);
  }


}
