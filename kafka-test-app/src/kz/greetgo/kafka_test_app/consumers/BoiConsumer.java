package kz.greetgo.kafka_test_app.consumers;

import kz.greetgo.kafka.consumer.annotations.GroupId;
import kz.greetgo.kafka.consumer.annotations.KafkaDescription;
import kz.greetgo.kafka.consumer.annotations.Topic;
import kz.greetgo.kafka.consumer.annotations.TopicListMethod;
import kz.greetgo.kafka_test_app.app001.ManyBoi;
import kz.greetgo.kafka_test_app.model.BoiKafka;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class BoiConsumer {

  public final ConcurrentHashMap<String, Integer> counts = new ConcurrentHashMap<>();

  public final AtomicInteger boiReadTopicCount = new AtomicInteger(7);

  @Topic({"BOI_000", "BOI_001"})
  @KafkaDescription("Считаем бои")
  @GroupId("boi_view_001")
  @TopicListMethod("countBoiTopics")
  public void countBoi(BoiKafka boi) {
    counts.compute(boi.topic, (String topic, Integer count) -> count == null ? 1 : count + 1);
  }

  @SuppressWarnings("unused")
  public Set<String> countBoiTopics() {
    int         topicCount = boiReadTopicCount.get();
    Set<String> ret        = new HashSet<>();
    for (int i = 0; i < topicCount; i++) {
      ret.add(ManyBoi.extractTopic(i));
    }
    //System.out.println("9xz8kLzf5r :: countBoiTopics returns " + ret);
    return ret;
  }

}
