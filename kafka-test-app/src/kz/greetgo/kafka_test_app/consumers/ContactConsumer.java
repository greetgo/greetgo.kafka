package kz.greetgo.kafka_test_app.consumers;

import kz.greetgo.kafka.consumer.annotations.GroupId;
import kz.greetgo.kafka.consumer.annotations.KafkaDescription;
import kz.greetgo.kafka.consumer.annotations.Offset;
import kz.greetgo.kafka.consumer.annotations.Partition;
import kz.greetgo.kafka.consumer.annotations.Topic;
import kz.greetgo.kafka_test_app.model.ContactKafka;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;

import static kz.greetgo.kafka_test_app.config.Topics.CONTACT;

public class ContactConsumer {
  private final Path outputDir;

  public ContactConsumer(Path outputDir) {
    this.outputDir = outputDir;
  }

  @Topic(CONTACT)
  @KafkaDescription("Экспортируем контакты")
  @GroupId("export_contact_3")
  public void exportContact(ContactKafka contact, @Partition int partition, @Offset long offset) throws Exception {

    File outFile = outputDir.resolve("contact-" + partition + ".txt").toFile();

    outFile.getParentFile().mkdirs();

    String s = "offset=" + offset
      + " id=" + contact.id
      + " clientId=" + contact.clientId
      + " phone=" + contact.phoneNumber
      + "\n";

    try (FileOutputStream fileOutputStream = new FileOutputStream(outFile, true)) {
      fileOutputStream.write(s.getBytes(StandardCharsets.UTF_8));
    }

  }

}
