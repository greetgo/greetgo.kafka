package kz.greetgo.kafka_test_app.app001;

import kz.greetgo.kafka.model.Box;
import kz.greetgo.kafka_test_app.model.BoiKafka;
import kz.greetgo.kafka_test_app.model.ClientKafka;
import kz.greetgo.kafka_test_app.model.Contact;
import kz.greetgo.kafka_test_app.model.ContactKafka;
import kz.greetgo.kafka_test_app.model.SpaceStar;
import kz.greetgo.kafka_test_app.model.SpaceStone;
import kz.greetgo.strconverter.StrConverter;
import kz.greetgo.strconverter.simple.StrConverterSimple;

public class StrConverterFactory {

  private final StrConverterSimple simple;

  public StrConverterFactory() {
    StrConverterSimple simple = new StrConverterSimple();

    simple.convertRegistry().register(Box.class);
    simple.convertRegistry().register(ClientKafka.class);
    simple.convertRegistry().register(Contact.class);
    simple.convertRegistry().register(ContactKafka.class);
    simple.convertRegistry().register(BoiKafka.class);
    simple.convertRegistry().register(SpaceStone.class);
    simple.convertRegistry().register(SpaceStar.class);

    this.simple = simple;
  }

  public StrConverter strConverter() {
    return simple;
  }
}
