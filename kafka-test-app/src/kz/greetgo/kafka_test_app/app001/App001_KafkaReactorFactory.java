package kz.greetgo.kafka_test_app.app001;

import kz.greetgo.kafka.consumer.config.ConsumerConfigDefaults;
import kz.greetgo.kafka.consumer.config.ConsumerConnectSetter;
import kz.greetgo.kafka.consumer.config.ConsumerReactorConfigFactory;
import kz.greetgo.kafka.core.KafkaReactor;
import kz.greetgo.kafka.core.config.EventConfigFileFactory;
import kz.greetgo.kafka.core.config.EventConfigFileFactoryOverStorage;
import kz.greetgo.kafka.core.config.EventConfigStorageZooKeeper;
import kz.greetgo.kafka.core.config.ZooConnectParams;
import kz.greetgo.kafka.producer.ProducerFacade;
import kz.greetgo.kafka.producer.config.ProducerConfigDefaults;
import kz.greetgo.kafka.producer.config.ProducerConnectSetter;
import kz.greetgo.kafka.producer.config.ProducerReactorConfigFactory;
import kz.greetgo.kafka_test_app.config.KafkaConfig;
import kz.greetgo.kafka_test_app.config.ZooConfig;
import kz.greetgo.kafka_test_app.util.InitOnce;
import kz.greetgo.kafka_test_app.util.ReactorUtil;
import kz.greetgo.strconverter.StrConverter;

import java.util.Collection;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.function.Consumer;

public class App001_KafkaReactorFactory implements AutoCloseable {
  private final KafkaConfig                      kafkaConfig;
  private final ZooConfig                        zooConfig;
  private final StrConverter                     strConverter;
  private final Collection<Object>               consumers;
  private final Consumer<ProducerFacade<Object>> producer;

  public App001_KafkaReactorFactory(KafkaConfig kafkaConfig,
                                    ZooConfig zooConfig,
                                    StrConverter strConverter,
                                    Collection<Object> consumers,
                                    Consumer<ProducerFacade<Object>> producer) {
    this.kafkaConfig  = kafkaConfig;
    this.zooConfig    = zooConfig;
    this.strConverter = strConverter;
    this.consumers    = consumers;
    this.producer     = producer;
  }

  private final InitOnce<KafkaReactor> kafkaReactor = new InitOnce<>(this::create);

  public KafkaReactor kafkaReactor() {
    return kafkaReactor.get();
  }

  private final ConcurrentLinkedQueue<AutoCloseable> deferList = new ConcurrentLinkedQueue<>();

  @Override
  public void close() {
    deferList.forEach(x -> {
      try {
        x.close();
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
    });
  }

  private <T extends AutoCloseable> T defer(T t) {
    deferList.add(t);
    return t;
  }

  private KafkaReactor create() {

    ConsumerConfigDefaults consumerDefs = ReactorUtil.createConsumerConfigDefaults();

    ProducerConfigDefaults producerDefs = ReactorUtil.createProducerConfigDefaults();

    var configStorage = new EventConfigStorageZooKeeper("greetgo-kafka",
                                                        zooConfig::zooServers,
                                                        ZooConnectParams.builder().build());

    EventConfigFileFactory top = defer(new EventConfigFileFactoryOverStorage(configStorage));

    var producerRCF = defer(ProducerReactorConfigFactory.builder()
                                                        .configFactory(top.cd("producers"))
                                                        .defaults(profile -> producerDefs)
                                                        .build());

    var consumerRCF = defer(ConsumerReactorConfigFactory.builder()
                                                        .configFactory(top.cd("consumers"))
                                                        .defaults(profile -> consumerDefs)
                                                        .build());

    var consumerRCF2 = defer(ConsumerReactorConfigFactory.builder()
                                                         .configFactory(top.cd("dynamic-consumers"))
                                                         .defaults(profile -> consumerDefs)
                                                         .build());

    return defer(KafkaReactor.builder()
                             .strConverter(() -> strConverter)
                             .producerConfigFactory(producerRCF)
                             .consumerConfigFactory(consumerRCF)
                             .dynamicConsumerConfigFactory(consumerRCF2)
                             .makeConsumerDescriptionMandatory()
                             .consumerConnectSetter(createConsumerConnectSetter())
                             .producerConnectSetter(createProducerConnectSetter())
                             .authorSupplier(() -> "JohnMorgan")
                             .consumerHostId(() -> "super-host")
                             .topicPrefix(() -> "a_")
                             .addControllers(consumers)
                             .onLogger(ReactorUtil::prepareLoggers)
                             .controllerToWorkerCountFileName(ReactorUtil::controllerToName)
                             .refreshTopicListMillis(() -> 5000)
                             .build());
  }

  private ConsumerConnectSetter createConsumerConnectSetter() {
    return (targetConfigMap, params) -> {
      targetConfigMap.put("bootstrap.servers", kafkaConfig.kafkaServers());
      System.out.println("TmMuH5lYy7 :: createConsumerConnectSetter: profile = " + params.profile());
    };
  }

  private ProducerConnectSetter createProducerConnectSetter() {
    return (targetConfigMap, params) -> {
      targetConfigMap.put("bootstrap.servers", kafkaConfig.kafkaServers());
      System.out.println("5volXPfL3z :: createProducerConnectSetter: producerName = " + params.producerName());
    };
  }

  public void init() {
    //noinspection resource
    ProducerFacade<Object> producer = kafkaReactor().createProducer("main");
    this.producer.accept(producer);

    //noinspection resource
    kafkaReactor().startConsumers();
  }
}
