package kz.greetgo.kafka_test_app.app001;

import kz.greetgo.kafka.consumer.AutoOffsetReset;
import kz.greetgo.kafka.consumer.DynamicReactor;
import kz.greetgo.kafka.core.KafkaReactor;
import kz.greetgo.kafka.producer.KafkaFuture;
import kz.greetgo.kafka.producer.ProducerFacade;
import kz.greetgo.kafka_test_app.model.ClientKafka;
import kz.greetgo.kafka_test_app.model.Contact;
import kz.greetgo.util.RND;
import kz.greetgo.util.fui.FUI;
import kz.greetgo.util.fui.IntAccessor;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import static kz.greetgo.kafka_test_app.config.Topics.CLIENT;
import static kz.greetgo.kafka_test_app.config.Topics.CONTACT;

public class AppProducerConsumer {

  public final  FUI            fui;
  public final  KafkaReactor   kafkaReactor;
  public final  ProducerFacade producer;
  private final IntAccessor    sendClientPortion;
  private final IntAccessor    sendClientThreadCount;
  public final  ManyBoi        manyBoi;
  public final  AtomicBoolean  appIsWorking = new AtomicBoolean(true);

  public AppProducerConsumer(FUI fui, KafkaReactor kafkaReactor, ProducerFacade producer) {
    this.fui              = fui;
    this.kafkaReactor     = kafkaReactor;
    this.producer         = producer;
    sendClientPortion     = fui.entryInt("SendClientPortion", 10);
    sendClientThreadCount = fui.entryInt("SendClientThreadCount", 10);
    manyBoi               = new ManyBoi(this);
  }

  public void run() {

    fui.button("Send clients", this::sendClients);

    initDynamicConsumersButtons();

    System.out.println("kR4jda55gU :: Application initiated and started");
    fui.go();
    appIsWorking.set(false);
    System.out.println("dYAz1v907O :: Application closing....");
    kafkaReactor.close();
    System.out.println("uDeZx7XC0P :: Application closed");
  }


  final static int COMMIT_PORTION = 5000;

  private final AtomicLong sendCount = new AtomicLong(0);

  private void sendClients() {

    int threadCount = sendClientThreadCount.get();

    AtomicBoolean working = new AtomicBoolean(true);

    SendThread[] threads = new SendThread[threadCount];
    for (int i = 0; i < threadCount; i++) {
      threads[i] = new SendThread();
      threads[i].setName("Sending thread " + i);
    }

    Thread showStatusThread = new Thread(() -> {
      while (working.get()) {

        try {
          Thread.sleep(1500);
        } catch (InterruptedException e) {
          return;
        }

//        for (final SendThread thread : threads) {
//          thread.showStatus();
//        }
        System.out.println("ddx5fZIk4R :: send count " + sendCount.get());
      }
    });

    showStatusThread.start();

    sendCount.set(0);

    for (final Thread thread : threads) {
      thread.start();
    }
    for (final Thread thread : threads) {
      try {
        thread.join();
      } catch (InterruptedException e) {
        throw new RuntimeException(e);
      }
    }

    working.set(false);

    try {
      showStatusThread.interrupt();
      showStatusThread.join();
    } catch (InterruptedException e) {
      throw new RuntimeException(e);
    }

    System.out.println("zxzPF0Y8Y7 :: отправлено клиентов " + sendCount.get());
  }

  private class SendThread extends Thread {

    @Override
    public void run() {
      int count = sendClientPortion.get();

      List<KafkaFuture> futureList = new ArrayList<>();

      for (int i = 0; i < count; i++) {
        ClientKafka client = new ClientKafka();
        client.id         = RND.str(10);
        client.surname    = RND.str(30);
        client.name       = RND.str(30);
        client.patronymic = RND.str(30);

        int contactCount = 3 + RND.plusInt(10);

        for (int j = 0; j < contactCount; j++) {
          Contact contact = new Contact();
          contact.id          = RND.str(10);
          contact.phoneNumber = "+" + RND.strInt(13);
          client.contactList.add(contact);
        }

        futureList.add(producer.sending(client)
                               .toTopic(CLIENT)
                               .setAuthor("Test user")
                               .go());

        sendCount.incrementAndGet();

        if (futureList.size() >= COMMIT_PORTION) {
          futureList.forEach(KafkaFuture::awaitAndGet);
          futureList.clear();
        }
      }

      futureList.forEach(KafkaFuture::awaitAndGet);
    }
  }

  private void initDynamicConsumersButtons() {
    fui.button("Dynamic/Show consumers", this::showDynamicReactors);

    fui.button("Dynamic/Start consumer reading-CONTACT2", this::startConsumer__CONTACT2);
    fui.button("Dynamic/Stop consumer reading-CONTACT2", this::stopConsumer__CONTACT2);

    fui.button("Dynamic/Start consumer reading-CLIENT", this::startConsumer__CLIENT);
    fui.button("Dynamic/Stop consumer reading-CLIENT", this::stopConsumer__CLIENT);
  }

  private void stopConsumer__CLIENT() {
    kafkaReactor.dynamicConsumers()
                .stopByFilter(dynamicReactor -> ("reading-" + CLIENT).equals(dynamicReactor.definition().getName()));
  }

  private void stopConsumer__CONTACT2() {
    kafkaReactor.dynamicConsumers()
                .stopByFilter(dynamicReactor -> ("reading-" + CONTACT).equals(dynamicReactor.definition().getName()));
  }

  private void showDynamicReactors() {
    for (final DynamicReactor reactor : kafkaReactor.dynamicConsumers().reactors()) {
      System.out.println("9d0P8YvnGK :: reactor[" + reactor.id() + "] = " + reactor.definition().logDisplay()
                           + ", isWorking = " + reactor.isWorking());
    }
  }

  private void startConsumer__CONTACT2() {

    kafkaReactor.dynamicConsumers()
                .adding()
                .name("reading-" + CONTACT)
                .autoOffsetReset(AutoOffsetReset.EARLIEST)
                .profile("external-kafka-cluster")
                .topic(CONTACT)
                .customConsumer((box, boxMeta) -> {
                  System.out.println("ctiK9x40G6 :: DYNAMIC CONSUMER reading-CONTACT2 :"
                                       + ", box.body = " + box.body);
                })
                .add();

  }

  private void startConsumer__CLIENT() {
    kafkaReactor.dynamicConsumers()
                .adding()
                .name("reading-" + CLIENT)
                .autoOffsetReset(AutoOffsetReset.EARLIEST)
                .topic(CLIENT)
                .customConsumer((box, boxMeta) -> {
                  System.out.println("sc65kmzKcx :: DYNAMIC CONSUMER reading-CLIENT :"
                                       + ", box.body = " + box.body);
                })
                .add();
  }

}
