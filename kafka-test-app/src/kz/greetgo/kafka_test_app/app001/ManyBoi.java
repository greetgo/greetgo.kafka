package kz.greetgo.kafka_test_app.app001;

import kz.greetgo.kafka.producer.KafkaFuture;
import kz.greetgo.kafka.util.KafkaStrUtil;
import kz.greetgo.kafka_test_app.consumers.BoiConsumer;
import kz.greetgo.kafka_test_app.model.BoiKafka;
import kz.greetgo.util.RND;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class ManyBoi {
  public static final int BOI_SEND_TOPIC_COUNT = 30;

  private final AppProducerConsumer app;

  private final AtomicReference<BoiConsumer> boiConsumer = new AtomicReference<>(null);

  public ManyBoi(AppProducerConsumer app) {
    this.app = app;
    init();
  }

  public void installBoiConsumer(BoiConsumer boiConsumer) {
    if (this.boiConsumer.get() != null) {
      throw new RuntimeException("h9L56aW6u5");
    }
    this.boiConsumer.set(boiConsumer);

    initThreadToShowCounts();

    app.fui.entryInt("Many_boi/boi_read_topic_count", boiConsumer.boiReadTopicCount.get(), boiConsumer.boiReadTopicCount::set);
  }

  private void initThreadToShowCounts() {
    Map<String, Integer> currentValues = new HashMap<>(boiConsumer.get().counts);
    new Thread(() -> {

      while (app.appIsWorking.get()) {
        try {
          Thread.sleep(3000);
        } catch (InterruptedException e) {
          return;
        }

        Map<String, Integer> newValues = new HashMap<>(boiConsumer.get().counts);

        if (!currentValues.equals(newValues)) {
          currentValues.clear();
          currentValues.putAll(newValues);

          int totalCount = 0;
          System.out.println("2jMYGsq487 :: Counts");
          for (final String key : currentValues.keySet().stream().sorted().collect(Collectors.toList())) {
            Integer count = currentValues.get(key);
            System.out.println("2jMYGsq487 ::     " + key + "  = " + count);
            totalCount += count;
          }
          System.out.println("2jMYGsq487 ::     TOTAL COUNT = " + totalCount);
          System.out.println();

        }

      }

    }).start();
  }


  private final AtomicBoolean working   = new AtomicBoolean(true);
  private final AtomicInteger sendCount = new AtomicInteger(1000);

  private void init() {
    app.fui.button("Many_boi/Send", this::sendManyBoi);
    app.fui.entryBool("Many_boi/Sending", working.get(), working::set);
    app.fui.entryInt("Many_boi/send_count", sendCount.get(), sendCount::set);
  }

  private void sendManyBoi() {
    new Thread(this::doSendManyBoi).start();
  }

  private void doSendManyBoi() {

    List<KafkaFuture> portion          = new ArrayList<>();
    int               portionSizeLimit = 70;

    AtomicInteger sentCount   = new AtomicInteger(0);
    long          startMillis = System.currentTimeMillis();

    AtomicBoolean localWorking = new AtomicBoolean(true);

    Thread viewThread = new Thread(() -> {
      while (localWorking.get()) {
        try {
          Thread.sleep(750);
        } catch (InterruptedException e) {
          return;
        }

        System.out.println("UCUQwqX3r3 ::" +
                             " sent to BOI " + sentCount.get() + " records" +
                             " for " + (System.currentTimeMillis() - startMillis) + " ms");
      }
    });

    viewThread.start();

    int count = sendCount.get();

    for (int i = 0; i < count && working.get(); i++) {
      BoiKafka boi        = new BoiKafka();
      int      topicIndex = i % BOI_SEND_TOPIC_COUNT;
      boi.topic  = extractTopic(topicIndex);
      boi.id     = RND.str(10);
      boi.label1 = "LABEL 001 " + RND.str(30);
      boi.label2 = "LABEL 002 " + RND.str(30);
      boi.label3 = "LABEL 003 " + RND.str(30);

      KafkaFuture kafkaFuture = app.producer.sending(boi).toTopic(boi.topic).go();
      sentCount.incrementAndGet();
      portion.add(kafkaFuture);


      if (portion.size() > portionSizeLimit) {
        portion.forEach(KafkaFuture::awaitAndGet);
        portion.clear();
      }
    }
    {
      portion.forEach(KafkaFuture::awaitAndGet);
      portion.clear();
    }
    long endMillis = System.currentTimeMillis();

    localWorking.set(false);

    try {
      viewThread.interrupt();
      viewThread.join();
    } catch (InterruptedException e) {
      throw new RuntimeException(e);
    }

    System.out.println("0TJDMtgLs4 :: FINISHED:" +
                         " Summary sent to BOI " + sentCount.get() + " records" +
                         " for " + (endMillis - startMillis) + " ms");

  }

  public static String extractTopic(int topicIndex) {
    return "BOI_" + KafkaStrUtil.toLen(topicIndex, 3);
  }
}
