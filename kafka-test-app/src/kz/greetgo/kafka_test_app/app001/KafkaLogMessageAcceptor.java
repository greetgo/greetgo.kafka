package kz.greetgo.kafka_test_app.app001;

import kz.greetgo.kafka.core.logger.LogMessageAcceptor;

public class KafkaLogMessageAcceptor implements LogMessageAcceptor {

  @Override
  public boolean isInfoEnabled() {
    return true;
  }

  @Override
  public void info(String message) {
    System.out.println("YVM1bzBvDH :: INFO " + message);
  }

  @Override
  public boolean isDebugEnabled() {
    return true;
  }

  @Override
  public void debug(String message) {
    System.out.println("YVM1bzBvDH :: DEBUG " + message);
  }

  @Override
  public void error(String message, Throwable throwable) {
    System.err.println("46tCQ504rj :: ERROR " + message);
    if (throwable != null) {
      throwable.printStackTrace(System.err);
    }
  }
}
