package kz.greetgo.kafka_test_app.launch;

import kz.greetgo.kafka.producer.ProducerFacade;
import kz.greetgo.kafka_test_app.app001.AppProducerConsumer;
import kz.greetgo.kafka_test_app.app001.App001_KafkaReactorFactory;
import kz.greetgo.kafka_test_app.app001.StrConverterFactory;
import kz.greetgo.kafka_test_app.app_common.StaticConfigFactory;
import kz.greetgo.kafka_test_app.config.KafkaConfig;
import kz.greetgo.kafka_test_app.config.ZooConfig;
import kz.greetgo.kafka_test_app.consumers.BoiConsumer;
import kz.greetgo.kafka_test_app.consumers.ClientConsumer;
import kz.greetgo.kafka_test_app.consumers.ContactConsumer;
import kz.greetgo.kafka_test_app.consumers.StringConsumer;
import kz.greetgo.util.fui.FUI;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class App001_ConsumerProducerLauncher {

  public static void main(String[] args) {
    App001_ConsumerProducerLauncher launcher = new App001_ConsumerProducerLauncher();
    launcher.init();
    launcher.app.run();
  }

  private AppProducerConsumer app;

  private void init() {

    ZooConfig   zooConfig   = StaticConfigFactory.zooConfig();
    KafkaConfig kafkaConfig = StaticConfigFactory.kfConfig();

    StrConverterFactory strConverterFactory = new StrConverterFactory();

    AtomicReference<ProducerFacade> producer = new AtomicReference<>(null);

    Path rootDir = Paths.get("build/" + getClass().getSimpleName());

    ClientConsumer  clientConsumer  = new ClientConsumer(producer::get);
    ContactConsumer contactConsumer = new ContactConsumer(rootDir.resolve("output"));
    BoiConsumer     boiConsumer     = new BoiConsumer();

    StringConsumer stringConsumer = new StringConsumer();

    //noinspection resource
    App001_KafkaReactorFactory kafkaReactorFactory = new App001_KafkaReactorFactory(kafkaConfig, zooConfig,
                                                                                    strConverterFactory.strConverter(),
                                                                                    List.of(clientConsumer, contactConsumer, boiConsumer, stringConsumer),
                                                                                    producer::set);

    kafkaReactorFactory.init();

    FUI fui = new FUI(rootDir);

    app = new AppProducerConsumer(fui, kafkaReactorFactory.kafkaReactor(), producer.get());
    app.manyBoi.installBoiConsumer(boiConsumer);
  }

}
