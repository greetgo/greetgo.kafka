package kz.greetgo.kafka_test_app.launch;

import kz.greetgo.kafka.consumer.ConsumerDefinition;
import kz.greetgo.kafka.consumer.ConsumerDefinitionOnController;
import kz.greetgo.kafka.consumer.config.ConsumerConfigDefaults;
import kz.greetgo.kafka.consumer.config.ConsumerReactorConfig;
import kz.greetgo.kafka.consumer.config.ConsumerReactorConfigFactory;
import kz.greetgo.kafka.core.config.EventConfigFileFactory;
import kz.greetgo.kafka.core.config.EventConfigFileFactoryOverStorage;
import kz.greetgo.kafka.core.config.EventConfigStorageZooKeeper;
import kz.greetgo.kafka.core.config.ZooConnectParams;
import kz.greetgo.kafka.core.logger.Logger;
import kz.greetgo.kafka_test_app.app002.App002_ParentLauncher;
import kz.greetgo.kafka_test_app.app002.GroundConsumer;
import kz.greetgo.kafka_test_app.app002.RiverConsumer;
import kz.greetgo.kafka_test_app.app002.SkyConsumer;
import kz.greetgo.kafka_test_app.app002.WinterConsumer;
import kz.greetgo.kafka_test_app.app_common.StaticConfigFactory;
import kz.greetgo.kafka_test_app.config.ZooConfig;
import kz.greetgo.util.fui.FUI;

import java.lang.reflect.Method;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

import static kz.greetgo.kafka.util_for_tests.ReflectionUtil.findMethod;

public class App002_ConsumerReactorConfigLauncher extends App002_ParentLauncher {


  public static void main(String[] args) {
    var launcher = new App002_ConsumerReactorConfigLauncher();
    launcher.run();
  }

  private void run() {

    ZooConfig zooConfig = StaticConfigFactory.zooConfig();
    Path      rootDir   = Paths.get("build/" + getClass().getSimpleName());

    Logger logger = new Logger();

    final SkyConsumer    skyConsumer          = new SkyConsumer();
    final Method         cloudsReadingMethod  = findMethod(skyConsumer, "cloudsReading");
    final Method         starsLightingMethod  = findMethod(skyConsumer, "starsLighting");
    final GroundConsumer groundConsumer       = new GroundConsumer();
    final Method         stoneBlowingMethod   = findMethod(groundConsumer, "stoneBlowing");
    final Method         catchingStonesMethod = findMethod(groundConsumer, "catchingStones");
    final RiverConsumer  riverConsumer        = new RiverConsumer();
    final Method         blowVolgaMethod      = findMethod(riverConsumer, "blowVolga");

    final WinterConsumer winterConsumer = new WinterConsumer();
    final Method         snowMethod     = findMethod(winterConsumer, "snow");

    ConsumerConfigDefaults consumerDefs = new ConsumerConfigDefaults();
    consumerDefs.addDefaults();

    String workerCountSkyFileName    = SkyConsumer.class.getSimpleName() + ".workerCount";
    String workerCountGroundFileName = GroundConsumer.class.getSimpleName() + ".workerCount";
    String workerCountRiverFileName  = RiverConsumer.class.getSimpleName() + ".workerCount";
    String workerCountWinterFileName = WinterConsumer.class.getSimpleName() + ".workerCount";

    final ConsumerDefinition consumerDef1 = new ConsumerDefinitionOnController(
      skyConsumer, cloudsReadingMethod, logger, null, null,
      workerCountSkyFileName, (x, y, z) -> true);
    final ConsumerDefinition consumerDef2 = new ConsumerDefinitionOnController(
      skyConsumer, starsLightingMethod, logger, null, null,
      workerCountSkyFileName, (x, y, z) -> true);

    final ConsumerDefinition consumerDef3 = new ConsumerDefinitionOnController(
      groundConsumer, stoneBlowingMethod, logger, null, null,
      workerCountGroundFileName, (x, y, z) -> true);
    final ConsumerDefinition consumerDef4 = new ConsumerDefinitionOnController(
      groundConsumer, catchingStonesMethod, logger, null, null,
      workerCountGroundFileName, (x, y, z) -> true);

    final ConsumerDefinition consumerDef5 = new ConsumerDefinitionOnController(
      riverConsumer, blowVolgaMethod, logger, null, null,
      workerCountRiverFileName, (x, y, z) -> true);

    final ConsumerDefinition consumerDef6 = new ConsumerDefinitionOnController(
      winterConsumer, snowMethod, logger, null, null,
      workerCountWinterFileName, (x, y, z) -> true);

    try (var configStorage = new EventConfigStorageZooKeeper("app-consumer-reactor-config",
                                                             zooConfig::zooServers,
                                                             ZooConnectParams.builder().build());

         EventConfigFileFactory top = new EventConfigFileFactoryOverStorage(configStorage);
         final ConsumerReactorConfigFactory consumers = ConsumerReactorConfigFactory.builder()
                                                                                    .configFactory(top.cd("consumers"))
                                                                                    .defaults(profile -> consumerDefs)
                                                                                    .build()
    ) {

      FUI fui = new FUI(rootDir);

      final ConsumerReactorConfig consumerConfig1 = consumers.getConsumerConfig(consumerDef1);
      final ConsumerReactorConfig consumerConfig2 = consumers.getConsumerConfig(consumerDef2);
      final ConsumerReactorConfig consumerConfig3 = consumers.getConsumerConfig(consumerDef3);
      final ConsumerReactorConfig consumerConfig4 = consumers.getConsumerConfig(consumerDef4);
      final ConsumerReactorConfig consumerConfig5 = consumers.getConsumerConfig(consumerDef5);
      final ConsumerReactorConfig consumerConfig6 = consumers.getConsumerConfig(consumerDef6);

      printHelp();

      interact(fui, "m_" + cloudsReadingMethod.getName(), consumerConfig1);
      interact(fui, "m_" + starsLightingMethod.getName(), consumerConfig2);
      interact(fui, "m_" + stoneBlowingMethod.getName(), consumerConfig3);
      interact(fui, "m_" + catchingStonesMethod.getName(), consumerConfig4);
      interact(fui, "m_" + blowVolgaMethod.getName(), consumerConfig5);
      interact(fui, "m_" + snowMethod.getName(), consumerConfig6);

      fui.button("createAllConfigFiles", () -> Stream.of(consumerConfig1, consumerConfig2, consumerConfig3, consumerConfig4,
                                                         consumerConfig5, consumerConfig6)
                                                     .peek(c -> c.getConfigMap(null))
                                                     .forEach(c -> c.getWorkerCount(null)));

      fui.button("killAllListeners", this::killAllListeners);

      for (final ChangeSnowConsumers variant : ChangeSnowConsumers.values()) {
        fui.button("snow/changeSnowConsumers_" + variant, () -> changeSnowConsumers(variant, winterConsumer));
      }

      fui.button("snow2/getSnowTopicsProfile",
                 () -> System.out.println("PXgGIAkj9U :: consumerDef6.topicList() = " + consumerDef6.topicList()));


      System.out.println("9iJw1cuTww :: " + getClass().getSimpleName() + " STARTED");

      fui.go();

      System.out.println("KS5N67v9dE :: " + getClass().getSimpleName() + " FINISHED");
    }
  }

}
