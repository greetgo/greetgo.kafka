package kz.greetgo.kafka_test_app.launch;

import kz.greetgo.kafka.core.KafkaReactor;
import kz.greetgo.kafka.producer.ProducerFacade;
import kz.greetgo.kafka_test_app.app001.StrConverterFactory;
import kz.greetgo.kafka_test_app.app003.App003_KafkaReactorFactory;
import kz.greetgo.kafka_test_app.app003.App003_Parent;
import kz.greetgo.kafka_test_app.app003.InKafkaMigrationRegister;
import kz.greetgo.kafka_test_app.app003.PlanetConsumer;
import kz.greetgo.kafka_test_app.app003.SpaceConsumer;
import kz.greetgo.kafka_test_app.app_common.StaticConfigFactory;
import kz.greetgo.kafka_test_app.config.KafkaConfig;
import kz.greetgo.kafka_test_app.config.ZooConfig;
import kz.greetgo.util.fui.BoolAccessor;
import kz.greetgo.util.fui.FUI;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.bridge.SLF4JBridgeHandler;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@Slf4j
public class App003_KafkaReaktorLauncher extends App003_Parent {
  public static void main(String[] args) {
    SLF4JBridgeHandler.removeHandlersForRootLogger();
    SLF4JBridgeHandler.install();
    new App003_KafkaReaktorLauncher().run();
  }

  private void run() {
    ZooConfig   zooConfig     = StaticConfigFactory.zooConfig();
    KafkaConfig kfConfig      = StaticConfigFactory.kfConfig();
    KafkaConfig kfOtherConfig = StaticConfigFactory.kfOtherConfig();
    Path        rootDir       = Paths.get("build/" + getClass().getSimpleName());

    StrConverterFactory strConverterFactory = new StrConverterFactory();

    AtomicReference<ProducerFacade<Object>> producer      = new AtomicReference<>(null);
    AtomicReference<ProducerFacade<String>> producerStr   = new AtomicReference<>(null);
    AtomicReference<ProducerFacade<byte[]>> producerBytes = new AtomicReference<>(null);



    final FUI fui = new FUI(rootDir);
    fui.setThrowAcceptor(e -> e.printStackTrace(System.err));

    InKafkaMigrationRegister inKafkaMigrationRegister= new InKafkaMigrationRegister(producerStr, fui);

    final BoolAccessor enabledEntry = fui.entryBool("Enabled", true);

    SpaceConsumer  spaceConsumer  = new SpaceConsumer(fui, producer::get);
    PlanetConsumer planetConsumer = new PlanetConsumer(fui, producerStr::get, producerBytes::get);

    var consumers = List.of(spaceConsumer, spaceConsumer.fromAnotherFolder, planetConsumer);

    try (var kafkaReactorFactory = new App003_KafkaReactorFactory(zooConfig,
                                                                  kfConfig,
                                                                  kfOtherConfig,
                                                                  strConverterFactory.strConverter(),
                                                                  consumers,
                                                                  producer::set,
                                                                  producerStr::set,
                                                                  producerBytes::set,
                                                                  enabledEntry::is
    )) {

      final KafkaReactor kafkaReactor = kafkaReactorFactory.start();

      spaceConsumer.initInterface(kafkaReactor);

      planetConsumer.initInterface(kafkaReactor);

      fui.button("resetProducers", () -> {
        producer.get().reset();
        producerStr.get().reset();
        producerBytes.get().reset();
      });

      log.info("kT6nvPqoE9 :: Application {} Started", getClass().getSimpleName());
      fui.go();

      spaceConsumer.stopWorking();
      inKafkaMigrationRegister.onExitApplication();
    }

    log.info("0g7EKPv2x6 :: Application {} Finished", getClass().getSimpleName());
  }
}
