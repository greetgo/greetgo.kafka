package kz.greetgo.kafka_test_app.probe;

import kz.greetgo.kafka.util_for_tests.Markers;
import kz.greetgo.util.RND;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.RetryNTimes;
import org.apache.zookeeper.client.ZKClientConfig;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import static java.nio.charset.StandardCharsets.UTF_8;

public class ProbeZooAuth {

  @SuppressWarnings("SpellCheckingInspection")
  public static void main(String[] args) throws Exception {
    int    maxRetries                = 3;
    int    sleepBetweenRetriesMillis = 100;
    String connectStr                = "localhost:13019";
    int    sessionTimeoutMillis      = 3000;
    int    connectTimeoutMillis      = 3000;

    var retryPolicy = new RetryNTimes(maxRetries, sleepBetweenRetriesMillis);

//    ZKClientConfig conf = new ZKClientConfig();
//    conf.setProperty(ZKClientConfig.ZK_SASL_CLIENT_USERNAME, "stone");
//    conf.setProperty(ZKClientConfig.ENABLE_CLIENT_SASL_KEY, "true");

    Random           rnd    = new Random();
    SimpleDateFormat sdf    = new SimpleDateFormat("yyyyMMdd-HHmmss-SSS");
    String           ENG    = "abcdefghijklmnopqrstuvwxyz";
    String           RUS    = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
    String           DEG    = "0123456789";
    String           SPACES = "              \n";
    char[]           ALL    = (ENG + ENG.toUpperCase() + RUS + RUS.toUpperCase() + DEG + SPACES).toCharArray();

    final String pathToConfig = Markers.projectRoot()
                                       .resolve("docker-kafka-with-auth")
                                       .resolve("zoo-auth")
                                       .resolve("conf")
                                       .resolve("client-jaas.conf")
                                       .toFile()
                                       .getAbsoluteFile()
                                       .toPath()
                                       .normalize()
                                       .toString();

    ZKClientConfig zkClientConfig = new ZKClientConfig();
    zkClientConfig.setProperty("asd", "dsa");

    System.setProperty("java.security.auth.login.config", pathToConfig);

    try (CuratorFramework client = CuratorFrameworkFactory.builder()
                                                          .connectString(connectStr)
                                                          .sessionTimeoutMs(sessionTimeoutMillis)
                                                          .connectionTimeoutMs(connectTimeoutMillis)
                                                          .retryPolicy(retryPolicy)
                                                          .authorization("digest", "stone:123456".getBytes(UTF_8))
                                                          .zkClientConfig(zkClientConfig)
                                                          .build()) {
      client.start();

      String path = "/zoo/probe-" + sdf.format(new Date()) + "-" + rnd.nextInt();

      char[] data = new char[100 + RND.plusInt(100)];
      for (int i = 0; i < data.length; i++) {
        data[i] = ALL[RND.plusInt(ALL.length)];
      }

      client.create().orSetData().creatingParentContainersIfNeeded().forPath(path, (new String(data) + "\n").getBytes(UTF_8));

      //client.delete().forPath(path);

      final byte[] asd_dsa_bytes = client.getData().forPath("/asd/dsa");

      System.out.println("6Y0tknPZxm :: asd_dsa_bytes.len = " + asd_dsa_bytes.length);
      System.out.println("6Y0tknPZxm :: asd_dsa_bytes = " + new String(asd_dsa_bytes, UTF_8));

    }

  }
}
