package kz.greetgo.kafka_test_app.probe;

import kz.greetgo.util.RND;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * Этот продюсер кидает данные в кафку, которая поднимается по пути:
 * <p>
 * PRG_ROOT/docker-kafka-with-auth/sasl-plaintext
 * <p>
 * Там можно ещё запустить kaf-drop
 */
public class ProbeProducer_SASL_SSL {

  public static void main(String[] args) throws ExecutionException, InterruptedException {

    System.out.println("134Tc4Cv1w :: Start sending");


    Map<String, Object> prop = new HashMap<>();

    prop.put("bootstrap.servers", "localhost:13113");
    prop.put("key.serializer", StringSerializer.class.getName());
    prop.put("value.serializer", StringSerializer.class.getName());
    prop.put("request.timeout.ms", "5000");
    prop.put("max.block.ms", "5000");
    prop.put("metadata.fetch.timeout.ms", "5000");
    prop.put("enable.auto.commit", "false");
    prop.put("acks", "all");

    prop.put("sasl.jaas.config", "org.apache.kafka.common.security.plain.PlainLoginModule" +
      " required serviceName=\"Kafka\" username=\"alice\" password=\"alice-secret\";");
    prop.put("sasl.mechanism", "PLAIN");
    prop.put("security.protocol", "SASL_SSL");

    prop.put("ssl.truststore.location", System.getenv("PWD") + "/docker-kafka-with-auth/sasl-ssl/cert/files/client.truststore.jks");
    prop.put("ssl.truststore.password", "111222");

    try (KafkaProducer<String, String> producer = new KafkaProducer<>(prop)) {

      final ProducerRecord<String, String> record = new ProducerRecord<>("PROBE2", "KeyAA-" + RND.str(10), "ValueAA-" + RND.str(10));

      final Future<RecordMetadata> future = producer.send(record);

      final RecordMetadata recordMetadata = future.get();

      System.out.println("Zw4qbr14Py :: Sent " + recordMetadata);
    }

    System.out.println("fKfOz3OWPe :: Closed");

  }


}
