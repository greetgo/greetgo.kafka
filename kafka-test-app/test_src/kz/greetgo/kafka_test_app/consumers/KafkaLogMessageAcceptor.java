package kz.greetgo.kafka_test_app.consumers;

import kz.greetgo.kafka.core.logger.LogMessageAcceptor;

public class KafkaLogMessageAcceptor implements LogMessageAcceptor {

  @Override
  public boolean isInfoEnabled() {
    return true;
  }

  @Override
  public void info(String message) {
    System.out.println("L58G0HB3Ml :: INFO : " + message);
  }

  @Override
  public boolean isDebugEnabled() {
    return true;
  }

  @Override
  public void debug(String message) {
    System.out.println("uth0SnUxY3 :: DEBUG : " + message);
  }

  @Override
  public void error(String message, Throwable throwable) {
    System.err.println("SAuRD93Ndq :: ERROR : " + message);
    throwable.printStackTrace(System.err);
  }
}
