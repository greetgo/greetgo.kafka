package kz.greetgo.kafka_test_app.consumers;

import kz.greetgo.kafka.core.KafkaReactor;
import kz.greetgo.kafka.core.KafkaSimulator;
import kz.greetgo.kafka.core.logger.LoggerExternal;
import kz.greetgo.kafka.core.logger.LoggerType;
import kz.greetgo.kafka.producer.ProducerFacade;
import kz.greetgo.kafka_test_app.app001.StrConverterFactory;
import kz.greetgo.kafka_test_app.config.Topics;
import kz.greetgo.kafka_test_app.model.ClientKafka;
import kz.greetgo.kafka_test_app.model.Contact;
import kz.greetgo.util.RND;
import org.apache.kafka.common.Cluster;
import org.apache.kafka.common.Node;
import org.apache.kafka.common.PartitionInfo;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public abstract class ParentKafkaSimulator {
  private final Cluster simulatorCluster = createCluster();

  private Cluster createCluster() {
    var nodes = new ArrayList<Node>(0);

    List<PartitionInfo> partitions
      = Topics.ALL.stream()
                  .map(it -> new PartitionInfo(it, 1, null, new Node[0], new Node[0], new Node[0]))
                  .collect(Collectors.toList());

    var unauthorizedTopics = Collections.<String>emptySet();
    var internalTopics     = Collections.<String>emptySet();
    return new Cluster(null, nodes, partitions, unauthorizedTopics, internalTopics, null);
  }


  private void prepareLogger(LoggerExternal logger) {
    logger.setDestination(new KafkaLogMessageAcceptor());
    logger.setShowLoggerTypes(Set.of(
      LoggerType.LOG_CONSUMER_ERROR_INVOKING,
      LoggerType.CONSUMER_LOG_DIRECT_INVOKE_ERROR,
      LoggerType.LOG_CONSUMER_ILLEGAL_ACCESS_EXCEPTION_INVOKING_METHOD
    ));
  }

  private final AtomicReference<ProducerFacade> producerFacade = new AtomicReference<>(null);

  protected ProducerFacade producer() {
    return producerFacade.get();
  }

  protected KafkaSimulator  kafkaSimulator;
  protected ClientConsumer  clientConsumer;
  protected ContactConsumer contactConsumer;

  @BeforeMethod
  public void clearSimulator() {
    kafkaSimulator.clearPushed();
    kafkaSimulator.clearAllProducers();
    kafkaSimulator.dynamicConsumers().stopByFilter(x -> true);//drop all dynamic consumers
  }

  @BeforeMethod
  public void clientConsumer_clear() {
    var x = clientConsumer;
    if (x != null) {
      x.comeClientList = null;
    }
  }

  @BeforeClass
  public void prepareSimulator() {
    clientConsumer  = new ClientConsumer(this::producer);
    contactConsumer = new ContactConsumer(Paths.get("build").resolve("output"));

    StrConverterFactory strConverterFactory = new StrConverterFactory();

    kafkaSimulator = KafkaReactor.simulatorBuilder()
                                 .clusterSupplier(() -> simulatorCluster)
                                 .onLogger(this::prepareLogger)
                                 .authorSupplier(() -> "test-user")
                                 .consumerHostId(() -> "host-id")
                                 .strConverter(strConverterFactory::strConverter)
                                 .addControllers(List.of(clientConsumer, contactConsumer))
                                 .build();

    producerFacade.set(kafkaSimulator.createProducer("main"));

    kafkaSimulator.startConsumers();
  }

  protected ClientKafka rndClientKafka() {
    ClientKafka ret = new ClientKafka();
    ret.id         = RND.str(10);
    ret.surname    = RND.str(10);
    ret.name       = RND.str(10);
    ret.patronymic = RND.str(10);
    int count = RND.plusInt(7) + 3;
    for (int i = 0; i < count; i++) {
      Contact c = new Contact();
      c.id          = RND.str(10);
      c.phoneNumber = "+" + RND.strInt(15);
      ret.contactList.add(c);
    }
    return ret;
  }

}
