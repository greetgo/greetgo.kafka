package kz.greetgo.kafka_test_app.consumers;

import kz.greetgo.kafka.model.Box;
import kz.greetgo.kafka_test_app.model.ClientKafka;
import kz.greetgo.kafka_test_app.util.IsInstance;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static kz.greetgo.kafka_test_app.config.Topics.CLIENT;
import static org.assertj.core.api.Assertions.assertThat;

public class ClientConsumerTest extends ParentKafkaSimulator {

  @Test
  public void simplePush() {

    ClientKafka c = rndClientKafka();

    producer().sending(c)
              .toTopic(CLIENT)
              .go().awaitAndGet();

    kafkaSimulator.push();

    List<ClientKafka> kafkaList = kafkaSimulator.allPushed()
                                                .stream()
                                                .map(ConsumerRecord::value)
                                                .map(x -> x.body)
                                                .flatMap(IsInstance.on(ClientKafka.class)::of)
                                                .collect(toList());

    assertThat(kafkaList).hasSize(1);
    assertThat(kafkaList.get(0).id).isEqualTo(c.id);

  }

  @Test
  public void pushToClientConsumer() {

    ClientKafka c = rndClientKafka();

    producer().sending(c)
              .toTopic(CLIENT)
              .go().awaitAndGet();

    clientConsumer.comeClientList = new ArrayList<>();

    kafkaSimulator.push(ClientConsumer.class);

    List<ClientKafka> kafkaList = kafkaSimulator.allPushed()
                                                .stream()
                                                .map(ConsumerRecord::value)
                                                .map(x -> x.body)
                                                .flatMap(IsInstance.on(ClientKafka.class)::of)
                                                .collect(toList());

    assertThat(kafkaList).hasSize(1);
    assertThat(kafkaList.get(0).id).isEqualTo(c.id);

    assertThat(clientConsumer.comeClientList).hasSize(1);
    assertThat(clientConsumer.comeClientList.get(0).id).isEqualTo(c.id);
  }

  @Test
  public void dynamicConsumers() {

    ClientKafka c = rndClientKafka();

    producer().sending(c)
              .toTopic(CLIENT)
              .go().awaitAndGet();

    clientConsumer.comeClientList = new ArrayList<>();

    List<Box> comeBoxes = new ArrayList<>();

    kafkaSimulator.dynamicConsumers()
                  .adding()
                  .name("read-some-client")
                  .topic(CLIENT)
                  .customConsumer((box, boxMeta) -> comeBoxes.add(box))
                  .add();

    kafkaSimulator.push();

    List<ClientKafka> kafkaList = kafkaSimulator.allPushed()
                                                .stream()
                                                .map(ConsumerRecord::value)
                                                .map(x -> x.body)
                                                .flatMap(IsInstance.on(ClientKafka.class)::of)
                                                .collect(toList());

    assertThat(kafkaList).hasSize(1);
    assertThat(kafkaList.get(0).id).isEqualTo(c.id);

    assertThat(comeBoxes).hasSize(1);
    assertThat(comeBoxes.get(0).body).isInstanceOf(ClientKafka.class);
    ClientKafka actual = (ClientKafka) comeBoxes.get(0).body;
    assertThat(actual.id).isEqualTo(c.id);
  }

}
