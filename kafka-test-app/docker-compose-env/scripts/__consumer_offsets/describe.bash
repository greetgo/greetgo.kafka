#!/usr/bin/env bash

set -e

cd "$(dirname "$0")" || exit 131

DOC="kafka-test-app-kf1"

docker exec -i "$DOC" kafka-topics.sh --bootstrap-server localhost:9092 --describe --topic P_STONES
