import json

TOPIC      = "P_STONES"
REP_COUNT  = 12
REP_FACTOR =  3
BROKERS    = "1,2,3,4"

out = {
  "version": 1,
  "partitions": []
}

brokers = list(map(int, BROKERS.split(',')))
len_of_brokers=len(brokers)

offset = 0

for i in range(REP_COUNT):
  replicas = []
  for j in range(REP_FACTOR):
    n = offset + j
    while (n >= len_of_brokers):
      n = n - len_of_brokers
    replicas.append(brokers[n])
  offset = offset + 1
  out["partitions"].append({"topic": TOPIC, "partition": i, "replicas": replicas})

jsonFileName="replicate-topic-" + TOPIC + ".json"

with open(jsonFileName, "w") as f:
  print(json.dumps(out, indent=2), file=f)

print('kafka-reassign-partitions.sh --bootstrap-server localhost:9092 --reassignment-json-file '+jsonFileName+' --execute')
