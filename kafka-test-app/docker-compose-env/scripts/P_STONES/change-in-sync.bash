#!/usr/bin/env bash

set -e

cd "$(dirname "$0")" || exit 131
DOC="kafka-test-app-kf1"

docker exec -i "$DOC" kafka-configs.sh --bootstrap-server localhost:9092 \
                                       --alter \
                                       --entity-type topics \
                                       --entity-name P_STONES \
                                       --add-config min.insync.replicas=2
