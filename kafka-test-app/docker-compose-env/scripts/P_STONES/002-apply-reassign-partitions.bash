#!/usr/bin/env bash

set -e

cd "$(dirname "$0")" || exit 131

JSON_FILE_NAME="replicate-topic-P_STONES.json"
DOC="kafka-test-app-kf1"

echo docker exec -i "$DOC" kafka-reassign-partitions.sh --bootstrap-server localhost:9092 \
                                                   --reassignment-json-file "/$JSON_FILE_NAME" \
                                                   --execute


