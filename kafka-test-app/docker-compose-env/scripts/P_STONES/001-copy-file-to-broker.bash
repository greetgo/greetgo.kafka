#!/usr/bin/env bash

set -e

cd "$(dirname "$0")" || exit 131

JSON_FILE_NAME="replicate-topic-P_STONES.json"
JSON_FILE="./$JSON_FILE_NAME"
DOC="kafka-test-app-kf1"

docker cp "$JSON_FILE" "$DOC:/$JSON_FILE_NAME"
