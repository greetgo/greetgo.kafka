#!/usr/bin/env bash

cd "$(dirname "$0")" | exit 113

cat str_data.txt | docker exec -i kafka-test-app-kf1 kafka-console-producer.sh --topic a_str1 --bootstrap-server localhost:9092
