#!/usr/bin/env bash

set -e

cd "$(dirname "$0")" || exit 131

yes | keytool -keystore files/client.truststore.jks -storepass 111222 -trustcacerts -importcert -file files/domain.pem
