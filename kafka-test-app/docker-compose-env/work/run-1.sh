#!/usr/bin/env bash

set -e
cd /kafka-data

if [ ! -f "/kafka-data/bootstrap.checkpoint" ] || [ ! -f  "/kafka-data/meta.properties" ]; then
  rm -f "/kafka-data/bootstrap.checkpoint"
  rm -f "/kafka-data/meta.properties"
  KAFKA_CLUSTER_ID="$(cat /work/kafka-cluster-id.txt)"
  kafka-storage.sh format -t $KAFKA_CLUSTER_ID -c /work/kf-other-1.server.properties
fi

exec kafka-server-start.sh /work/kf-other-1.server.properties
