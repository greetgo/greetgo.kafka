#!/usr/bin/env bash

set -e

cd "$(dirname "$0")" || exit 131

bash 002-remove.bash

docker compose up -d

GREEN='\033[1;32m'
NC='\033[0m' # No Color
# shellcheck disable=SC2059
printf "\n${GREEN}      RECREATE OK${NC}\n\n"
