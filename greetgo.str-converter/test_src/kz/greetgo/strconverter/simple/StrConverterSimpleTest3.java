package kz.greetgo.strconverter.simple;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class StrConverterSimpleTest3 {

  public static class Model1 {
    public String s1;
    public String s2;
  }

  public static class Model2 {
    public int                 intField;
    public Model1              fieldA;
    public Map<String, String> mapField;
    public Model1              fieldB;
  }

  Model2 source;

  @BeforeMethod
  public void initSource() {
    Model1 mA = new Model1();
    mA.s1 = "Hello 01";
    mA.s2 = "Hello 02";

    Model1 mB = new Model1();
    mB.s1 = "Stone 01";
    mB.s2 = "Stone 02";

    source          = new Model2();
    source.intField = 43267;
    source.fieldA   = mA;
    source.mapField = new HashMap<>();
    source.mapField.put("Str1", "Hello World");
    source.mapField.put("Str2", "Winter Fell");
    source.fieldB = mB;
  }

  @Test
  public void absentFieldClass() {

    StrConverterSimple converter = new StrConverterSimple();
    converter.convertRegistry().register(Model1.class, "Mo1");
    converter.convertRegistry().register(Model2.class, "Mo2");

    String string = converter.toStr(source);
    System.out.println("YM1XMLfLGU :: string = " + string);

    {
      Model2 actual = converter.fromStr(string);

      assertThat(actual).isNotNull();
      assertThat(actual.intField).isEqualTo(43267);
      assertThat(actual.fieldA).isNotNull();
      assertThat(actual.mapField).isEqualTo(Map.of("Str1", "Hello World", "Str2", "Winter Fell"));
      assertThat(actual.fieldB).isNotNull();
      assertThat(actual.fieldA.s1).isEqualTo("Hello 01");
      assertThat(actual.fieldA.s2).isEqualTo("Hello 02");
    }

    {
      StrConverterSimple converter2 = new StrConverterSimple();
      converter2.convertRegistry().register(Model2.class, "Mo2");

      Model2 actual = converter2.fromStr(string);

      assertThat(actual).isNotNull();
      assertThat(actual.intField).isEqualTo(43267);
      assertThat(actual.fieldA).isNull();
      assertThat(actual.mapField).isEqualTo(Map.of("Str1", "Hello World", "Str2", "Winter Fell"));
      assertThat(actual.fieldB).isNull();
    }

  }

  @Test
  public void absentMainClass() {

    StrConverterSimple converter = new StrConverterSimple();
    converter.convertRegistry().register(Model1.class, "Mo1");
    converter.convertRegistry().register(Model2.class, "Mo2");

    String string = converter.toStr(source);
    System.out.println("z048BeHK2p :: string = " + string);

    StrConverterSimple converter2 = new StrConverterSimple();

    Object object = converter2.fromStr(string);

    assertThat(object).isNull();

  }
}
