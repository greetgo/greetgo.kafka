package kz.greetgo.strconverter.simple;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class StrConverterSimpleTest2 {

  public enum Model1 {
    A1, A2, A3
  }

  public static class Model2 {
    public int                 intField;
    public Model1              fieldA;
    public Map<String, String> mapField;
    public Model1              fieldB;
  }

  Model2 source;

  @BeforeMethod
  public void initSource() {

    source          = new Model2();
    source.intField = 43267;
    source.fieldA   = Model1.A1;
    source.mapField = new HashMap<>();
    source.mapField.put("Str1", "Hello World");
    source.mapField.put("Str2", "Winter Fell");
    source.fieldB = Model1.A2;
  }

  @Test
  public void absentFieldClass() {

    StrConverterSimple converter = new StrConverterSimple();
    converter.convertRegistry().register(Model1.class, "Mo1");
    converter.convertRegistry().register(Model2.class, "Mo2");

    String string = converter.toStr(source);
    System.out.println("FM24r1JLh3 :: string = " + string);

    {
      Model2 actual = converter.fromStr(string);

      assertThat(actual).isNotNull();
      assertThat(actual.intField).isEqualTo(43267);
      assertThat(actual.fieldA).isNotNull();
      assertThat(actual.mapField).isEqualTo(Map.of("Str1", "Hello World", "Str2", "Winter Fell"));
      assertThat(actual.fieldB).isNotNull();
      assertThat(actual.fieldA).isEqualTo(Model1.A1);
      assertThat(actual.fieldB).isEqualTo(Model1.A2);
    }

    {
      StrConverterSimple converter2 = new StrConverterSimple();
      converter2.convertRegistry().register(Model2.class, "Mo2");

      Model2 actual = converter2.fromStr(string);

      assertThat(actual).isNotNull();
      assertThat(actual.intField).isEqualTo(43267);
      assertThat(actual.fieldA).isNull();
      assertThat(actual.mapField).isEqualTo(Map.of("Str1", "Hello World", "Str2", "Winter Fell"));
      assertThat(actual.fieldB).isNull();
    }

  }

  @Test
  public void absentMainClass() {

    StrConverterSimple converter = new StrConverterSimple();
    converter.convertRegistry().register(Model1.class, "Mo1");
    converter.convertRegistry().register(Model2.class, "Mo2");

    String string = converter.toStr(source);
    System.out.println("eZ5ek1ak27 :: string = " + string);

    StrConverterSimple converter2 = new StrConverterSimple();

    Object object = converter2.fromStr(string);

    assertThat(object).isNull();

  }

  @Test
  public void absentEnumClass() {

    StrConverterSimple converter = new StrConverterSimple();
    converter.convertRegistry().register(Model1.class, "Mo1");
    converter.convertRegistry().register(Model2.class, "Mo2");

    String string = converter.toStr(Model1.A3);
    System.out.println("4rI1JNXSma :: string = " + string);

    {
      Model1 model = converter.fromStr(string);

      assertThat(model).isEqualTo(Model1.A3);
    }

    {
      StrConverterSimple converter2 = new StrConverterSimple();

      Object object = converter2.fromStr(string);

      assertThat(object).isNull();
    }
  }

}
