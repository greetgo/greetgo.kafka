#!/usr/bin/env bash

set -e

cd "$(dirname "$0")" || exit 131

openssl pkcs12 -password pass:111222 -export -out files/server.keystore.p12 -in files/domain.pem -inkey files/domain.key
