#!/usr/bin/env bash

set -e

CLIENT_JVMFLAGS="-Djava.security.auth.login.config=/conf/client-jaas.conf"

DOC="docker exec -e CLIENT_JVMFLAGS=$CLIENT_JVMFLAGS -it zoo-auth-zoo"

$DOC zkCli.sh ls /asd/dsa

#$DOC zkCli.sh create /asd/dsa/sinus

