#!/usr/bin/env bash

# shellcheck disable=SC2034
MID=myid

if [[ ! -f "/data/$MID" ]]; then
    echo "1" > "/data/$MID"
fi

exec zkServer.sh start-foreground
