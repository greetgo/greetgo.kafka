#!/usr/bin/env bash

set -e

cd "$(dirname "$0")" || exit 131

docker-compose down

docker run --rm -v "$HOME/volumes/greetgo.kafka/kafka-with-auth/:/data" \
       busybox:1.28 \
       find /data -mindepth 1 -maxdepth 1 -exec \
       rm -rf {} \;
