#!/usr/bin/env bash

set -e

cd "$(dirname "$0")" || exit 113

KAFKA_CLUSTER_ID="$(docker run --rm -i bitnami/kafka:3.5.1 kafka-storage.sh random-uuid)"
KAFKA_CLUSTER_ID="$(echo -n "$KAFKA_CLUSTER_ID" | tr -d '[:blank:]' | tr -d '\n')"

echo -n "$KAFKA_CLUSTER_ID" > config/kafka-cluster-id.txt
