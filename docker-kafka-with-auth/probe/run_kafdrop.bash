#!/usr/bin/env bash

set -e

cd "$(dirname "$0")" || exit 113

docker run --rm -p 9000:9000 --network kafka-with-auth \
    -v ./ssl:/ssl \
    -e KAFKA_BROKERCONNECT=kf-other-1:9091 \
    -e KAFKA_PROPERTIES="$(cat kafdrop/kafka.properties | base64)" \
    -e KAFKA_TRUSTSTORE="$(cat truststore/kafka.truststore.jks | base64)" \
    -e KAFKA_KEYSTORE="$(cat keystore/kafka.keystore.jks | base64)" \
    obsidiandynamics/kafdrop:3.31.0
