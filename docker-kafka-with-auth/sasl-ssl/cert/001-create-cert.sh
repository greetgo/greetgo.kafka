#!/usr/bin/env bash

set -e

cd "$(dirname "$0")" || exit 131

mkdir -p files

openssl req -subj "/CN=localhost/" -newkey rsa:2048 -nodes -keyout files/domain.key -x509 -days 365 -out files/domain.pem
