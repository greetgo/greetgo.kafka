package kz.greetgo.kafka.util_for_tests;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Markers {
  public static Path projectRoot() {
    Path current     = Paths.get(".");
    Path prevCurrent = null;

    while (true) {

      if (isProjectRoot(current)) {
        return current;
      }

      if (prevCurrent != null) {
        try {
          if (Files.isSameFile(prevCurrent, current)) {
            throw new RuntimeException("2Cw1r9zzm1 :: Cannot find project root from "
                                         + new File(".").getAbsoluteFile().toPath().normalize());
          }
        } catch (IOException e) {
          throw new RuntimeException(e);
        }
      }

      prevCurrent = current;
      current     = prevCurrent.resolve("..");
    }

  }

  private static boolean isProjectRoot(Path path) {
    return Files.exists(path.resolve(".greetgo").resolve("project-name.txt"));
  }

}
